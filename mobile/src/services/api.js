import axios from "axios";

const api = axios.create({
  baseURL: 'https://f1aa6968389f.ngrok.io/'
});

export default api;