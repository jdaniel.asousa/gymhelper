import React from 'react';
import { View } from 'react-native';

import Header from '../../components/header/index';
import Footer from '../../components/footer/index';
import ExerciseHistory from '../../components/exerciseHistory/index';

export default function exerciseHistory() {
  return <View style={{flex: 1}}>
    <Header/>
    <ExerciseHistory/>
    <Footer/>
  </View>;
}