import React from 'react';
import { View } from 'react-native';

import Header from '../../components/header/index';
import Footer from '../../components/footer/index';
import WorkoutPlans from '../../components/workoutPlans/index';

export default function workoutPlans() {
  return <View style={{flex: 1}}>
    <Header/>
    <WorkoutPlans/>
    <Footer/>
  </View>;
}
