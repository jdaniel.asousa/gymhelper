import React from 'react';
import { View } from 'react-native';

import Header from '../../components/header/index';
import Footer from '../../components/footer/index';
import MachineExercises from '../../components/machineExercises/index';

export default function machineExercises({route}) {
  return <View style={{flex: 1}}>
    <Header/>
    <MachineExercises url={route.params.url}/>
    <Footer/>
  </View>;
}
