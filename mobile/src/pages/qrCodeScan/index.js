import React from 'react';
import { View } from 'react-native';

// import { Container } from './styles';
import Header from '../../components/header/index';
import Footer from '../../components/footer/index';
import QrCodeScanner from '../../components/qrCodeScanner/index';

export default function qrCodeScan() {
  return <View style={{flex: 1}}>
    <Header/>
    <QrCodeScanner/>
    <Footer/>
  </View>;
}