import React from 'react';
import { View } from 'react-native';

import Header from '../../components/header/index';
import Footer from '../../components/footer/index';
import WorkoutPlanDetails from '../../components/workoutPlanDetails/index';

export default function workoutPlanDetails({route}) {

  return <View style={{flex: 1, backgroundColor: "#F7F7F7"}}>
    <Header/>
    <WorkoutPlanDetails workoutNumber={route.params.workoutNumber} workoutId={route.params.workoutId}/>
    <Footer/>
  </View>;
}
