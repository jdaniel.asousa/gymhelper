import React, { useState } from 'react';
import { View, Text, Dimensions, ImageBackground, Image, TextInput, TouchableOpacity, TouchableHighlight, Alert } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { AntDesign, FontAwesome, Ionicons } from '@expo/vector-icons';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { useNavigation } from '@react-navigation/native';

import api from '../../services/api';

import { AppLoading } from 'expo';

import background from '../../assets/background.png';
import logo from '../../assets/favicon.png';

import styles from './styles';

export default function changePassword() {
  let [password, setPassword] = useState("");
  let [confirmPassword, setConfirmPassword] = useState("");
  let [token, setToken] = useState("");
  let [equalPassword, setEqualPassword] = useState(true);
  const navigation = useNavigation();

  let [fontsLoaded] = useFonts({
    Poppins_400Regular
  });

  let [showing, setShowing] = useState(true);
  let [isPress, setIsPress] = useState(false);

  function navigateLogin() {
    navigation.navigate("Login");
  }

  function showPassword() {
    if(showing) {
      setShowing(false);
    } else {
      setShowing(true);
    }
  }

  function validatePassword() {
    if (password !== confirmPassword) {
      setEqualPassword(false);
      return false;
    } else {
      return true;
    }
  }

  function checkInputsFill() {
    if(password === "" || confirmPassword === "" || token === "") {
      return false;
    }
    return true;
  }

  async function handleSubmit() {
    const filled = checkInputsFill();
    const isEqual = validatePassword();
    if(isEqual && filled) {
      await api.post('api/user/reset-password', {
        password: password,
        confirmPassword: confirmPassword,
        token: token
      }).then(() => {
        Alert.alert("Password alterada com sucesso", "A sua password foi alterada com sucesso.");
        navigation.navigate("Login");
      }, (error) => {
        Alert.alert("Erro", "Não foi possível alterar a sua password. Tente novamente.");
      });
    } else {
      if(!filled) {
        Alert.alert("Campos em falta", "Tem de preencher todos os campos.");
      } else {
        Alert.alert("Password Diferente", "A confirmação da nova password não corresponde com a sua password desejada.");
      }
    }

  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {

  return <View style={styles.container}>
    <View style={[styles.logo, {flexBasis: Dimensions.get('window').height / 3, 
    flexGrow: 0, width: Dimensions.get('window').width, justifyContent: "center", 
    alignItems: "center"}]}>
      <ImageBackground source={background} style={styles.background}>
        <LinearGradient style={styles.colors} colors={["#F77F00", "#003049"]}>
          <Image style={styles.logo} source={logo} alt={"Icon"}/>
        </LinearGradient>
      </ImageBackground>
    </View>
    <View style={[styles.login, {flexBasis:Dimensions.get('window').height / 3 * 2, flexGrow:0}]}>
      <View style={styles.input_password}>
        <View style={styles.password_icon}>
          <Ionicons name="md-key" size={23} color="#50504E" style={{marginLeft: 10}}/>
        </View>
        <TextInput secureTextEntry={showing} style={[styles.inputPassword, {fontFamily: "Poppins_400Regular"}]} placeholder="Nova Password" onChangeText={text => setPassword(text)}/>
        <TouchableHighlight onPress={showPassword} underlayColor={"#DCDCDC"} onHideUnderlay={() => setIsPress(false)} onShowUnderlay={() => setIsPress(true)} style={isPress ? styles.buttonPress : styles.buttonNormal}>
          <Ionicons name={showing ? "md-eye-off" : "md-eye"} size={22} color="#50504E"/>
        </TouchableHighlight>
      </View>
      <View style={equalPassword ? styles.input_password : styles.input_password_wrong}>
        <View style={styles.password_icon}>
          <Ionicons name="md-key" size={23} color="#50504E" style={{marginLeft: 10}}/>
        </View>
        <TextInput secureTextEntry={showing} style={[styles.inputPassword, {fontFamily: "Poppins_400Regular"}]} placeholder="Confirmação Password" onChangeText={text => setConfirmPassword(text)}/>
        <TouchableHighlight onPress={showPassword} underlayColor={"#DCDCDC"} onHideUnderlay={() => setIsPress(false)} onShowUnderlay={() => setIsPress(true)} style={isPress ? styles.buttonPress : styles.buttonNormal}>
          <Ionicons name={showing ? "md-eye-off" : "md-eye"} size={22} color="#50504E"/>
        </TouchableHighlight>
      </View>
      <View style={styles.input_password}>
        <View style={styles.password_icon}>
          <Ionicons name="md-key" size={23} color="#50504E" style={{marginLeft: 10}}/>
        </View>
        <TextInput style={[styles.inputPassword, {fontFamily: "Poppins_400Regular"}]} placeholder="Token" onChangeText={text => setToken(text)}/>
      </View>

      <TouchableOpacity onPress={handleSubmit} style={{width: "75%", height: 50, backgroundColor: "#003049", 
      justifyContent: "center", alignItems: "center", borderRadius: 5}}>
        <Text style={{color: "#F7F7F7", fontSize: 13, fontFamily: "Poppins_400Regular"}}>Alterar Password</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={navigateLogin} style={{width: "75%", flex: 0, flexDirection: "row-reverse", marginTop: 10}}>
        <AntDesign name="right" style={{marginTop: 4, marginLeft: 10}} color="#003049"/>
        <Text style={{fontSize: 13, color: '#003049', fontFamily: "Poppins_400Regular"}}>Iniciar Sessão</Text>
      </TouchableOpacity>
    </View>
  </View>;
  }
}