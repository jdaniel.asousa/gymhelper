import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column"
  },
  background: {
    height: Dimensions.get('window').height / 2,
    width:Dimensions.get('window').width,
    justifyContent: "center",
    alignItems: "center",
  },
  login: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F7F7F7",
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50
  },
  colors: {
    justifyContent: "center",
    alignItems: "center",
    opacity: 0.7,
    height: Dimensions.get('window').height / 2,
    width:Dimensions.get('window').width,
  },
  logo: {
    height: 120,
    width: 120,
  },
  input_password: {
    flexDirection: "row",
    height: 50, 
    width: "77%", 
    borderWidth: 1,
    marginBottom: 20,
    borderRadius: 5,
    borderColor: "#F7F7F7",
    backgroundColor: "#DCDCDC"
  },
  input_password_wrong: {
    flexDirection: "row",
    height: 50, 
    width: "77%", 
    borderWidth: 1,
    marginBottom: 20,
    borderRadius: 5,
    borderWidth: 2,
    borderColor: "#8B0000",
    backgroundColor: "#DCDCDC"
  },
  inputPassword: {
    height: "100%", 
    width: "80%", 
    borderWidth: 1,
    marginBottom: 20,
    marginLeft: 5,
    paddingLeft: 5,
    borderRadius: 5,
    borderColor: "#DCDCDC",
    backgroundColor: "#DCDCDC"
  },
  password_icon: {
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 3,
    height: 45, 
  },
  buttonPress: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 5,
    paddingBottom: 3,
    height: 45,
    backgroundColor: "#DCDCDC",
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5
  },
  buttonNormal: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 5,
    paddingBottom: 3,
    height: 45,
    backgroundColor: "#DCDCDC",
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5
  }
});