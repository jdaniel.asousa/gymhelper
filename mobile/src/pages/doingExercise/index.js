import React from 'react';
import { View } from 'react-native';

import Header from '../../components/header/index';
import Footer from '../../components/footer/index';
import DoingExercise from '../../components/doingExercise/index';

export default function doingExercise({route}) {
  return <View style={{flex: 1}}>
    <Header/>
    <DoingExercise exercises={route.params.allExercises}/>
    <Footer/>
  </View>;
}