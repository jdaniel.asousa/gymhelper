import React, { useState, useEffect } from 'react';
import { View, Text, Dimensions, ImageBackground, Image, TextInput, TouchableOpacity, Alert } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { AntDesign, FontAwesome, Ionicons } from '@expo/vector-icons';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { AppLoading } from 'expo';

import background from '../../assets/background.png';
import logo from '../../assets/favicon.png';

import styles from './styles';
import { TouchableHighlight } from 'react-native-gesture-handler';

import api from "../../services/api";

export default function login() {
  const navigation = useNavigation();

  let [fontsLoaded] = useFonts({
    Poppins_400Regular
  });

  let [username, setUsername] = useState("");
  let [password, setPassword] = useState("");
  let [showing, setShowing] = useState(true);
  let [isPress, setIsPress] = useState(false);

  useEffect(() => {
    setUsername("");
    setPassword("");
    checkLog();
  }, []);

  async function checkLog() {
    try {
      let userId = await AsyncStorage.getItem("id");
      let token = await AsyncStorage.getItem("token");
      const response = await api.get(`api/User/user?request_id=${userId}&id=${userId}`,
      {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })

      navigation.navigate("Dashboard");
    } catch (error) {
      if(error.response) {
        if(error.response.status === 400) {
          return;
        }
      }
    }
  }
  

  function navigateResetPassword() {
    navigation.navigate("ResetPassword");
  }

  function navigateChangePassword() {
    navigation.navigate("ChangePassword");
  }

  function checkInputsFill() {
    if(password === "" || username === "") {
      return false;
    }
    return true;
  }

  function showPassword() {
    if(showing) {
      setShowing(false);
    } else {
      setShowing(true);
    }
  }

  async function handleSubmit() {
    const filled = checkInputsFill();
    if(filled) {
      try {
        const response = await api.post('api/user/login', {
          email: username,
          password: password
        })

        if(response.data.role !== 2) {
          Alert.alert("Acesso negado", "Não possui permissões para aceder à aplicação");
          return;
        }

        AsyncStorage.setItem('token', response.data.token);
        AsyncStorage.setItem('id', response.data.id.toString());
        AsyncStorage.setItem('name', response.data.name);
        navigation.navigate("Dashboard");
      } catch (error) {
        if(error.response) {
          if(error.response.status === 400) {
            Alert.alert("Erro no Login", "O seu email ou a palavra-passe estão incorretos. Verifique e tente novamente.");
          }
        }
      }
    } else {
      Alert.alert("Campos em falta", "Tem de preencher todos os campos.");
    }
  };
  

  async function getToken() {
    const value = await AsyncStorage.getItem('token')
    if(value !== null) {
      console.log(value);
    }
  }


  if(!fontsLoaded) {
    return <AppLoading/>
  } else {

    return <View style={styles.container}>
      <View style={[styles.logo, {flexBasis: Dimensions.get('window').height / 3, 
      flexGrow: 0, width: Dimensions.get('window').width, justifyContent: "center", 
      alignItems: "center"}]}>
        <ImageBackground source={background} style={styles.background}>
          <LinearGradient style={styles.colors} colors={["#F77F00", "#003049"]}>
            <Image style={styles.logo} source={logo} alt={"Icon"}/>
          </LinearGradient>
        </ImageBackground>
      </View>
      <View style={[styles.login, {flexBasis:Dimensions.get('window').height / 3 * 2, flexGrow:0}]}>
        <View style={styles.input_username}>
          <View style={styles.user_icon}>
            <FontAwesome name="user-circle" size={17} color="#50504E" style={{marginLeft: 10}}/>
          </View>
          <TextInput style={[styles.input, {fontFamily: "Poppins_400Regular"}]} placeholder="Email" onChangeText={text => setUsername(text)}/>
        </View>
        <View style={styles.input_password}>
          <View style={styles.password_icon}>
            <Ionicons name="md-key" size={23} color="#50504E" style={{marginLeft: 10}}/>
          </View>
          <TextInput secureTextEntry={showing} style={[styles.inputPassword, {fontFamily: "Poppins_400Regular"}]} placeholder="Password" onChangeText={text => setPassword(text)}/>
          <TouchableHighlight onPress={showPassword} underlayColor={"#DCDCDC"} onHideUnderlay={() => setIsPress(false)} onShowUnderlay={() => setIsPress(true)} style={isPress ? styles.buttonPress : styles.buttonNormal}>
            <Ionicons name={showing ? "md-eye-off" : "md-eye"} size={22} color="#50504E"/>
          </TouchableHighlight>
        </View>

        <TouchableOpacity onPress={handleSubmit} style={{width: "75%", height: 50, backgroundColor: "#003049", 
        justifyContent: "center", alignItems: "center", borderRadius: 5}}>
          <Text style={{color: "#F7F7F7", fontSize: 13, fontFamily: "Poppins_400Regular"}}>Iniciar Sessão</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={navigateResetPassword} style={{width: "75%", flex: 0, flexDirection: "row-reverse", marginTop: 10}}>
          <AntDesign name="right" style={{marginTop: 4, marginLeft: 10}} color="#003049"/>
          <Text style={{fontSize: 13, color: '#003049', fontFamily: "Poppins_400Regular"}}>Recuperar Password</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={navigateChangePassword} style={{width: "75%", flex: 0, flexDirection: "row-reverse", marginTop: 10}}>
          <AntDesign name="right" style={{marginTop: 4, marginLeft: 10}} color="#003049"/>
          <Text style={{fontSize: 13, color: '#003049', fontFamily: "Poppins_400Regular"}}>Nova Password (Utilizador Novo)</Text>
        </TouchableOpacity>
      </View>
    </View>;
  }
}
