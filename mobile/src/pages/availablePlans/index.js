import React from 'react';
import { View } from 'react-native';

import Header from '../../components/header';
import Footer from '../../components/footer';
import AvailablePlans from '../../components/availablePlans/index';

export default function availablePlans() {
  return <View style={{flex: 1}}>
    <Header/>
    <AvailablePlans/>
    <Footer/>
  </View>;
}