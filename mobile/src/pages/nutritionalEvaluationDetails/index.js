import React from 'react';
import { View } from 'react-native';

import Header from '../../components/header/index';
import Footer from '../../components/footer/index';
import NutritionalEvaluationsDetails from '../../components/nutritionalEvaluationDetails/index';

export default function nutritionalEvaluationDetails({route}) {
  return <View style={{flex: 1}}>
    <Header/>
    <NutritionalEvaluationsDetails number={route.params.planNumber} id={route.params.planId}/>
    <Footer/>
  </View>;
}