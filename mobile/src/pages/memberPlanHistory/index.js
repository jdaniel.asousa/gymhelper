import React from 'react';
import { View } from 'react-native';

import Header from '../../components/header/index';
import Footer from '../../components/footer/index';
import MemberPlanHistory from '../../components/memberPlanHistory/index';

export default function memberPlanHistory({ route }) {

  return <View style={{flex: 1}}>
    <Header/>
    <MemberPlanHistory plans={route.params.plans}/>
    <Footer/>
  </View>;
}