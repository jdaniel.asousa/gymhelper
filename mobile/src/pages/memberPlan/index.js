import React from 'react';
import { View } from 'react-native';

import Header from '../../components/header/index';
import Footer from '../../components/footer/index';
import MemberPlan from '../../components/memberPlan/index';

export default function memberPlan() {
  return <View style={{flex: 1}}>
    <Header/>
    <MemberPlan/>
    <Footer/>
  </View>;
}