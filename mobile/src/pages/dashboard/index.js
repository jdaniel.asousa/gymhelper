import React from 'react';
import { View } from 'react-native';

import Header from '../../components/header/index';
import Footer from '../../components/footer/index';
import Dashboard from '../../components/dashboard/index';

export default function dashboard() {
  return <View style={{flex: 1}}>
    <Header/>
    <Dashboard/>
    <Footer/>
  </View>;
}