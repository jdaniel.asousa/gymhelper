import React from 'react';
import { View } from 'react-native';

import Header from '../../components/header/index';
import Footer from '../../components/footer/index';
import NutritionPlans from '../../components/nutritionPlans/index';

export default function nutritionPlans() {
  return <View style={{flex: 1, backgroundColor: "#F7F7F7"}}>
    <Header/>
    <NutritionPlans/>
    <Footer/>
  </View>;
}
