import React from 'react';
import { View } from 'react-native';

import Header from '../../components/header/index';
import Footer from '../../components/footer/index';
import PhysicalEvaluationDetails from '../../components/physicalEvaluationDetails/index';

export default function physicalEvaluationDetails({ route }) {
  return <View style={{flex: 1}}>
    <Header/>
    <PhysicalEvaluationDetails number={route.params.evaluationNumber} id={route.params.evaluationId}/>
    <Footer/>
  </View>;
}