import React, { useState } from 'react';
import { View, Text, Dimensions, ImageBackground, Image, TextInput, TouchableOpacity, Alert } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { AntDesign, FontAwesome } from '@expo/vector-icons';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { useNavigation } from '@react-navigation/native';

import api from '../../services/api';

import { AppLoading } from 'expo';

import background from '../../assets/background.png';
import logo from '../../assets/favicon.png';

import styles from './styles';

export default function resetPassword() {
  const navigation = useNavigation();
  let [email, setEmail] = useState("");

  let [fontsLoaded] = useFonts({
    Poppins_400Regular
  });

  function checkInputFill() {
    if(email === "") {
      return false;
    }
    return true;
  }

  async function handleSubmit() {
    const filled = checkInputFill();

    if(filled) {
      await api.post('api/user/forgot-password', {
        email: email,
        origin: 'mobile'
      }).then((response) => {
        Alert.alert("Email Enviado", "Siga as instruções enviadas para o seu email.");
        navigation.navigate("ChangePassword");
      }, (error) => {
        console.log(error);
      });
    } else {
      Alert.alert("Campo em falta", "Tem de preencher o campo com o seu email.");
    }
  }

  function navigateLogin() {
    navigation.navigate("Login");
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
      <View style={[styles.logo, {flexBasis: Dimensions.get('window').height / 3, 
      flexGrow: 0, width: Dimensions.get('window').width, justifyContent: "center", 
      alignItems: "center"}]}>
        <ImageBackground source={background} style={styles.background}>
          <LinearGradient style={styles.colors} colors={["#F77F00", "#003049"]}>
            <Image style={styles.logo} source={logo} alt={"Icon"}/>
          </LinearGradient>
        </ImageBackground>
      </View>
      <View style={[styles.login, {flexBasis:Dimensions.get('window').height / 3 * 2, flexGrow:0}]}>
        <View style={styles.input_username}>
          <View style={styles.user_icon}>
            <FontAwesome name="user-circle" size={17} color="#50504E" style={{marginLeft: 10}}/>
          </View>
          <TextInput onChangeText={text => setEmail(text)} style={[styles.input, {fontFamily: "Poppins_400Regular"}]} placeholder="Email" onChangeText={text => setEmail(text)}/>
        </View>

        <TouchableOpacity onPress={handleSubmit} style={{width: "75%", height: 50, backgroundColor: "#003049", 
        justifyContent: "center", alignItems: "center", borderRadius: 5}}>
          <Text style={{color: "#F7F7F7", fontSize: 13, fontFamily: "Poppins_400Regular"}}>Recuperar Password</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={navigateLogin} style={{width: "75%", flex: 0, flexDirection: "row-reverse", marginTop: 10}}>
          <AntDesign name="right" style={{marginTop: 4, marginLeft: 10}} color="#003049"/>
          <Text style={{fontSize: 13, color: '#003049', fontFamily: "Poppins_400Regular"}}>Iniciar Sessão</Text>
        </TouchableOpacity>
      </View>
    </View>;
  }
}