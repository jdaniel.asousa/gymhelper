import React, { useState } from 'react';
import { View, Text, ScrollView } from 'react-native';

import Header from '../../components/header/index';
import Footer from '../../components/footer/index';
import UserInfo from '../../components/userInfo/index';
import { SafeAreaView } from 'react-native-safe-area-context';

export default function profile() {

  return <View style={{flex: 1}}>
    <Header showName={false}/>
    <UserInfo/>
    <Footer/>
  </View>;
}