import React from 'react';
import { View } from 'react-native';

import Header from '../../components/header/index';
import Footer from '../../components/footer/index';
import PhysicalEvaluations from '../../components/physicalEvaluations/index';

export default function physicalEvaluations() {
  return <View style={{flex: 1}}>
    <Header/>
    <PhysicalEvaluations/>
    <Footer/>
  </View>;
}