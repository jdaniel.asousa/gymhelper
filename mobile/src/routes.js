import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const AppStack = createStackNavigator();

import login from './pages/login/index';
import resetPassword from './pages/resetPassword/index';
import changePassword from './pages/changePassword/index';
import profile from './pages/profile/index';
import qrCodeScan from './pages/qrCodeScan/index';
import workoutPlans from './pages/workoutPlans/index';
import workoutPlanDetails from './pages/workoutPlanDetails/index';
import nutritionPlans from './pages/nutritionPlans/index';
import doingExercise from './pages/doingExercise/index';
import machineExercises from './pages/machineExercises/index';
import memberPlan from './pages/memberPlan/index';
import availablePlans from './pages/availablePlans/index';
import memberPlanHistory from './pages/memberPlanHistory';
import nutritionalEvaluationDetails from './pages/nutritionalEvaluationDetails/index';
import dashboard from './pages/dashboard/index';
import exerciseHistory from './pages/exerciseHistory/index';
import physicalEvaluations from './pages/physicalEvaluations/index';
import physicalEvaluationDetails from './pages/physicalEvaluationDetails/index';

export default function Routes() {
  return(
    <NavigationContainer>

      <AppStack.Navigator screenOptions={{ headerShown: false }}>
        <AppStack.Screen name="Login" component={login}/>
        <AppStack.Screen name="ResetPassword" component={resetPassword}/>
        <AppStack.Screen name="ChangePassword" component={changePassword}/>
        <AppStack.Screen name="Profile" component={profile}/>
        <AppStack.Screen name="QrCodeScan" component={qrCodeScan}/>
        <AppStack.Screen name="WorkoutPlans" component={workoutPlans}/>
        <AppStack.Screen name="WorkoutPlanDetails" component={workoutPlanDetails}/>
        <AppStack.Screen name="NutritionPlans" component={nutritionPlans}/>
        <AppStack.Screen name="DoingExercise" component={doingExercise}/>
        <AppStack.Screen name="MachineExercises" component={machineExercises}/>
        <AppStack.Screen name="MemberPlan" component={memberPlan}/>
        <AppStack.Screen name="AvailablePlans" component={availablePlans}/>
        <AppStack.Screen name="MemberPlanHistory" component={memberPlanHistory}/>
        <AppStack.Screen name="NutritionalEvaluationDetails" component={nutritionalEvaluationDetails}/>
        <AppStack.Screen name="Dashboard" component={dashboard}/>
        <AppStack.Screen name="ExerciseHistory" component={exerciseHistory}/>
        <AppStack.Screen name="PhysicalEvaluations" component={physicalEvaluations}/>
        <AppStack.Screen name="PhysicalEvaluationDetails" component={physicalEvaluationDetails}/>
      </AppStack.Navigator>

    </NavigationContainer>
  );
}