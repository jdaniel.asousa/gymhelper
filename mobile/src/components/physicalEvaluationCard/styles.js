import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  card: {
    width: "90%",
    display: "flex",
    flexDirection: "row",
    borderRadius: 25,
    backgroundColor: "#FFFFFF",
    marginBottom: 15,
    marginLeft: 10,
    marginRight: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  textCard: {
    width: "80%",
    display: "flex",
    paddingLeft: "10%",
    paddingBottom: "5%",
    paddingTop: "5%"
  },
  colorCard: {
    width: "20%",
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
    zIndex: 2,
  },
  title: {
    fontSize: 20,
  },
  dates: {
    fontSize: 10,
  },
  weekDay: {
    fontSize: 13,
  },
  text: {
    marginTop: 2,
    color: "#003049"
  }
});