import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';
import { useNavigation } from '@react-navigation/native';

import styles from './styles';

export default function physicalEvaluationCard(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  const navigation = useNavigation();

  let date = props.date.getDate().toString() + "/" + (props.date.getMonth() + 1).toString() + "/" + props.date.getUTCFullYear().toString();

  function navigateToPhysicalEvaluationDetails() {
    navigation.navigate("PhysicalEvaluationDetails", {evaluationNumber: props.number, evaluationId: props.id});
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.card}>
      <View style={styles.textCard}>
        <TouchableOpacity onPress={navigateToPhysicalEvaluationDetails}>
          <Text style={[styles.title, styles.text, {fontFamily: "Poppins_400Regular"}]}>Avaliação Física {props.number}</Text>
        </TouchableOpacity>
        <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Realizada em: {date}</Text>
      </View>
      <View style={[styles.colorCard, {backgroundColor: props.color}]}></View>
    </View>;
  }
}