import React, {useState, useEffect} from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import {useFonts, Poppins_400Regular, Poppins_300Light} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';

import ExerciseCard from '../exerciseCard/index';

import api from '../../services/api';

import styles from './styles';

export default function workoutPlanDetails(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
    Poppins_300Light
  });

  const navigation = useNavigation();

  let button;

  let [exercises, setExercises] = useState([]);
  let [showButton, setShowButton] = useState(false);
  let [observation, setObservation] = useState("");

  let observationsContainer;

  useEffect(() => {
    (async () => {
      let userId = await AsyncStorage.getItem("id");
      let token = await AsyncStorage.getItem("token");
      try {
        const response = await api.get(`api/TrainingPlan/training-plan?request_id=${userId}&id=${props.workoutId}`, 
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        });

        console.log(response.data);
        //console.log(response.data.training_exercises[1]);

        setExercises(response.data.training_exercises);
        setObservation(response.data.description);
        setShowButton(true);
      } catch(error) {
        if(error.response) {
          if(error.response.status === 401) {
            logout();
          }
        }
      }

      
    })()
  }, [])

  async function logout() {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("id");
    await AsyncStorage.removeItem("name");
    await navigation.navigate("Login");
  }

  function navigateToDoingExercise() {
    //console.log("Testestestestets: ", exercises[0]);
    navigation.navigate("DoingExercise", {allExercises: exercises});
  }

  if(showButton) {
    button = <View style={styles.buttonStart}>
                <TouchableOpacity onPress={navigateToDoingExercise} style={styles.button}>
                  <Text style={[styles.buttonText, {fontFamily: "Poppins_400Regular"}]}>Realizar Treino</Text>
                </TouchableOpacity>
            </View>
  } else {
    button = null;
  }

  if(observation === "") {
    observationsContainer = null;
  } else {
    observationsContainer = <View style={styles.observations}>
      <Text style={[styles.observationTitle, styles.text, {fontFamily: "Poppins_400Regular"}]}>Observações:</Text>
      <View style={styles.descriptionContainer}>
        <Text style={[styles.text, {fontFamily: "Poppins_300Light"}]}>{observation}</Text>
      </View>
    </View>
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
      <View style={styles.cardInfo}>
        <Text style={[styles.planTitle, styles.text, {fontFamily: "Poppins_400Regular"}]}>Plano de Treino {props.workoutNumber}</Text>
        {observationsContainer}
        {button}
      </View>
      <ScrollView contentContainerStyle={styles.exercises}>
        {exercises.map((exercise, index) => (
          <ExerciseCard key={index} code={exercise.id} name={exercise.exercise.name} group={exercise.exercise.muscle} sets={exercise.sets} repetitions={exercise.repetitions} time={exercise.time} circuit={exercise.circuit} type={exercise.exercise.type} machine={exercise.exercise.machine}/>
        ))}
      </ScrollView>
    </View>;
  }
}
