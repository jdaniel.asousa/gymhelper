import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    marginTop: "15%",
    backgroundColor: "#F7F7F7",
  },
  cardInfo: {
    width: "90%",
    backgroundColor: "#F7F7F7",
    paddingBottom: "5%",
    marginLeft: "5%",
    borderBottomWidth: 1,
    borderBottomColor: "#DCDCDC",
  },
  exercises: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "5%",
    paddingBottom: 370
  },
  planTitle: {
    textAlign: "center",
    fontSize: 25
  },
  observations: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "90%",
  },
  buttonStart: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10
  },
  button: {
    height: 60,
    width: 195,
    backgroundColor: "#003049",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 20
  },
  observationTitle: {
    fontSize: 14,
  },
  descriptionContainer: {
    width: "70%",
    marginLeft: "5%",
  },
  text: {
    color: "#003049"
  }
});