import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    marginTop: "15%",
    justifyContent: "center",
    alignItems: "center",
  },
  exerciseTitle: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
  },
  arrowBack: {
    width: "10%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  titleContainer: {
    width: "90%",
    justifyContent: "center",
    alignItems: "center",
  },
  exerciseDescription: {
    display: "flex",
    flexDirection: "row",
    width: "90%",
    marginTop: 15,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#DCDCDC",
  },
  descriptionLeft: {
    width: "50%",
    paddingLeft: 20
  },
  descriptionRight: {
    width: "50%",
    display: "flex",
    borderLeftWidth: 1,
    borderLeftColor: "#DCDCDC",
    paddingLeft: 25
  },
  exerciseCountdownContainer: {
    marginTop: 15,
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  countdownsContainer: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    marginBottom: 275,
  },
  button: {
    backgroundColor: "#003049",
    width: 150,
    height: 50,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonRep: {
    backgroundColor: "#003049",
    width: "40%",
    height: 50,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  setInformation: {
    justifyContent: "center",
    alignItems: "center",
    width: "100%"
  },
  exerciseImage: {
    width: 320,
    height: 200,
    borderRadius: 10,
    marginRight: 20
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 20
  },
  title: {
    fontSize: 25,
    marginRight: 15
  },
  text: {
    color: "#003049"
  }
});