import React, { useState, useEffect, useRef } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Alert, Image, Linking } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';

import CountDown from 'react-native-countdown-component';
import InputSpinner from "react-native-input-spinner";

import { FontAwesome5 } from '@expo/vector-icons';

import moment from 'moment';

import { useNavigation } from '@react-navigation/native';

import api from '../../services/api';

import styles from './styles';

export default function doingExercise(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  const navigation = useNavigation();

  const [totalDuration, setTotalDuration] = useState(0);

  let [exercises, setExercises] = useState(props.exercises);
  let [currentExercise, setCurrentExercise] = useState(0);
  let [showCountdown, setShowCountdown] = useState(false);
  let [showStartButton, setShowStartButton] = useState(true);
  let [showStopButton, setShowStopButton] = useState(false);
  let [showFinishMessage, setShowFinishMessage] = useState(false);
  let [pause, setPause] = useState(true);
  let [startDate, setStartDate] = useState(moment().format('YYYY-MM-DD hh:mm:ss'));
  let [index, setIndex] = useState(0);
  let [set, setSet] = useState(index+1);
  let [registrations, setRegistrations] = useState([]);
  let [repetitions, setRepetitions] = useState([]);
  let [durations, setDurations] = useState([]);
  let [exerciseDone, setExerciseDone] = useState(false);
  let [timeExercise, setTimeExercise] = useState(false);
  let [exerciseImages, setExerciseImages] = useState([]);
  let [showImages, setShowImages] = useState(false);
  let [exerciseVideo, setExerciseVideo] = useState("");
  let [showVideo, setShowVideo] = useState(false);
  
  let countdown;

  let startButton;
  let stopButton;

  let machine;

  let finishMessage;

  let times;

  let exercise;
  let images;
  let video;

  useEffect(() => {
    setShowImages(false);
    setRegistrations([]);
    setDurations([]);

    if(exercises[currentExercise].time !== null) {
      setTimeExercise(true);
      let tmp = exercises[currentExercise].time.split("-");
      setDurations(tmp);
      setShowStartButton(true);
      setStartDate(moment().format('YYYY-MM-DD hh:mm:ss'));
      setPause(true);
      setIndex(0);
      setShowFinishMessage(false);
      setSet(1);
      setExerciseDone(false);
    }
  
    if(exercises[currentExercise].repetitions !== null) {
      setTimeExercise(false);
      let tmp1 = [];
      let tmp2 = [];

      tmp2 = exercises[currentExercise].repetitions.split("-");
      setRepetitions(tmp2);
      tmp2.map(repetition => {
        tmp1.push(repetition);
      });
      setRegistrations(tmp1);
    }

    if(exercises[currentExercise].exercise.exercise_images !== null) {
      setExerciseImages(exercises[currentExercise].exercise.exercise_images);
      setShowImages(true);
    }

    if(exercises[currentExercise].exercise.video !== null) {
      setExerciseVideo(exercises[currentExercise].exercise.video);
      setShowVideo(true);
    }
  }, [currentExercise]);

  function start() {    
    let d = parseInt(durations[index]);

    //converting in seconds
    setTotalDuration(d);

    setStartDate(moment().format('YYYY-MM-DD hh:mm:ss'));

    setShowCountdown(true);
    setShowStopButton(true);
    setShowStartButton(false);
    setPause(true);
  }

  function stopCountdown() {
    setPause(false);
    let diffr = moment.duration(moment(moment().format('YYYY-MM-DD hh:mm:ss')).diff(startDate));
    //let timeDone = `${diffr.minutes()}:${diffr.seconds()}`;
    let timeDone = ((diffr.minutes() * 60) + diffr.seconds()).toString();
    registrations.push(timeDone);
    setShowCountdown(false);
    if(index < durations.length-1) {
      setIndex(index+1);
      setSet(index+2);
      setShowStopButton(false);
      setShowStartButton(true);
    } else {
      setShowStopButton(false);
      setShowFinishMessage(true);
      setExerciseDone(true);
    }
  }

  async function nextExercise() {
    if(currentExercise < exercises.length-1) {
      let userId = await AsyncStorage.getItem("id");
      let token = await AsyncStorage.getItem("token");

      if(timeExercise) {
        try {
          let times = registrations.join("-");

          const response = await api.post('api/TrainingExerciseHistory/create-training-exercise-history', 
          {
            "request_id": userId,
            "training_exercise_id": exercises[currentExercise].id,
            "time": times
          },
          {
            headers: {
              Authorization: `Bearer ${token}`
            }
          });
          Alert.alert("Exercicio completado", "Registo do exercício feito com sucesso");
          setCurrentExercise(currentExercise+1);
        } catch(error) {
          if(error.response) {
            if(error.response.status === 401) {
              logout();
            }
          }
        }
      } else {
        try {
          let repetitions = registrations.join("-");

          const response = await api.post('api/TrainingExerciseHistory/create-training-exercise-history', 
          {
            "request_id": userId,
            "training_exercise_id": exercises[currentExercise].id,
            "repetitions": repetitions
          },
          {
            headers: {
              Authorization: `Bearer ${token}`
            }
          });
          
          Alert.alert("Exercicio completado", "Registo do exercício feito com sucesso");
          setCurrentExercise(currentExercise+1);
        } catch(error) {
          if(error.response) {
            if(error.response.status === 401) {
              logout();
            }
          }
        }
      }
    } else {
      let userId = await AsyncStorage.getItem("id");
      let token = await AsyncStorage.getItem("token");
  
      if(timeExercise) {
        try {
          let times = registrations.join("-");
  
          const response = await api.post('api/TrainingExerciseHistory/create-training-exercise-history', 
          {
            "request_id": userId,
            "training_exercise_id": exercises[currentExercise].id,
            "time": times
          },
          {
            headers: {
              Authorization: `Bearer ${token}`
            }
          });
          Alert.alert("Exercicio completado", "Registo do exercício feito com sucesso");
          goBack();
        } catch(error) {
          if(error.response) {
            if(error.response.status === 401) {
              logout();
            }
          }
        }
      } else {
        try {
          let repetitions = registrations.join("-");
  
          const response = await api.post('api/TrainingExerciseHistory/create-training-exercise-history', 
          {
            "request_id": userId,
            "training_exercise_id": exercises[currentExercise].id,
            "repetitions": repetitions
          },
          {
            headers: {
              Authorization: `Bearer ${token}`
            }
          });
            
          Alert.alert("Exercicio completado", "Registo do exercício feito com sucesso");
          goBack();
        } catch(error) {
          if(error.response) {
            if(error.response.status === 401) {
              logout();
            }
          }
        }
      }
    }
  }

  async function sendRegistration() {
    let userId = await AsyncStorage.getItem("id");
    let token = await AsyncStorage.getItem("token");

    if(timeExercise) {
      try {
        let times = registrations.join("-");

        const response = await api.post('api/TrainingExerciseHistory/create-training-exercise-history', 
        {
          "request_id": userId,
          "training_exercise_id": exercises[currentExercise].id,
          "time": times
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        });
        Alert.alert("Exercicio completado", "Registo do exercício feito com sucesso");
      } catch(error) {
        if(error.response) {
          if(error.response.status === 401) {
            logout();
          }
        }
      }
    } else {
      try {
        let repetitions = registrations.join("-");

        const response = await api.post('api/TrainingExerciseHistory/create-training-exercise-history', 
        {
          "request_id": userId,
          "training_exercise_id": exercises[currentExercise].id,
          "repetitions": repetitions
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        });
        
        Alert.alert("Exercicio completado", "Registo do exercício feito com sucesso");
      } catch(error) {
        if(error.response) {
          if(error.response.status === 401) {
            logout();
          }
        }
      }
    }
  }

  function goBack() {
    navigation.goBack();
  }

  async function logout() {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("id");
    await AsyncStorage.removeItem("name");
    await navigation.navigate("Login");
  }

  function openLink() {
    Linking.openURL(exerciseVideo);
  }

  if(showCountdown) {
    countdown = <CountDown
    until={totalDuration}
    size={30}
    onFinish={stopCountdown}
    digitStyle={{backgroundColor: '#FFF', marginRight: 15, marginLeft: 15, fontFamily: "Poppins_400Regular"}}
    digitTxtStyle={{color: '#003049'}}
    timeToShow={['M', 'S']}
    timeLabels={{m: 'Minutos', s: 'Segundos'}}
    running={pause}
  />
  } else {
    countdown = null;
  }

  if(showStartButton) {
    startButton = <TouchableOpacity style={styles.button} onPress={start}>
      <Text style={[styles.buttonText, {fontFamily: "Poppins_400Regular"}]}>Começar</Text>
    </TouchableOpacity>
  } else {
    startButton = null;
  }

  if(showStopButton) {
    stopButton = <TouchableOpacity style={[styles.button, {marginTop: 10}]} onPress={stopCountdown}>
    <Text style={[styles.buttonText, {fontFamily: "Poppins_400Regular"}]}>Parar</Text>
  </TouchableOpacity>;
  } else {
    stopButton = null;
  }

  if(exercises[currentExercise].exercise.machine === null) {
    machine = <Text style={{fontFamily: "Poppins_400Regular"}}>Não necessita</Text>
  } else {
    machine = <Text style={{fontFamily: "Poppins_400Regular"}}>{props.machine}</Text>
  }

  if(showFinishMessage) {
    finishMessage = <Text style={{marginBottom: 25, fontFamily: "Poppins_400Regular"}}>Exercício Completado</Text>
  } else {
    finishMessage = null;
  }

  if(exerciseDone) {
    times = <View style={{width: "100%", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center"}}>
              <View style={{display: "flex", flexDirection: "row", width: "100%", justifyContent: "center", alignItems: "center", flexWrap: "wrap"}}>
                {registrations.map((registration, index) => (
                  <View key={index} style={{marginLeft: 5, marginRight: 5, marginBottom: 2.5, marginTop: 2.5, justifyContent: "center", alignItems: "center", borderWidth: 2, borderColor: "#003049", width: 70, borderRadius: 10}}>
                    <View style={{borderBottomWidth: 1, borderBottomColor: "#DCDCDC"}}>
                      <Text style={{fontFamily: "Poppins_400Regular"}}>Série {index + 1}</Text>
                    </View>
                    <View>
                      <Text style={{fontFamily: "Poppins_400Regular"}}>{registration}</Text>
                    </View>
                  </View>
                ))}
              </View>
              <TouchableOpacity onPress={nextExercise} style={[styles.button, {marginTop: 10}]}>
                <Text style={[styles.buttonText, {fontFamily: "Poppins_400Regular"}]}>Confirmar</Text>
              </TouchableOpacity>
            </View>
    
  } else {
    times = null;
  }

  if(showImages) {
    images = <ScrollView horizontal
                pagingEnabled
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{justifyContent: "center", alignItems: "center", marginLeft: 20, paddingRight: 20, marginBottom: 10}}>
              {exerciseImages.map((image, index) => (
                <Image key={index} style={styles.exerciseImage} source={{
                  uri: image.path
                }}/>
              ))}
            </ScrollView>
  } else {
    images = null;
  }

  if(showVideo) {
    video = <TouchableOpacity onPress={openLink} style={[styles.button, {marginTop: 10, marginBottom: 10}]}>
      <Text style={[styles.buttonText, {fontFamily: "Poppins_400Regular"}]}>Video</Text>
    </TouchableOpacity>
  } else {
    video = null;
  }

  if(timeExercise) {
    exercise = <ScrollView contentContainerStyle={styles.exerciseCountdownContainer}>
      <View style={styles.setInformation}>
        {images}
        {video}
        <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Série: {set}</Text>
      </View>
      <ScrollView contentContainerStyle={styles.countdownsContainer}>
        {countdown}
        {startButton}
        {stopButton}
        {finishMessage}
        {times}
      </ScrollView>
    </ScrollView>
  } else {
    exercise = <ScrollView contentContainerStyle={{display: "flex",
    marginTop: 5,
    paddingTop: 10,
    paddingBottom: 275,
    width: "100%",}}>
      <View style={{justifyContent: "center", alignItems: "center"}}>
        {images}
        {video}
        <View style={{justifyContent: "center", alignItems: "center"}}>
          <Text style={{fontFamily: "Poppins_400Regular"}}>Série: {set}</Text>
        </View>
        <View>
          {registrations.map((registrtation, index) => (
            <View key={index} style={{display: "flex", flexDirection: "row", justifyContent: "center", alignItems: "center", width: "100%", marginTop: 10}}>
              <View style={{marginRight: 30}}>
                <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Série {index+1} ({repetitions[index]})</Text>
              </View>
              <View>
                <InputSpinner
                  max={repetitions[index]}
                  min={0} 
                  step={1}
                  colorMax={"#003049"}
                  colorMin={"#003049"}
                  value={registrations[index]}
                  onChange={(num) => {
                    registrations[index] = num;
                  }}/>
              </View>
            </View>
          ))}  
        </View>
        <TouchableOpacity style={[styles.buttonRep, {marginTop: 10}]} onPress={nextExercise}>
          <Text style={[styles.buttonText, {fontFamily: "Poppins_400Regular"}]}>Confirmar</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
      <View style={styles.exerciseTitle}>
        <View style={styles.arrowBack}>
          <TouchableOpacity onPress={goBack}>
            <FontAwesome5 name="arrow-left" size={24} color="#003049" />
          </TouchableOpacity>
        </View>
        <View style={styles.titleContainer}>
          <Text style={[styles.title, styles.text, {fontFamily: "Poppins_400Regular"}]}>{exercises[currentExercise].exercise.name}</Text>
        </View>
      </View>
      <View style={styles.exerciseDescription}>
        <View style={styles.descriptionLeft}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Circuito</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Grupo Muscular</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Máquina</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Séries</Text>
        </View>
        <View style={styles.descriptionRight}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{exercises[currentExercise].circuit}</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{exercises[currentExercise].exercise.muscle}</Text>
          {machine}
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{exercises[currentExercise].sets}</Text>
        </View>
      </View>
      {exercise}
      
    </View>;
  }
}