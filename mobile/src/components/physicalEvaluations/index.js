import React, { useState, useEffect } from 'react';
import { View, ScrollView, Text, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';
import { useNavigation } from '@react-navigation/native';
import { FontAwesome5 } from '@expo/vector-icons';

import PhysicalEvaluationCard from '../physicalEvaluationCard/index';

import api from '../../services/api';

import styles from './styles';


export default function physicalEvaluations() {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  const navigation = useNavigation();

  let [physicalEvaluations, setPhysicalEvaluations] = useState([]);

  let physicalEvaluationsContainer;

  useEffect(() => {
    (async () => {
      let token = await AsyncStorage.getItem("token");
      try {
        let userId = await AsyncStorage.getItem('id');
        const response = await api.get(`api/PhysicalEvaluation/physical-evaluations-by-athlete-id?request_id=${userId}`, 
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })

        setPhysicalEvaluations(response.data);
      } catch (error) {
        if(error.response) {
          if(error.response.status === 401) {
            logout();
          }
        }
      }
    })()
  }, []);

  function goBack() {
    navigation.goBack();
  }

  async function logout() {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("id");
    await AsyncStorage.removeItem("name");
    await navigation.navigate("Login");
  }

  if(physicalEvaluations.length === 0) {
    physicalEvaluationsContainer = <View style={styles.message}>
      <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Não possui nenhuma avaliação física</Text>
    </View>;
  } else {
    physicalEvaluationsContainer = <ScrollView contentContainerStyle={styles.scroll}>
        {physicalEvaluations.map((evaluation, index) => (
          <PhysicalEvaluationCard key={index} number={index+1} id={evaluation.id} date={new Date(evaluation.created_at)} color={"#003049"}/>
        ))}
    </ScrollView> 
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
      <View style={styles.titleContainer}>
        <View style={styles.arrowBack}>
          <TouchableOpacity onPress={goBack}>
            <FontAwesome5 name="arrow-left" size={24} color="#003049" />
          </TouchableOpacity>
        </View>
        <View style={styles.titleTextContainer}>
          <Text style={[styles.title, {fontFamily: "Poppins_400Regular"}]}>Avaliações Físicas</Text>
        </View>
      </View>
      {physicalEvaluationsContainer}
    </View>;
  }
}