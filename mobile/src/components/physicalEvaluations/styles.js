import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: "100%",
    marginTop: "20%",
    justifyContent: "center",
    alignItems: "center",
  },
  scroll: {
    width: "100%",
    marginTop: 10,
    display: "flex",
    alignItems: "center",
    paddingBottom: 150
  },
  titleContainer: {
    width: "100%",
    borderBottomColor: "#DCDCDC",
    borderBottomWidth: 1,
    paddingBottom: 5,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  title: {
    fontSize: 25,
    color: "#003049"
  },
  text: {
    color: "#003049",
    fontSize: 20,
    textAlign: "center"
  },
  message: {
    width: "100%",
    marginTop: 150,
    justifyContent: "center",
    alignItems: "center"
  },
  arrowBack: {
    width: "10%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 20,
  },
  titleTextContainer: {
    width: "90%",
    justifyContent: "center",
    alignItems: "center",
  },
});