import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { FontAwesome5, MaterialIcons, MaterialCommunityIcons, Entypo } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

import styles from './styles';

function footer() {
  const navigation = useNavigation();

  function navigateToScanner() {
    navigation.navigate("QrCodeScan");
  }

  function navigateToWorkoutPlans() {
    navigation.navigate("WorkoutPlans");
  }

  function navigateToNutritionPlans() {
    navigation.navigate("NutritionPlans");
  }

  function navigateToMemberPlan() {
    navigation.navigate("MemberPlan");
  }

  function navigateToDashboard() {
    navigation.navigate("Dashboard");
  }

  return <View style={styles.navbar}>
    <TouchableOpacity onPress={navigateToDashboard}>
      <MaterialIcons name="dashboard" size={25} color={"#003049"}/>
    </TouchableOpacity>
    <TouchableOpacity onPress={navigateToWorkoutPlans}>
      <FontAwesome5 name="dumbbell" size={20} color={"#003049"}/>
    </TouchableOpacity>
    <TouchableOpacity onPress={navigateToScanner}>
      <MaterialCommunityIcons name="qrcode-scan" size={25} color={"#003049"}/>
    </TouchableOpacity>
    <TouchableOpacity onPress={navigateToNutritionPlans}>
      <MaterialIcons name="restaurant" size={25} color={"#003049"}/>
    </TouchableOpacity>
    <TouchableOpacity onPress={navigateToMemberPlan}>
      <MaterialIcons name="credit-card" size={27} color={"#003049"}/>
    </TouchableOpacity>
  </View>;
}

export default footer;