import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  navbar: {
    height: "10%",
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    bottom: 0,
    position: "absolute",
  }
});