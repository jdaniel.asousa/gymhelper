import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: 325,
    marginTop: 10,
    backgroundColor: "#FFFFFF",
    paddingTop: 10,
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center"
  },
  description: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
  },
  line: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 10,
    paddingRight: 10,
  },
  textName: {
    width: "50%",
    color: "#003049"
  },
  textValue: {
    width: "50%",
    color: "#003049"
  },
  divisor: {
    width: "90%",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#DCDCDC"
  },
  exerciseImage: {
    width: 300,
    height: 200,
    borderRadius: 10,
    marginRight: 20
  },
  button: {
    backgroundColor: "#003049",
    width: 150,
    height: 50,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 20
  },
});