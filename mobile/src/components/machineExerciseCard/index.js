import React from 'react';
import { View, Text, ScrollView, Image, Linking, TouchableOpacity } from 'react-native';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';

import styles from './styles';

export default function machineExerciseCard(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  let exerciseImages;
  let exerciseVideo;
  let exerciseType;

  function openLink() {
    Linking.openURL(exerciseVideo);
  }

  if(props.exerciseImages !== undefined) {
    exerciseImages = <ScrollView horizontal
                        pagingEnabled
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{justifyContent: "center", alignItems: "center", marginLeft: 12, paddingRight: 10, marginBottom: 10, marginTop: 10}}>
                          {props.exerciseImages.map((image, index) => (
                            <Image key={index} source={{
                              uri: image.path
                            }} style={styles.exerciseImage}/>
                          ))}

                    </ScrollView>
  } else {
    exerciseImages = null;
  }

  if(props.exerciseVideo !== null) {
    exerciseVideo = <TouchableOpacity style={styles.button}>
      <Text style={styles.buttonText}>Vídeo</Text>
    </TouchableOpacity>
  } else {
    exerciseVideo = null;
  }

  if(props.type==="Time") {
    exerciseType = <Text style={[styles.textValue, {fontFamily: "Poppins_400Regular"}]}>Tempo</Text>
  } else {
    exerciseType = <Text style={[styles.textValue, {fontFamily: "Poppins_400Regular"}]}>Repetições</Text>
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
      <View style={styles.description}>
        <View style={styles.line}>
          <Text style={[styles.textName, {fontFamily: "Poppins_400Regular"}]}>Nome:</Text>
          <Text style={[styles.textValue, {fontFamily: "Poppins_400Regular"}]}>{props.name}</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.textName, {fontFamily: "Poppins_400Regular"}]}>Grupo Muscular:</Text>
          <Text style={[styles.textValue, {fontFamily: "Poppins_400Regular"}]}>{props.group}</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.textName, {fontFamily: "Poppins_400Regular"}]}>Descrição:</Text>
          <Text style={[styles.textValue, {fontFamily: "Poppins_400Regular"}]}>{props.description}</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.textName, {fontFamily: "Poppins_400Regular"}]}>Tipo:</Text>
          {exerciseType}
        </View>
      </View>
      <View style={styles.divisor}></View>
      {exerciseImages}
      {exerciseVideo}
    </View>;
  }
}