import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  infos: {
    flexDirection: "column",
    width: "90%",
    height: "60%",
    backgroundColor: "#FFFFFF",
    borderRadius: 5,
    justifyContent: "space-evenly",
    alignItems: "center",
    paddingBottom: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  info: {
    width: "95%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 5,
    paddingRight: 5,
    borderBottomWidth: 2,
    borderBottomColor: "#DCDCDC"
  },
  infoDetails: {
    flexDirection: "row",
  },
  informationText: {
    color: "#003049"
  },
  buttonText: {
    color: "#F7F7F7", 
    fontSize: 15
  },
  modal: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: "100%",
    marginRight: 20,
    marginLeft: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalPassword: {
    justifyContent: "center",
    alignItems: "center",
    height: "30%",
    marginTop: "100%",
    marginRight: 20,
    marginLeft: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalButtonsContainer: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 10
  },
  modalButtons: {
    width: "40%",
    height: 35, 
    backgroundColor: "#003049", 
    justifyContent: "center", 
    alignItems: "center", 
    borderRadius: 5
  },
  modalButtonsText: {
    color: "#F7F7F7", 
    fontSize: 13
  },
  inputContainer: {
    backgroundColor: "blue",
    width: "100%",
    height: "35%",
    marginTop: 0,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#DCDCDC",
    borderRadius: 5
  },
  inputPasswordContainer: {
    flexDirection: "column",
    width: "100%",
    height: "65%",
    marginBottom: 20
  },
  passwordContainer: {
    backgroundColor: "#DCDCDC",
    flexDirection: "row",
    width: "100%",
    height: "50%",
    marginBottom: 10,
    borderRadius: 5
  },
  inputConfirmPasswordContainer: {
    backgroundColor: "#DCDCDC",
    flexDirection: "row",
    width: "100%",
    height: "50%",
    borderRadius: 5
  },
  userIcon: {
    marginLeft: 5,
    marginTop: 7
  },
  emailIcon: {
    marginLeft: 5,
    marginTop: 7,
  },
  passwordIcon: {
    marginLeft: 5,
    marginTop: 7,
  },
  input: {
    height: "100%", 
    width: "85%", 
    borderWidth: 1,
    marginBottom: 20,
    marginLeft: 5,
    paddingLeft: 5,
    borderRadius: 5,
    borderColor: "#DCDCDC",
    backgroundColor: "#DCDCDC"
  },
  inputPassword: {
    height: "100%", 
    width: "78.5%", 
    borderWidth: 1,
    marginBottom: 20,
    marginLeft: 5,
    paddingLeft: 5,
    borderColor: "#DCDCDC",
    backgroundColor: "#DCDCDC"
  },
  buttonPress: {
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
    paddingBottom: 3,
    paddingRight: 4,
    backgroundColor: "#DCDCDC",
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5
  },
  buttonNormal: {
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 3,
    height: "100%",
    paddingRight: 4,
    backgroundColor: "#DCDCDC",
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5
  }
});