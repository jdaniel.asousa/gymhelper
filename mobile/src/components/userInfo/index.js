import React,  { useState, useEffect } from 'react';
import { View, TouchableOpacity, Text, Modal, TextInput, TouchableHighlight, Image } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as ImagePicker from 'expo-image-picker';
import { useNavigation } from '@react-navigation/native';

import DateTimePicker from '@react-native-community/datetimepicker';

import { AntDesign, MaterialIcons, FontAwesome, Ionicons } from '@expo/vector-icons';

import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';

import moment from 'moment';

import api from '../../services/api';

import styles from './styles';
import { useLinkProps } from '@react-navigation/native';

function userInfo(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular
  });

  const navigation = useNavigation();

  let [initialName, setInitialName] = useState("");
  let [initialEmail, setInitialEmail] = useState("");
  let [initialDate, setInitialDate] = useState("");

  let [informationLoaded, setInformationLoaded] = useState(false);

  let [hidePassword, setHidePassword] = useState(true);
  let [isPress, setIsPress] = useState(false);

  let [modalName, setModalName] = useState(false);
  let [modalEmail, setModalEmail] = useState(false);
  let [modalPassword, setModalPassword] = useState(false);
  let [show, setShow] = useState(false);

  let [name, setName] = useState("");
  let [email, setEmail] = useState("");
  let [date, setDate] = useState(new Date());
  let [password, setPassword] = useState("");
  let [confirmPassword, setConfirmPassword] = useState("");

  let [imageExists, setImageExists] = useState(false);
  let [userImageProfile, setUserImageProfile] = useState("");

  //Date Input
  let datePicker;

  let userImage;
  let imageUri;

  useEffect(() => {
    (async () => {
      let token = await AsyncStorage.getItem("token");
      try {
        let userId = await AsyncStorage.getItem('id');
        const response = await api.get(`api/user/user?request_id=${userId}&id=${userId}`, 
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })

        let birthDate = response.data.athlete_info.birth_date;
        let birthDateArray = birthDate.split("T");

        setInitialName(response.data.name);
        setInitialEmail(response.data.email);
        setInitialDate(new Date(response.data.athlete_info.birth_date).getDate().toString() + "/" + (new Date(response.data.athlete_info.birth_date).getMonth()+1).toString() + "/" + new Date(response.data.athlete_info.birth_date).getFullYear().toString());

        setName(response.data.name);
        setEmail(response.data.email);
        setDate(new Date(birthDateArray[0]));

        if(response.data.avatar === null) {
          setImageExists(false);
        } else {
          await AsyncStorage.setItem("imageUri", response.data.avatar);
          setUserImageProfile(response.data.avatar);
          setImageExists(true);
        }

        setInformationLoaded(true);
      } catch (error) {
        if(error.response) {
          if(error.response.status === 401) {
            logout();
          }
        }
      }
    })();
  }, []);

  async function logout() {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("id");
    await AsyncStorage.removeItem("name");
    await navigation.navigate("Login");
  }

  async function pickImage() {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    setUserImageProfile(result.uri);
    imageUri = result.uri;
    setImageExists(true);
    updateAvatar();

    /*if (!result.cancelled) {
      setImage(result.uri);
      setImageExists(true);
    }*/
   
  }

  async function updateName() {
    try {
      let userId = parseInt(await AsyncStorage.getItem('id'));
      let token = await AsyncStorage.getItem("token");

      const data = new FormData();
      data.append("id", userId);
      data.append("name", name);
      const response = await api.put('api/user/update-user', data, 
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': `Bearer ${token}`
        }
      })

      setInitialName(response.data.name);
      AsyncStorage.setItem('name', response.data.name);
      showNameModal();
    } catch (error) {
      if(error.response) {
        if(error.response.status === 401) {
          logout();
        }
      }
    }
  }

  async function updateAvatar() {
    try {
      let userId = parseInt(await AsyncStorage.getItem('id'));
      let token = await AsyncStorage.getItem("token");

      let imageSplit = imageUri;
      let userImage = imageSplit[imageSplit.length-1];

      const data = new FormData();
      data.append("id", userId);

      let file;

      if(userImage.localeCompare("jpg")) {
        file = {
          uri: imageUri,
          name: `image.jpg`,
          type: `image/jpg`,
        }
      } else {
        file = {
          uri: imageUri,
          name: `image.png`,
          type: `image/png`,
        }
      }

      data.append("avatar", file);

      const response = await api.put('api/user/update-user', data, 
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': `Bearer ${token}`
        }
      })
    } catch (error) {
      if(error.response) {
        if(error.response.status === 401) {
          logout();
        }
      }
    }
  }

  async function updateDate(date) {
    try {
      let userId = await AsyncStorage.getItem('id');
      let token = await AsyncStorage.getItem("token");

      const tmp = "00:00:00 AM"
      date = moment(date).format("MM/DD/YYYY");
      const data = new FormData();
      data.append("id", userId);
      data.append("birth_date", `${date.toString()} ${tmp}`);
      const response = await api.put('api/user/update-user', data, 
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': `Bearer ${token}`
        }
      })
      
      let showingDate = moment(date).date() + "/" + (moment(date).month()+1) + "/" + moment(date).year();
      setInitialDate(showingDate);
    } catch (error) {
      if(error.response) {
        if(error.response.status === 401) {
          logout();
        }
      }
    }
  }

  async function updatePassword() {
    try {
      let userId = await AsyncStorage.getItem('id');
      let token = await AsyncStorage.getItem("token");
      
      const data = new FormData();
      data.append("id", userId);
      data.append("password", password);
      data.append("confirmPassword", confirmPassword);

      const response = await api.put('api/user/update-user', data, 
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': `Bearer ${token}`
        }
      })

      showPasswordModal();
    } catch (error) {
      if(error.response) {
        if(error.response.status === 401) {
          logout();
        }
      }
    }
  }

  async function logout() {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("id");
    await AsyncStorage.removeItem("name");
    await navigation.navigate("Login");
  }

  function showPassword() {
    if(hidePassword) {
      setHidePassword(false);
    } else {
      setHidePassword(true);
    }
  }

  function showNameModal() {
    setModalName(!modalName);
  }

  function showPasswordModal() {
    setModalPassword(!modalPassword);
  } 

  function showDatePicker() {
    setShow(!show);
  }

  function getInitials() {
    var names = name.split(' '),
        initials = names[0].substring(0, 1).toUpperCase();
      
    if (names.length > 1) {
        initials += names[names.length - 1].substring(0, 1).toUpperCase();
    }

    return initials;
  }

  //When any button in date picker is clicked
  const onChange = (event, selectedDate) => {
    //When "Ok" button is clicked 
    if(event.type == "set") {
      date = selectedDate;
      console.log("dashjodaskjldaskljndsakljndaklsdaskdjjaskldklj" + date);
      updateDate(date);
    }
    setShow(false);
  };

  //Show date Input if show is true
  if(show) {
    datePicker = <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode="date"
          is24Hour={true}
          display="default"
          onChange={onChange}
          />
  } else {
    datePicker = null;
  }

  if(imageExists) {
    userImage = <Image source={{uri: userImageProfile}} style={{width: 100, height: 100, borderRadius: 60, marginBottom: 10}}/>
  } else {
    let initials = getInitials();
    userImage = <View style={{width: 100, height: 100, borderRadius: 60, backgroundColor: "#003049", justifyContent: "center", alignItems: "center", marginBottom: 10}}>
      <Text style={{color: "#FFFFFF", fontSize: 30}}>{initials}</Text>
    </View>
  }

  if(!fontsLoaded || !informationLoaded) {
    return <AppLoading/>
  } else {
    return <View style={{alignItems: 'center', marginTop: "2%"}}>
      <Text style={[styles.informationText, {fontFamily: "Poppins_400Regular"}]}>Poderá ver/alterar as informações pessoais</Text>
      <View style={styles.infos}>
        <View style={styles.info}>
          <Text style={[styles.informationText, {fontFamily: "Poppins_400Regular"}]}>Nome</Text>
            <TouchableOpacity onPress={showNameModal} style={styles.infoDetails}>
              <Text style={[styles.informationText, {fontFamily: "Poppins_400Regular"}]}>{initialName}</Text>
              <AntDesign name="right" style={{marginTop: 6, marginLeft: 3}} color="#003049"/>
            </TouchableOpacity>
        </View>
        <View style={styles.info}>
          <Text style={[styles.informationText, {fontFamily: "Poppins_400Regular"}]}>Email</Text>
          <Text style={[styles.informationText, {fontFamily: "Poppins_400Regular"}]}>{initialEmail}</Text>
        </View>
        <View style={styles.info}>
          <Text style={[styles.informationText, {fontFamily: "Poppins_400Regular"}]}>Data de Nascimento</Text>
            <TouchableOpacity onPress={showDatePicker} style={styles.infoDetails}>
              <Text style={[styles.informationText, {fontFamily: "Poppins_400Regular"}]}>{initialDate.toString()}</Text>
              <AntDesign name="right" style={{marginTop: 6, marginLeft: 3}} color="#003049"/>
            </TouchableOpacity>
        </View>
        <View style={styles.info}>
          <Text style={[styles.informationText, {fontFamily: "Poppins_400Regular"}]}>Avatar</Text>
          <TouchableOpacity onPress={pickImage} style={styles.infoDetails}>
            {userImage}
          </TouchableOpacity>
        </View>
      </View>

      <TouchableOpacity onPress={showPasswordModal} style={{marginTop: 20, width: "45%", height: 50, backgroundColor: "#003049", justifyContent: "center", alignItems: "center", borderRadius: 5}}>
        <Text style={[styles.buttonText, {fontFamily: "Poppins_400Regular"}]}>Alterar Password</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={logout} style={{marginTop: 10, width: "45%", height: 50, backgroundColor: "#003049", justifyContent: "center", alignItems: "center", borderRadius: 5}}>
        <Text style={[styles.buttonText, {fontFamily: "Poppins_400Regular"}]}>Logout</Text>
      </TouchableOpacity>

      <Modal animationType="slide" transparent={true} visible={modalName}>
        <View style={styles.modal}>
          <View style={styles.inputContainer}>
            <FontAwesome name="user-circle" size={24} color="#50504E" style={styles.userIcon}/>
            <TextInput style={[styles.input, {fontFamily: "Poppins_400Regular"}]} placeholder="Nome" onChangeText={text => setName(text)}/>
          </View>
          <View style={styles.modalButtonsContainer}>
            <TouchableOpacity style={styles.modalButtons} onPress={showNameModal}><Text style={[styles.modalButtonsText, {fontFamily: "Poppins_400Regular"}]}>Cancelar</Text></TouchableOpacity>
            <TouchableOpacity onPress={updateName} style={styles.modalButtons}><Text style={[styles.modalButtonsText, {fontFamily: "Poppins_400Regular"}]}>Guardar</Text></TouchableOpacity>
          </View>
        </View>
      </Modal>

      <Modal animationType="slide" transparent={true} visible={modalPassword}>
        <View style={styles.modalPassword}>
          <View style={styles.inputPasswordContainer}>
            <View style={styles.passwordContainer}>
              <Ionicons name="md-key" size={24} color="#50504E" style={styles.passwordIcon}/>
              <TextInput secureTextEntry={hidePassword} style={[styles.inputPassword, {fontFamily: "Poppins_400Regular"}]} placeholder="Nova Password" onChangeText={text => setPassword(text)}/>
              <TouchableHighlight onPress={showPassword.bind(this)} underlayColor={"#DCDCDC"} onHideUnderlay={() => setIsPress(false)} onShowUnderlay={() => setIsPress(true)} style={isPress ? styles.buttonPress : styles.buttonNormal}>
                <Ionicons name={hidePassword ? "md-eye-off" : "md-eye"} size={24} color="#50504E"/>
              </TouchableHighlight>
            </View>
            <View style={styles.inputConfirmPasswordContainer}>
              <Ionicons name="md-key" size={24} color="#50504E" style={styles.passwordIcon}/>
              <TextInput secureTextEntry={hidePassword} style={[styles.inputPassword, {fontFamily: "Poppins_400Regular"}]} placeholder="Confirmar Password" onChangeText={text => setConfirmPassword(text)}/>
              <TouchableHighlight onPress={showPassword} underlayColor={"#DCDCDC"} onHideUnderlay={() => setIsPress(false)} onShowUnderlay={() => setIsPress(true)} style={isPress ? styles.buttonPress : styles.buttonNormal}>
                <Ionicons name={hidePassword ? "md-eye-off" : "md-eye"} size={24} color="#50504E"/>
              </TouchableHighlight>
            </View>
          </View>
          <View style={styles.modalButtonsContainer}>
            <TouchableOpacity style={styles.modalButtons} onPress={showPasswordModal}><Text style={[styles.modalButtonsText, {fontFamily: "Poppins_400Regular"}]}>Cancelar</Text></TouchableOpacity>
            <TouchableOpacity onPress={updatePassword} style={styles.modalButtons}><Text style={[styles.modalButtonsText, {fontFamily: "Poppins_400Regular"}]}>Guardar</Text></TouchableOpacity>
          </View>
        </View>
      </Modal>
      
      {datePicker}

    </View>;
  }
}

export default userInfo;