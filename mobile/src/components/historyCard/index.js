import React from 'react';
import { View, Text } from 'react-native';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';

import styles from './styles'; 

export default function historyCard(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  let date = props.date.getDate().toString() + "/" + (props.date.getMonth() + 1).toString() + "/" + props.date.getUTCFullYear().toString();

  let type;
  let typeValue;

  if(props.time === undefined) {
    typeValue = <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.repetitions}</Text>
    type = <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Repetições Realizadas</Text>
  } else {
    typeValue = <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.time}</Text>
    type = <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Tempo Realizado</Text>
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.cardContainer}>
              <View style={styles.upperLine}>
                {type}
                <Text style={[styles.text, styles.middleText , {fontFamily: "Poppins_400Regular"}]}>Objetivo</Text>
                <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Data de realização</Text>
              </View>
              <View style={styles.line}>
                {typeValue}
                <Text style={[styles.text, styles.middleText, {fontFamily: "Poppins_400Regular"}]}>{props.objective}</Text>
                <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{date}</Text>
              </View>
            </View>;
  }
}