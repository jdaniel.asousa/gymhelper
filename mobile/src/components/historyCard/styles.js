import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  cardContainer: {
    justifyContent: "center",
    alignItems: "center",
    width: "90%",
    backgroundColor: "#FFFFFF", 
    borderRadius: 15,
    marginTop: 10,
    marginBottom: 10, 
    padding: 15,   
  },
  upperLine: {
    width: "100%", 
    display: "flex", 
    flexDirection: "row", 
    justifyContent: "space-between", 
    alignItems: "center",
    borderBottomColor: "#DCDCDC",
    borderBottomWidth: 1,
  },
  line: {
    width: "100%", 
    display: "flex", 
    flexDirection: "row", 
    justifyContent: "space-between", 
    alignItems: "center",
    marginTop: 5,
  },
  text: {
    width: "30%",
    color: "#003049",
    textAlign: "center",
    fontSize: 12,
  },
  middleText: {
    borderLeftColor: "#DCDCDC",
    borderRightColor: "#DCDCDC",
    borderLeftWidth: 1,
    borderRightWidth: 1,
  }
});