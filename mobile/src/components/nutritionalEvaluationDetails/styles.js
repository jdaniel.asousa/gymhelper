import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: "100%",
    marginTop: "20%",
    justifyContent: "center",
    alignItems: "center"
  },
  titleContainer: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    fontSize: 25,
    color: "#003049"
  },
  description: {
    display: "flex",
    flexDirection: "row",
    width: "90%",
    marginTop: 10,
    borderBottomColor: "#DCDCDC",
    borderBottomWidth: 1,
    paddingBottom: 5,
  },
  descriptionDesignation: {
    width: "35%",
    paddingLeft: 25,
  },
  descriptionValue: {
    width: "65%",
    marginLeft: 5,
  },
  text: {
    color: "#003049",
  },
  scroll: {
    width: 360,
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 200,
  },
  secondTitleContainer: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  secondTitle: {
    fontSize: 22,
  },
  informationContainer: {
    width: "90%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: 10,
    padding: 10,
    marginBottom: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  line: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  thirthTitleContainer: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
  },
  thirthTitle: {
    fontSize: 20
  }
});