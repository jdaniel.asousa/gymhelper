import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';
import { useNavigation } from '@react-navigation/native';

import MealCard from '../mealCard/index';

import api from '../../services/api';

import styles from './styles';

export default function nutritionalEvaluationDetails(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  const navigation = useNavigation();

  let [suggestion, setSuggestion] = useState("");
  let [nutritionalPlan, setNutritionalPlan] = useState({});
  let [nutritionalPlanExists, setNutritionalPlanExists] = useState(false);
  let [meals, setMeals] = useState([]);
  let [infoLoaded, setInfoLoaded] = useState(false);

  let nutritionalPlanContainer;

  useEffect(() => {
    (async () => {
      let token = await AsyncStorage.getItem("token");
      try {
        let userId = await AsyncStorage.getItem('id');
        const response = await api.get(`api/NutritionalEvaluation/nutritionalEvaluation?request_id=${userId}&id=${props.id}`, 
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })

        setSuggestion(response.data.suggestions);

        if(response.data.nutrition_plan !== null) {
          setNutritionalPlan(response.data.nutrition_plan);
          setMeals(response.data.nutrition_plan.meals);
          setNutritionalPlanExists(true);
        }

        setInfoLoaded(true);
      } catch (error) {
        if(error.response) {
          if(error.response.status === 400) {
            logout();
          }
        }
      }
    })()
  }, []);

  async function logout() {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("id");
    await AsyncStorage.removeItem("name");
    await navigation.navigate("Login");
  }

  if(nutritionalPlan) {
    nutritionalPlanContainer = <ScrollView contentContainerStyle={styles.scroll}>
      <View style={styles.secondTitleContainer}>
        <Text style={[styles.secondTitle, styles.text, {fontFamily: "Poppins_400Regular"}]}>Plano Nutricional</Text>
      </View>
      <View style={styles.informationContainer}>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Total de calorias</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{nutritionalPlan.total_calories} Kcal</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Total de carbohidratos</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{nutritionalPlan.total_carbohydrates} Kcal</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Total de gorduras</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{nutritionalPlan.total_fats} Kcal</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Total de proteínas</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{nutritionalPlan.total_protein} Kcal</Text>
        </View>
      </View>
      <View style={styles.thirthTitleContainer}>
          <Text style={[styles.thirthTitle, styles.text, {fontFamily: "Poppins_400Regular"}]}>Refeições</Text>
      </View>
      <View>
        {meals.map((meal, index) => (
          <MealCard key={index} title={meal.description} calories={meal.calories} carbohydrates={meal.carbohydrates} fats={meal.fats} protein={meal.protein}/>
        ))}
      </View>
    </ScrollView>
  } else {
    nutritionalPlanContainer = null;
  }

  if(!fontsLoaded || !infoLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={[styles.title, {fontFamily: "Poppins_400Regular"}]}>Avaliação Nutriticional {props.number}</Text>
      </View>
      <View style={styles.description}>
        <View style={styles.descriptionDesignation}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Sugestão:</Text>
        </View>
        <View style={styles.descriptionValue}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{suggestion}</Text>
        </View>
      </View>
      {nutritionalPlanContainer}
    </View>;
  }
}