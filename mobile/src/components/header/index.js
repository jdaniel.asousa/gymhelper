import React, { useState, useEffect } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';

import Person from '../../assets/person.jpg';

import api from '../../services/api';

import styles from './styles';

function header(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  const navigation = useNavigation();

  let [userName, setUserName] = useState("");
  let [imageUri, setImageUri] = useState("");
  let [imageExists, setImageExists] = useState(false);

  let userImage;
  let userInfo;

  useEffect(() => {
    (async () => {
      let token = await AsyncStorage.getItem("token");
      try {
        let userId = await AsyncStorage.getItem('id');
        const response = await api.get(`api/user/user?request_id=${userId}&id=${userId}`, 
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })

        if(response.data.avatar === null) {
          setImageExists(false);
        } else {
          setImageUri(response.data.avatar);
          setImageExists(true);
        }
        setUserName(response.data.name);
      } catch (error) {
        if(error.response) {
          if(error.response.status === 401) {
            logout();
          }
        }
      }
    })();
  }, []);

  function navigateToProfile() {
    navigation.navigate("Profile");
  }

  async function logout() {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("id");
    await AsyncStorage.removeItem("name");
    await navigation.navigate("Login");
  }

  function getInitials() {
    var names = userName.split(' '),
        initials = names[0].substring(0, 1).toUpperCase();
      
    if (names.length > 1) {
        initials += names[names.length - 1].substring(0, 1).toUpperCase();
    }
    return initials;
  }

  if(imageExists) {
    userImage = <Image source={{uri: imageUri}} style={{width: 50, height: 50, borderRadius: 30}}/>
  } else {
    let initials = getInitials();
    userImage = <View style={{width: 50, height: 50, borderRadius: 30, backgroundColor: "#003049", justifyContent: "center", alignItems: "center"}}>
                  <Text style={{color: "#FFFFFF", fontSize: 20}}>{initials}</Text>
              </View>
  }

  if(props.showName !== undefined) {
    userInfo = null;
  } else {
    userInfo = <TouchableOpacity onPress={navigateToProfile} style={styles.user}>
                {userImage}
                <Text style={[styles.profileName, {fontFamily: "Poppins_400Regular"}]}>{userName}</Text>
              </TouchableOpacity>
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.header}>
      {userInfo}
    </View>;
  }
}

export default header;