import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  header: {
    height: 75,
    backgroundColor: "#003049",
    justifyContent: "center",
    alignItems: "center",
    zIndex: 2,
    top: 0
  },
  user: {
    height: 85,
    width: "70%",
    marginTop: "25%",
    backgroundColor: "#FFFFFF",
    borderRadius: 25,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  profileImage: {
    height: 50,
    width: 50,
    borderRadius: 50,
  },
  profileName: {
    color: "#003049",
    fontSize: 17,
  }
});