import React from 'react';
import { View, Text } from 'react-native';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';

import styles from './styles';

export default function availablePlanCard(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  let description;

  if(props.description !== null) {
    description = <View style={styles.line}>
                    <View style={styles.designationContainer}>
                      <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Descrição:</Text>
                    </View>
                    <View style={styles.valueContainer}>
                      <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.description}</Text>
                    </View>
                  </View>
  } else {
    description = null;
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={[styles.title, styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.name}</Text>
        </View>
        <View style={styles.info}>
          {description}
          <View style={styles.line}>
            <View style={styles.designationContainer}>
              <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Entradas:</Text>
            </View>
            <View style={styles.valueContainer}>
              <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.entries}</Text>
            </View>
          </View>
        </View>
        <View style={styles.priceContainer}>
          <Text style={[styles.price, styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.price}€</Text>
        </View>
    </View>;
  }
}