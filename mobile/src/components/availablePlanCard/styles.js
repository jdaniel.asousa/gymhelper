import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: 320,
    backgroundColor: "#FFFFFF",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    marginBottom: 10,
    marginRight: 10,
    marginLeft: 10,
    borderRadius: 25,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  titleContainer: {
    width: "90%",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 25,
    textAlign: "center",
    borderBottomColor: "#DCDCDC",
    borderBottomWidth: 1
  },
  info: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  line: {
    display: "flex",
    flexDirection: "row",
  },
  designationContainer: {
    paddingLeft: 15,
    width: "50%",
    justifyContent: "flex-start"
  },
  valueContainer: {
    paddingRight: 15,
    width: "50%",
    alignItems: "flex-end"
  },
  priceContainer: {
    width: "90%",
    justifyContent: "center",
    alignItems: "center",
    borderTopColor: "#DCDCDC",
    borderTopWidth: 1,
    paddingTop: 5
  },
  price: {
    fontSize: 20
  },
  text: {
    color: "#003049",
  }
});