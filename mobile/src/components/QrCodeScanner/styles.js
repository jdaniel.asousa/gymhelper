import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    height: "90%",
    width: "100%",
    marginTop: "20%",
    marginBottom: "20%",
    justifyContent: 'center',
    position: "absolute",
    zIndex: 2,
  },
  containerNormal: {
    flex: 1,
    flexDirection: "column",
    height: "60%",
    width: "100%",
    marginTop: "20%",
    marginBottom: "20%",
    justifyContent: 'center',
    alignItems: "center",
  },
  containerGlobal: {
    flex: 1,
    flexDirection: "column",
    height: "60%",
    width: "100%",
    marginTop: "20%",
    marginBottom: "20%",
    justifyContent: 'center',
  },
  buttonScan: {
    width: "40%", 
    height: "20%",
    marginTop: 20,
    backgroundColor: "#003049", 
    justifyContent: "center", 
    alignItems: "center", 
    borderRadius: 5
  },
  overlay: {
    position: 'absolute',
    marginLeft: 61,
    marginRight: 61,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    },
    unfocusedContainer: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.7)',
    },
    middleContainer: {
    flexDirection: 'row',
    flex: 1,
    },
    focusedContainer: {
    flex: 3,
    },
    sightTop: {
      height: "50%",
      width: "100%",
      justifyContent: "space-between",
      flexDirection: "row"
    },
    sightBottom: {
      flexDirection: "row",
      height: "50%",
      width: "100%",
      justifyContent: "space-between",
      alignItems: "flex-end",
    },
    sightTopLeft: {
      height: "60%",
      width: "30%",
      borderTopWidth: 5,
      borderLeftWidth: 5,
      borderTopColor: "white",
      borderLeftColor: "white",
    },
    sightTopRight: {
      height: "60%",
      width: "30%",
      borderTopWidth: 5,
      borderRightWidth: 5,
      borderTopColor: "white",
      borderRightColor: "white",
    },
    sightBottomLeft: {
      height: "60%",
      width: "30%",
      borderBottomWidth: 5,
      borderLeftWidth: 5,
      borderBottomColor: "white",
      borderLeftColor: "white",
    },
    sightBottomRight: {
      height: "60%",
      width: "30%",
      borderBottomWidth: 5,
      borderRightWidth: 5,
      borderBottomColor: "white",
      borderRightColor: "white",
    }
});