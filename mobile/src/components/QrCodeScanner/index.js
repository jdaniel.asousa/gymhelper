import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, Button, Alert, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { MaterialCommunityIcons } from '@expo/vector-icons';

import { BarCodeScanner } from 'expo-barcode-scanner';

import { useNavigation } from '@react-navigation/native';

import api from '../../services/api';

import styles from './styles';

export default function qrCodeScanner() {
  const navigation = useNavigation();

  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const [showScan, setShowScan] = useState(false);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  const handleBarCodeScanned = async ({ type, data }) => {
    setShowScan(false);

    let codeType = data.includes("MemberPlan");
    if(codeType) {
      let userId = await AsyncStorage.getItem("id");
      let token = await AsyncStorage.getItem("token");
      try {
        const response = await api.put(data, {
          request_id: userId
        }, 
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        });
        Alert.alert("Entrada registada", "Seja bem-vindo!");
      } catch(error) {
        if(error.response) {
          if(error.response.status === 400) {
            Alert.alert("Entrada inválida", "Verifique com um funcionário/rececionista do estabelecimento a sua tarifa.");
          }
          if(error.response.status === 401) {
            logout();
          }
        }
      }
    } else {
      let dataSplitted = data.split("5001/");

      navigateToMachineExercises(dataSplitted[1]);
    }
  }

  async function logout() {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("id");
    await AsyncStorage.removeItem("name");
    await navigation.navigate("Login");
  }

  function navigateToMachineExercises(param) {
    navigation.navigate("MachineExercises", {url: param});
  }

  if(hasPermission === null) {
    return <Text>Requesting for camera permission</Text>
  }
  if(hasPermission === false) {
    return <Text>No access to camera</Text>
  }

  return <View style={styles.containerGlobal}>
      {!showScan && <View style={styles.containerNormal}>
        <MaterialCommunityIcons name="qrcode-scan" size={200} color={"#DCDCDC"}/>

        <TouchableOpacity onPress={() => setShowScan(true)} style={styles.buttonScan}>
          <Text style={{fontSize: 20, color: '#FFFFFF'}}>Scan Code</Text>
        </TouchableOpacity>
      </View>}
      

      {showScan && <View style={styles.container}>
            <BarCodeScanner onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
          style={StyleSheet.absoluteFillObject}/>

          <View style={styles.overlay}>
            <View style={styles.unfocusedContainer}>

            </View>
            <View style={styles.middleContainer}>
              <View style={styles.unfocusedContainer}></View>
              <View style={styles.focusedContainer}>
                <View style={styles.sightTop}>
                  <View style={styles.sightTopLeft}></View>
                  <View style={styles.sightTopRight}></View>
                </View>
                <View style={styles.sightBottom}>
                  <View style={styles.sightBottomLeft}></View>
                  <View style={styles.sightBottomRight}></View>
                </View>
              </View>
              <View style={styles.unfocusedContainer}></View>
            </View>
            <View style={styles.unfocusedContainer}></View>
          </View>
        </View>}

    </View>;
    
}