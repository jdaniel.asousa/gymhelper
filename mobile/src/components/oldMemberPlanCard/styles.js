import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: 320,
    display: "flex",
    borderRadius: 25,
    paddingBottom: 5,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    marginBottom: 15,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  titleContainer: {
    width: "90%",
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: "#DCDCDC",
    borderBottomWidth: 1,
    paddingTop: 5,
  },
  title: {
    fontSize: 22,
  },
  info: {
    width: "100%",
    marginTop: 5,
    marginBottom: 5,
  },
  line: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5,
  },
  priceContainer: {
    width: "90%",
    justifyContent: "center",
    alignItems: "center",
    borderTopColor: "#DCDCDC",
    borderTopWidth: 1,
    paddingTop: 5,
  },
  price: {
    fontSize: 20,
  },
  text: {
    color: "#003049",
  }
});