import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "20%",
  },
  scroll: {
    display: "flex",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 230,
    zIndex: 0,
  },
  title: {
    width: "90%",
    fontSize: 25,
    color: "#003049",
    borderBottomColor: "#DCDCDC",
    borderBottomWidth: 1
  }
});