import React, { useState, useEffect } from 'react';
import { ScrollView, View, Text } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';
import { useNavigation } from '@react-navigation/native';

import AvailablePlanCard from '../availablePlanCard/index';

import api from '../../services/api';

import styles from './styles';

export default function availablePlans() {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  const navigation = useNavigation();

  let [plans, setPlans] = useState([]);
  
  useEffect(() => {
    (async () => {
      let token = await AsyncStorage.getItem("token");
      try {
        let userId = await AsyncStorage.getItem('id');
        const response = await api.get(`api/MonthlyPlan/monthly-plans?request_id=${userId}`, 
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        });

        console.log(response.data);
        setPlans(response.data);

      } catch (error) {
        if(error.response) {
          if(error.response.status === 401) {
            logout();
          }
        }
      }
    })()
  }, []);

  async function logout() {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("id");
    await AsyncStorage.removeItem("name");
    await navigation.navigate("Login");
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={[styles.title, {fontFamily: "Poppins_400Regular"}]}>Tarifas disponíveis</Text>
      </View>
      <ScrollView contentContainerStyle={styles.scroll}>
        {plans.map((plan, index) => (
          <AvailablePlanCard key={index} name={plan.name} description={plan.description} price={plan.price} entries={plan.entrys}/>
        ))}
      </ScrollView>
    </View>
  }
}