import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: "100%",
    marginTop: "20%",
    justifyContent: "center",
    alignItems: "center"
  },
  scroll: {
    width: "100%",
  },
  titleContainer: {
    paddingBottom: 10,
  },
  title: {
    width: "90%",
    fontSize: 25,
    color: "#003049",
    borderBottomColor: "#DCDCDC",
    borderBottomWidth: 1
  }
});