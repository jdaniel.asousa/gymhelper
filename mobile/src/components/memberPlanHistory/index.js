import React from 'react';
import { View, ScrollView, Text } from 'react-native';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';

import OldMemberPlanCard from '../oldMemberPlanCard/index';

import styles from './styles';

export default function memberPlanHistory(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={[styles.title, {fontFamily: "Poppins_400Regular"}]}>Histórico de tarifas</Text>
      </View>
      <ScrollView contentContainerStyle={styles.scroll}>
        {props.plans.map((plan, index) => (
          <OldMemberPlanCard key={index} name={plan.monthlyPlan.name} description={plan.monthlyPlan.description} entryDate={new Date(plan.entry_date)} expiricyDate={new Date(plan.expired_date)} entries={plan.monthlyPlan.entrys} validEntries={plan.valid_entrys} price={plan.monthlyPlan.price}/>
        ))}
      </ScrollView>
    </View>;
  }
}