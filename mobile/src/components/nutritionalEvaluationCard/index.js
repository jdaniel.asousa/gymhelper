import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';
import { useNavigation } from '@react-navigation/native';

import styles from './styles';

export default function nutritionalEvaluationCard(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  const navigation = useNavigation();

  let date = props.date.getDate().toString() + "/" + (props.date.getMonth() + 1).toString() + "/" + props.date.getUTCFullYear().toString(); 

  function navigateToNutritionalEvaluationDetails() {
    navigation.navigate("NutritionalEvaluationDetails", {planNumber: props.number, planId: props.id});
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.card}>
    <View style={styles.textCard}>
      <TouchableOpacity onPress={navigateToNutritionalEvaluationDetails}>
        <Text style={[styles.title, styles.text, {fontFamily: "Poppins_400Regular"}]}>Avaliação Nutricional {props.number}</Text>
      </TouchableOpacity>
      <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Marcada para: {date}</Text>
    </View>
    <View style={[styles.colorCard, {backgroundColor: props.color}]}></View>
  </View>;
  }
}