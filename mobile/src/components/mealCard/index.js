import React from 'react';
import { View, Text } from 'react-native';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';

import styles from './styles';

export default function mealCard(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
      <View style={styles.cardHeader}>
        <Text style={[styles.title, {fontFamily: "Poppins_400Regular"}]}>{props.title}</Text>
      </View>
      <View style={styles.cardInformation}>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Calorias</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.calories} Kcal</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Hidratos de Carbono</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.carbohydrates} Kcal</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Proteínas</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.fats} Kcal</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Gorduras</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.protein} Kcal</Text>
        </View>
      </View>
    </View>;
  }
}
