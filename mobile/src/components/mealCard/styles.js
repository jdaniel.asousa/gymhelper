import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: 324,
    display: "flex",
    borderRadius: 25,
    paddingBottom: 5,
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    marginBottom: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  cardHeader: {
    height: 75,
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    backgroundColor: "#003049"
  },
  cardInformation: {
    marginTop: 5,
    marginBottom: 10,
    paddingBottom: 10,
    width: "90%",
  },
  line: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  title: {
    color: "#FFFFFF",
    fontSize: 20,
    textAlign: "center",
  },
  text: {
    color: "#003049",
    fontSize: 15
  }
});