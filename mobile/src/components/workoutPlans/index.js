import React, { useState, useEffect } from 'react';
import { ScrollView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';

import WorkoutCard from '../workoutCard/index';

import api from '../../services/api';

import styles from './styles';

export default function workoutPlans() {
  let [plans, setPlans] = useState([]);

  const navigation = useNavigation();

  useEffect(() => {
    (async () => {

      try {
        let userId = await AsyncStorage.getItem('id');
        let token = await AsyncStorage.getItem('token');
        const response = await api.get(`api/TrainingPlan/training-plan-user?request_id=${userId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })
        
        setPlans(response.data);
      } catch (error) {
        if(error.response) {
          if(error.response.status === 401) {
            logout();
          }
        }
      }
    })();
  }, [])

  async function logout() {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("id");
    await AsyncStorage.removeItem("name");
    await navigation.navigate("Login");
  }


  return <ScrollView contentContainerStyle={styles.container}>
    {plans.map((plan, index) => (
      <WorkoutCard key={plan.id} number={index+1} id={plan.id} exercises={plan.training_exercises} color="#003049"/>
    ))}
  </ScrollView>;
}
