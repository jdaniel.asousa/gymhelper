import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: "100%",
    marginTop: "20%",
    display: "flex",
    alignItems: "center",
    paddingBottom: 150
  }
});