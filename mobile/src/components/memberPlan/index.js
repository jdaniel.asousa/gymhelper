import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

import MemberPlanCard from '../memberPlanCard/index';

import api from '../../services/api';

import styles from './styles';

export default function memberPlan() {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  const navigation = useNavigation();

  let [currentDate, setCurrentDate] = useState(new Date());
  let currentPlanIndex = 0;
  let [oldPlans, setOldPlans] = useState([]);
  let [planValid, setPlanValid] = useState(false);
  let [planName, setPlanName] = useState("");
  let [planDescription, setPlanDescription] = useState("");
  let [planExpirityDate, setPlanExpirityDate] = useState(new Date());
  let [planEntryDate, setPlanEntryDate] = useState(new Date());
  let [planEntries, setPlanEntries] = useState("");
  let [planValidEntries, setPlanValidEntries] = useState("");
  let [planPrice, setPlanPrice] = useState("");
  let [infoLoaded, setInfoLoaded] = useState(false);

  let container;
  let historyButton;

  useEffect(() => {
    (async () => {
      let token = await AsyncStorage.getItem("token");
      try {
        let userId = await AsyncStorage.getItem('id');
        const response = await api.get(`api/user/user?request_id=${userId}&id=${userId}`, 
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })

        //console.log(response.data.athlete_info.member_plans);
        if(response.data.athlete_info.member_plans.length === 0) {
          setPlanValid(false);
        } else if(response.data.athlete_info.member_plans.length === 1){
          response.data.athlete_info.member_plans.map((plan, index) => {
            let tmp1 = new Date(plan.expired_date);
            let tmp2 = new Date(plan.entry_date);
            let tmp3 = new Date();

            let expiredDate = tmp1.getTime();
            let entryDate = tmp2.getTime();
            let today = tmp3.getTime();
            if(expiredDate > today && entryDate < today) {
              setCurrentDate(plan);
              setPlanValid(true);
              setPlanName(plan.monthlyPlan.name);
              setPlanPrice(plan.monthlyPlan.price);
              setPlanDescription(plan.monthlyPlan.description);
              setPlanExpirityDate(new Date(plan.expired_date));
              setPlanEntryDate(new Date(plan.entry_date));
              setPlanEntries(plan.monthlyPlan.entrys);
              setPlanValidEntries(plan.valid_entrys);
              return;
            }
          });
        } else {
          response.data.athlete_info.member_plans.map((plan, index) => {
            let tmp1 = new Date(plan.expired_date);
            let tmp2 = new Date(plan.entry_date);
            let tmp3 = new Date();

            let expiredDate = tmp1.getTime();
            let entryDate = tmp2.getTime();
            let today = tmp3.getTime();
            if(expiredDate > today && entryDate < today) {
              console.log("AQUIIII ", index);
              setCurrentDate(plan);
              setPlanValid(true);
              setPlanName(plan.monthlyPlan.name);
              setPlanPrice(plan.monthlyPlan.price);
              setPlanDescription(plan.monthlyPlan.description);
              setPlanExpirityDate(new Date(plan.expired_date));
              setPlanEntryDate(new Date(plan.entry_date));
              setPlanEntries(plan.monthlyPlan.entrys);
              setPlanValidEntries(plan.valid_entrys);
              currentPlanIndex = index;
              return;
            }
          });

          response.data.athlete_info.member_plans.splice(0, 1);
          let otherPlans = response.data.athlete_info.member_plans;
          setOldPlans(otherPlans);
        }

        setInfoLoaded(true);
      } catch (error) {
        if(error.response) {
          if(error.response.status === 401) {
            logout();
          }
        }
      }
    })();
  }, []);

  async function logout() {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("id");
    await AsyncStorage.removeItem("name");
    await navigation.navigate("Login");
  }

  function navigateToAvailablePlans() {
    navigation.navigate("AvailablePlans");
  }

  function navigateToMemberPlanHistory() {
    navigation.navigate("MemberPlanHistory", {plans: oldPlans});
  }

  if(planValid) {
    container = <View style={styles.planContainer}>
      <View style={styles.titleContainer}>
        <Text style={[styles.title, {fontFamily: "Poppins_400Regular"}]}>Tarifa Ativa</Text>
      </View>
      <MemberPlanCard name={planName} description={planDescription} entryDate={planEntryDate} expiricyDate={planExpirityDate} entries={planEntries} validEntries={planValidEntries} price={planPrice}/>
    </View>
  } else {
    container = <View style={styles.messageContainer}>
      <MaterialCommunityIcons name="credit-card-off-outline" size={200} color="#DCDCDC" />
      <Text style={[styles.message, {fontFamily: "Poppins_400Regular"}]}>Não possui nenhum plano ativo neste momento.</Text>
    </View>
  }

  if(oldPlans.length > 0) {
    historyButton = <TouchableOpacity onPress={navigateToMemberPlanHistory} style={styles.button}>
                      <Text style={[styles.textButton, {fontFamily: "Poppins_400Regular"}]}>Histórico</Text>
                    </TouchableOpacity>
  } else {
    historyButton = null;
  }

  if(!infoLoaded || !fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
      {container}
      {historyButton}
      <TouchableOpacity onPress={navigateToAvailablePlans} style={styles.button}>
        <Text style={[styles.textButton, {fontFamily: "Poppins_400Regular"}]}>Tarifas disponíveis</Text>
      </TouchableOpacity>
    </View>
  }
}