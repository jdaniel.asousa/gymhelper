import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: "100%",
    height: "60%",
    marginTop: "20%",
    justifyContent: "center",
    alignItems: "center"
  },
  messageContainer: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  message: {
    width: "70%",
    fontSize: 20,
    textAlign: "center",
    color: "#003049",
  },
  button: {
    marginTop: 10, 
    width: "50%", 
    height: 50, 
    backgroundColor: "#003049", 
    justifyContent: "center", 
    alignItems: "center", 
    borderRadius: 5,
  },
  textButton: {
    color: "#FFFFFF",
    fontSize: 15
  },
  planContainer: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  titleContainer: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    fontSize: 25,
    color: "#003049"
  }
});