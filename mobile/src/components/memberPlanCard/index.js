import React from 'react';
import { View, Text } from 'react-native';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';

import styles from './styles';

export default function memberPlanCard(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  let description;
  let expiricyDate = props.expiricyDate.getDate().toString() + "/" + (props.expiricyDate.getMonth() + 1).toString() + "/" + props.expiricyDate.getUTCFullYear().toString(); 
  let entryDate = props.entryDate.getDate().toString() + "/" + (props.entryDate.getMonth() + 1).toString() + "/" + props.entryDate.getUTCFullYear().toString();

  if(props.description !== null) {
    description = <View style={styles.line}>
                    <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Descrição:</Text>
                    <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.description}</Text>
                  </View>
  } else {
    description = null;
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={[styles.title, styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.name}</Text>
      </View>
      <View style={styles.info}>
        {description}
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Data de Início:</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{entryDate}</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Data de expiração:</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{expiricyDate}</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Acessos:</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.entries}</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Acessos restantes:</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.validEntries}</Text>
        </View>
      </View>
      <View style={styles.priceContainer}>
        <Text style={[styles.price, styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.price}€</Text>
      </View>
    </View>;
  }
}