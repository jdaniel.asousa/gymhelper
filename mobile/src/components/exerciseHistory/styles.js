import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    marginTop: "30%",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  headerContainer: {
    display: "flex",
    flexDirection: "row",
    width: "90%",
    borderBottomColor: "#DCDCDC",
    borderBottomWidth: 1,
  },
  iconContainer: {
    width: "20%",
    justifyContent: "center",
    alignItems: "center",
  },
  titleContainer: {
    width: "80%",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    marginRight: 50,
    fontSize: 25,
    textAlign: "center"
  },
  text: {
    color: "#003049",
  },
  scroll: {
    width: "100%", 
    justifyContent: "center", 
    alignItems: "center", 
    paddingBottom: 300 
  },
  infoContainer: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  }
});