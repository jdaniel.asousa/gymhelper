import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';
import { useNavigation } from '@react-navigation/native';
import {Picker} from '@react-native-community/picker';

import HistoryCard from '../historyCard/index';

import styles from './styles';

import api from '../../services/api';

export default function exerciseHistory() {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  let [names, setNames] = useState([]);
  let [exerciceName, setExerciseName] = useState("Supino Reto");
  let [history, setHistory] = useState([]);
  let [infoLoaded, setInfoLoaded] = useState(false);

  const navigation = useNavigation();

  let historyContainer;

  useEffect(() => {
    (async () => {
      let token = await AsyncStorage.getItem("token");
      try {
        let userId = await AsyncStorage.getItem('id');
        const response = await api.get(`api/TrainingExerciseHistory/training-exercise-history-user?request_id=${userId}`, 
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })

        let tmp = [];
        let namesTmp = [];

        response.data.map(registration => {
          if(!namesTmp.includes(registration.training_exercise.exercise.name)) {
            namesTmp.push(registration.training_exercise.exercise.name);
          }

          if(registration.training_exercise.exercise.name === exerciceName) {
            tmp.push(registration);
          }
        });

        setHistory(tmp);
        setNames(namesTmp);
      } catch (error) {
        console.log(error);
        if(error.response) {
          if(error.response.status === 401) {
            logout();
          }
        }
      }
    })()
  }, [exerciceName]);

  function goBack() {
    navigation.goBack();
  }

  async function logout() {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("id");
    await AsyncStorage.removeItem("name");
    await navigation.navigate("Login");
  }

  function mostrar() {
    console.log(names);
  }

  if(history.length > 0) {
    if(history[0].time === null) {
      historyContainer = <ScrollView contentContainerStyle={styles.scroll}>
        {history.map((registration, index) => (
          <HistoryCard key={index} repetitions={registration.repetitions} objective={registration.training_exercise.repetitions} date={new Date(registration.created_at)}/>
        ))}
      </ScrollView>
    } else {
      historyContainer = <ScrollView contentContainerStyle={{width: "100%", justifyContent: "center", alignItems: "center", paddingBottom: 1000}}>
      {history.map((registration, index) => (
        <HistoryCard  key={index} time={registration.time} objective={registration.training_exercise.time} date={new Date(registration.created_at)}/>
      ))}
    </ScrollView>
    }
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
      <View style={styles.headerContainer}>
        <View style={styles.iconContainer}>
          <TouchableOpacity onPress={goBack}>
            <FontAwesome5 name="arrow-left" style={styles.icon} size={24} color="#003049" />
          </TouchableOpacity>
        </View>
        <View style={styles.titleContainer}>
          <Text style={[styles.title, styles.text, {fontFamily: "Poppins_400Regular"}]}>Histórico de exercícios</Text>
        </View>
      </View>
      <View style={styles.infoContainer}>
        <Picker selectedValue={exerciceName}
            onValueChange={value => setExerciseName(value)}
            mode="dropdown"
            style={{ width:  160}}>
          {names.map((name, index) => (
            <Picker.Item key={index} label={name} value={name} />
          ))}
        </Picker>
        {historyContainer}
      </View>
    </View>;
  }
}