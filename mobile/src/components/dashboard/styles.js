import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    marginTop: "15%",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  titleContainer: {
    width: "90%",
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: "#DCDCDC",
    borderBottomWidth: 1,
    paddingBottom: 10,
  },
  title: {
    fontSize: 25,
  },
  text: {
    color: "#003049",
  },
  chartContainer: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  chartTitle: {
    fontSize: 18
  },
  scroll: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 200,
  },
  button: {
    width: "45%", 
    height: 60, 
    backgroundColor: "#003049", 
    justifyContent: "center", 
    alignItems: "center", 
    borderRadius: 5,
    marginBottom: 10,
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 17,
    textAlign: "center",
  }
});