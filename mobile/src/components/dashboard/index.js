import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import {Picker} from '@react-native-community/picker';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { LineChart } from 'expo-chart-kit';
import { useNavigation } from '@react-navigation/native';

import api from '../../services/api';

import styles from './styles';

export default function dashboard() {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  const navigation = useNavigation();

  let [dates, setDates] = useState([]);
  let [weights, setWeights] = useState([]);
  let [infoLoaded, setInfoLoaded] = useState(false);
  let [year, setYear] = useState(2020);
  let [allYears, setAllYears] = useState([]);

  let chartContainer;

  useEffect(() => {
    (async () => {
      setAllYears([]);
      let token = await AsyncStorage.getItem("token");
      try {
        let userId = await AsyncStorage.getItem('id');
        const response = await api.get(`api/user/user?request_id=${userId}&id=${userId}`, 
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })

        let tmpDates = [];
        let tmpWeights = [];
        let allYearsTmp = [];

        response.data.athlete_info.physical_evaluations.map((evaluation) => {
          if(allYearsTmp.includes(new Date(evaluation.created_at).getUTCFullYear()) === false) {
            allYearsTmp.unshift(new Date(evaluation.created_at).getUTCFullYear());
          }
          if(new Date(evaluation.created_at).getUTCFullYear() === year) {
            let formatDate = new Date(evaluation.created_at).getDate().toString() + "/" + (new Date(evaluation.created_at).getMonth() + 1).toString();
            tmpDates.unshift(formatDate);
            tmpWeights.unshift(evaluation.weight);
          }
        });

        setDates(tmpDates);
        setWeights(tmpWeights);
        setAllYears(allYearsTmp);

        setInfoLoaded(true);
      } catch (error) {
        if(error.response) {
          if(error.response.status === 401) {
            logout();
          }
        }
      }
    })()
  }, [year]);

  async function logout() {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("id");
    await AsyncStorage.removeItem("name");
    await navigation.navigate("Login");
  }

  if(allYears.length === 0) {
    chartContainer = null;
  } else {
    chartContainer = <View style={styles.chartContainer}>
      <Picker selectedValue={year}
          onValueChange={value => setYear(value)}
          mode="dropdown"
          style={{ width:  160}}>
        {allYears.map((year, index) => (
          <Picker.Item key={index} label={year.toString()} value={year} />
        ))}
      </Picker>
      <Text style={[styles.text, styles.chartTitle, {fontFamily: "Poppins_400Regular"}]}>Peso (Kg) durante {year}</Text>
      <LineChart
        data={{
          labels: dates,
          datasets: [{
            data: weights
          }]
        }}
        width={350} // from react-native
        height={220}
        chartConfig={{
          backgroundColor: '#003049',
          backgroundGradientFrom: '#003049',
          backgroundGradientTo: '#003049',
          decimalPlaces: 2, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 16
          }
        }}
        bezier
        style={{
          marginVertical: 8,
          borderRadius: 16
        }}
      />
      <TouchableOpacity onPress={navigateToExerciseHistory} style={styles.button}>
        <Text style={[styles.buttonText, {fontFamily: "Poppins_400Regular"}]}>Histórico de Exercícios</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={navigateToPhysicalEvaluations} style={styles.button}>
        <Text style={[styles.buttonText, {fontFamily: "Poppins_400Regular"}]}>Avaliações Físicas</Text>
      </TouchableOpacity>
    </View>
  }

  function navigateToExerciseHistory() {
    navigation.navigate("ExerciseHistory");
  }

  function navigateToPhysicalEvaluations() {
    navigation.navigate("PhysicalEvaluations");
  }

  if(!fontsLoaded || !infoLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={[styles.title, styles.text, {fontFamily: "Poppins_400Regular"}]}>Dashboard</Text>
      </View>
      <ScrollView contentContainerStyle={styles.scroll}>
        {chartContainer}
      </ScrollView>
    </View>;
  }
}