import React from 'react';
import { View, Text, TouchableOpacity, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {useFonts, Poppins_400Regular, Poppins_300Light} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';

import styles from './styles';

export default function workoutCard(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
    Poppins_300Light
  });

  const navigation = useNavigation();

  function navigateToDetails() {
    navigation.navigate('WorkoutPlanDetails', {workoutNumber: props.number, workoutId: props.id});
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.card}>
      <View style={styles.textCard}>
        <TouchableOpacity onPress={navigateToDetails}>
          <Text style={[styles.title, styles.text, {fontFamily: "Poppins_400Regular"}]}>Plano de Treino {props.number}</Text>
        </TouchableOpacity>
      </View>
      <View style={[styles.colorCard, {backgroundColor: props.color}]}></View>
    </View>;
  }
}
