import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  cardContainer: {
    width: "90%",
    display: "flex",
    flexDirection: "row",
    borderRadius: 25,
    backgroundColor: "#FFFFFF",
    marginBottom: 15,
    marginTop: 15,
    justifyContent: "space-between",
    paddingLeft: 15,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  cardInformation: {
    width: "50%"
  },
  cardValues: {
    width: "50%",
    marginLeft: 2
  },
  doExerciseButton: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
  },
  arrowIcon: {
    marginTop: 7
  },
  text: {
    color: "#003049"
  },
});