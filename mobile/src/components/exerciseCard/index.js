import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';

import { AntDesign } from '@expo/vector-icons';

import { useNavigation } from '@react-navigation/native';

import styles from './styles';

export default function exerciseCard(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  const navigation = useNavigation();

  function navigateToDoingExercise() {
    if(props.time !== null) {
      navigation.navigate("DoingExercise",  {exerciseId: props.code, time: props.time, circuit: props.circuit, name: props.name, group: props.group, sets: props.sets, machine: props.machine});
    } else {
      navigation.navigate("DoingExercise",  { exerciseId: props.code, repetitions: props.repetitions, circuit: props.circuit, name: props.name, group: props.group, sets: props.sets, machine: props.machine});
    }
  }

  let typeExercise;
  let typeExerciseValue;
  let machine;
  let machineValue;

  let typeEx = props.type;

  if (props.machine !== null) {
    machine = <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Máquina</Text>
    machineValue = <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.machine.name}</Text>
  } else {
    machine = <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Máquina</Text>
    machineValue = <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Não necessita</Text>
  }

  if(typeEx === "Reps") {
    if(props.repetitions !== null) {  
      typeExercise = <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Repetitions</Text>
      typeExerciseValue = <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.repetitions}</Text>
    } else {
      typeExercise = <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Repetitions</Text>
      typeExerciseValue = <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Não definido</Text>
    }
  } else if(typeEx === "Time"){
    if(props.time !== null) {
      typeExercise = <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Tempo</Text>
      typeExerciseValue = <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.time}</Text>
    } else {
      typeExercise = <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Tempo</Text>
      typeExerciseValue = <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Não definido</Text>
    }
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.cardContainer}>
      <View style={styles.cardInformation}>
        <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Circuito</Text>
        <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Nome</Text>
        <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Grupo Muscular</Text>
        {machine}
        <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Séries</Text>
        {typeExercise}
      </View>
      <View style={styles.cardValues}>
        <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.circuit}</Text>
        <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.name}</Text>
        <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.group}</Text>
        {machineValue}
        <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{props.sets}</Text>
        {typeExerciseValue}
      </View>
    </View>;
  }
}