import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';
import { useNavigation } from '@react-navigation/native';
import { FontAwesome5 } from '@expo/vector-icons';

import MealCard from '../mealCard/index';

import api from '../../services/api';

import styles from './styles';
import { set } from 'react-native-reanimated';

export default function physicalEvaluationDetails(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  const navigation = useNavigation();

  let [evaluation, setEvaluation] = useState({});
  let [description, setDescription] = useState("");
  let [infoLoaded, setInfoLoaded] = useState(false);

  let evaluationContainer;

  useEffect(() => {
    (async () => {
      let token = await AsyncStorage.getItem("token");
      try {
        let userId = await AsyncStorage.getItem('id');
        const response = await api.get(`api/PhysicalEvaluation/physical-evaluation?request_id=${userId}&id=${props.id}`, 
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })

        setEvaluation(response.data);
        setDescription(response.data.description);

        setInfoLoaded(true);
      } catch (error) {
        if(error.response) {
          if(error.response.status === 401) {
            logout();
          }
        }
      }
    })()
  }, []);

  async function logout() {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("id");
    await AsyncStorage.removeItem("name");
    await navigation.navigate("Login");
  }

  function goBack() {
    navigation.goBack();
  }

  if(evaluation) {
    evaluationContainer = <ScrollView contentContainerStyle={styles.scroll}>
      <View style={styles.informationContainer}>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Peso</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{evaluation.weight} Kg</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Altura</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{evaluation.height} m</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>IMC</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{evaluation.imc}</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Água</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{evaluation.body_water}%</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Gordura Corporal</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{evaluation.body_fat}%</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Massa Muscular</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{evaluation.muscle_mass}%</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Costas</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{evaluation.lat_size} cm</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Bicep Esquerdo</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{evaluation.left_bicep_size} cm</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Bicep Direito</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{evaluation.right_bicep_size} cm</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Peito</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{evaluation.chest_size} cm</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Cintura</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{evaluation.waist_size} cm</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Glúteo</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{evaluation.glute_size} cm</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Quadril</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{evaluation.hip_size} cm</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Coxa</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{evaluation.thigh_size} cm</Text>
        </View>
        <View style={styles.line}>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Gémeos</Text>
          <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{evaluation.calfs_size} cm</Text>
        </View>
      </View>
    </ScrollView>
  } else {
    evaluationContainer = null;
  }

  if(!fontsLoaded || !infoLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
    <View style={styles.titleContainer}>
      <View style={styles.arrowBack}>
        <TouchableOpacity onPress={goBack}>
          <FontAwesome5 name="arrow-left" size={24} color="#003049" />
        </TouchableOpacity>
      </View>
      <View style={styles.titleTextContainer}>
        <Text style={[styles.title, {fontFamily: "Poppins_400Regular"}]}>Avaliação Física {props.number}</Text>
      </View>
    </View>
    <View style={styles.description}>
      <View style={styles.descriptionDesignation}>
        <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>Descrição:</Text>
      </View>
      <View style={styles.descriptionValue}>
        <Text style={[styles.text, {fontFamily: "Poppins_400Regular"}]}>{description}</Text>
      </View>
    </View>
    {evaluationContainer}
  </View>;
  }
}
