import React, { useState, useEffect } from 'react';
import { View, ScrollView, Text } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';
import { useNavigation } from '@react-navigation/native';

import NutritionalEvaluationCard from '../nutritionalEvaluationCard/index'; 

import api from '../../services/api';

import MealCard from '../mealCard/index';

import styles from './styles';

export default function nutritionPlans() {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  const navigation = useNavigation();

  let [nutritionalEvaluations, setNutritionalEvaluations] = useState([]);

  useEffect(() => {
    (async () => {
      let token = await AsyncStorage.getItem("token");
      try {
        let userId = await AsyncStorage.getItem('id');
        const response = await api.get(`api/user/user?request_id=${userId}&id=${userId}`, 
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })
        
        setNutritionalEvaluations(response.data.athlete_info.nutritional_evaluations);
      } catch (error) {
        if(error.response) {
          if(error.response.status === 401) {
            logout();
          }
        }
      }
    })()
  }, []);

  async function logout() {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("id");
    await AsyncStorage.removeItem("name");
    await navigation.navigate("Login");
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={[styles.title, {fontFamily: "Poppins_400Regular"}]}>Avaliações Nutricionais</Text>
      </View>
      <ScrollView contentContainerStyle={styles.scroll}>
        {nutritionalEvaluations.map((plan, index) => (
          <NutritionalEvaluationCard key={index} number={index+1} id={plan.id} date={new Date(plan.schedule)} color={"#003049"}/>
        ))}
      </ScrollView> 
    </View>
  }
}