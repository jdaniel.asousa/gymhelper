import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: "100%",
    marginTop: "20%",
    justifyContent: "center",
    alignItems: "center",
  },
  scroll: {
    width: "100%",
    marginTop: 10,
    display: "flex",
    alignItems: "center",
    paddingBottom: 150
  },
  titleContainer: {
    width: "100%",
    borderBottomColor: "#DCDCDC",
    borderBottomWidth: 1,
    paddingBottom: 5,
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    fontSize: 25,
    color: "#003049"
  }
});