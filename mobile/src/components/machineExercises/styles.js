import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    marginTop: "20%",
    justifyContent: "center",
    alignItems: "center",
  },
  machineInfo: {
    width: "90%",
    display: "flex",
    paddingBottom: 5,
    borderBottomWidth: 1,
    borderBottomColor: "#DCDCDC",
  },
  titleContainer: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10
  },
  description: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly"
  },
  exercises: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 220
  },
  title: {
    fontSize: 25,
    color: "#003049"
  },
  exercisesTitleContainer: {
    width: "90%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: "#DCDCDC",
    borderBottomWidth: 1,
    paddingBottom: 5,
  },
  exercisesTitle: {
    fontSize: 20,
    color: "#003049"
  },
  machineImages: {
    width: 300,
    height: 200,
    borderRadius: 10,
    marginRight: 20
  }
});