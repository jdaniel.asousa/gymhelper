import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import { AppLoading } from 'expo';
import { useNavigation } from '@react-navigation/native';

import MachineExerciseCard from '../machineExerciseCard/index';

import api from '../../services/api';

import styles from './styles';

export default function machineExercises(props) {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
  });

  const navigation = useNavigation();

  let [machineName, setMachineName] = useState("");
  let [machineState, setMachineState] = useState(true);
  let [machineImages, setMachineImages] = useState([]);
  let [machineExercises, setMachineExercises] = useState([]);
  let [exerciseImages, setExerciseImages] = useState([]);

  let machineStateText;
  let machineImagesContainer;
  let machineExercisesContainer;

  useEffect(() => {
    (async () => {
      let userId = await AsyncStorage.getItem("id");
      let token = await AsyncStorage.getItem("token");
      let urlSplitted = props.url.split("?");
      try { 
        const response = await api.get(`${urlSplitted[0]}?request_id=${userId}&${urlSplitted[1]}`, 
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        });

        setMachineName(response.data[0].machine.name);
        setMachineState(response.data[0].machine.state);
        setMachineImages(response.data[0].machine.machine_images);
        setMachineExercises(response.data);
        setExerciseImages(response.data[0].exercise_images);
      } catch(error) {
        if(error.response) {
          if(error.response.status === 401) {
            logout();
          }
        }
      }
    })()
  }, []);

  async function logout() {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("id");
    await AsyncStorage.removeItem("name");
    await navigation.navigate("Login");
  }

  if(machineState) {
    machineStateText = <Text style={{color: "#003049"}}>Operacional</Text>
  } else {
    machineStateText = <Text style={{color: "#003049"}}>Não operacional</Text>
  }

  if(machineImages !== null) {
    machineImagesContainer = <ScrollView horizontal
                pagingEnabled
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{justifyContent: "center", alignItems: "center", marginLeft: 20, paddingRight: 20, marginBottom: 10}}>
                  {machineImages.map((image, index) => (
                    <Image source={{uri: image.path}} style={styles.machineImages}/>
                  ))}
      </ScrollView>
  } else {
    machineImagesContainer = null;
  }

  if(machineExercises.length > 0) {
    machineExercisesContainer = <ScrollView contentContainerStyle={styles.exercises}>
                                    {machineExercises.map((exercise, index) => (
                                      <MachineExerciseCard key={index} exerciseVideo={exercise.video} exerciseImages={exercise.exercise_images} name={exercise.name} group={exercise.muscle} type={exercise.type} description={exercise.description}/>
                                    ))}
                                  </ScrollView>
  } else {
    machineExercisesContainer = null;
  }

  if(!fontsLoaded) {
    return <AppLoading/>
  } else {
    return <View style={styles.container}>
      <View style={styles.machineInfo}>
        <View style={styles.titleContainer}>
          <Text style={[styles.title, {fontFamily: "Poppins_400Regular"}]}>{machineName}</Text>
        </View>
        <View style={styles.description}>
          <Text style={{fontFamily: "Poppins_400Regular", color: "#003049"}}>Estado:</Text>
          {machineStateText}
        </View>
        {machineImagesContainer}
      </View>
      <View style={styles.exercisesTitleContainer}>
        <Text style={[styles.exercisesTitle, {fontFamily: "Poppins_400Regular"}]}>Exercícios:</Text>
      </View>
      {machineExercisesContainer}
    </View>;
  }
}
