﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Helpers
{
    /// <summary>
    /// App Settings Entity
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// Secret
        /// </summary>
        public string Secret { get; set; }
        /// <summary>
        /// Email From
        /// </summary>
        public string EmailFrom { get; set; }
        /// <summary>
        /// Smtp Host
        /// </summary>
        public string SmtpHost { get; set; }
        /// <summary>
        /// Smtp Port
        /// </summary>
        public int SmtpPort { get; set; }
        /// <summary>
        /// Smtp User
        /// </summary>
        public string SmtpUser { get; set; }
        /// <summary>
        /// Smtp Pass
        /// </summary>
        public string SmtpPass { get; set; }
        /// <summary>
        /// ImgBBKey
        /// </summary>
        public string ImgBBKey { get; set; }
        /// <summary>
        /// ImgBBPostUrl
        /// </summary>
        public string ImgBBPostUrl { get; set; }
        /// <summary>
        /// QRCodeURL
        /// </summary>
        public string QRCodeURL { get; set; }

    }
}
