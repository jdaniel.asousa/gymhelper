﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Globalization;

namespace backend.Helpers
{
    /// <summary>
    /// App Exception
    /// </summary>
    public class AppException : Exception
    {
        /// <summary>
        /// Empty App Exception Constructor
        /// </summary>
        public AppException() : base() { }
        /// <summary>
        /// App Exception Constructor with message
        /// </summary>
        /// <param name="message">Message</param>
        public AppException(string message) : base(message) { }
        /// <summary>
        /// App Exception Constructor with message and args
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="args">Args</param>
        public AppException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args)) { }
    }
}
