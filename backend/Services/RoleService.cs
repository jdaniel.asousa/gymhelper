﻿using backend.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using backend.Entities;
using MySqlConnector;
/// <summary>
/// Role Service Interface
/// </summary>
public interface IRoleService
{
    /// <summary>
    /// Generate Roles Method
    /// </summary>
    void GenerateRoles();
    /// <summary>
    /// Verify Roles already on database
    /// </summary>
    /// <returns>Boolean value that represents if roles on database are already valid</returns>
    Boolean VerifyRoles();
}

namespace backend.Services
{
    /// <summary>
    /// Role Service
    /// </summary>
    public class RoleService : IRoleService
    {
        private IConfiguration _config;
        private AppDbContext _appDbContext;
        private string[] roles = { "admin", "athlete", "receptionist", "personalTrainer", "nutritionist" };
        /// <summary>
        /// Role service Constructor
        /// </summary>
        /// <param name="config">Config</param>
        /// <param name="context">Context</param>
        /// <param name="emailService">Email Service</param>
        public RoleService(IConfiguration config, AppDbContext context, IEmailService emailService)
        {
            _config = config;
            _appDbContext = context;
        }
        /// <summary>
        /// Generate Roles Method
        /// </summary>
        public void GenerateRoles()
        {
            _appDbContext.Database.ExecuteSqlRaw("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE table roles; SET FOREIGN_KEY_CHECKS = 1; ");

            foreach (string role in roles)
            {
                _appDbContext.roles.Add(new Role { name = role }) ;
            }
            _appDbContext.SaveChanges();
        }
        /// <summary>
        /// Verify Roles already on database
        /// </summary>
        /// <returns>Boolean value that represents if roles on database are already valid</returns>
        public Boolean VerifyRoles()
        {
            foreach (string role in roles)
            {
                if (_appDbContext.roles.SingleOrDefault(x => x.name == role) == null)
                    return false;
            }
            return true;
        }
    }
}
