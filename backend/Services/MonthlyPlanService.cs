﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Entities;
using backend.Models;
using backend.Models.MonthlyPlans;
using Microsoft.Extensions.Configuration;

/// <summary>
/// Monthly Plan Service Interface 
/// </summary>
public interface IMonthlyPlanService
{
    /// <summary>
    /// Create Monthly Plan
    /// </summary>
    /// <param name="createMonthlyPlanRequest"> Create Monthly Plan Request Model</param>
    /// <returns>Monthly Plan Created</returns>
    MonthlyPlan CreateMonthlyPlan(CreateMonthlyPlanRequest createMonthlyPlanRequest);
    /// <summary>
    /// Update Monthly Plan
    /// </summary>
    /// <param name="updateMonthlyPlanRequest">Update Monthly Plan Request Model</param>
    /// <returns>Monthly Plan Updated</returns>
    MonthlyPlan UpdateMonthlyPlan(UpdateMonthlyPlanRequest updateMonthlyPlanRequest);
    /// <summary>
    /// Get Monthly Plans
    /// </summary>
    /// <param name="request_id">ID of who makes the request</param>
    /// <returns>List of Monthly Plans</returns>
    List<MonthlyPlan> GetMonthlyPlans(int request_id);
    /// <summary>
    /// Get Monthly Plan by ID
    /// </summary>
    /// <param name="monthlyPlanIDRequest">Monthly Plan ID Request Model</param>
    /// <returns>A monthly plan by ID</returns>
    MonthlyPlan GetByID(MonthlyPlanIDRequest monthlyPlanIDRequest);
    /// <summary>
    /// Deletes a Monthly Plan
    /// </summary>
    /// <param name="monthlyPlanIDRequest">Monthly Plan ID Request Model</param>
    /// <returns>Deletes a monthly plan</returns>
    MonthlyPlan Delete(MonthlyPlanIDRequest monthlyPlanIDRequest);
}
namespace backend.Services
{
    /// <summary>
    /// Monthly Plan Service
    /// </summary>
    public class MonthlyPlanService : IMonthlyPlanService
    {
        private IConfiguration _config;
        private AppDbContext _appDbContext;
        /// <summary>
        /// Monthly Plan Service Constructor
        /// </summary>
        /// <param name="config">Config</param>
        /// <param name="context">Context</param>

        public MonthlyPlanService(IConfiguration config, AppDbContext context)
        {
            _config = config;
            _appDbContext = context;
        }
        /// <summary>
        /// Create Monthly Plan
        /// </summary>
        /// <param name="createMonthlyPlanRequest">Create Monthly Plan Request</param>
        /// <returns>Monthly Plan Created</returns>
        public MonthlyPlan CreateMonthlyPlan(CreateMonthlyPlanRequest createMonthlyPlanRequest)
        {
            if (createMonthlyPlanRequest.request_id < 0 || string.IsNullOrEmpty(createMonthlyPlanRequest.name.ToString()) || createMonthlyPlanRequest.price < 0 || createMonthlyPlanRequest.entrys<0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == createMonthlyPlanRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 4||requestUser.role_id == 5)
                return null;

            MonthlyPlan monthlyPlan = new MonthlyPlan { name = createMonthlyPlanRequest.name, description = createMonthlyPlanRequest.description, price = createMonthlyPlanRequest.price, entrys = createMonthlyPlanRequest.entrys };

            _appDbContext.monthly_plans.Add(monthlyPlan);
            return monthlyPlan;
        }
        /// <summary>
        /// Delete Monthly Plan
        /// </summary>
        /// <param name="monthlyPlanIDRequest">Monthly Plan ID Request</param>
        /// <returns>Monthly Plan Deleted</returns>
        public MonthlyPlan Delete(MonthlyPlanIDRequest monthlyPlanIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == monthlyPlanIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 4 || requestUser.role_id == 5)
                return null;

            MonthlyPlan monthlyPlanToRemove = _appDbContext.monthly_plans.SingleOrDefault(x => x.id == monthlyPlanIDRequest.monthly_plan_id);

            if (monthlyPlanToRemove != null)
                _appDbContext.monthly_plans.Remove(monthlyPlanToRemove);
            return monthlyPlanToRemove;
        }
        /// <summary>
        /// Get Monthly Plan by ID
        /// </summary>
        /// <param name="monthlyPlanIDRequest">Monthly Plan ID request</param>
        /// <returns>Return a monthly plan</returns>
        public MonthlyPlan GetByID(MonthlyPlanIDRequest monthlyPlanIDRequest)
        {

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == monthlyPlanIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 4 || requestUser.role_id == 5)
                return null;

            MonthlyPlan monthlyPlan = _appDbContext.monthly_plans.SingleOrDefault(x => x.id == monthlyPlanIDRequest.monthly_plan_id);

            return monthlyPlan;
        }
        /// <summary>
        /// Gets a list of Monthly Plans
        /// </summary>
        /// <param name="request_id">ID of who makes the request</param>
        /// <returns>A list of monthly plans</returns>
        public List<MonthlyPlan> GetMonthlyPlans(int request_id)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == request_id);
            if (requestUser == null || requestUser.role_id == 4 || requestUser.role_id == 5)
                return null;

            List<MonthlyPlan> monthlyPlans = _appDbContext.monthly_plans.ToList();

            return monthlyPlans;
        }
        /// <summary>
        /// Updates a Monthly Plan
        /// </summary>
        /// <param name="updateMonthlyPlanRequest">Update Monthly Plan Request Model</param>
        /// <returns>Monthly Plan Updated</returns>
        public MonthlyPlan UpdateMonthlyPlan(UpdateMonthlyPlanRequest updateMonthlyPlanRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == updateMonthlyPlanRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 4 || requestUser.role_id == 5)
                return null;

            MonthlyPlan requestMonthlyPlan = _appDbContext.monthly_plans.SingleOrDefault(x => x.id == updateMonthlyPlanRequest.monthly_plan_id);
            if (requestMonthlyPlan == null)
                return null;

            if (!string.IsNullOrEmpty(updateMonthlyPlanRequest.name))
                requestMonthlyPlan.name = updateMonthlyPlanRequest.name;
            if (!string.IsNullOrEmpty(updateMonthlyPlanRequest.description))
                requestMonthlyPlan.description = updateMonthlyPlanRequest.description;
            if (updateMonthlyPlanRequest.price > 0)
                requestMonthlyPlan.price = updateMonthlyPlanRequest.price;
            if (updateMonthlyPlanRequest.entrys > 0)
                requestMonthlyPlan.entrys = updateMonthlyPlanRequest.entrys;

            _appDbContext.monthly_plans.Update(requestMonthlyPlan);

            return requestMonthlyPlan;
        }
    }
}
