﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Helpers;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;

namespace backend.Services
{
    /// <summary>
    /// Email Service Interface
    /// </summary>
    public interface IEmailService
    {
        /// <summary>
        /// Send Method
        /// </summary>
        /// <param name="to">To</param>
        /// <param name="subject">Subject</param>
        /// <param name="html">HTML</param>
        void Send(string to, string subject, string html);
    }
    /// <summary>
    /// Email Service
    /// </summary>
    public class EmailService : IEmailService
    {
        private readonly AppSettings _appSettings;
        /// <summary>
        /// Email Service Constructor
        /// </summary>
        /// <param name="appSettings">App Settings</param>
        public EmailService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }
        /// <summary>
        /// Send Method
        /// </summary>
        /// <param name="to">To</param>
        /// <param name="subject">Subject</param>
        /// <param name="html">HTML</param>
        public void Send(string to, string subject, string html)
        {
            var email = new MimeMessage();

            email.From.Add(MailboxAddress.Parse(_appSettings.EmailFrom));
            email.To.Add(MailboxAddress.Parse(to));
            email.Subject = subject;
            email.Body = new TextPart(TextFormat.Html) { Text = html };

            using var smtp = new SmtpClient();
            smtp.Connect(_appSettings.SmtpHost, _appSettings.SmtpPort, SecureSocketOptions.SslOnConnect);
            smtp.Authenticate(_appSettings.SmtpUser, _appSettings.SmtpPass);
            smtp.Send(email);
            smtp.Disconnect(true);
        }
    }
}
