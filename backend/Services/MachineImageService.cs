﻿using backend.Entities;
using backend.Models;
using backend.Models.MachineImages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/// <summary>
/// Machine Image Service Interface
/// </summary>
public interface IMachineImageService
{
    /// <summary>
    /// Create Machine Image
    /// </summary>
    /// <param name="createMachineImageRequest">Create Machine Image Request Model</param>
    /// <returns>Machine Image Created</returns>
    Task<MachineImage> CreateMachineImage(CreateMachineImageRequest createMachineImageRequest);
    /// <summary>
    /// Update Machine Image
    /// </summary>
    /// <param name="updateMachineImageRequest">Update Machine Image Request Model</param>
    /// <returns>Machine Updated Image</returns>
    Task<MachineImage> UpdateMachineImage(UpdateMachineImageRequest updateMachineImageRequest);
    /// <summary>
    /// Get Machine Images
    /// </summary>
    /// <param name="request_id">Request ID</param>
    /// <returns>Machine Images</returns>
    List<MachineImage> GetMachineImages(int request_id);
    /// <summary>
    /// Get Machine Images By ID
    /// </summary>
    /// <param name="machineImageIDRequest">Machine Image ID Request Model</param>
    /// <returns>Machine Imagine</returns>
    MachineImage GetByID(MachineImageIDRequest machineImageIDRequest);
    /// <summary>
    /// Delete
    /// </summary>
    /// <param name="machineImageIDRequest">Machine ID Request Model</param>
    /// <returns>Machine Image Deleted</returns>
    MachineImage Delete(MachineImageIDRequest machineImageIDRequest);
}

namespace backend.Services
{
    /// <summary>
    /// Machine Image Service
    /// </summary>
    public class MachineImageService : IMachineImageService
    {
        private AppDbContext _appDbContext;
        private IImageService _imageService;
        /// <summary>
        /// Machine Image Service Constructor
        /// </summary>
        /// <param name="context">Context</param>
        /// <param name="imageService">ImageService</param>
        public MachineImageService( AppDbContext context, IImageService imageService)
        {
            _appDbContext = context;
            _imageService = imageService;
        }
        /// <summary>
        /// Create Machine Image
        /// </summary>
        /// <param name="createMachineImageRequest">Create Machine Image Request Model</param>
        /// <returns>Machine Image Created</returns>
        public async Task<MachineImage> CreateMachineImage(CreateMachineImageRequest createMachineImageRequest)
        {
            if (createMachineImageRequest.request_id <= 0 || createMachineImageRequest.image == null || createMachineImageRequest.machine_id <= 0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == createMachineImageRequest.request_id);
            if (requestUser == null || requestUser.role_id != 1)
                return null;

            Machine requestMachine = _appDbContext.machines.SingleOrDefault(x => x.id == createMachineImageRequest.machine_id);
            if (requestMachine == null )
                return null;

            ImgBBResponse imgBBResponse = await _imageService.PostPhoto(createMachineImageRequest.image);

            MachineImage machineImageToAdd = new MachineImage { path = imgBBResponse.data.display_url, machine_id = createMachineImageRequest.machine_id };

            _appDbContext.machine_images.Add(machineImageToAdd);

            return machineImageToAdd;
        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="machineImageIDRequest">Machine ID Request Model</param>
        /// <returns>Machine Image Deleted</returns>
        public MachineImage Delete(MachineImageIDRequest machineImageIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == machineImageIDRequest.request_id);
            if (requestUser == null || requestUser.role_id != 1)
                return null;

            MachineImage machineImageToRemove = _appDbContext.machine_images.SingleOrDefault(x => x.id == machineImageIDRequest.machine_image_id);

            if (machineImageToRemove != null)
                _appDbContext.machine_images.Remove(machineImageToRemove);

            return machineImageToRemove;
        }
        /// <summary>
        /// Get Machine Images By ID
        /// </summary>
        /// <param name="machineImageIDRequest">Machine Image ID Request Model</param>
        /// <returns>Machine Imagine</returns>
        public MachineImage GetByID(MachineImageIDRequest machineImageIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == machineImageIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            List<Machine> machines = _appDbContext.machines.ToList();
            MachineImage machineImage = _appDbContext.machine_images.SingleOrDefault(x => x.id == machineImageIDRequest.machine_image_id); ;

            return machineImage;
        }
        /// <summary>
        /// Get Machine Images
        /// </summary>
        /// <param name="request_id">Request ID</param>
        /// <returns>Machine Images</returns>
        public List<MachineImage> GetMachineImages(int request_id)
        {
            if (request_id <= 0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            List<Machine> machines = _appDbContext.machines.ToList();
            List<MachineImage> machineImages = _appDbContext.machine_images.ToList();

            return machineImages;
        }
        /// <summary>
        /// Update Machine Image
        /// </summary>
        /// <param name="updateMachineImageRequest">Update Machine Image Request Model</param>
        /// <returns>Machine Updated Image</returns>
        public async Task<MachineImage> UpdateMachineImage(UpdateMachineImageRequest updateMachineImageRequest)
        {
            if (updateMachineImageRequest.request_id <= 0 || updateMachineImageRequest.machine_image_id <= 0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == updateMachineImageRequest.request_id);
            if (requestUser == null || requestUser.role_id != 1)
                return null;

            MachineImage requestMachineImage = _appDbContext.machine_images.SingleOrDefault(x => x.id == updateMachineImageRequest.machine_image_id);
            if (requestMachineImage == null)
                return null;

            if (updateMachineImageRequest.image != null)
            {
                ImgBBResponse imgBBResponse = await _imageService.PostPhoto(updateMachineImageRequest.image);
                requestMachineImage.path = imgBBResponse.data.display_url;
            }
                
            if (updateMachineImageRequest.machine_id > 0)
            {
                Machine requestMachine = _appDbContext.machines.SingleOrDefault(x => x.id == updateMachineImageRequest.machine_id);
                if (requestMachine == null)
                    return null;
                else
                    requestMachineImage.machine_id = requestMachine.id;
            }

            _appDbContext.machine_images.Update(requestMachineImage);

            return requestMachineImage;
        }
    }
}
