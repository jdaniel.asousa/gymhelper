﻿using backend.Helpers;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

/// <summary>
/// QRCode Service Interface
/// </summary>
public interface IQRCodeServiceInterface
{
    /// <summary>
    /// Create
    /// </summary>
    /// <param name="requestUrl">Request Url</param>
    Task<string> Create(string requestUrl);
}

namespace backend.Services
{
    /// <summary>
    /// QRCode Service
    /// </summary>
    public class QRCodeService : IQRCodeServiceInterface
    {
        private readonly AppSettings _appSettings;
        /// <summary>
        /// QRCode Service Constructor
        /// </summary>
        /// <param name="appSettings">App Settings</param>
        public QRCodeService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }
        /// <summary>
        /// Create
        /// </summary>
        /// <param name="requestUrl">Request Url</param>
        public async Task<string> Create(string requestUrl)
        {
            HttpClient client = new HttpClient();
            string createURL = _appSettings.QRCodeURL.Replace("dataURL", requestUrl);
            HttpResponseMessage response = await client.GetAsync(createURL);
            response.EnsureSuccessStatusCode();
            var result = await response.Content.ReadAsStringAsync();

            return createURL;
        }
    }
}
