﻿using backend.Entities;
using backend.Models;
using backend.Models.TrainingExerciseHistories;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/// <summary>
/// Exercise Servise Interface
/// </summary>
public interface ITrainingExerciseHistoryService
{
    /// <summary>
    /// Create Training Exercise History and add it to database service
    /// </summary>
    /// <param name="createTrainingExerciseHistoryRequest"> Create Training Exercise History Request</param>
    /// <returns>Training Exercise History</returns>
    TrainingExercisesHistory CreateTrainingExerciseHistory(CreateTrainingExerciseHistoryRequest createTrainingExerciseHistoryRequest);
    /// <summary>
    /// Get all Training Exercise History from database service
    /// </summary>
    /// <param name="request_id">Request ID</param>
    /// <returns>all Training Exercise History</returns>
    List<TrainingExercisesHistory> GetTrainingExercisesHistories(int request_id);
    /// <summary>
    /// Get a Training Exercise History from database service
    /// </summary>
    /// <param name="trainingExerciseHistoryIDRequest">Training Exercise History ID Request</param>
    /// <returns>Training Exercise History</returns>
    TrainingExercisesHistory GetByID(TrainingExerciseHistoryIDRequest trainingExerciseHistoryIDRequest);
    /// <summary>
    /// Get all Training Exercise History of a specified user from database service
    /// </summary>
    /// <param name="request_id">Request ID</param>
    /// <returns>all Training Exercise History from the specified user ID</returns>
    List<TrainingExercisesHistory> GetByUserID(int request_id);
}

namespace backend.Services
{
    /// <summary>
    /// Training Exercise History Service
    /// </summary>
    public class TrainingExerciseHistoryService : ITrainingExerciseHistoryService
    {
        private IConfiguration _config;
        private AppDbContext _appDbContext;

        /// <summary>
        /// Exercise Service Constructor
        /// </summary>
        public TrainingExerciseHistoryService(IConfiguration config, AppDbContext context)
        {
            _config = config;
            _appDbContext = context;
        }
        /// <summary>
        /// Create Training Exercise History and add it to database service
        /// </summary>
        /// <param name="createTrainingExerciseHistoryRequest"> Create Training Exercise History Request</param>
        /// <returns>Training Exercise History</returns>
        public TrainingExercisesHistory CreateTrainingExerciseHistory(CreateTrainingExerciseHistoryRequest createTrainingExerciseHistoryRequest)
        {
            if (createTrainingExerciseHistoryRequest.request_id <= 0 || createTrainingExerciseHistoryRequest.training_exercise_id <= 0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == createTrainingExerciseHistoryRequest.request_id);
            if (requestUser == null || requestUser.role_id != 2)
                return null;

            TrainingExercise requestTrainingExercise = _appDbContext.training_exercises.SingleOrDefault(x => x.id == createTrainingExerciseHistoryRequest.training_exercise_id);
            if (requestTrainingExercise == null)
                return null;

            Exercise requestExercise = _appDbContext.exercises.SingleOrDefault(x => x.id == requestTrainingExercise.exercise_id);
            if (requestExercise == null)
                return null;

            List<TrainingPlan> trainingPlans = _appDbContext.training_plans.ToList();

            AthletesInfo requestAthletesInfo = _appDbContext.athletes_info.SingleOrDefault(x => x.id == requestTrainingExercise.plan.athlete_id);
            if (requestAthletesInfo == null)
                return null;

            if (requestAthletesInfo.user_id != createTrainingExerciseHistoryRequest.request_id)
                return null;

            if (requestExercise.type.Equals("Time"))
            {
                String[] timeSplited = createTrainingExerciseHistoryRequest.time.Split('-');

                if (timeSplited.Length != requestTrainingExercise.sets)
                    return null;
                foreach (String time in timeSplited)
                {
                    if (Int32.Parse(time) <= 0)
                        return null;
                }
            }
            if (requestExercise.type.Equals("Reps"))
            {
                String[] repsSplited = createTrainingExerciseHistoryRequest.repetitions.Split('-');

                if (repsSplited.Length != requestTrainingExercise.sets)
                    return null;
                foreach (String reps in repsSplited)
                {
                    if (Int32.Parse(reps) <= 0)
                        return null;
                }
            }

            TrainingExercisesHistory requestTrainingExercisesHistory = new TrainingExercisesHistory { training_exercise_id = requestTrainingExercise.id, time = requestExercise.type.Equals("Reps") ? null : createTrainingExerciseHistoryRequest.time, repetitions = requestExercise.type.Equals("Time") ? null : createTrainingExerciseHistoryRequest.repetitions, created_at = DateTime.Now };

            _appDbContext.training_exercises_history.Add(requestTrainingExercisesHistory);

            return requestTrainingExercisesHistory;
        }
        /// <summary>
        /// Get a Training Exercise History from database service
        /// </summary>
        /// <param name="trainingExerciseHistoryIDRequest">Training Exercise History ID Request</param>
        /// <returns>Training Exercise History</returns>
        public TrainingExercisesHistory GetByID(TrainingExerciseHistoryIDRequest trainingExerciseHistoryIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == trainingExerciseHistoryIDRequest.request_id);
            if (requestUser == null || requestUser.role_id != 2 )
                return null;

            TrainingExercisesHistory trainingExercisesHistory = _appDbContext.training_exercises_history.SingleOrDefault(x => x.id == trainingExerciseHistoryIDRequest.training_exercise_history_id);
            if (trainingExercisesHistory == null)
                return null;

            List<TrainingExercise> trainingExercises = _appDbContext.training_exercises.ToList();
            List<TrainingPlan> trainingPlans = _appDbContext.training_plans.ToList();
            List<AthletesInfo> athletesInfos = _appDbContext.athletes_info.ToList();


            if (trainingExercisesHistory.training_exercise.plan.athlete.user_id != trainingExerciseHistoryIDRequest.request_id)
                return null;

            return trainingExercisesHistory;
        }
        /// <summary>
        /// Get all Training Exercise History from database service
        /// </summary>
        /// <param name="request_id">Request ID</param>
        /// <returns>all Training Exercise History</returns>
        public List<TrainingExercisesHistory> GetTrainingExercisesHistories(int request_id)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            List<TrainingExercisesHistory> trainingExercisesHistories = _appDbContext.training_exercises_history.ToList();

            return trainingExercisesHistories;
        }
        /// <summary>
        /// Get all Training Exercise History of a specified user from database service
        /// </summary>
        /// <param name="request_id">Request ID</param>
        /// <returns>all Training Exercise History from the specified user ID</returns>
        public List<TrainingExercisesHistory> GetByUserID(int request_id)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == request_id);
            if (requestUser == null || requestUser.role_id != 2)
                return null;

            List<TrainingExercisesHistory> trainingExercisesHistories = _appDbContext.training_exercises_history.ToList();
            List<TrainingExercise> trainingExercises = _appDbContext.training_exercises.ToList();
            List<TrainingPlan> trainingPlans = _appDbContext.training_plans.ToList();
            List<AthletesInfo> athletesInfos = _appDbContext.athletes_info.ToList();
            List<Exercise> exercises = _appDbContext.exercises.ToList();

            List<TrainingExercisesHistory> trainingExercisesHistoriesToReturn = new List<TrainingExercisesHistory>();

            if(requestUser.athlete_info.training_plans!=null)
            {
                foreach (TrainingPlan trainingPlan in requestUser.athlete_info.training_plans)
                {
                    if (trainingPlan.training_exercises != null)
                    {
                        foreach (TrainingExercise trainingExercise in trainingPlan.training_exercises)
                        {
                            if (trainingExercise.training_exercises_history != null)
                            {
                                foreach (TrainingExercisesHistory trainingExercisesHistory in trainingExercise.training_exercises_history)
                                {
                                    trainingExercisesHistoriesToReturn.Add(trainingExercisesHistory);
                                }
                            }
                        }
                    }
                }
            }
            

            return trainingExercisesHistoriesToReturn;
        }
    }
}
