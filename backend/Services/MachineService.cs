﻿using backend.Entities;
using backend.Models;
using backend.Models.Machines;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
/// <summary>
/// Machine Service Interface
/// </summary>
public interface IMachineService
{
    /// <summary>
    /// Create Machine
    /// </summary>
    /// <param name="createMachineRequest">Create Machine Request Model</param>
    /// <returns>Machine Created</returns>
    Machine CreateMachine(CreateMachineRequest createMachineRequest);
    /// <summary>
    /// Update Machine QR Code
    /// </summary>
    /// <param name="machineIDRequest">Machine ID Request Model</param>
    /// <returns>Machine Updated</returns>
    Task<Machine> UpdateMachineQRCode(MachineIDRequest machineIDRequest);
    /// <summary>
    /// Update Machine
    /// </summary>
    /// <param name="updateMachineRequest">Update Machine Request Model</param>
    /// <returns>Machine Updated</returns>
    Machine UpdateMachine(UpdateMachineRequest updateMachineRequest);
    /// <summary>
    /// Get Machines
    /// </summary>
    /// <param name="request_id">Request ID</param>
    /// <returns>Machines</returns>
    List<Machine> GetMachines(int request_id);
    /// <summary>
    /// Get Machine By ID
    /// </summary>
    /// <param name="machineIDRequest">Machine ID Request Model</param>
    /// <returns>Machine</returns>
    Machine GetByID(MachineIDRequest machineIDRequest);
    /// <summary>
    /// Delete
    /// </summary>
    /// <param name="machineIDRequest">Machine ID Request Model</param>
    /// <returns>Machine Deleted</returns>
    Machine Delete(MachineIDRequest machineIDRequest);
}

namespace backend.Services
{
    /// <summary>
    /// Machine Service
    /// </summary>
    public class MachineService : IMachineService
    {
        private IConfiguration _config;
        private AppDbContext _appDbContext;
        private IQRCodeServiceInterface _QRCodeService;
        /// <summary>
        /// Machine Service Constructor
        /// </summary>
        /// <param name="config">Config</param>
        /// <param name="context">Context</param>
        /// <param name="QRCodeService"></param>
        public MachineService(IConfiguration config, AppDbContext context, IQRCodeServiceInterface QRCodeService)
        {
            _QRCodeService = QRCodeService;
            _config = config;
            _appDbContext = context;
        }
        /// <summary>
        /// Create Machine Method
        /// </summary>
        /// <param name="createMachineRequest">Create Machine Request Model</param>
        /// <returns>Machine Created</returns>
        public Machine CreateMachine(CreateMachineRequest createMachineRequest)
        {
            if (createMachineRequest.request_id <= 0 || string.IsNullOrEmpty(createMachineRequest.name) || string.IsNullOrEmpty(createMachineRequest.state.ToString()))
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == createMachineRequest.request_id);
            if (requestUser == null || requestUser.role_id != 1)
                return null;


            Machine machineToAdd = new Machine { name = createMachineRequest.name, state = createMachineRequest.state};

            _appDbContext.machines.Add(machineToAdd);

            return machineToAdd;
        }
        /// <summary>
        /// Update Machine
        /// </summary>
        /// <param name="updateMachineRequest">Update Machine Request Model</param>
        /// <returns>Machine Updated</returns>
        public Machine UpdateMachine(UpdateMachineRequest updateMachineRequest)
        {
            if (updateMachineRequest.request_id <= 0 || updateMachineRequest.machine_id <= 0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == updateMachineRequest.request_id);
            if (requestUser == null || requestUser.role_id != 1)
                return null;

            Machine requestMachine = _appDbContext.machines.SingleOrDefault(x => x.id == updateMachineRequest.machine_id);
            if (requestMachine == null )
                return null;

            if (!string.IsNullOrEmpty(updateMachineRequest.name))
                requestMachine.name = updateMachineRequest.name;
            if (!string.IsNullOrEmpty(updateMachineRequest.state.ToString()))
                requestMachine.state = updateMachineRequest.state;

            _appDbContext.machines.Update(requestMachine);

            return requestMachine;
        }
        /// <summary>
        /// Get All Machines
        /// </summary>
        /// <param name="request_id">ID of who makes the request</param>
        /// <returns>List of Machines</returns>
        public List<Machine> GetMachines(int request_id)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            List<MachineImage> machineImages = _appDbContext.machine_images.ToList();
            List<Machine> machines = _appDbContext.machines.ToList();

            return machines;
        }
        /// <summary>
        /// Get Machine by ID
        /// </summary>
        /// <param name="machineIDRequest">Machine ID Request Model</param>
        /// <returns>Machine</returns>
        public Machine GetByID(MachineIDRequest machineIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == machineIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            List<MachineImage> machineImages = _appDbContext.machine_images.ToList();
            Machine machine = _appDbContext.machines.SingleOrDefault(x => x.id == machineIDRequest.machine_id); ;

            return machine;
        }
        /// <summary>
        /// Delete machine using ID
        /// </summary>
        /// <param name="machineIDRequest">Machine ID Request Model</param>
        /// <returns>Deleted Machine</returns>
        public Machine Delete(MachineIDRequest machineIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == machineIDRequest.request_id);
            if (requestUser == null || requestUser.role_id != 1 )
                return null;

            Machine machineToRemove = _appDbContext.machines.SingleOrDefault(x => x.id == machineIDRequest.machine_id);

            if(machineToRemove!=null)
                _appDbContext.machines.Remove(machineToRemove); 

            return machineToRemove;
        }
        /// <summary>
        /// Update Machine QR Code
        /// </summary>
        /// <param name="machineIDRequest">Machine ID Request Model</param>
        /// <returns>Machine Updated</returns>
        public async Task<Machine> UpdateMachineQRCode(MachineIDRequest machineIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == machineIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            Machine requestMachine = _appDbContext.machines.SingleOrDefault(x => x.id == machineIDRequest.machine_id);
            if (requestMachine == null)
                return null;

            //Add url get exercises of a machine 
            string qrcode = await _QRCodeService.Create("https://localhost:5001/api/Exercise/exercises-by-machine?machine_id=" + requestMachine.id);

            requestMachine.qrcode = qrcode;

            _appDbContext.machines.Update(requestMachine);

            return requestMachine;
        }
    }
}
