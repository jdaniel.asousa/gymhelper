﻿using backend.Entities;
using backend.Models;
using backend.Models.Meals;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
/// <summary>
/// Meal Service Interface
/// </summary>
public interface IMealService
{
    /// <summary>
    /// Create Meal and add it to database service
    /// </summary>
    /// <param name="createMealRequest">Create Meal request model</param>
    /// <returns>Meal created</returns>
    Meal CreateMeal(CreateMealRequest createMealRequest);
    /// <summary>
    /// Updates a meal on database service
    /// </summary>
    /// <param name="updateMealRequest">Update Meal request model</param>
    /// <returns>Meal updated</returns>
    Meal UpdateMeal(UpdateMealRequest updateMealRequest);
    /// <summary>
    /// Get Meals from database service
    /// </summary>
    /// <param name="request_id">ID of who makes the request</param>
    /// <returns>List of Meals</returns>
    List<Meal> GetMeals(int request_id);
    /// <summary>
    /// Get a Meal from database service
    /// </summary>
    /// <param name="mealIDRequest">ID of who makes the request</param>
    /// <returns>A meal</returns>
    Meal GetByID(MealIDRequest mealIDRequest);
    /// <summary>
    /// Deletes a Meal from database service
    /// </summary>
    /// <param name="mealIDRequest">ID of who makes the request</param>
    /// <returns>Meal deleted</returns>
    Meal Delete(MealIDRequest mealIDRequest);
}


namespace backend.Services
{
    /// <summary>
    /// Meal Service
    /// </summary>
    public class MealService : IMealService
    {
        private IConfiguration _config;
        private AppDbContext _appDbContext;
        /// <summary>
        /// Meal Service constructor
        /// </summary>
        /// <param name="config">Config</param>
        /// <param name="context">Context</param>
        public MealService(IConfiguration config, AppDbContext context)
        {
            _config = config;
            _appDbContext = context;
        }
        /// <summary>
        /// Creates a Meal 
        /// </summary>
        /// <param name="createMealRequest">Create meal request model</param>
        /// <returns>Meal created</returns>
        public Meal CreateMeal(CreateMealRequest createMealRequest)
        {

            if (createMealRequest.request_id <=0 || createMealRequest.calories <=0 || createMealRequest.protein <=0 || createMealRequest.fats <=0 || createMealRequest.carbohydrates <=0 || createMealRequest.nutrition_plan_id<=0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == createMealRequest.request_id);
            if (requestUser == null || requestUser.role_id != 5)
                return null;

            NutritionPlan requestNutritionPlan = _appDbContext.nutrition_plans.SingleOrDefault(x => x.id == createMealRequest.nutrition_plan_id);
            if (requestNutritionPlan == null)
               return null;

            requestNutritionPlan.total_calories += createMealRequest.calories;
            requestNutritionPlan.total_protein += createMealRequest.protein;
            requestNutritionPlan.total_fats += createMealRequest.fats;
            requestNutritionPlan.total_carbohydrates += createMealRequest.carbohydrates;

            _appDbContext.nutrition_plans.Update(requestNutritionPlan);

            Meal mealToAdd = new Meal { calories = createMealRequest.calories, protein = createMealRequest.protein, fats = createMealRequest.fats, carbohydrates = createMealRequest.carbohydrates,description = createMealRequest.description, nutrition_plan_id = createMealRequest.nutrition_plan_id, };

            _appDbContext.meals.Add(mealToAdd);

                return mealToAdd;
        }
        /// <summary>
        /// Deletes a meal using ID
        /// </summary>
        /// <param name="mealIDRequest">Meal ID request model</param>
        /// <returns>Meal deleted</returns>
        public Meal Delete(MealIDRequest mealIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == mealIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 4)
                return null;

            Meal mealToRemove = _appDbContext.meals.SingleOrDefault(x => x.id == mealIDRequest.meal_id);
            if (mealToRemove == null)
                return null;

            NutritionPlan requestNutritionPlan = _appDbContext.nutrition_plans.SingleOrDefault(x => x.id == mealToRemove.nutrition_plan_id);
            if (requestNutritionPlan != null)
            {
                requestNutritionPlan.total_calories -= mealToRemove.calories;
                requestNutritionPlan.total_protein -= mealToRemove.protein;
                requestNutritionPlan.total_fats -= mealToRemove.fats;
                requestNutritionPlan.total_carbohydrates -= mealToRemove.carbohydrates;
            }

            _appDbContext.meals.Remove(mealToRemove);

            return mealToRemove;
        }
        /// <summary>
        /// Gets a Meal using ID
        /// </summary>
        /// <param name="mealIDRequest">Meal ID request model</param>
        /// <returns>Meal</returns>
        public Meal GetByID(MealIDRequest mealIDRequest)
        {
            var requestUser = _appDbContext.users.SingleOrDefault(x => x.id == mealIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 4)
                return null;

            Meal meal = _appDbContext.meals.SingleOrDefault(x => x.id == mealIDRequest.meal_id); ;

            return meal;
        }
        /// <summary>
        /// Gets a list of meals
        /// </summary>
        /// <param name="request_id">ID of who makes the request</param>
        /// <returns>A list of meals</returns>
        public List<Meal> GetMeals(int request_id)
        {
            var requestUser = _appDbContext.users.SingleOrDefault(x => x.id == request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 4)
                return null;

            List<Meal> meals = _appDbContext.meals.ToList();

            return meals;
        }
        /// <summary>
        /// Update Meal
        /// </summary>
        /// <param name="updateMealRequest">Update Meal Request Model</param>
        /// <returns>Meal updated</returns>
        public Meal UpdateMeal(UpdateMealRequest updateMealRequest)
        {
            if (string.IsNullOrEmpty(updateMealRequest.request_id.ToString()) || string.IsNullOrEmpty(updateMealRequest.meal_id.ToString()))
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == updateMealRequest.request_id);
            if (requestUser == null || requestUser.role_id != 5)
                return null;

            Meal requestMeal = _appDbContext.meals.SingleOrDefault(x => x.id == updateMealRequest.meal_id);
            if (requestMeal == null)
                return null;

            NutritionPlan nutritionPlan = _appDbContext.nutrition_plans.SingleOrDefault(x => x.id == requestMeal.nutrition_plan_id);
            if (nutritionPlan == null)
                return null;

            if (updateMealRequest.calories > 0)
            {
                nutritionPlan.total_calories -= requestMeal.calories;
                requestMeal.calories = updateMealRequest.calories;
                nutritionPlan.total_calories += requestMeal.calories;

            }
                
            if (updateMealRequest.protein > 0)
            {
                nutritionPlan.total_protein -= requestMeal.protein;
                requestMeal.protein = updateMealRequest.protein;
                nutritionPlan.total_protein += requestMeal.protein;
            }
                
            if (updateMealRequest.fats > 0)
            {
                nutritionPlan.total_fats -= requestMeal.fats;
                requestMeal.fats = updateMealRequest.fats;
                nutritionPlan.total_fats += requestMeal.fats;

            }
                
            if (updateMealRequest.carbohydrates > 0)
            {
                nutritionPlan.total_carbohydrates -= requestMeal.carbohydrates;
                requestMeal.carbohydrates = updateMealRequest.carbohydrates;
                nutritionPlan.total_carbohydrates += requestMeal.carbohydrates;
            }
                
            if (!string.IsNullOrEmpty(updateMealRequest.description))
                requestMeal.description = updateMealRequest.description;

            _appDbContext.nutrition_plans.Update(nutritionPlan);
            _appDbContext.meals.Update(requestMeal);

            return requestMeal;
        }
    }
}
