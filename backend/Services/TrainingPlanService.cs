﻿using backend.Entities;
using backend.Models;
using backend.Models.TrainingPlans;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
/// <summary>
/// Training Plan Service Interface
/// </summary>
public interface ITrainingPlanService
{
    /// <summary>
    /// Create Training Plan
    /// </summary>
    /// <param name="createTrainingPlanRequest">Create Training Plan Request Model</param>
    /// <returns>Training Plan created</returns>
    TrainingPlan CreateTrainingPlan(CreateTrainingPlanRequest createTrainingPlanRequest);
    /// <summary>
    /// Updated Training Plan
    /// </summary>
    /// <param name="updateTrainingPlanRequest">Update Training Plan Request Model</param>
    /// <returns>Training Plan Updated</returns>
    TrainingPlan UpdateTrainingPlan(UpdateTrainingPlanRequest updateTrainingPlanRequest);
    /// <summary>
    /// Get Training Plans
    /// </summary>
    /// <param name="request_id">ID of who makes the request</param>
    /// <returns>List of Training Plans</returns>
    List<TrainingPlan> GetTrainingPlans(int request_id);
    /// <summary>
    /// Get Training Plan by ID
    /// </summary>
    /// <param name="trainingPlanIDRequest">Training Plan ID Request Model</param>
    /// <returns>Training Plan</returns>
    TrainingPlan GetByID(TrainingPlanIDRequest trainingPlanIDRequest);
    /// <summary>
    /// Delete Training Plan using ID
    /// </summary>
    /// <param name="trainingPlanIDRequest">Training Plan ID Request Model</param>
    /// <returns>Training Plan Deleted</returns>
    TrainingPlan Delete(TrainingPlanIDRequest trainingPlanIDRequest);
    /// <summary>
    /// Add Exercise To Taining Plan
    /// </summary>
    /// <param name="addExerciseToPlanRequest">Add Exercise To Plan Request Model</param>
    /// <returns>Training Plan Updated</returns>
    TrainingPlan AddExerciseToPlanRequest(AddExerciseToPlanRequest addExerciseToPlanRequest);
    /// <summary>
    /// Gets the training plans of a specific user (himself)
    /// </summary>
    /// <param name="request_id">Request ID</param>
    /// <returns>Training plans</returns>
    List<TrainingPlan> GetByUserID(int request_id);
}

namespace backend.Services
{
    /// <summary>
    /// Training Plan Service
    /// </summary>
    public class TrainingPlanService : ITrainingPlanService
    {
        private IConfiguration _config;
        private AppDbContext _appDbContext;
        /// <summary>
        /// Training Plan Service Constructor
        /// </summary>
        /// <param name="config">Config</param>
        /// <param name="context">Context</param>
        public TrainingPlanService(IConfiguration config, AppDbContext context)
        {
            _config = config;
            _appDbContext = context;
        }
        /// <summary>
        /// Add Exercise To Plan Request
        /// </summary>
        /// <param name="addExerciseToPlanRequest">Add Exercise To Plan Request Model</param>
        /// <returns>Training Plan Updated</returns>
        public TrainingPlan AddExerciseToPlanRequest(AddExerciseToPlanRequest addExerciseToPlanRequest)
        {
            if (addExerciseToPlanRequest.request_id<=0 || addExerciseToPlanRequest.plan_id <= 0 || addExerciseToPlanRequest.exercise_id <= 0 || addExerciseToPlanRequest.circuit <= 0 || addExerciseToPlanRequest.sets <= 0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == addExerciseToPlanRequest.request_id);
            if (requestUser == null || requestUser.role_id != 4)
                return null;

            TrainingPlan requestTrainingPlan = _appDbContext.training_plans.SingleOrDefault(x => x.id == addExerciseToPlanRequest.plan_id);
            if (requestTrainingPlan == null)
                return null;

            Exercise requestExercise = _appDbContext.exercises.SingleOrDefault(x => x.id == addExerciseToPlanRequest.exercise_id);
            if (requestExercise == null)
                return null;

            if ((requestExercise.type.Equals("Time") && string.IsNullOrEmpty(addExerciseToPlanRequest.time)) || (requestExercise.type.Equals("Reps") && string.IsNullOrEmpty(addExerciseToPlanRequest.repetitions)))
                return null;

            if (requestExercise.type.Equals("Time")){
                String[] timeSplited = addExerciseToPlanRequest.time.Split('-');

                if (timeSplited.Length != addExerciseToPlanRequest.sets)
                    return null;
                foreach(String time in timeSplited)
                {
                    if (Int32.Parse(time) <= 0)
                        return null;
                }
            }
            if (requestExercise.type.Equals("Reps"))
            {
                String[] repsSplited = addExerciseToPlanRequest.repetitions.Split('-');

                if (repsSplited.Length != addExerciseToPlanRequest.sets)
                    return null;
                foreach (String reps in repsSplited)
                {
                    if (Int32.Parse(reps) <= 0)
                        return null;
                }
            }

            TrainingExercise trainingExerciseToAdd = new TrainingExercise { plan_id = requestTrainingPlan.id, exercise_id = requestExercise.id, circuit = addExerciseToPlanRequest.circuit, time = requestExercise.type.Equals("Reps") ? null : addExerciseToPlanRequest.time, repetitions = requestExercise.type.Equals("Time") ? null : addExerciseToPlanRequest.repetitions, sets = addExerciseToPlanRequest.sets };

            _appDbContext.training_exercises.Add(trainingExerciseToAdd);

            TrainingPlan trainingPlanUpdated = _appDbContext.training_plans.SingleOrDefault(x => x.id == addExerciseToPlanRequest.plan_id);

            return trainingPlanUpdated;
        }
        /// <summary>
        /// Create Training Plan
        /// </summary>
        /// <param name="createTrainingPlanRequest">Create Training Plan Request Model</param>
        /// <returns>Training Plan Created</returns>
        public TrainingPlan CreateTrainingPlan(CreateTrainingPlanRequest createTrainingPlanRequest)
        {
            if (string.IsNullOrEmpty(createTrainingPlanRequest.request_id.ToString()) || string.IsNullOrEmpty(createTrainingPlanRequest.athlete_id.ToString()))
                return null;

            var requestUser = _appDbContext.users.SingleOrDefault(x => x.id == createTrainingPlanRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            var requestAthlete = _appDbContext.athletes_info.SingleOrDefault(x => x.id == createTrainingPlanRequest.athlete_id);
            if (requestAthlete == null)
                return null;

            TrainingPlan trainingPlan = new TrainingPlan { description = createTrainingPlanRequest.description, athlete_id = requestAthlete.id };

            _appDbContext.training_plans.Add(trainingPlan);

            return trainingPlan;
        }
        /// <summary>
        /// Delete Training Plan using ID
        /// </summary>
        /// <param name="trainingPlanIDRequest">Training Plan ID Request Model</param>
        /// <returns>Deleted Training Plan</returns>
        public TrainingPlan Delete(TrainingPlanIDRequest trainingPlanIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == trainingPlanIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            TrainingPlan trainingPlanToRemove = _appDbContext.training_plans.SingleOrDefault(x => x.id == trainingPlanIDRequest.training_plan_id);

            if (trainingPlanToRemove != null)
                _appDbContext.training_plans.Remove(trainingPlanToRemove); ;

            return trainingPlanToRemove;
        }
        /// <summary>
        /// Get Training Plan By ID
        /// </summary>
        /// <param name="trainingPlanIDRequest">Training Plan ID Request Model</param>
        /// <returns>Training Plan</returns>
        public TrainingPlan GetByID(TrainingPlanIDRequest trainingPlanIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == trainingPlanIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            List<MachineImage> machineImage = _appDbContext.machine_images.ToList();
            List<Machine> machine = _appDbContext.machines.ToList();
            List<ExerciseImage> exerciseImages = _appDbContext.exercise_images.ToList();
            List<TrainingExercise> trainingExercises = _appDbContext.training_exercises.ToList();
            List<AthletesInfo> athletesInfos = _appDbContext.athletes_info.ToList();
            List<Exercise> exercises = _appDbContext.exercises.ToList();

            TrainingPlan trainingPlan = _appDbContext.training_plans.SingleOrDefault(x => x.id == trainingPlanIDRequest.training_plan_id);

            if (requestUser.role_id == 2 && trainingPlan.athlete.user_id != requestUser.id)
                return null;

            return trainingPlan;
        }
        /// <summary>
        /// Get Training Plans
        /// </summary>
        /// <param name="request_id">ID of who makes the request</param>
        /// <returns>List of Training Plans</returns>
        public List<TrainingPlan> GetTrainingPlans(int request_id)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            List<MachineImage> machineImage = _appDbContext.machine_images.ToList();
            List<Machine> machine = _appDbContext.machines.ToList();
            List<TrainingExercise> trainingExercises = _appDbContext.training_exercises.ToList();
            List<Exercise> exercises = _appDbContext.exercises.ToList();

            List<TrainingPlan> trainingPlans = _appDbContext.training_plans.ToList();

            return trainingPlans;
        }
        /// <summary>
        /// Update Training Plan
        /// </summary>
        /// <param name="updateTrainingPlanRequest">Update Training Plan Request Model</param>
        /// <returns>Training Plan updated</returns>
        public TrainingPlan UpdateTrainingPlan(UpdateTrainingPlanRequest updateTrainingPlanRequest)
        {
            if (string.IsNullOrEmpty(updateTrainingPlanRequest.request_id.ToString()) || string.IsNullOrEmpty(updateTrainingPlanRequest.training_plan_id.ToString()))
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == updateTrainingPlanRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            TrainingPlan requestTrainingPlan = _appDbContext.training_plans.SingleOrDefault(x => x.id == updateTrainingPlanRequest.training_plan_id);
            if (requestTrainingPlan == null)
                return null;

            if (!string.IsNullOrEmpty(updateTrainingPlanRequest.description))
                requestTrainingPlan.description = updateTrainingPlanRequest.description;
            if (updateTrainingPlanRequest.athlete_id != 0)
            {
                AthletesInfo athletesInfo = _appDbContext.athletes_info.SingleOrDefault(x => x.id == updateTrainingPlanRequest.athlete_id);
                if (athletesInfo == null)
                    return null;

                requestTrainingPlan.athlete_id = updateTrainingPlanRequest.athlete_id;
            }

            _appDbContext.training_plans.Update(requestTrainingPlan);

            return requestTrainingPlan;
        }
        /// <summary>
        /// Gets the training plans of a specific user (himself)
        /// </summary>
        /// <param name="request_id">Request ID</param>
        /// <returns>Training plans</returns>
        public List<TrainingPlan> GetByUserID(int request_id)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == request_id);
            if (requestUser == null || requestUser.role_id != 2 )
                return null;

            List<TrainingExercise> trainingExercises = _appDbContext.training_exercises.ToList();
            List<TrainingPlan> allTrainingPlans = _appDbContext.training_plans.ToList();
            List<AthletesInfo> athletesInfo =_appDbContext.athletes_info.ToList();
            List<TrainingPlan> userTrainingPlans = requestUser.athlete_info.training_plans;

            return userTrainingPlans;
        }
    }
}
