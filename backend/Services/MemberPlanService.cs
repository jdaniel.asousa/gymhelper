﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Entities;
using backend.Models;
using backend.Models.MemberPlans;
using Microsoft.Extensions.Configuration;

/// <summary>
/// Member Plan Service Interface
/// </summary>
public interface IMemberPlanService
{
    /// <summary>
    /// Create a member plan and add it to database service
    /// </summary>
    /// <param name="createMemberPlanRequest">Create Member Plan Request Model</param>
    /// <returns>Creates a Member Plan</returns>
    MemberPlan CreateMemberPlan(CreateMemberPlanRequest createMemberPlanRequest);
    /// <summary>
    /// Update a member plan on database service
    /// </summary>
    /// <param name="updateMemberPlanRequest">Update Member Plan Request Model</param>
    /// <returns>Updates a Member Plan</returns>
    MemberPlan UpdateMemberPlan(UpdateMemberPlanRequest updateMemberPlanRequest);
    /// <summary>
    /// Gets a list of member plans from the database service
    /// </summary>
    /// <param name="request_id">)ID of who makes the request</param>
    /// <returns>Gets a list of Member Plans</returns>
    List<MemberPlan> GetMemberPlans(int request_id);
    /// <summary>
    /// Get a member plan from the database service
    /// </summary>
    /// <param name="memberPlanIDRequest">Member Plan ID Request Model</param>
    /// <returns>Gets a Member Plan</returns>
    MemberPlan GetByID(MemberPlanIDRequest memberPlanIDRequest);
    /// <summary>
    /// Deletes a Member Plan from the database service
    /// </summary>
    /// <param name="memberPlanIDRequest">Member Plan ID Request Model</param>
    /// <returns>Member Plan Deleted</returns>
    MemberPlan Delete(MemberPlanIDRequest memberPlanIDRequest);
    /// <summary>
    /// Update Member Plan Valid Entrys
    /// </summary>
    /// <param name="giveEntryReceptionistRequest">Member plan ID Request Model</param>
    /// <returns>Validates a entry</returns>
    MemberPlan GiveEntry(GiveEntryReceptionistRequest giveEntryReceptionistRequest);
    /// <summary>
    /// Update Member Plan Valid Entrys when atlhete
    /// </summary>
    /// <param name="id">id of makes the request</param>
    /// <returns>Validates a entry</returns>
    MemberPlan GiveEntryAthlete(int id);
}
namespace backend.Services
{
    /// <summary>
    /// Member Plan Service
    /// </summary>
    public class MemberPlanService : IMemberPlanService
    {
        private IConfiguration _config;
        private AppDbContext _appDbContext;
        /// <summary>
        /// Member Plan Service Constructor
        /// </summary>
        /// <param name="config">Config</param>
        /// <param name="context">Context</param>
        public MemberPlanService(IConfiguration config, AppDbContext context)
        {
            _config = config;
            _appDbContext = context;
        }
        /// <summary>
        /// Creates a Member Plan and add it to database service
        /// </summary>
        /// <param name="createMemberPlanRequest">Create Member Plan Request Model</param>
        /// <returns>Member Plan Created</returns>
        public MemberPlan CreateMemberPlan(CreateMemberPlanRequest createMemberPlanRequest)
        { 

            if (createMemberPlanRequest.request_id <= 0 || createMemberPlanRequest.monthly_plan_id <= 0 || createMemberPlanRequest.athlete_id <= 0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == createMemberPlanRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 4|| requestUser.role_id == 5)
                return null;

            MonthlyPlan requestMonthlyPlan = _appDbContext.monthly_plans.SingleOrDefault(x => x.id == createMemberPlanRequest.monthly_plan_id);
            if (requestMonthlyPlan == null)
                return null;

            AthletesInfo requestAthlete = _appDbContext.athletes_info.SingleOrDefault(x => x.id == createMemberPlanRequest.athlete_id);
            if (requestAthlete == null)
               return null;

            List<MemberPlan> memberPlans= _appDbContext.member_plans.ToList();

            if (requestAthlete.member_plans!=null)
            {
                
                foreach (MemberPlan member_Plan in requestAthlete.member_plans)
                {
                    if (DateTime.Now >= member_Plan.entry_date && DateTime.Now <= member_Plan.expired_date)
                        return null;
                }
            }

            MemberPlan memberPlan = new MemberPlan { monthly_plan_id = createMemberPlanRequest.monthly_plan_id, athlete_id = createMemberPlanRequest.athlete_id,valid_entrys=requestMonthlyPlan.entrys,entry_date=DateTime.Now,expired_date=DateTime.Now.AddMonths(1)};

              _appDbContext.member_plans.Add(memberPlan);

            return memberPlan;
        }
        /// <summary>
        /// Deletes a Member Plan from the Database service
        /// </summary>
        /// <param name="memberPlanIDRequest">Member Plan ID Request Model</param>
        /// <returns>Member Plan Deleted</returns>

        public MemberPlan Delete(MemberPlanIDRequest memberPlanIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == memberPlanIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 4 || requestUser.role_id == 5)
                return null;

            MemberPlan memberPlanToRemove = _appDbContext.member_plans.SingleOrDefault(x => x.id == memberPlanIDRequest.member_plan_id);

            if (memberPlanToRemove != null)
                _appDbContext.member_plans.Remove(memberPlanToRemove);

            return memberPlanToRemove;

        }
        /// <summary>
        /// Gets a Member Plan
        /// </summary>
        /// <param name="memberPlanIDRequest">Member Plan ID Request Model</param>
        /// <returns>Gets a member plan</returns>
        public MemberPlan GetByID(MemberPlanIDRequest memberPlanIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == memberPlanIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 4 || requestUser.role_id == 5)
                return null;

            MemberPlan requestMemberPlan = _appDbContext.member_plans.SingleOrDefault(x => x.id == memberPlanIDRequest.member_plan_id);
            if (requestMemberPlan == null)
                return null;

            if (requestUser.role_id == 2)
            { 
                if (requestMemberPlan.athlete_id != requestUser.athlete_info.id)
                    return null;
            }

            MemberPlan memberPlan = _appDbContext.member_plans.SingleOrDefault(x => x.id == memberPlanIDRequest.member_plan_id);

            return memberPlan;

        }
        /// <summary>
        /// Gets a list of Member plans
        /// </summary>
        /// <param name="request_id">ID of who makes the request</param>
        /// <returns>A list of member plans</returns>
        public List<MemberPlan> GetMemberPlans(int request_id)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 4 || requestUser.role_id == 5)
                return null;

            List<MemberPlan> memberPlans = _appDbContext.member_plans.ToList();

            return memberPlans;
        }
        /// <summary>
        /// Updates a member plan valid entrys
        /// </summary>
        /// <param name="giveEntryReceptionistRequest">ID of who makes the request</param>
        /// <returns>Member Plan valid entrys updated</returns>
        public MemberPlan GiveEntry(GiveEntryReceptionistRequest giveEntryReceptionistRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == giveEntryReceptionistRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 4 || requestUser.role_id == 5)
                return null;

            List<MemberPlan> memberPlans = _appDbContext.member_plans.ToList();
            AthletesInfo athletesInfo = _appDbContext.athletes_info.SingleOrDefault(x => x.id == giveEntryReceptionistRequest.athlete_id);

            List<MemberPlan> requestMemberPlans = athletesInfo.member_plans;
            MemberPlan currentMemberPlan = new MemberPlan();

            foreach (MemberPlan memberPlan in requestMemberPlans)
            {
                if (memberPlan.entry_date < DateTime.Now && memberPlan.expired_date > DateTime.Now)
                    currentMemberPlan = memberPlan;
            }

            if (currentMemberPlan.id == 0)
                return null;

            if (currentMemberPlan.valid_entrys == 0)
                return null;

            currentMemberPlan.valid_entrys -= 1;

            _appDbContext.member_plans.Update(currentMemberPlan);

            return currentMemberPlan;
        }

        /// <summary>
        /// Updates a member plan
        /// </summary>
        /// <param name="updateMemberPlanRequest">Update Member Plan Request Model</param>
        /// <returns>Member Plan Updated</returns>
        public MemberPlan UpdateMemberPlan(UpdateMemberPlanRequest updateMemberPlanRequest)
        {
            if (updateMemberPlanRequest.request_id <=0 || updateMemberPlanRequest.member_plan_id <=0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == updateMemberPlanRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 4 || requestUser.role_id == 5)
                return null;

            MemberPlan requestMemberPlan = _appDbContext.member_plans.SingleOrDefault(x => x.id == updateMemberPlanRequest.member_plan_id);
            if (requestMemberPlan == null)
                return null;

            if (updateMemberPlanRequest.monthly_id > 0)
            {
                MonthlyPlan requestMonthlyPlan = _appDbContext.monthly_plans.SingleOrDefault(x => x.id == updateMemberPlanRequest.monthly_id);
                if (requestMemberPlan == null)
                    return null;
                
                requestMemberPlan.monthly_plan_id = updateMemberPlanRequest.monthly_id;
            }
  
            if (updateMemberPlanRequest.athlete_id > 0)
            {
                AthletesInfo requestAthletesInfo= _appDbContext.athletes_info.SingleOrDefault(x => x.id == updateMemberPlanRequest.athlete_id);
                if (requestAthletesInfo == null)
                    return null;

                requestMemberPlan.athlete_id = updateMemberPlanRequest.athlete_id;
            }
           
            _appDbContext.member_plans.Update(requestMemberPlan);

            return requestMemberPlan;

        }
        /// <summary>
        /// Updates a member plan valid entrys
        /// </summary>
        /// <param name="id">ID of who makes the request</param>
        /// <returns>Member Plan valid entrys updated</returns>
        public MemberPlan GiveEntryAthlete(int id)
        {

            List<AthletesInfo> athletesInfos = _appDbContext.athletes_info.ToList();
            List<MemberPlan> memberPlans = _appDbContext.member_plans.ToList();

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == id);
            if (requestUser == null || requestUser.role_id != 2)
                return null;


            List<MemberPlan> requestMemberPlans = requestUser.athlete_info.member_plans;
            MemberPlan currentMemberPlan = new MemberPlan();

            foreach(MemberPlan memberPlan in requestMemberPlans){
                if (memberPlan.entry_date < DateTime.Now && memberPlan.expired_date > DateTime.Now)
                    currentMemberPlan = memberPlan;
            }

            if (currentMemberPlan.id == 0)
                return null;

            if (currentMemberPlan.valid_entrys == 0)
                return null;

            currentMemberPlan.valid_entrys -= 1;

            _appDbContext.member_plans.Update(currentMemberPlan);

            return currentMemberPlan;
        }
    }
}
