﻿using backend.Entities;
using backend.Models;
using backend.Models.PhysicalEvaluations;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/// <summary>
/// Physical Evaluation Service Interface
/// </summary>
public interface IPhysicalEvaluationService
{
    /// <summary>
    /// Create Physical Evaluation and add it to database service
    /// </summary>
    PhysicalEvaluation CreatePhysicalEvaluation(CreatePhysicalEvaluationRequest createPhysicalEvaluationRequest);
    /// <summary>
    /// Update Physical Evaluation on database service
    /// </summary>
    PhysicalEvaluation UpdatePhysicalEvaluation(UpdatePhysicalEvaluationRequest updatePhysicalEvaluationRequest);
    /// <summary>
    /// Get Physical Evaluations from database service
    /// </summary>
    List<PhysicalEvaluation> GetPhysicalEvaluations(int request_id);
    /// <summary>
    /// Get an Physical Evaluation from database service
    /// </summary>
    PhysicalEvaluation GetByID(PhysicalEvaluationIDRequest physicalEvaluationIDRequest);
    /// <summary>
    /// Delete Physical Evaluation from database service
    /// </summary>
    PhysicalEvaluation Delete(PhysicalEvaluationIDRequest physicalEvaluationIDRequest);
    /// <summary>
    /// Get Physical Evaluations from database service of a specific athlete
    /// </summary>
    List<PhysicalEvaluation> GetAllOfAthlete(int request_id);
}

namespace backend.Services
{
    /// <summary>
    /// Physical Evaluation Service 
    /// </summary>
    public class PhysicalEvaluationService : IPhysicalEvaluationService
    {
        private IConfiguration _config;
        private AppDbContext _appDbContext;

        /// <summary>
        /// Physical Evaluation Service Constructor
        /// </summary>
        public PhysicalEvaluationService(IConfiguration config, AppDbContext context)
        {
            _config = config;
            _appDbContext = context;
        }
        /// <summary>
        /// Create Physical Evaluation and add it to database service
        /// </summary>
        public PhysicalEvaluation CreatePhysicalEvaluation(CreatePhysicalEvaluationRequest createPhysicalEvaluationRequest)
        {
            if (string.IsNullOrEmpty(createPhysicalEvaluationRequest.request_id.ToString()) || createPhysicalEvaluationRequest.height<=0 || createPhysicalEvaluationRequest.weight <= 0 || createPhysicalEvaluationRequest.imc <= 0 || string.IsNullOrEmpty(createPhysicalEvaluationRequest.description.ToString()) || createPhysicalEvaluationRequest.body_water <= 0 || createPhysicalEvaluationRequest.body_fat <= 0 || createPhysicalEvaluationRequest.muscle_mass <= 0 || createPhysicalEvaluationRequest.lat_size <= 0 || createPhysicalEvaluationRequest.left_bicep_size <= 0 || createPhysicalEvaluationRequest.right_bicep_size <= 0 || createPhysicalEvaluationRequest.chest_size <= 0 || createPhysicalEvaluationRequest.waist_size <= 0 || createPhysicalEvaluationRequest.glute_size <= 0 || createPhysicalEvaluationRequest.hip_size <= 0 || createPhysicalEvaluationRequest.thigh_size <= 0 || createPhysicalEvaluationRequest.calfs_size <= 0 || createPhysicalEvaluationRequest.athlete_id <= 0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == createPhysicalEvaluationRequest.request_id);
            if (requestUser == null || requestUser.role_id != 4)
                return null;

            AthletesInfo requestAthletesInfo = _appDbContext.athletes_info.SingleOrDefault(x => x.id == createPhysicalEvaluationRequest.athlete_id);
            if (requestAthletesInfo == null)
                return null;

            PhysicalEvaluation physicalEvaluationToAdd = new PhysicalEvaluation { created_at = DateTime.Now,  height = createPhysicalEvaluationRequest.height, weight = createPhysicalEvaluationRequest.weight, imc = createPhysicalEvaluationRequest.imc, description = createPhysicalEvaluationRequest.description, body_water = createPhysicalEvaluationRequest.body_water, body_fat = createPhysicalEvaluationRequest.body_fat, muscle_mass = createPhysicalEvaluationRequest.muscle_mass, lat_size = createPhysicalEvaluationRequest.lat_size, left_bicep_size = createPhysicalEvaluationRequest.left_bicep_size, right_bicep_size = createPhysicalEvaluationRequest.right_bicep_size, chest_size = createPhysicalEvaluationRequest.chest_size, waist_size = createPhysicalEvaluationRequest.waist_size, glute_size = createPhysicalEvaluationRequest.glute_size, hip_size = createPhysicalEvaluationRequest.hip_size, thigh_size = createPhysicalEvaluationRequest.thigh_size, calfs_size = createPhysicalEvaluationRequest.calfs_size, athlete_id = createPhysicalEvaluationRequest.athlete_id};

            _appDbContext.physical_evaluations.Add(physicalEvaluationToAdd);

            return physicalEvaluationToAdd;

        }

        /// <summary>
        /// Delete Physical Evaluation from database service
        /// </summary>
        public PhysicalEvaluation Delete(PhysicalEvaluationIDRequest physicalEvaluationIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == physicalEvaluationIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            PhysicalEvaluation physicalEvaluationToRemove = _appDbContext.physical_evaluations.SingleOrDefault(x => x.id == physicalEvaluationIDRequest.physical_evaluation_id);

            if (physicalEvaluationToRemove != null)
                _appDbContext.physical_evaluations.Remove(physicalEvaluationToRemove); ;

            return physicalEvaluationToRemove;
        }
        /// <summary>
        /// Get Physical Evaluations from database service of a specific athlete
        /// </summary>
        /// <param name="request_id">ID of who makes the request</param>
        /// <returns> Physical Evaluations from database service of a specific athlete</returns>
        public List<PhysicalEvaluation> GetAllOfAthlete(int request_id)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == request_id);
            if (requestUser == null || requestUser.role_id != 2)
                return null;

            List<AthletesInfo> athletesInfos = _appDbContext.athletes_info.ToList();
            List<PhysicalEvaluation> physicalEvaluationList = _appDbContext.physical_evaluations.ToList();
            List<PhysicalEvaluation> physicalEvaluation = requestUser.athlete_info.physical_evaluations;
            if (physicalEvaluation == null)
                physicalEvaluation = new List<PhysicalEvaluation>();

            return physicalEvaluation;
        }

        /// <summary>
        /// Get an Physical Evaluation from database service
        /// </summary>
        public PhysicalEvaluation GetByID(PhysicalEvaluationIDRequest physicalEvaluationIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == physicalEvaluationIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            PhysicalEvaluation physicalEvaluation = _appDbContext.physical_evaluations.SingleOrDefault(x => x.id == physicalEvaluationIDRequest.physical_evaluation_id);

            List<AthletesInfo> athletesInfoList = _appDbContext.athletes_info.ToList();

            if(requestUser.role_id == 2)
            {
                if(physicalEvaluation.athlete_id != requestUser.athlete_info.id)
                {
                    return null;
                }
            }

            return physicalEvaluation;
        }

        /// <summary>
        /// Get Physical Evaluation from database service
        /// </summary>
        public List<PhysicalEvaluation> GetPhysicalEvaluations(int request_id)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            List<AthletesInfo> athletesInfos = _appDbContext.athletes_info.ToList();

            List<PhysicalEvaluation> physicalEvaluation = _appDbContext.physical_evaluations.ToList();

            return physicalEvaluation;
        }

        /// <summary>
        /// Update Physical Evaluation on database service
        /// </summary>
        public PhysicalEvaluation UpdatePhysicalEvaluation(UpdatePhysicalEvaluationRequest updatePhysicalEvaluationRequest)
        {
            if (updatePhysicalEvaluationRequest.request_id <= 0 && updatePhysicalEvaluationRequest.physical_evaluation_id <= 0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == updatePhysicalEvaluationRequest.request_id);
            if (requestUser == null || requestUser.role_id != 4)
                return null;

            PhysicalEvaluation requestPhysicalEvaluation = _appDbContext.physical_evaluations.SingleOrDefault(x => x.id == updatePhysicalEvaluationRequest.physical_evaluation_id);
            if (requestPhysicalEvaluation == null)
                return null;

            if (updatePhysicalEvaluationRequest.height > 0)
                requestPhysicalEvaluation.height = updatePhysicalEvaluationRequest.height;
            if (updatePhysicalEvaluationRequest.weight > 0)
                requestPhysicalEvaluation.weight = updatePhysicalEvaluationRequest.weight;
            if (updatePhysicalEvaluationRequest.imc > 0)
                requestPhysicalEvaluation.imc = updatePhysicalEvaluationRequest.imc;
            if (!string.IsNullOrEmpty(updatePhysicalEvaluationRequest.description))
                requestPhysicalEvaluation.description = updatePhysicalEvaluationRequest.description;
            if (updatePhysicalEvaluationRequest.body_water > 0)
                requestPhysicalEvaluation.body_water = updatePhysicalEvaluationRequest.body_water;
            if (updatePhysicalEvaluationRequest.body_fat > 0)
                requestPhysicalEvaluation.body_fat = updatePhysicalEvaluationRequest.body_fat;
            if (updatePhysicalEvaluationRequest.muscle_mass > 0)
                requestPhysicalEvaluation.muscle_mass = updatePhysicalEvaluationRequest.muscle_mass;
            if (updatePhysicalEvaluationRequest.lat_size > 0)
                requestPhysicalEvaluation.lat_size = updatePhysicalEvaluationRequest.lat_size;
            if (updatePhysicalEvaluationRequest.left_bicep_size > 0)
                requestPhysicalEvaluation.left_bicep_size = updatePhysicalEvaluationRequest.left_bicep_size;
            if (updatePhysicalEvaluationRequest.right_bicep_size > 0)
                requestPhysicalEvaluation.right_bicep_size = updatePhysicalEvaluationRequest.right_bicep_size;
            if (updatePhysicalEvaluationRequest.chest_size > 0)
                requestPhysicalEvaluation.chest_size = updatePhysicalEvaluationRequest.chest_size;
            if (updatePhysicalEvaluationRequest.waist_size > 0)
                requestPhysicalEvaluation.waist_size = updatePhysicalEvaluationRequest.waist_size;
            if (updatePhysicalEvaluationRequest.glute_size > 0)
                requestPhysicalEvaluation.glute_size = updatePhysicalEvaluationRequest.glute_size;
            if (updatePhysicalEvaluationRequest.hip_size > 0)
                requestPhysicalEvaluation.hip_size = updatePhysicalEvaluationRequest.hip_size;
            if (updatePhysicalEvaluationRequest.thigh_size > 0)
                requestPhysicalEvaluation.thigh_size = updatePhysicalEvaluationRequest.thigh_size;
            if (updatePhysicalEvaluationRequest.calfs_size > 0)
                requestPhysicalEvaluation.calfs_size = updatePhysicalEvaluationRequest.calfs_size;
            if (updatePhysicalEvaluationRequest.athlete_id > 0)
            {
                AthletesInfo athletesInfo = _appDbContext.athletes_info.SingleOrDefault(x => x.id == updatePhysicalEvaluationRequest.athlete_id);

                if (athletesInfo == null)
                    return null;
                requestPhysicalEvaluation.athlete_id = updatePhysicalEvaluationRequest.athlete_id;
            }

            _appDbContext.physical_evaluations.Update(requestPhysicalEvaluation);

            return requestPhysicalEvaluation;
        }
    }
}
