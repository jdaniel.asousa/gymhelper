﻿using backend.Helpers;
using backend.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

/// <summary>
/// Image Service Interface
/// </summary>
public interface IImageService
{
    /// <summary>
    /// Post Method
    /// </summary>
    /// <param name="image">image</param> 
    /// <returns>ImgBBResponse</returns>
    Task<ImgBBResponse> PostPhoto(IFormFile image);
}
namespace backend.Services
{
    /// <summary>
    /// Image Service
    /// </summary>
    public class ImageService : IImageService
    {
        private readonly AppSettings _appSettings;
        /// <summary>
        /// Image Service Constructor
        /// </summary>
        /// <param name="appSettings">App Settings</param>
        public ImageService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }
        /// <summary>
        /// Post Method
        /// </summary>
        /// <param name="image">image</param>
        /// <returns>ImgBBResponse</returns>
        public async Task<ImgBBResponse> PostPhoto(IFormFile image)
        {
            HttpClient client = new HttpClient();
            MultipartFormDataContent form = new MultipartFormDataContent();
            client.DefaultRequestHeaders.Clear();
            byte[] bytess;

            using (var memoryStream = new MemoryStream())
            {
                await image.CopyToAsync(memoryStream);
                bytess = memoryStream.ToArray();
            }

            HttpContent bytesContent = new ByteArrayContent(bytess);

            form.Add(new StringContent(_appSettings.ImgBBKey), "key");
            form.Add(bytesContent, "image", "image.jpg");

            HttpResponseMessage response = await client.PostAsync(_appSettings.ImgBBPostUrl, form);
            var k = response.Content.ReadAsStringAsync().Result;
            ImgBBResponse imgBBResponse = JsonConvert.DeserializeObject<ImgBBResponse>(k);

            if (imgBBResponse.status != 200)
                return null;

            return imgBBResponse;
        }
    }
}
