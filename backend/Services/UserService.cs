﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BC = BCrypt.Net.BCrypt;
using backend.Services;
using System.Security.Cryptography;
using backend.Entities;
using backend.Models;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using backend.Helpers;
using System.IO;
using backend.Models.Users;
using System.Net.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
/// <summary>
/// User Service Interface
/// </summary>
public interface IUserService
{
    /// <summary>
    /// Authenticate Method
    /// </summary>
    /// <param name="email">Email</param>
    /// <param name="password">Password</param>
    /// <returns>User</returns>
    User Authenticate(string email, string password);
    /// <summary>
    /// Create User
    /// </summary>
    /// <param name="user">User</param>
    /// <returns>User Created</returns>
    User CreateUser(CreateUserRequest user);
    /// <summary>
    /// Update User
    /// </summary>
    /// <param name="user">User</param>
    /// <returns>Updated User</returns>
    Task<User> UpdateUser(UpdateUserRequest user);
    /// <summary>
    /// Set Password
    /// </summary>
    /// <param name="email">Email</param>
    /// <param name="origin">Origin</param>
    void SetPassword(string email, string origin);
    /// <summary>
    /// Forgot Password Method
    /// </summary>
    /// <param name="email">Email</param>
    /// <param name="origin">Origin</param>
    void ForgotPassword(string email, string origin);
    /// <summary>
    /// Reset Password
    /// </summary>
    /// <param name="token">Token</param>
    /// <param name="password">Password</param>
    /// <param name="confirmPassword">Confirm Password</param>
    void ResetPassword(string token, string password, string confirmPassword);
    /// <summary>
    /// Generate JSON Web Token
    /// </summary>
    /// <param name="userInfo">User</param>
    /// <returns>Token</returns>
    string GenerateJSONWebToken(User userInfo);
    /// <summary>
    /// Get Users
    /// </summary>
    /// <param name="request_id">ID of who makes the request</param>
    /// <returns></returns>
    List<User> GetUsers(int request_id);
    /// <summary>
    /// Get User By ID
    /// </summary>
    /// <param name="getUserRequest">Get User Request Model</param>
    /// <returns>User</returns>
    User GetUserByID(GetUserRequest getUserRequest);
    /// <summary>
    /// Delete User using ID
    /// </summary>
    /// <param name="userIDRequest">Get User Request Model</param>
    /// <returns>User Deleted</returns>
    User Delete(GetUserRequest userIDRequest);
}

namespace backend.Services
{
    /// <summary>
    /// User Service
    /// </summary>
    public class UserService : IUserService
    {
        private IConfiguration _config;
        private AppDbContext _appDbContext;
        private IEmailService _emailService;
        private IImageService _imageService;
        /// <summary>
        /// User Service Constructor
        /// </summary>
        /// <param name="config">Config</param>
        /// <param name="context">Context</param>
        /// <param name="emailService">Email Service</param>
        /// <param name="imageService">ImageService</param>
        public UserService(IConfiguration config, AppDbContext context, IEmailService emailService,IImageService imageService)
        {
            _config = config;
            _appDbContext = context;
            _emailService = emailService;
            _imageService = imageService;
        }
        /// <summary>
        /// Authenticate Method
        /// </summary>
        /// <param name="email">Email</param>
        /// <param name="password">Password</param>
        /// <returns>User</returns>
        public User Authenticate(string email, string password)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
                return null;

            var user = _appDbContext.users.SingleOrDefault(user => user.email == email);

            if(user == null)
            {
                return null;
            }

            if (!BC.Verify(password, user.password))
                return null;

            return user;
        }
        /// <summary>
        /// Create User
        /// </summary>
        /// <param name="user">User</param>
        /// <returns>User Created</returns>
        public User CreateUser(CreateUserRequest user)
        {
            if (string.IsNullOrEmpty(user.email)|| string.IsNullOrEmpty(user.name) || user.role_id < 2 || user.role_id > 5)
                return null;

            var requestUser = _appDbContext.users.SingleOrDefault(x => x.id == user.request_id);
            if ( requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 4 || requestUser.role_id == 5)
                return null;

            User userCheck = VerifyEmail(user.email);

            if (userCheck != null)
                return null;

            string encriptedPassword = BC.HashPassword(randomString(8));

            if (ValidateAdmin(user.request_id))
            {
                User userToAdd = new User { name = user.name, email = user.email, password = encriptedPassword, role_id = user.role_id };

                _appDbContext.users.Add(userToAdd);

                _appDbContext.SaveChanges();

                User useradded = VerifyEmail(user.email);

                if (user.role_id == 2)
                {
                    _appDbContext.athletes_info.Add(new AthletesInfo {birth_date = DateTime.Parse(user.birth_date,System.Globalization.CultureInfo.InvariantCulture), user_id = useradded.id, personal_trainer_id = user.personal_trainer_id });
                }
                else 
                {
                    EmployeeInfo employeeInfo = new EmployeeInfo { user_id = useradded.id };
                    _appDbContext.employees_info.Add(employeeInfo);
                }

                return useradded;
            }
            else if (ValidateReceptionist(user.request_id))
            {
                if (user.role_id == 2)
                {
                    User userToAdd = new User { name = user.name, email = user.email, password = encriptedPassword, role_id = user.role_id };

                    _appDbContext.users.Add(userToAdd);

                    _appDbContext.SaveChanges();

                    User useradded = VerifyEmail(user.email);
                    
                    _appDbContext.athletes_info.Add(new AthletesInfo { birth_date = DateTime.Parse(user.birth_date, System.Globalization.CultureInfo.InvariantCulture), user_id = useradded.id, personal_trainer_id = user.personal_trainer_id});

                    return useradded;
                }
            }
            return null;
        }
        /// <summary>
        /// Set Password
        /// </summary>
        /// <param name="email">Email</param>
        /// <param name="origin">Origin</param>
        public void SetPassword(string email,string origin)
        {
            var user = VerifyEmail(email);

            if (user == null)
                return;

            user.reset_token = generateRandomToken();
            user.reset_token_expires = DateTime.UtcNow.AddMinutes(10);

            _appDbContext.users.Update(user);
            _appDbContext.SaveChanges();

            // send email
            sendSetPasswordEmail(user,origin);
        }
        /// <summary>
        /// Update User
        /// </summary>
        /// <param name="user">User</param>
        /// <returns>Updated User</returns>
        public async Task<User> UpdateUser(UpdateUserRequest user)
        {
            if (user.id <= 0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == user.id);
            if (requestUser == null)
                return null;

            if (user.avatar!= null)
            {
                ImgBBResponse imgBBResponse = await _imageService.PostPhoto(user.avatar);

                requestUser.avatar = imgBBResponse.data.display_url;
            }

            if (!string.IsNullOrEmpty(user.name))
                requestUser.name = user.name;
            if (!string.IsNullOrEmpty(user.password))
            {
                if (user.password == user.confirmPassword)
                {
                    string encriptedPassword = BC.HashPassword(user.password);
                    requestUser.password = encriptedPassword;

                }
                else
                {
                    return null;
                }
            }

            if (!string.IsNullOrEmpty(user.birth_date))
            {
                var athlete = _appDbContext.athletes_info.SingleOrDefault(athlete => athlete.user_id == user.id);
                if (athlete == null)
                    return null;

                athlete.birth_date = DateTime.Parse(user.birth_date, System.Globalization.CultureInfo.InvariantCulture);
                _appDbContext.athletes_info.Update(athlete);
            }

            List<AthletesInfo> athletesInfos = _appDbContext.athletes_info.ToList();
            _appDbContext.users.Update(requestUser);

            return requestUser;
        }
        /// <summary>
        /// Reset Password
        /// </summary>
        /// <param name="token">Token</param>
        /// <param name="password">Password</param>
        /// <param name="confirmPassword">Confirm Password</param>
        public void ResetPassword(string token, string password, string confirmPassword)
        {
            var user = ValidateResetParams(token, password, confirmPassword);

            if (user == null)
                return;

            user.password = BC.HashPassword(password);
            user.password_reset = DateTime.UtcNow;
            user.reset_token = null;
            user.reset_token_expires = null;

            _appDbContext.users.Update(user);
            _appDbContext.SaveChanges();
        }
        private User ValidateResetParams(string token, string password, string confirmPassword)
        {
            var user = _appDbContext.users.SingleOrDefault(user =>
                user.reset_token == token &&
                user.reset_token_expires > DateTime.UtcNow);

            if (user == null || !password.Equals(confirmPassword))
                return null;

            return user;
        }
        /// <summary>
        /// Forgot Password Method
        /// </summary>
        /// <param name="email">Email</param>
        /// <param name="origin">Origin</param>
        public void ForgotPassword(string email, string origin)
        {
            var user = VerifyEmail(email);

            if (user == null)
                return;

            user.reset_token = generateRandomToken();
            user.reset_token_expires = DateTime.UtcNow.AddMinutes(5);

            _appDbContext.users.Update(user);
            _appDbContext.SaveChanges();

            // send email
            sendResetPasswordEmail(user, origin);
        }
        private void sendSetPasswordEmail(User user, string origin)
        {
            var resetUrl = $"{origin}/{user.reset_token}";
            string html = File.ReadAllText("Layout/setPassword.html").Replace("<%= link %>", resetUrl).Replace("<%= token %>", user.reset_token);

            _emailService.Send(
                to: user.email,
                subject: "Definição de Password",
                html: html
            );
        }
        private User VerifyEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                return null;

            var user = _appDbContext.users.SingleOrDefault(user => user.email == email);

            return user;
        }
        private void sendResetPasswordEmail(User user, string origin)
        {
            var resetUrl = $"{origin}/{user.reset_token}";
            string html = File.ReadAllText("Layout/reset.html").Replace("<%= link %>", resetUrl);

            if (origin.Equals("mobile"))
            {
                resetUrl = $"{user.reset_token}";
                html = File.ReadAllText("Layout/resetMobile.html").Replace("<%= link %>", resetUrl);
            }
            _emailService.Send(
                to: user.email,
                subject: "Recuperação de Password",
                html: html
            );
        }
        private string generateRandomToken()
        {
            using var rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            var randomBytes = new byte[40];

            rngCryptoServiceProvider.GetBytes(randomBytes);

            return BitConverter.ToString(randomBytes).Replace("-", "");
        }
        /// <summary>
        /// Generate JSON Web Token
        /// </summary>
        /// <param name="userInfo">User</param>
        /// <returns>Token</returns>
        public string GenerateJSONWebToken(User userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Email,userInfo.email),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
            };

            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddMinutes(120),
                signingCredentials: credentials);

            var encodeToken = new JwtSecurityTokenHandler().WriteToken(token);

            return encodeToken;
        }
        /// <summary>
        /// Get Users
        /// </summary>
        /// <param name="request_id">ID of who makes the request</param>
        /// <returns></returns>
        public List<User> GetUsers(int request_id)
        {
            var requestUser = _appDbContext.users.SingleOrDefault(x => x.id == request_id);
            if (requestUser == null || requestUser.role_id == 2 )
                return null;

            List<EmployeeInfo> employeeInfos = _appDbContext.employees_info.ToList();
            List<AthletesInfo> athletesInfos = _appDbContext.athletes_info.ToList();
            List<MonthlyPlan> monthlyPlans = _appDbContext.monthly_plans.ToList();
            List<MemberPlan> memberPlans = _appDbContext.member_plans.ToList();
            List<User> users = _appDbContext.users.ToList();

            return users;
        }
        /// <summary>
        /// Get User By ID
        /// </summary>
        /// <param name="getUserRequest">Get User Request Model</param>
        /// <returns>User</returns>
        public User GetUserByID(GetUserRequest getUserRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == getUserRequest.request_id);

            if (requestUser == null)
                return null;


            if (requestUser.role_id == 2 && getUserRequest.request_id != getUserRequest.id)
                return null;

            if(requestUser.role_id == 2)
            {
                List<Meal> meals = _appDbContext.meals.ToList();
                List<NutritionalEvaluation> nutritionalEvaluations = _appDbContext.nutritional_evaluations.ToList();
                List<NutritionPlan> nutritionPlans = _appDbContext.nutrition_plans.ToList();
                List<PhysicalEvaluation> physicalEvaluations = _appDbContext.physical_evaluations.ToList();
            }
            List<EmployeeInfo> employeeInfos = _appDbContext.employees_info.ToList();
            List<AthletesInfo> athletesInfos = _appDbContext.athletes_info.ToList();
            List<MonthlyPlan> monthlyPlans = _appDbContext.monthly_plans.ToList();
            List<MemberPlan> memberPlans = _appDbContext.member_plans.ToList();

            User user = _appDbContext.users.SingleOrDefault(x => x.id == getUserRequest.id);

            return user;
        }
        /// <summary>
        /// Delete User using ID
        /// </summary>
        /// <param name="userIDRequest">Get User Request Model</param>
        /// <returns>User Deleted</returns>
        public User Delete(GetUserRequest userIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == userIDRequest.request_id);
            if (requestUser == null || requestUser.role_id != 1)
                return null;

            User userToRemove = _appDbContext.users.SingleOrDefault(x => x.id == userIDRequest.id);

            if (userToRemove != null)
                _appDbContext.users.Remove(userToRemove); ;

            return userToRemove;
        }

        private Boolean ValidateAdmin(int user_id)
        {
            var user = _appDbContext.users.SingleOrDefault(user => user.id == user_id);

            if (user == null || user.role_id != 1)
                return false;

            return true;
        }
        private Boolean ValidateReceptionist(int user_id)
        {
            var user = _appDbContext.users.SingleOrDefault(user => user.id == user_id);

            if (user == null || user.role_id != 3)
                return false;

            return true;
        }
        private static Random random = new Random();
        private static string randomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
