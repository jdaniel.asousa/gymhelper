﻿using backend.Entities;
using backend.Models;
using backend.Models.NutritionPlans;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/// <summary>
/// Nutrition Plan Service Interface
/// </summary>

public interface INutritionPlanService
{
    /// <summary>
    /// Create Nutrition Plan
    /// </summary>
    /// <param name="createNutritionPlanRequest">Create Nutrition Plan Request Model</param>
    /// <returns>Training Plan created</returns>
    NutritionPlan CreateNutritionPlan(CreateNutritionPlanRequest createNutritionPlanRequest);
    /// <summary>
    /// Updated Nutrition Plan
    /// </summary>
    /// <param name="updateNutritionPlanRequest">Update Nutrition Plan Request Model</param>
    /// <returns>Nutrition Plan Updated</returns>
    NutritionPlan UpdateNutritionPlan(UpdateNutritionPlanRequest updateNutritionPlanRequest);
    /// <summary>
    /// Get Nutrition Plans
    /// </summary>
    /// <param name="request_id">ID of who makes the request</param>
    /// <returns>List of Nutrition Plans</returns>
    List<NutritionPlan> GetNutritionPlans(int request_id);
    /// <summary>
    /// Get Nutrition Plan by ID
    /// </summary>
    /// <param name="nutritionPlanIDRequest">Nutrition Plan ID Request</param>
    NutritionPlan GetByID(NutritionPlanIDRequest nutritionPlanIDRequest);
    /// <summary>
    /// Delete Nutrition Plan using ID
    /// </summary>
    /// <param name="nutritionPlanIDRequest">Nutrition Plan ID Request Model</param>
    /// <returns>Nutrition Plan Deleted</returns>
    NutritionPlan Delete(NutritionPlanIDRequest nutritionPlanIDRequest);

    
}

namespace backend.Services
{
    /// <summary>
    /// Nutrition Plan Service
    /// </summary>
    public class NutritionPlanService : INutritionPlanService
    {
        private IConfiguration _config;
        private AppDbContext _appDbContext;
        /// <summary>
        /// Nutrition Plan Service Constructor
        /// </summary>
        /// <param name="config">Config</param>
        /// <param name="context">Context</param>
        public NutritionPlanService (IConfiguration config, AppDbContext context)
        {
            _config = config;
            _appDbContext = context;
        }
        /// <summary>
        /// Create Nutrition Plan
        /// </summary>
        /// <param name="createNutritionPlanRequest">Create Nutrition Plan Request Model</param>
        /// <returns>Nutrition Plan Created</returns>
        public NutritionPlan CreateNutritionPlan(CreateNutritionPlanRequest createNutritionPlanRequest)
        {
            if (createNutritionPlanRequest.request_id <=0 || createNutritionPlanRequest.nutritional_evaluation_id <=0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == createNutritionPlanRequest.request_id);
            if (requestUser == null || requestUser.role_id==1|| requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 4)
                return null;
            
            NutritionPlan requestNutritionPlan= _appDbContext.nutrition_plans.SingleOrDefault(x => x.nutritional_evaluation_id == createNutritionPlanRequest.nutritional_evaluation_id);
            if (requestNutritionPlan != null)
                return null;
            

             NutritionalEvaluation requestNutritionalEvaluation = _appDbContext.nutritional_evaluations.SingleOrDefault(x => x.id == createNutritionPlanRequest.nutritional_evaluation_id);
            if (requestNutritionalEvaluation == null)
                return null;
           
            NutritionPlan nutritionPlan = new NutritionPlan { total_calories = 0, total_protein = 0, total_fats = 0, total_carbohydrates = 0, nutritional_evaluation_id = requestNutritionalEvaluation.id};

            _appDbContext.nutrition_plans.Add(nutritionPlan);

            return nutritionPlan;
        }
        /// <summary>
        /// Delete Nutrition Plan using ID
        /// </summary>
        /// <param name="nutritionPlanIDRequest">Nutrition Plan ID Request Model</param>
        /// <returns>Deleted Nutrition Plan</returns>
        public NutritionPlan Delete(NutritionPlanIDRequest nutritionPlanIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == nutritionPlanIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 4)
                return null;

            NutritionPlan nutritionPlanToRemove= _appDbContext.nutrition_plans.SingleOrDefault(x => x.id == nutritionPlanIDRequest.nutrition_plan_id);

            if (nutritionPlanToRemove != null)
                _appDbContext.nutrition_plans.Remove(nutritionPlanToRemove); ;

            return nutritionPlanToRemove;

        }
        /// <summary>
        /// Get Nutrition Plan By ID
        /// </summary>
        /// <param name="nutritionPlanIDRequest">Nutrition Plan ID Request Model</param>
        /// <returns>Nutrition Plan</returns>
        public NutritionPlan GetByID(NutritionPlanIDRequest nutritionPlanIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == nutritionPlanIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 4)
                return null;

            List<Meal> meals = _appDbContext.meals.ToList();

            NutritionPlan nutritionPlan = _appDbContext.nutrition_plans.SingleOrDefault(x => x.id == nutritionPlanIDRequest.nutrition_plan_id);

            return nutritionPlan;
        }
        /// <summary>
        /// Get Nutrition Plans
        /// </summary>
        /// <param name="request_id">ID of who makes the request</param>
        /// <returns>List of Nutrition Plans</returns>
        public List<NutritionPlan> GetNutritionPlans(int request_id)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 4)
                return null;

            List<Meal> meals = _appDbContext.meals.ToList();

            List<NutritionPlan> nutritionPlans = _appDbContext.nutrition_plans.ToList();

            return nutritionPlans;
        }
        /// <summary>
        /// Update Nutrition Plan
        /// </summary>
        /// <param name="updateNutritionPlanRequest">Update Nutrition Plan Request Model</param>
        /// <returns>Nutrition Plan updated</returns>
        public NutritionPlan UpdateNutritionPlan(UpdateNutritionPlanRequest updateNutritionPlanRequest)
        {

            if (updateNutritionPlanRequest.request_id <= 0 || updateNutritionPlanRequest.nutritional_evaluation_id <= 0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == updateNutritionPlanRequest.request_id);
            if (requestUser == null || requestUser.role_id == 1 || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 4)
                return null;

            NutritionPlan requestNutritionPlan = _appDbContext.nutrition_plans.SingleOrDefault(x => x.id == updateNutritionPlanRequest.nutrition_plan_id);
            if(requestNutritionPlan == null)
                return null;

            if(updateNutritionPlanRequest.nutritional_evaluation_id > 0)
            {
                NutritionalEvaluation nutritionalEvaluation = _appDbContext.nutritional_evaluations.SingleOrDefault(x => x.id == updateNutritionPlanRequest.nutritional_evaluation_id);
                if (nutritionalEvaluation == null)
                    return null;

                requestNutritionPlan.nutritional_evaluation_id = updateNutritionPlanRequest.nutritional_evaluation_id;
            }

            _appDbContext.nutrition_plans.Update(requestNutritionPlan);

            return requestNutritionPlan;



        }
    }
}
