﻿using backend.Entities;
using backend.Models;
using backend.Models.ExerciseImages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/// <summary>
/// Exercise Image Service Interface
/// </summary>
public interface IExerviseImageService
{
    /// <summary>
    /// Create Exercise Image
    /// </summary>
    /// <param name="createExerciseImageRequest">Create Exercise Image Request Model</param>
    /// <returns>Exercise Image Created</returns>
    Task<ExerciseImage> CreateExerciseImage(CreateExerciseImageRequest createExerciseImageRequest);
    /// <summary>
    /// Update Exercise Image
    /// </summary>
    /// <param name="updateExerciseImageRequest">Update Exercise Image Request Model</param>
    /// <returns>Exercise Updated Image</returns>
    Task<ExerciseImage> UpdateExerciseImage(UpdateExerciseImageRequest updateExerciseImageRequest);
    /// <summary>
    /// Get Exercise Images
    /// </summary>
    /// <param name="request_id">Request ID</param>
    /// <returns>Exercise Images</returns>
    List<ExerciseImage> GetExerciseImages(int request_id);
    /// <summary>
    /// Get Exercise Images By ID
    /// </summary>
    /// <param name="exerciseImageIDRequest">Exercise Image ID Request Model</param>
    /// <returns>Exercise Imagine</returns>
    ExerciseImage GetByID(ExerciseImageIDRequest exerciseImageIDRequest);
    /// <summary>
    /// Delete
    /// </summary>
    /// <param name="exerciseImageIDRequest">Exercise ID Request Model</param>
    /// <returns>Exercise Image Deleted</returns>
    ExerciseImage Delete(ExerciseImageIDRequest exerciseImageIDRequest);
}

namespace backend.Services
{
    /// <summary>
    /// Exercise Image Service
    /// </summary>
    public class ExerciseImageService : IExerviseImageService
    {
        private AppDbContext _appDbContext;
        private IImageService _imageService;
        /// <summary>
        /// Exercise Image Service Constructor
        /// </summary>
        /// <param name="context">Context</param>
        /// <param name="imageService">ImageService</param>
        public ExerciseImageService(AppDbContext context, IImageService imageService)
        {
            _appDbContext = context;
            _imageService = imageService;
        }
        /// <summary>
        /// Create Exercise Image
        /// </summary>
        /// <param name="createExerciseImageRequest">Create Exercise Image Request Model</param>
        /// <returns>Exercise Image Created</returns>
        public async Task<ExerciseImage> CreateExerciseImage(CreateExerciseImageRequest createExerciseImageRequest)
        {
            if (createExerciseImageRequest.request_id <= 0 || createExerciseImageRequest.image == null || createExerciseImageRequest.exercise_id <= 0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == createExerciseImageRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            Exercise requestExercise = _appDbContext.exercises.SingleOrDefault(x => x.id == createExerciseImageRequest.exercise_id);
            if (requestExercise == null)
                return null;

            ImgBBResponse imgBBResponse = await _imageService.PostPhoto(createExerciseImageRequest.image);

            ExerciseImage exerciseImageToAdd = new ExerciseImage { path = imgBBResponse.data.display_url, exercise_id = createExerciseImageRequest.exercise_id };

            _appDbContext.exercise_images.Add(exerciseImageToAdd);

            return exerciseImageToAdd;
        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="exerciseImageIDRequest">Exercise ID Request Model</param>
        /// <returns>Exercise Image Deleted</returns>
        public ExerciseImage Delete(ExerciseImageIDRequest exerciseImageIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == exerciseImageIDRequest.request_id);
            if (requestUser == null || requestUser.role_id  == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            ExerciseImage exerciseImageToRemove = _appDbContext.exercise_images.SingleOrDefault(x => x.id == exerciseImageIDRequest.exercise_image_id);

            if (exerciseImageToRemove != null)
                _appDbContext.exercise_images.Remove(exerciseImageToRemove);

            return exerciseImageToRemove;
        }
        /// <summary>
        /// Get Exercise Images By ID
        /// </summary>
        /// <param name="exerciseImageIDRequest">Exercise Image ID Request Model</param>
        /// <returns>Exercise Imagine</returns>
        public ExerciseImage GetByID(ExerciseImageIDRequest exerciseImageIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == exerciseImageIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            List<Exercise> exercises = _appDbContext.exercises.ToList();
            ExerciseImage exerciseImage = _appDbContext.exercise_images.SingleOrDefault(x => x.id == exerciseImageIDRequest.exercise_image_id); ;

            return exerciseImage;
        }
        /// <summary>
        /// Get Exercise Images
        /// </summary>
        /// <param name="request_id">Request ID</param>
        /// <returns>Exercise Images</returns>
        public List<ExerciseImage> GetExerciseImages(int request_id)
        {
            if (request_id <= 0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            List<Exercise> exercises = _appDbContext.exercises.ToList();
            List<ExerciseImage> exerciseImages = _appDbContext.exercise_images.ToList();

            return exerciseImages;
        }
        /// <summary>
        /// Update Exercise Image
        /// </summary>
        /// <param name="updateExerciseImageRequest">Update Exercise Image Request Model</param>
        /// <returns>Exercise Updated Image</returns>
        public async Task<ExerciseImage> UpdateExerciseImage(UpdateExerciseImageRequest updateExerciseImageRequest)
        {
            if (updateExerciseImageRequest.request_id <= 0 || updateExerciseImageRequest.exercise_image_id <= 0)
                return null;

            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == updateExerciseImageRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id==5)
                return null;

            ExerciseImage requestExerciseImage = _appDbContext.exercise_images.SingleOrDefault(x => x.id == updateExerciseImageRequest.exercise_image_id);
            if (requestExerciseImage == null)
                return null;

            if (updateExerciseImageRequest.image != null)
            {
                ImgBBResponse imgBBResponse = await _imageService.PostPhoto(updateExerciseImageRequest.image);
                requestExerciseImage.path = imgBBResponse.data.display_url;
            }

            if (updateExerciseImageRequest.exercise_id > 0)
            {
                Exercise requestExercise = _appDbContext.exercises.SingleOrDefault(x => x.id == updateExerciseImageRequest.exercise_id);
                if (requestExercise == null)
                    return null;
                else
                    requestExerciseImage.exercise_id = requestExercise.id;
            }

            _appDbContext.exercise_images.Update(requestExerciseImage);

            return requestExerciseImage;
        }
    }
}
