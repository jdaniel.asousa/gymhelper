﻿using backend.Entities;
using backend.Models;
using backend.Models.Exercises;
using backend.Models.Machines;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/// <summary>
/// Exercise Service Interface
/// </summary>
public interface IExerciseService
{
    /// <summary>
    /// Create Exercise and add it to database service
    /// </summary>
    Exercise CreateExercise(CreateExerciseRequest createExerciseRequest);
    /// <summary>
    /// Update Exercise on database service
    /// </summary>
    Exercise UpdateExercise(UpdateExerciseRequest updateExerciseRequest);
    /// <summary>
    /// Get Exercises from database service
    /// </summary>
    List<Exercise> GetExercises(int request_id);
    /// <summary>
    /// Get an exercise from database service
    /// </summary>
    Exercise GetByID(ExerciseIDRequest exerciseIDRequest);
    /// <summary>
    /// Delete Exercise from database service
    /// </summary>
    Exercise Delete(ExerciseIDRequest exerciseIDRequest);
    /// <summary>
    /// Get Exercises of machine from database service 
    /// </summary>
    List<Exercise> GetExercisesOfMachine(MachineIDRequest machineIDRequest);
}

namespace backend.Services
{
    /// <summary>
    /// Exercise Service 
    /// </summary>
    public class ExerciseService : IExerciseService
    {
        private IConfiguration _config;
        private AppDbContext _appDbContext;

        /// <summary>
        /// Exercise Service Constructor
        /// </summary>
        public ExerciseService(IConfiguration config, AppDbContext context)
        {
            _config = config;
            _appDbContext = context;
        }
        /// <summary>
        /// Create Exercise and add it to database service
        /// </summary>
        public Exercise CreateExercise(CreateExerciseRequest createExerciseRequest)
        {
            if (string.IsNullOrEmpty(createExerciseRequest.request_id.ToString()) || string.IsNullOrEmpty(createExerciseRequest.type) || string.IsNullOrEmpty(createExerciseRequest.name) || string.IsNullOrEmpty(createExerciseRequest.muscle))
                return null;

            var requestUser = _appDbContext.users.SingleOrDefault(x => x.id == createExerciseRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            if (!createExerciseRequest.type.Equals("Reps") && !createExerciseRequest.type.Equals("Time"))
                return null;

            Machine machine = null;
            Exercise exerciseToAdd = null;
            if (createExerciseRequest.machine_id != 0)
            {
                machine = _appDbContext.machines.SingleOrDefault(x => x.id == createExerciseRequest.machine_id);

                if (machine == null || machine.state == false)
                    return null;
                else
                    exerciseToAdd = new Exercise { type = createExerciseRequest.type, name = createExerciseRequest.name, muscle = createExerciseRequest.muscle, video = createExerciseRequest.video, description = createExerciseRequest.description, machine_id = createExerciseRequest.machine_id };
            }
            else            
                exerciseToAdd = new Exercise { type = createExerciseRequest.type, name = createExerciseRequest.name, muscle = createExerciseRequest.muscle, video = createExerciseRequest.video, description = createExerciseRequest.description, machine_id = null };
            
            _appDbContext.exercises.Add(exerciseToAdd);

            return exerciseToAdd;
        }
        /// <summary>
        /// Delete Exercise from database service
        /// </summary>
        public Exercise Delete(ExerciseIDRequest exerciseIDRequest)
        {
            var requestUser = _appDbContext.users.SingleOrDefault(x => x.id == exerciseIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            Exercise exerciseToRemove = _appDbContext.exercises.SingleOrDefault(x => x.id == exerciseIDRequest.exercise_id);

            if (exerciseToRemove != null)
                _appDbContext.exercises.Remove(exerciseToRemove); ;

            return exerciseToRemove;
        }

        /// <summary>
        /// Get an exercise from database service
        /// </summary>
        public Exercise GetByID(ExerciseIDRequest exerciseIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == exerciseIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;


            List<MachineImage> machineImages = _appDbContext.machine_images.ToList();
            List<Machine> machines = _appDbContext.machines.ToList();
            List<ExerciseImage> exerciseImages = _appDbContext.exercise_images.ToList();
            Exercise exercise = _appDbContext.exercises.SingleOrDefault(x => x.id == exerciseIDRequest.exercise_id);

            return exercise;
        }
        /// <summary>
        /// Get Exercises from database service
        /// </summary>
        public List<Exercise> GetExercises(int request_id)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            List<MachineImage> machineImages = _appDbContext.machine_images.ToList();
            List<Machine> machines = _appDbContext.machines.ToList();
            List<ExerciseImage> exerciseImages = _appDbContext.exercise_images.ToList();
            List<Exercise> exercises = _appDbContext.exercises.ToList();

            return exercises;
        }
        /// <summary>
        /// Get Exercises of machine from database service 
        /// </summary>
        public List<Exercise> GetExercisesOfMachine(MachineIDRequest machineIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == machineIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            Machine requestMachine = _appDbContext.machines.SingleOrDefault(x => x.id == machineIDRequest.machine_id);
            if (requestMachine == null)
                return null;

            List<ExerciseImage> exerciseImages = _appDbContext.exercise_images.ToList();
            List<Exercise> exercises = _appDbContext.exercises.ToList();

            return requestMachine.exercises;
        }

        /// <summary>
        /// Update Exercise on database service
        /// </summary>
        public Exercise UpdateExercise(UpdateExerciseRequest updateExerciseRequest)
        {
            if (string.IsNullOrEmpty(updateExerciseRequest.request_id.ToString()) || string.IsNullOrEmpty(updateExerciseRequest.exercise_id.ToString()))
                return null;

            var requestUser = _appDbContext.users.SingleOrDefault(x => x.id == updateExerciseRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 5)
                return null;

            var requestExercise = _appDbContext.exercises.SingleOrDefault(x => x.id == updateExerciseRequest.exercise_id);
            if (requestExercise == null)
                return null;

            if (!string.IsNullOrEmpty(updateExerciseRequest.type))
                requestExercise.type = updateExerciseRequest.type;
            if (!string.IsNullOrEmpty(updateExerciseRequest.name))
                requestExercise.name = updateExerciseRequest.name;
            if (!string.IsNullOrEmpty(updateExerciseRequest.muscle))
                requestExercise.muscle = updateExerciseRequest.muscle;
            if (!string.IsNullOrEmpty(updateExerciseRequest.video))
                requestExercise.video = updateExerciseRequest.video;
            if (!string.IsNullOrEmpty(updateExerciseRequest.description))
                requestExercise.description = updateExerciseRequest.description;
            if (updateExerciseRequest.machine_id != null)
            {
                Machine machine = _appDbContext.machines.SingleOrDefault(x => x.id == updateExerciseRequest.machine_id);

                if (machine == null || machine.state == false)
                    return null;
                else
                    requestExercise.machine_id = updateExerciseRequest.machine_id;
            }
                


            _appDbContext.exercises.Update(requestExercise);

            return requestExercise;
        }
    }
}
