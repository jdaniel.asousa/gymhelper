﻿using backend.Entities;
using backend.Models;
using backend.Models.NutritionalEvaluations;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/// <summary>
/// Nutrition Evaluation Service Interface
/// </summary>
public interface INutritionalEvaluationService
{
    /// <summary>
    /// Create Nutrition Evaluation
    /// </summary>
    /// <param name="createNutritionalEvaluationRequest">Create Nutrition Evaluation Request Model</param>
    /// <returns>Nutrition Evaluation created</returns>
    NutritionalEvaluation CreateNutritionalEvaluation(CreateNutritionalEvaluationRequest createNutritionalEvaluationRequest);
    /// <summary>
    /// Update Nutritional Evaluation
    /// </summary>
    /// <param name="updateNutritionalEvaluationRequest"></param>
    /// <returns></returns>
    NutritionalEvaluation UpdateNutritionalEvaluation(UpdateNutritionalEvaluationRequest updateNutritionalEvaluationRequest);
    /// <summary>
    /// Gets All the Nutritional Evaluations
    /// </summary>
    /// <param name="request_id"></param>
    /// <returns></returns>
    List<NutritionalEvaluation> GetNutritionalEvaluations(int request_id);
    /// <summary>
    /// Get a Nutritional Evaluation
    /// </summary>
    /// <param name="nutritionalEvaluationIDRequest"></param>
    /// <returns></returns>
    NutritionalEvaluation GetByID(NutritionalEvaluationIDRequest nutritionalEvaluationIDRequest);
    /// <summary>
    /// Delete a Nutritional Evaluation
    /// </summary>
    /// <param name="nutritionalEvaluationIDRequest"></param>
    /// <returns></returns>
    NutritionalEvaluation Delete(NutritionalEvaluationIDRequest nutritionalEvaluationIDRequest);
    
}

namespace backend.Services
{   
    /// <summary>
    /// Nutrition Evaluation Service
    /// </summary>
    public class NutritionalEvaluationService : INutritionalEvaluationService
    {
        /// <summary>
        /// Nutritional Evaluation Service
        /// </summary>
        private IConfiguration _config;
        private AppDbContext _appDbContext;
        /// <summary>
        /// Nutritional Evaluation Service Constructor
        /// </summary>
        /// <param name="config"></param>
        /// <param name="context"></param>
        public NutritionalEvaluationService (IConfiguration config, AppDbContext context)
        {
            _config = config;
            _appDbContext = context;
        }
        /// <summary>
        /// Create Nutritional Evaluation
        /// </summary>
        /// <param name="createNutritionalEvaluationRequest"></param>
        /// <returns></returns>
        public NutritionalEvaluation CreateNutritionalEvaluation(CreateNutritionalEvaluationRequest createNutritionalEvaluationRequest)
        {
           
            if (string.IsNullOrEmpty(createNutritionalEvaluationRequest.request_id.ToString()) || string.IsNullOrEmpty(createNutritionalEvaluationRequest.schedule)|| string.IsNullOrEmpty(createNutritionalEvaluationRequest.athlete_id.ToString())||string.IsNullOrEmpty(createNutritionalEvaluationRequest.nutricionist_id.ToString()))
                return null;
           

            var requestUser = _appDbContext.users.SingleOrDefault(x => x.id == createNutritionalEvaluationRequest.request_id);
            if (requestUser == null || requestUser.role_id == 1|| requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 4)
                return null;
           

            NutritionalEvaluation nutritionalEvaluationToAdd = null;
            EmployeeInfo nutritionist = null;
            AthletesInfo athlete = null; 
            

            if(createNutritionalEvaluationRequest.athlete_id != 0)
            {
                athlete = _appDbContext.athletes_info.SingleOrDefault(x => x.id == createNutritionalEvaluationRequest.athlete_id);
            }

            if(createNutritionalEvaluationRequest.request_id != 0)
            {
                nutritionist = _appDbContext.employees_info.SingleOrDefault(x => x.user_id == createNutritionalEvaluationRequest.request_id);
            }

            if (athlete == null || nutritionist == null)
                return null;
            
            nutritionalEvaluationToAdd = new NutritionalEvaluation { suggestions = createNutritionalEvaluationRequest.suggestions, schedule = DateTime.Parse(createNutritionalEvaluationRequest.schedule, System.Globalization.CultureInfo.InvariantCulture), athlete_id = createNutritionalEvaluationRequest.athlete_id, nutricionist_id = nutritionist.id };
            
            _appDbContext.nutritional_evaluations.Add(nutritionalEvaluationToAdd);
                
            return nutritionalEvaluationToAdd; 
        }
            /// <summary>
            /// Delete Nutritional Evaluation using ID
            /// </summary>
            /// <param name="nutritionalEvaluationIDRequest"></param>
            /// <returns></returns>
            public NutritionalEvaluation Delete(NutritionalEvaluationIDRequest nutritionalEvaluationIDRequest)
        {
            var requestUser = _appDbContext.users.SingleOrDefault(x => x.id == nutritionalEvaluationIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 4)
                return null;

            NutritionalEvaluation nutritionalEvaluationToRemove = _appDbContext.nutritional_evaluations.SingleOrDefault(x => x.id == nutritionalEvaluationIDRequest.nutritional_evaluation_id);

            if (nutritionalEvaluationToRemove != null)
                _appDbContext.nutritional_evaluations.Remove(nutritionalEvaluationToRemove);
            return nutritionalEvaluationToRemove;
        }
        /// <summary>
        /// Get Nutritional Evaluation by ID
        /// </summary>
        /// <param name="nutritionalEvaluationIDRequest"></param>
        /// <returns></returns>

        public NutritionalEvaluation GetByID(NutritionalEvaluationIDRequest nutritionalEvaluationIDRequest)
        {
            User requestUser = _appDbContext.users.SingleOrDefault(x => x.id == nutritionalEvaluationIDRequest.request_id);
            if (requestUser == null || requestUser.role_id == 3 || requestUser.role_id == 4)
                return null;

            List<NutritionPlan> nutritionPlans = _appDbContext.nutrition_plans.ToList();
            List<AthletesInfo> athletesInfos = _appDbContext.athletes_info.ToList();
            List<EmployeeInfo> employeeInfos = _appDbContext.employees_info.ToList();
            List<User> users = _appDbContext.users.ToList();
            List<Meal> meals = _appDbContext.meals.ToList();

            NutritionalEvaluation nutritionalEvaluation = _appDbContext.nutritional_evaluations.SingleOrDefault(x => x.id == nutritionalEvaluationIDRequest.nutritional_evaluation_id);

            if(requestUser.role_id == 2)
            {
                if (nutritionalEvaluation != null)
                {
                    if (nutritionalEvaluation.athlete_id != requestUser.athlete_info.id)
                    {
                        return null;
                    }
                }
            }

            return nutritionalEvaluation;
        }
        /// <summary>
        /// Get Nutritional Evaluations
        /// </summary>
        /// <param name="request_id"></param>
        /// <returns></returns>

        public List<NutritionalEvaluation> GetNutritionalEvaluations(int request_id)
        {
            var requestUser = _appDbContext.users.SingleOrDefault(x => x.id == request_id);
            if (requestUser == null || requestUser.role_id == 2 || requestUser.role_id == 3 || requestUser.role_id == 4)
                return null;

            List<NutritionPlan> nutritionPlans = _appDbContext.nutrition_plans.ToList();
            List<AthletesInfo> athletesInfos = _appDbContext.athletes_info.ToList();
            List<EmployeeInfo> employeeInfos = _appDbContext.employees_info.ToList();
            List<User> users = _appDbContext.users.ToList();
            List<Meal> meals = _appDbContext.meals.ToList();

            List<NutritionalEvaluation> nutritionalEvaluations = _appDbContext.nutritional_evaluations.ToList();

            return nutritionalEvaluations;
        }
        /// <summary>
        /// Updates a Nutritional Evaluation
        /// </summary>
        /// <param name="updateNutritionalEvaluationRequest"></param>
        /// <returns></returns>
        public NutritionalEvaluation UpdateNutritionalEvaluation(UpdateNutritionalEvaluationRequest updateNutritionalEvaluationRequest)
        {

            if (string.IsNullOrEmpty(updateNutritionalEvaluationRequest.request_id.ToString()) || string.IsNullOrEmpty(updateNutritionalEvaluationRequest.nutritional_evaluation_id.ToString()))
                return null;


            var requestUser = _appDbContext.users.SingleOrDefault(x => x.id == updateNutritionalEvaluationRequest.request_id);
            if (requestUser == null || requestUser.role_id != 5)
                return null;

            NutritionalEvaluation requestNutritionEvaluation = _appDbContext.nutritional_evaluations.SingleOrDefault(x => x.id == updateNutritionalEvaluationRequest.nutritional_evaluation_id);
            if (requestNutritionEvaluation == null)
                return null;

            if (!string.IsNullOrEmpty(updateNutritionalEvaluationRequest.suggestions))
                requestNutritionEvaluation.suggestions =updateNutritionalEvaluationRequest.suggestions;

            if (!string.IsNullOrEmpty(updateNutritionalEvaluationRequest.schedule))
                requestNutritionEvaluation.schedule = DateTime.Parse(updateNutritionalEvaluationRequest.schedule);

            if(updateNutritionalEvaluationRequest.athlete_id != 0)
            {
                AthletesInfo athletesInfo = _appDbContext.athletes_info.SingleOrDefault(x => x.id == updateNutritionalEvaluationRequest.athlete_id);
                if (athletesInfo == null)
                    return null;

                requestNutritionEvaluation.athlete_id = updateNutritionalEvaluationRequest.athlete_id;

            }

            _appDbContext.nutritional_evaluations.Update(requestNutritionEvaluation);

            return requestNutritionEvaluation;

        }



    }


}
