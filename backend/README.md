## Execute Project

dotnet run

## Application Settings

```json
{
  "AppSettings": {
    "Secret": "SECRET_KEY",
    "EmailFrom": "EMAIL",
    "SmtpHost": "HOST",
    "SmtpPort": PORT,
    "SmtpUser": "USER",
    "SmtpPass": "PASSWORD"
  },
  "ConnectionStrings": {
    "DefaultConnection": "server=SERVER;port=PORT;database=DB;uid=ID;password=PASSWORD;default command timeout=300"
  },
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*",

  "Jwt": {
    "Key": KEY,
    "Issuer": ISSUER
  }
}
```
