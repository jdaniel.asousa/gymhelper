﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    /// <summary>
    /// Exercise Entity
    /// </summary>
    public class Exercise
    {
        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Type
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// Muscle
        /// </summary>
        public string muscle { get; set; }
        /// <summary>
        /// Video
        /// </summary>
        public string video { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// Machine ID
        /// </summary>
        public int? machine_id { get; set; }
        /// <summary>
        /// Machine
        /// </summary>
        public Machine machine { get; set; }
        /// <summary>
        /// List of Exercise Images
        /// </summary>
        public List<ExerciseImage> exercise_images { get; set; }
        /// <summary>
        /// List of Training Exercises
        /// </summary>
        public List<TrainingExercise> training_exercises { get; set; }
    }
}
