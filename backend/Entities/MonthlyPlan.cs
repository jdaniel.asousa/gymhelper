﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    /// <summary>
    /// Monthly Plan Entity
    /// </summary>
    public class MonthlyPlan
    {
        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// Price
        /// </summary>
        public float price { get; set; }
        /// <summary>
        /// Entrys
        /// </summary>
        public int entrys { get; set; }
        /// <summary>
        /// Member Plans list
        /// </summary>
        public List<MemberPlan> member_plans { get; set; }



    }
}
