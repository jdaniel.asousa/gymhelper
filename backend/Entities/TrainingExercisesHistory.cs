﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    /// <summary>
    /// Training Exercises History
    /// </summary>
    public class TrainingExercisesHistory
    {
        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Training Exercise ID
        /// </summary>
        public int training_exercise_id { get; set; }
        /// <summary>
        /// Training Exercise
        /// </summary>
        public TrainingExercise training_exercise { get; set; }
        /// <summary>
        /// Time
        /// </summary>
        public String time { get; set; }
        /// <summary>
        /// Repetitions
        /// </summary>
        public String repetitions { get; set; }
        /// <summary>
        /// Date of the register
        /// </summary>
        public DateTime created_at { get; set; }
    }
}
