﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    /// <summary>
    /// Machine
    /// </summary>
    public class Machine
    {
        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// State
        /// </summary>
        public Boolean state { get; set; }
        /// <summary>
        /// QR Code
        /// </summary>
        public string qrcode { get; set; }
        /// <summary>
        /// List of Machine Images
        /// </summary>
        public List<MachineImage> machine_images { get; set; }
        /// <summary>
        /// List of exerciese
        /// </summary>
        public List<Exercise> exercises { get; set; }
    }
}
