﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    /// <summary>
    /// Nutrition Plan
    /// </summary>
    public class NutritionPlan
    {
        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Total Calories
        /// </summary>
        public float total_calories { get; set; }
        /// <summary>
        /// Total Protein
        /// </summary>
        public float total_protein { get; set; }
        /// <summary>
        /// Total Fats
        /// </summary>
        public float total_fats { get; set; }
        /// <summary>
        /// Total Carbohydrates
        /// </summary>
        public float total_carbohydrates { get; set; }
        /// <summary>
        /// Nutritional Evaluation ID
        /// </summary>
        public int nutritional_evaluation_id { get; set; }
        /// <summary>
        /// Nutritional Evaluation
        /// </summary>
        public NutritionalEvaluation nutritional_evaluation { get; set; }
        /// <summary>
        /// List of Meals
        /// </summary>
        public List<Meal> meals { get; set; }
    }
} 
