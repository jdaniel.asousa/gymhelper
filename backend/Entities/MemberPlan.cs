﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    /// <summary>
    /// Member Plan Entity
    /// </summary>
    public class MemberPlan
    {
        /// <summary>
        /// Member Plan ID
        /// </summary>
        public int id {get;set;}
        /// <summary>
        /// Valid Entrys
        /// </summary>
        public int valid_entrys { get; set; }
        /// <summary>
        /// Entry Month
        /// </summary>
        public DateTime entry_date { get; set; }
        /// <summary>
        /// Expired Date
        /// </summary>
        public DateTime expired_date { get; set; }
        /// <summary>
        /// Monthly Plan
        /// </summary>
        public MonthlyPlan monthlyPlan { get; set; }
        /// <summary>
        /// Monthly Plan ID
        /// </summary>
        public int monthly_plan_id { get; set; }
        /// <summary>
        /// Athlete Info
        /// </summary>
        public AthletesInfo athletesInfo { get; set; }
        /// <summary>
        /// Athlete ID
        /// </summary>
        public int athlete_id { get; set; }
        



    }
}
