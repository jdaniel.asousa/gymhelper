﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    /// <summary>
    /// Athletes Info Entity
    /// </summary>
    public class AthletesInfo
    { 
        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Birth Date
        /// </summary>
        public DateTime birth_date { get; set; }
        /// <summary>
        /// User ID
        /// </summary>
        public int user_id { get; set; }
        /// <summary>
        /// User
        /// </summary>
        public User user { get; set; }
        /// <summary>
        /// Personal Trainer ID
        /// </summary>
        public int personal_trainer_id { get; set; }
        /// <summary>
        /// Personal Trainer
        /// </summary>
        public EmployeeInfo personal_trainer { get; set; }
        /// <summary>
        /// List of Physical Evaluations
        /// </summary>
        public List<PhysicalEvaluation> physical_evaluations { get; set; }
        /// <summary>
        /// List of Nutritional Evaluations
        /// </summary>
        public List<NutritionalEvaluation> nutritional_evaluations { get; set; }
        /// <summary>
        /// List of Training Plans
        /// </summary>
        public List<TrainingPlan> training_plans { get; set; }
        /// <summary>
        /// List of Member Plans
        /// </summary>
        public List<MemberPlan> member_plans { get; set; }
    }
}
