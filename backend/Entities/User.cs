﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    /// <summary>
    /// User Entity
    /// </summary>
    public class User
    {
        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string email { get; set; }
        /// <summary>
        /// Password Encrypted
        /// </summary>
        public string password { get; set; }
        /// <summary>
        /// Reset token
        /// </summary>
        public string reset_token { get; set; }
        /// <summary>
        /// Reset Token Expiration Date
        /// </summary>
        public DateTime? reset_token_expires { get; set; }
        /// <summary>
        /// Reset Password Date
        /// </summary>
        public DateTime? password_reset { get; set; }
        /// <summary>
        /// Avatar
        /// </summary>
        public string avatar { get; set; }
        /// <summary>
        /// Role ID
        /// </summary>
        public int role_id { get; set; }
        /// <summary>
        /// Role
        /// </summary>
        public Role role { get; set; }
        /// <summary>
        /// Athlete Info
        /// </summary>
        public AthletesInfo athlete_info { get; set; }
        /// <summary>
        /// Employee Info
        /// </summary>
        public EmployeeInfo employee_info { get; set; }
    }
}
