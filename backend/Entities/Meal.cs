﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    /// <summary>
    /// Meal Entity
    /// </summary>
    public class Meal
    {
        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Calories
        /// </summary>
        public float calories { get; set; }
        /// <summary>
        /// Protein
        /// </summary>
        public float protein { get; set; }
        /// <summary>
        /// Fats
        /// </summary>
        public float fats { get; set; }
        /// <summary>
        /// Carbohydrates
        /// </summary>
        public float carbohydrates { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// Nutrition Plan ID
        /// </summary>
        public int nutrition_plan_id { get; set; }
        /// <summary>
        /// Nutrition Plan
        /// </summary>
        public NutritionPlan nutrition_plan { get; set; }
    }
}
