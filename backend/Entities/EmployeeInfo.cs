﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    /// <summary>
    /// EmployeeInfo
    /// </summary>
    public class EmployeeInfo
    {
        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// User ID
        /// </summary>
        public int user_id { get; set; }
        /// <summary>
        /// User
        /// </summary>
        public User user { get; set; }
        /// <summary>
        /// List of athletes
        /// </summary>
        public List<AthletesInfo> athletes { get; set; }
        /// <summary>
        /// List of nutritional evaluations
        /// </summary>
        public List<NutritionalEvaluation> nutritional_evaluations { get; set; }
    }
}
