﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    /// <summary>
    /// Training Plan Entity
    /// </summary>
    public class TrainingPlan
    {
        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// List of Training Exercises
        /// </summary>
        public List<TrainingExercise> training_exercises { get; set; }
        /// <summary>
        /// Athlete ID
        /// </summary>
        public int athlete_id { get; set; }
        /// <summary>
        /// Athlete
        /// </summary>
        public AthletesInfo athlete { get; set; }
    }
}
