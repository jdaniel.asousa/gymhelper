﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    /// <summary>
    /// Exercise Image
    /// </summary>
    public class ExerciseImage
    {
        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Path
        /// </summary>
        public string path { get; set; }
        /// <summary>
        /// Exercise ID
        /// </summary>
        public int exercise_id { get; set; }
        /// <summary>
        /// Exercise
        /// </summary>
        public Exercise exercise { get; set; }
    }
}
