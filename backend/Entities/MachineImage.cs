﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    /// <summary>
    /// Machine Image
    /// </summary>
    public class MachineImage
    {
        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Path
        /// </summary>
        public string path { get; set; }
        /// <summary>
        /// Machine ID
        /// </summary>
        public int machine_id { get; set; }
        /// <summary>
        /// Machine
        /// </summary>
        public Machine machine { get; set; }
    }
}
