﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    /// <summary>
    /// Training Exercise Entity
    /// </summary>
    public class TrainingExercise
    {
        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Training Plan ID
        /// </summary>
        public int plan_id { get; set; }
        /// <summary>
        /// Training Plan 
        /// </summary>
        public TrainingPlan plan { get; set; }
        /// <summary>
        /// Exercise ID
        /// </summary>
        public int exercise_id { get; set; }
        /// <summary>
        /// Exercise
        /// </summary>
        public Exercise exercise { get; set; }
        /// <summary>
        /// Circuit
        /// </summary>
        public int circuit { get; set; }
        /// <summary>
        /// Time
        /// </summary>
        public String time { get; set; }
        /// <summary>
        /// Repetitions
        /// </summary>
        public String repetitions { get; set; }
        /// <summary>
        /// Sets
        /// </summary>
        public int sets { get; set; }
        /// <summary>
        /// List of Training Exercise History
        /// </summary>
        public List<TrainingExercisesHistory> training_exercises_history { get; set; }
    }
}
