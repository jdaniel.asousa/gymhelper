﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    /// <summary>
    /// Nutritional Evaluation
    /// </summary>
    public class NutritionalEvaluation
    {
        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Suggestions
        /// </summary>
        public string suggestions { get; set; }
        /// <summary>
        /// Schedule
        /// </summary>
        public DateTime schedule { get; set; }
        /// <summary>
        /// Nutrition Plan
        /// </summary>
        public NutritionPlan nutrition_plan { get; set; }
        /// <summary>
        /// Athlete ID
        /// </summary>
        public int athlete_id { get; set; }
        /// <summary>
        /// Athlete
        /// </summary>
        public AthletesInfo athlete { get; set; }
        /// <summary>
        /// Nutritionist ID
        /// </summary>
        public int nutricionist_id { get; set; }
        /// <summary>
        /// Nutritionist
        /// </summary>
        public EmployeeInfo nutricionist { get; set; }
    }
}
