﻿using backend.Entities;
using backend.Models;
using backend.Models.ExerciseImages;
using backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Controllers
{
    /// <summary>
    /// Exercise Image Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ExerciseImageController : ControllerBase
    {
        private IExerviseImageService _exerciseImageService;
        private readonly AppDbContext _appDbContext;
        /// <summary>
        /// Exercise Image Controller Constructor
        /// </summary>
        public ExerciseImageController(IExerviseImageService exerciseImageService, AppDbContext appDbContext)
        {
            _exerciseImageService = exerciseImageService;
            _appDbContext = appDbContext;
        }
        /// <summary>
        /// Cretes a exercise image, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /create-exercise-image
        ///     {
        ///        "request_id": request_id,
        ///        "image": "image",
        ///        "exercise_id": exercise_id
        ///     }
        ///
        /// </remarks> 
        /// <param name="createExerciseImageRequest">Json object with request ID, image and exercise id</param>
        /// <returns>If the exercise Image was created, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The exercise image was created </response>
        /// <response code="400">The request was invalid, so the exercise image wasn't created, verify your role or the params</response>
        [Authorize]
        [HttpPost("create-exercise-image")]
        public async Task<ActionResult<ExerciseImage>> CreateMachine([FromForm] CreateExerciseImageRequest createExerciseImageRequest)
        {
            ExerciseImage exerciseImageCreated = _exerciseImageService.CreateExerciseImage(createExerciseImageRequest).Result;

            if (exerciseImageCreated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Exercise Image created.",
                exerciseImageCreated
            });
        }
        /// <summary>
        /// Updates a exercise image, if authorized and if it exists
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /update-exercise-image
        ///     {
        ///        "request_id": request_id,
        ///        "exercise_image_id": exercise_image_id,
        ///        "image": image, -> not required
        ///        "exercise_id": exercise_id -> not required
        ///     }
        ///
        /// </remarks> 
        /// <param name="updateExerciseImageRequest">Json object with request ID, exercise_image_id, image and exercise_id</param>
        /// <returns>If the exercise image was updated, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The exercise image was updated </response>
        /// <response code="400">The request was invalid, so the exercise image wasn't updated, verify your role or the params</response>
        [Authorize]
        [HttpPut("update-exercise-image")]
        public async Task<ActionResult<ExerciseImage>> UpdateExerciseImage([FromForm] UpdateExerciseImageRequest updateExerciseImageRequest)
        {
            ExerciseImage exerciseImageUpdated = await _exerciseImageService.UpdateExerciseImage(updateExerciseImageRequest);

            if (exerciseImageUpdated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Exercise Image updated.",
                exerciseImageUpdated
            });
        }
        /// <summary>
        /// Gets all the exercise images in the system, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /exercise-images
        ///
        /// </remarks>
        /// <returns>Ok if the exercise images were returned, NotAutorized if not authorized</returns>       
        /// <param name="request_id">ID of who makes the request.</param>
        /// <response code="200">The exercise images were returned </response>
        /// <response code="401">Not Authorized</response>
        [Authorize]
        [HttpGet("exercise-images")]
        public ActionResult<ExerciseImage> GetAll(int request_id)
        {
            List<ExerciseImage> exerciseImages = _exerciseImageService.GetExerciseImages(request_id);

            if (exerciseImages == null)
                return Unauthorized(new { message = "Invalid request. Check your role." });

            return Ok(exerciseImages);
        }
        /// <summary>
        /// Gets a exercise image by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /exercise-image
        ///
        /// </remarks>
        /// <returns>Ok if the exercise image was returned, BadRequest if not authorized or exercise_id not valid</returns>       
        /// <param name="request_id">ID of who makes the request</param>
        /// <param name="id">ID of the exercise image to get</param>
        /// <response code="200">The exercise image was returned </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpGet("exercise-image")]
        public ActionResult<Machine> GetByID(int request_id, int id)
        {
            ExerciseImageIDRequest exerciseImageIDRequest = new ExerciseImageIDRequest { request_id = request_id, exercise_image_id = id };
            ExerciseImage exerciseImage = _exerciseImageService.GetByID(exerciseImageIDRequest);

            if (exerciseImage == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            return Ok(exerciseImage);
        }
        /// <summary>
        /// Deletes a exercise image by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /exercise-image
        ///      {
        ///        "request_id": id,
        ///        "exercise_image_id": id,
        ///      }
        ///
        /// </remarks>
        /// <returns>Ok if the exercise image was deleted, BadRequest if not authorized or exercise_id not valid</returns>       
        /// <param name="exerciseImageIDRequest">ID of who makes the request and ID of the exercise image to delete.</param>
        /// <response code="200">The exercise image was deleted </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpDelete("exercise-image")]
        public async Task<ActionResult<ExerciseImage>> Delete([FromBody] ExerciseImageIDRequest exerciseImageIDRequest)
        {
            ExerciseImage exerciseImage = _exerciseImageService.Delete(exerciseImageIDRequest);

            if (exerciseImage == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new { message = "Exercise Image deleted.", exerciseImage });
        }
    }
}
