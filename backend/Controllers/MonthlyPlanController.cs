﻿using backend.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using backend.Entities;
using backend.Models.MonthlyPlans;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;

namespace backend.Controllers
{
    /// <summary>
    /// Monthly Plan Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MonthlyPlanController : ControllerBase
    {

        private IMonthlyPlanService _monthlyPlanService;
        private readonly AppDbContext _appDbContext;
        /// <summary>
        /// Monthly Plan Constructor
        /// </summary>
        /// <param name="monthlyPlanService">Monthly Plan Service</param>
        /// <param name="appDbContext">Database Context</param>
        public MonthlyPlanController(IMonthlyPlanService monthlyPlanService,AppDbContext appDbContext)
        {
            _monthlyPlanService = monthlyPlanService;
            _appDbContext = appDbContext;
        }
        /// <summary>
        /// Creates a monthly plan if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /create-monthly-plan
        ///     {
        ///        "request_id": request_id,
        ///        "name": "name,
        ///        "description": "description", -> not required
        ///        "price": price,
        ///        "entrys": entrys
        /// }
        ///
        /// </remarks> 
        /// <param name="createMonthlyPlanRequest">Create Monthly Plan Request Model</param>
        /// <returns>Monthly Plan Created. BadRequest if not authorized or monthly plan not valid</returns>
        /// <response code="200">The monthly plan was created </response>
        /// <response code="401">Not Authorized</response>
        [Authorize]
        [HttpPost("create-monthly-plan")]
        public async Task<ActionResult<MonthlyPlan>> CreateMonthlyPlan([FromBody]CreateMonthlyPlanRequest createMonthlyPlanRequest)
        {
            MonthlyPlan monthlyPlanCreated = _monthlyPlanService.CreateMonthlyPlan(createMonthlyPlanRequest);

            if(monthlyPlanCreated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Monthly plan created.",
                monthlyPlanCreated
            });

        }
        /// <summary>
        /// Deletes a Monthly Plan by ID, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /monthly-plan
        ///      {
        ///        "request_id": id,
        ///        "monthly_plan_id": id
        ///      }
        ///
        /// </remarks>
        /// <param name="monthlyPlanIDRequest">Monthly Plan ID Request Model</param>
        /// <returns>Monthly Plan deleted. BadRequest if not authorized or monthly plan not valid</returns>
        /// <response code="200">The monthly plan was returned </response>
        /// <response code="401">Not Authorized</response>
        [Authorize]
        [HttpDelete("monthly-plan")]
        public async Task<ActionResult<MonthlyPlan>> Delete([FromBody] MonthlyPlanIDRequest monthlyPlanIDRequest)
        {
            MonthlyPlan monthlyPlan = _monthlyPlanService.Delete(monthlyPlanIDRequest);

            if (monthlyPlan == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Monthly Plan deleted.",
                monthlyPlan
            });
        }
        /// <summary>
        /// Gets a list of monthly plans, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /monthly-plans
        ///
        /// </remarks>
        /// <param name="request_id">ID of who makes the request</param>
        /// <returns>List of Monthly Plans. BadRequest if not authorized or monthly plan not valid</returns>
        /// <response code="200">The monthly plans were returned </response>
        /// <response code="401">Not Authorized</response>

        [Authorize]
        [HttpGet("monthly-plans")]

        public ActionResult<MonthlyPlan> GetAll(int request_id)
        {
            List<MonthlyPlan> monthlyPlans = _monthlyPlanService.GetMonthlyPlans(request_id);

            if (monthlyPlans == null)
                return Unauthorized(new { message = "Invalid request. Check your role." });

            return Ok(monthlyPlans);
        }
        /// <summary>
        /// Get a monthly plan by ID, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /monthly-plan
        ///
        /// </remarks>
        /// <param name="request_id">ID of who makes the request</param>
        /// <param name="id">ID of monthly plan</param>
        /// <returns>Ok if monthly plan was returned, BadRequest if not authorized or monthly plan not valid</returns>
        /// <response code="200">The monthly plan was returned </response>
        /// <response code="400">Not Authorized or bad request</response>

        [Authorize]
        [HttpGet("monthly-plan")]
        public ActionResult<MonthlyPlan> GetByID(int request_id, int id)
        {
            MonthlyPlanIDRequest monthlyPlanIDRequest = new MonthlyPlanIDRequest { request_id = request_id, monthly_plan_id = id };
            MonthlyPlan monthlyPlan= _monthlyPlanService.GetByID(monthlyPlanIDRequest);

            if (monthlyPlan == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            return Ok(monthlyPlan);
        }
        /// <summary>
        /// Updates a Monthly Plan, if authorized and if it exists
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /update-monthly-plan
        ///     {
        ///        "request_id": request_id,
        ///        "monthly-plan-id": monthly-plan-id,
        ///        "name": "name", -> not required
        ///        "description" : "description" -> not required
        ///        "price": price -> not required
        ///        "entrys": entrys -> not required
        ///     }
        ///
        /// </remarks>
        /// <param name="updateMonthlyPlanRequest">Update MOnthly Plan Request Model</param>
        /// <returns>The monthly plan was updated. ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The nutrition plan was updated </response>
        /// <response code="400">The request was invalid, so the nutritional plan wasn't updated, verify your role or the params</response>
        [Authorize]
        [HttpPut("update-monthly-plan")]
        public async Task<ActionResult<MonthlyPlan>> UpdateMonthlyPlan([FromBody] UpdateMonthlyPlanRequest updateMonthlyPlanRequest)
        {
            MonthlyPlan monthlyPlanUpdated = _monthlyPlanService.UpdateMonthlyPlan(updateMonthlyPlanRequest);

            if (monthlyPlanUpdated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Monthly Plan updated.",
                monthlyPlanUpdated
            });
        }

    }
}
