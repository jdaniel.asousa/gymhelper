﻿using backend.Entities;
using backend.Models;
using backend.Models.Machines;
using backend.Models.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Controllers
{
    /// <summary>
    /// Machine Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MachineController : ControllerBase
    {
        private IMachineService _machineService;
        private readonly AppDbContext _appDbContext;
        /// <summary>
        /// Machine Controller Constructor
        /// </summary>
        public MachineController(IMachineService machineService, AppDbContext appDbContext)
        {
            _machineService = machineService;
            _appDbContext = appDbContext;
        }

        /// <summary>
        /// Cretes a machine, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /create-machine
        ///     {
        ///        "request_id": request_id,
        ///        "name": "Name",
        ///        "state": state
        ///     }
        ///
        /// </remarks> 
        /// <param name="createMachineRequest">Json object with request ID, name, state and qrcode</param>
        /// <returns>If the machine was created, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The machine was created </response>
        /// <response code="400">The request was invalid, so the machine wasn't created, verify your role or the params</response>
        [Authorize]
        [HttpPost("create-machine")]
        public async Task<ActionResult<Machine>> CreateMachine([FromBody] CreateMachineRequest createMachineRequest)
        {
            Machine machineCreated = _machineService.CreateMachine(createMachineRequest);

            if(machineCreated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            MachineIDRequest machineIDRequest = new MachineIDRequest { request_id = createMachineRequest.request_id, machine_id = machineCreated.id };

            Machine machineUpdated = await _machineService.UpdateMachineQRCode(machineIDRequest);

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Machine created.",
                id = machineUpdated.id,
                name = machineUpdated.name,
                state = machineUpdated.state,
                qrcode = machineUpdated.qrcode
            });
        }
        /// <summary>
        /// Updates a machine, if authorized and if it exists
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /update-machine
        ///     {
        ///        "request_id": request_id,
        ///        "machine_id": machine_id,
        ///        "name": "Name", -> not required
        ///        "state": state, 
        ///        "qrcode": "qrcode" -> not required
        ///     }
        ///
        /// </remarks> 
        /// <param name="updateMachineRequest">Json object with request ID, machine_id, name, state and qrcode</param>
        /// <returns>If the machine was updated, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The machine was updated </response>
        /// <response code="400">The request was invalid, so the machine wasn't updated, verify your role or the params</response>
        [Authorize]
        [HttpPut("update-machine")]
        public async Task<ActionResult<Machine>> UpdateMachine([FromBody] UpdateMachineRequest updateMachineRequest)
        { 
            Machine machineUpdated = _machineService.UpdateMachine(updateMachineRequest);

            if (machineUpdated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Machine updated.",
                id = machineUpdated.id,
                name = machineUpdated.name,
                state = machineUpdated.state,
                qrcode = machineUpdated.qrcode
            });
        }
        /// <summary>
        /// Gets all the machines in the system, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /machines
        ///
        /// </remarks>
        /// <returns>Ok if the machines were returned, NotAutorized if not authorized</returns>       
        /// <param name="request_id">ID of who makes the request.</param>
        /// <response code="200">The machines were returned </response>
        /// <response code="401">Not Authorized</response>
        [Authorize]
        [HttpGet("machines")]
        public  ActionResult<Machine> GetAll(int request_id)
        {
            List<Machine> machines = _machineService.GetMachines(request_id);

            if (machines == null)
                return Unauthorized(new { message = "Invalid request. Check your role." });

            return Ok(machines);
        }
        /// <summary>
        /// Gets a machine by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /machine
        ///
        /// </remarks>
        /// <returns>Ok if the machine was returned, BadRequest if not authorized or machine_id not valid</returns>       
        /// <param name="request_id">ID of who makes the request</param>
        /// <param name="id">ID of the machine to get</param>
        /// <response code="200">The machine was returned </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpGet("machine")]
        public ActionResult<Machine> GetByID(int request_id, int id)
        {
            MachineIDRequest getUserRequest = new MachineIDRequest { request_id = request_id, machine_id = id };
            Machine machine = _machineService.GetByID(getUserRequest);

            if (machine == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            return Ok(machine);
        }
        /// <summary>
        /// Deletes a machine by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /machine
        ///      {
        ///        "request_id": id,
        ///        "machine_id": id,
        ///      }
        ///
        /// </remarks>
        /// <returns>Ok if the machine was deleted, BadRequest if not authorized or machine_id not valid</returns>       
        /// <param name="machineIDRequest">ID of who makes the request and ID of the machine to delete.</param>
        /// <response code="200">The machine was deleted </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpDelete("machine")]
        public async Task<ActionResult<Machine>> Delete([FromBody] MachineIDRequest machineIDRequest)
        {
            Machine machine = _machineService.Delete(machineIDRequest);

            if (machine == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new { message = "Machine deleted.", machine });
        }
        }
}
