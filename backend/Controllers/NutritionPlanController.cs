﻿using backend.Entities;
using backend.Models;
using backend.Models.NutritionPlans;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Controllers
{
    /// <summary>
    /// Nutritional Plan Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class NutritionPlanController : ControllerBase
    {
        private INutritionPlanService _nutritionPlanService;
        private readonly AppDbContext _appDbContext;
        /// <summary>
        /// Nutritional Plan Controller Constructor
        /// </summary>
        /// <param name="nutritionPlanService">Nutrition Plan Service</param>
        /// <param name="appDbContext">Database Context</param>
        public NutritionPlanController(INutritionPlanService nutritionPlanService, AppDbContext appDbContext)
        {
            _nutritionPlanService = nutritionPlanService;
            _appDbContext = appDbContext;
        }


        /// <summary>
        /// Creates a nutrition plan, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /create-nutritionPlan
        ///     {
        ///        "request_id": request_id,
        ///        "nutritional_evaluation_id": nutritional_evaluation_id      
        ///     }
        ///
        /// </remarks> 
        /// <param name="createNutritionPlanRequest">Json object with request_id, nutritional_evaluation_id</param>
        /// <returns>If the nutritional plan was created, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The nutritional plan was created </response>
        /// <response code="400">The request was invalid, so the nutritional plan wasn't created, verify your role or the params</response>


        [Authorize]
        [HttpPost("create-nutritionPlan")]
        
        public async Task<ActionResult<NutritionPlan>> CreateNutritionPlan([FromBody] CreateNutritionPlanRequest createNutritionPlanRequest)
        {
            NutritionPlan nutritionPlanCreated = _nutritionPlanService.CreateNutritionPlan(createNutritionPlanRequest);
            
            if (nutritionPlanCreated==null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Nutrition Plan created.",
                nutritionPlanCreated

            });
        }


        /// <summary>
        /// Gets a nutrition plan by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /nutritionPlan
        ///
        /// </remarks>
        /// <param name="request_id">ID of who makes the request</param>
        /// <param name="id">ID of the nutrition plan to get</param>
        /// <returns>Ok if the nutrition plan was returned, BadRequest if not authorized or nutrition plan not valid</returns>
        /// <response code="200">The nutrition plan was returned </response>
        /// <response code="400">Not Authorized or bad request</response>
        
        [Authorize]
        [HttpGet("nutritionPlan")]
        public ActionResult<NutritionPlan> GetByID(int request_id, int id)
        {
            NutritionPlanIDRequest nutritionPlanIDRequest= new NutritionPlanIDRequest{ request_id = request_id, nutrition_plan_id = id };
            NutritionPlan nutritionPlan = _nutritionPlanService.GetByID(nutritionPlanIDRequest);

            if (nutritionPlan == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            return Ok(nutritionPlan);
        }

        /// <summary>
        /// Gets all the nutrition plans in the system, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /nutritionPlans
        ///
        /// </remarks>
        /// <returns>Ok if the nutrition plans were returned, NotAutorized if not authorized</returns>       
        /// <param name="request_id">ID of who makes the request.</param>
        /// <response code="200">The nutrition plans were returned </response>
        /// <response code="401">Not Authorized</response>

        [Authorize]
        [HttpGet("nutritionPlans")]
        public ActionResult<NutritionPlan> GetAll(int request_id)
        {
            List<NutritionPlan> nutritionPlans= _nutritionPlanService.GetNutritionPlans(request_id);

            if (nutritionPlans == null)
                return Unauthorized(new { message = "Invalid request. Check your role." });

            return Ok(nutritionPlans);
        }

        /// <summary>
        /// Deletes a training plan by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /nutritionPlan
        ///      {
        ///        "request_id": id,
        ///        "nutrition_plan_id": id
        ///      }
        ///
        /// </remarks>
        /// <returns>Ok if the nutrition plan was deleted, BadRequest if not authorized or nutrition plan not valid</returns>       
        /// <param name="nutritionPlanIDRequest">ID of who makes the request and ID of the nutrition plan to delete.</param>
        /// <response code="200">The nutrition plan was deleted </response>
        /// <response code="400">Not Authorized or bad request</response>

        [Authorize]
        [HttpDelete("nutritionPlan")]
        public async Task<ActionResult<TrainingPlan>> Delete([FromBody] NutritionPlanIDRequest nutritionPlanIDRequest)
        {
            NutritionPlan nutritionPlan = _nutritionPlanService.Delete(nutritionPlanIDRequest);

            if (nutritionPlan == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new { message = "Nutrition Plan deleted.", nutritionPlan});
        }

        /// <summary>
        /// Updates a nutrition plan, if authorized and if it exists
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /update-nutritionPlan
        ///     {
        ///        "request_id": request_id,
        ///        "nutrition_plan_id": training_plan_id,
        ///        "nutritional_evaluation_id": "nutritional_evaluation_id" -> not required
        ///        
        ///     }
        ///
        /// </remarks> 
        /// <param name="updateNutritionPlanRequest">Json object with request ID, nutrition_plan_id and nutritional_evaluation_id</param>
        /// <returns>If the nutrition plan was updated, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The nutrition plan was updated </response>
        /// <response code="400">The request was invalid, so the nutritional plan wasn't updated, verify your role or the params</response>
        [Authorize]
        [HttpPut("update-nutritionPlan")]
        public async Task<ActionResult<NutritionPlan>> UpdateNutritionPlan([FromBody] UpdateNutritionPlanRequest updateNutritionPlanRequest)
        {
            NutritionPlan nutritionPlanUpdated = _nutritionPlanService.UpdateNutritionPlan(updateNutritionPlanRequest);

            if (nutritionPlanUpdated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Nutrition Plan updated.",
                nutritionPlanUpdated
            });
        }


    }

}
