﻿using backend.Entities;
using backend.Models;
using backend.Models.MemberPlans;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Controllers
{
    /// <summary>
    /// Member Plan Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MemberPlanController : ControllerBase
    {
        private IMemberPlanService _memberPlanService;
        private readonly AppDbContext _appDbContext;
        /// <summary>
        /// Member Plan Controller Constructor
        /// </summary>
        /// <param name="memberPlanService">Member Plan Service</param>
        /// <param name="appDbContext">Database Context</param>
        public MemberPlanController(IMemberPlanService memberPlanService, AppDbContext appDbContext)
        {
            _memberPlanService = memberPlanService;
            _appDbContext = appDbContext;
        }
        /// <summary>
        /// Cretes a member plan, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /create-member-plan
        ///     {
        ///        "request_id": request_id,
        ///        "monthly_plan_id": monthly_plan_id,
        ///        "athlete_id": athlete_id
        ///     }
        ///
        /// </remarks> 
        /// <param name="createMemberPlanRequest">Json object with request ID, monthly plan ID, athlete ID</param>
        /// <returns>If the Member Plan was created, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The Member plan was created </response>
        /// <response code="400">The request was invalid, so the member plan wasn't created, verify your role or the params</response>
        [Authorize]
        [HttpPost("create-member-plan")]
        public async Task<ActionResult<MemberPlan>> CreateMemberPlan([FromBody] CreateMemberPlanRequest createMemberPlanRequest)
        {
            MemberPlan memberPlanCreated = _memberPlanService.CreateMemberPlan(createMemberPlanRequest);

            if (memberPlanCreated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Member plan created.",
                memberPlanCreated
            });

        }

        /// <summary>
        /// Gets all member plans, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /member-plans?request_id=1
        ///     
        /// </remarks> 
        /// <param name="request_id">Id of who makes the request</param>
        /// <returns>If the Member Plan were returned, NotAutorized if not authorized</returns>
        /// <response code="200">The Member plan were returned</response>
        /// <response code="400">Not authorized</response>

        [Authorize]
        [HttpGet("member-plans")]
        public ActionResult<MemberPlan> GetAll(int request_id)
        {
            List<MemberPlan> memberPlans = _memberPlanService.GetMemberPlans(request_id);

            if (memberPlans == null)
                return Unauthorized(new { message = "Invalid request. Check your role." });

            return Ok(memberPlans);
        }

        /// <summary>
        /// Gets a member plan by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /member-plan
        ///
        /// </remarks>
        /// <param name="request_id">ID of who makes the request</param>
        /// <param name="id">ID of the member plan to get</param>
        /// <returns>Ok if the member plan was returned, BadRequest if not authorized or id not valid</returns>
        /// <response code="200">The member plan was returned </response>
        /// <response code="400">Not Authorized or bad request</response>

        [Authorize]
        [HttpGet("member-plan")]
        public ActionResult<MemberPlan> GetByID(int request_id, int id)
        {
            MemberPlanIDRequest memberPlanIDRequest= new MemberPlanIDRequest{ request_id = request_id, member_plan_id = id };
            MemberPlan memberPlan = _memberPlanService.GetByID(memberPlanIDRequest);


            if (memberPlan == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            return Ok(memberPlan);
        }
        /// <summary>
        /// Deletes a member plan by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /member-plan
        ///      {
        ///        "request_id": id,
        ///        "member_plan_id": id,
        ///      }
        ///
        /// </remarks>
        /// <returns>Ok if the member plan was deleted, BadRequest if not authorized or member plan not valid</returns>       
        /// <param name="memberPlanIDRequest">ID of who makes the request and ID of the member plan to delete.</param>
        /// <response code="200">The member plan was deleted </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpDelete("member-plan")]
        public async Task<ActionResult<MemberPlan>> Delete([FromBody] MemberPlanIDRequest memberPlanIDRequest)
        {
            MemberPlan memberPlan = _memberPlanService.Delete(memberPlanIDRequest);

            if (memberPlan == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new { message = "Member plan deleted.", memberPlan});
        }
        /// <summary>
        /// Updates a Member Plan, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /member-plan
        ///      {
        ///        "request_id": id,
        ///        "member_plan_id": member_plan_id,
        ///        "athlete_id: athlete_id,
        ///        "monthly_id": monthly_id
        ///      }
        ///
        /// </remarks>
        /// <param name="updateMemberPlanRequest">Update Member Plan Request Model</param>
        /// <returns>Ok if the member plan was updated, BadRequest if not authorized or member plan not valid</returns>       
        /// <response code="200">The member plan was updated </response>
        /// <response code="400">Not Authorized or bad request</response>

        [Authorize]
        [HttpPut("update-member-plan")]
        public async Task<ActionResult<MemberPlan>> UpdateMemberPlan([FromBody] UpdateMemberPlanRequest updateMemberPlanRequest)
        {
            MemberPlan memberPlanUpdated= _memberPlanService.UpdateMemberPlan(updateMemberPlanRequest);

            if (memberPlanUpdated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Member Plan updated.",
                memberPlanUpdated
            });
        }

        /// <summary>
        /// Updates a member plan entry, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /update-entry
        ///      {
        ///        "request_id": id,
        ///        "athlete_id": athlete_id
        ///      }
        ///
        /// </remarks>
        /// <param name="giveEntryReceptionistRequest">ID of who makes the request</param>
        /// <returns>Member Plan valid entrys updated</returns>
        [Authorize]
        [HttpPut("update-entry")]

        public async Task<ActionResult<MemberPlan>> GiveEntry([FromBody] GiveEntryReceptionistRequest giveEntryReceptionistRequest)
        {

            MemberPlan memberPlanUpdated = _memberPlanService.GiveEntry(giveEntryReceptionistRequest);

            if (memberPlanUpdated == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Entry validated.",
                memberPlanUpdated
            });
        }

        /// <summary>
        /// Updates a member plan entry when athelete, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///      PUT /update-entry-athlete
        ///      {
        ///        "request_id": id
        ///      }
        ///
        /// </remarks>
        /// <param name="GiveEntryIDRequest">ID of who makes the request</param>
        /// <returns>>Member Plan valid entrys updated</returns>
        /// <response code="200">The entrys were updated </response>
        /// <response code="400">The request was invalid, so the entrys weren't updated</response>
        [Authorize]
        [HttpPut("update-entry-athlete")]

        public async Task<ActionResult<MemberPlan>> GiveEntryAthlete([FromBody] GiveEntryIDRequest GiveEntryIDRequest)
        {

            MemberPlan memberPlanUpdated = _memberPlanService.GiveEntryAthlete(GiveEntryIDRequest.request_id);

            if (memberPlanUpdated == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Entry validated.",
                memberPlanUpdated
            });
        }

    }
}
