﻿using backend.Entities;
using backend.Models;
using backend.Models.TrainingPlans;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Controllers
{
    /// <summary>
    /// Training Plan Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TrainingPlanController : ControllerBase
    {
        private ITrainingPlanService _trainingPlanService;
        private readonly AppDbContext _appDbContext;
        /// <summary>
        /// Training Controller constructor
        /// </summary>
        /// <param name="trainingPlanService">Training plan service</param>
        /// <param name="appDbContext">Database Context</param>
        public TrainingPlanController(ITrainingPlanService trainingPlanService, AppDbContext appDbContext)
        {
            _trainingPlanService = trainingPlanService;
            _appDbContext = appDbContext;
        }
        /// <summary>
        /// Cretes a training plan, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /create-training-plan
        ///     {
        ///        "request_id": request_id,
        ///        "description": "description", -> not required     
        ///        "athlete_id": athlete_id  
        ///     }
        ///
        /// </remarks> 
        /// <param name="createTrainingPlanRequest">Json object with request ID, description and athlete_id</param>
        /// <returns>If the training plan was created, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The training plan was created </response>
        /// <response code="400">The request was invalid, so the training plan wasn't created, verify your role or the params</response>
        [Authorize]
        [HttpPost("create-training-plan")]
        public async Task<ActionResult<TrainingPlan>> CreateTrainingPlan([FromBody] CreateTrainingPlanRequest createTrainingPlanRequest)
        {
            TrainingPlan trainingPlanCreated = _trainingPlanService.CreateTrainingPlan(createTrainingPlanRequest);

            if (trainingPlanCreated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Training Plan created.",
                trainingPlanCreated
            });
        }
        /// <summary>
        /// Updates a training plan, if authorized and if it exists
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /update-training-plan
        ///     {
        ///        "request_id": request_id,
        ///        "training_plan_id": training_plan_id,
        ///        "description": "description", -> not required
        ///        "athlete_id": athlete_id -> not required
        ///     }
        ///
        /// </remarks> 
        /// <param name="updateTrainingPlanRequest">Json object with request ID, training_plan_id, description and athlete_id</param>
        /// <returns>If the training plan was updated, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The training plan was updated </response>
        /// <response code="400">The request was invalid, so the training plan wasn't updated, verify your role or the params</response>
        [Authorize]
        [HttpPut("update-training-plan")]
        public async Task<ActionResult<TrainingPlan>> UpdateExercise([FromBody] UpdateTrainingPlanRequest updateTrainingPlanRequest)
        {
            TrainingPlan trainingPlanUpdated = _trainingPlanService.UpdateTrainingPlan(updateTrainingPlanRequest);

            if (trainingPlanUpdated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Training Plan updated.",
                trainingPlanUpdated
            });
        }
        /// <summary>
        /// Gets all the training plans in the system, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /training-plans
        ///
        /// </remarks>
        /// <returns>Ok if the training plans were returned, NotAutorized if not authorized</returns>       
        /// <param name="request_id">ID of who makes the request.</param>
        /// <response code="200">The training plans were returned </response>
        /// <response code="401">Not Authorized</response>
        [Authorize]
        [HttpGet("training-plans")]
        public ActionResult<TrainingPlan> GetAll(int request_id)
        {
            List<TrainingPlan> trainingPlans = _trainingPlanService.GetTrainingPlans(request_id);

            if (trainingPlans == null)
                return Unauthorized(new { message = "Invalid request. Check your role." });

            return Ok(trainingPlans);
        }
        /// <summary>
        /// Gets a training plans by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /training plan
        ///
        /// </remarks>
        /// <param name="request_id">ID of who makes the request</param>
        /// <param name="id">ID of the training plan to get</param>
        /// <returns>Ok if the training plan was returned, BadRequest if not authorized or training plan not valid</returns>
        /// <response code="200">The training plan was returned </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpGet("training-plan")]
        public ActionResult<TrainingPlan> GetByID(int request_id, int id)
        {
            TrainingPlanIDRequest trainingPlanIDRequest = new TrainingPlanIDRequest { request_id = request_id, training_plan_id = id };
            TrainingPlan trainingPlan = _trainingPlanService.GetByID(trainingPlanIDRequest);

            if (trainingPlan == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            return Ok(trainingPlan);
        }
        /// <summary>
        /// Deletes a training plan by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /training-plan
        ///      {
        ///        "request_id": id,
        ///        "training_plan_id": id,
        ///      }
        ///
        /// </remarks>
        /// <returns>Ok if the training plan was deleted, BadRequest if not authorized or training plan not valid</returns>       
        /// <param name="trainingPlanIDRequest">ID of who makes the request and ID of the training plan to delete.</param>
        /// <response code="200">The training plan was deleted </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpDelete("training-plan")]
        public async Task<ActionResult<TrainingPlan>> Delete([FromBody] TrainingPlanIDRequest trainingPlanIDRequest)
        {
            TrainingPlan trainingPlan = _trainingPlanService.Delete(trainingPlanIDRequest);

            if (trainingPlan == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new { message = "Training Plan deleted.", trainingPlan });
        }
        /// <summary>
        /// Add Exercise to a training plan if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /add-exercise-to-plan
        ///     {
        ///        "request_id": request_id,
        ///        "plan_id": plan_id,
        ///        "exercise_id": exercise_id,
        ///        "circuit": circuit,
        ///        "time": "time_in_sec", -> if exercise type is Time (e.g. - when set is 4 "10-10-10-10")
        ///        "repetitions": "repetitions", -> if exercise type is Reps (e.g. - when set is 4 "10-10-10-10")
        ///        "sets": sets
        ///     }
        ///
        /// </remarks> 
        /// <param name="addExerciseToPlanRequest">Json object with request ID, plan ID, Exercise ID, circuit, repetitions and sets</param>
        /// <returns>If the exercise was added to training plan , ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The exercise was added to training plan </response>
        /// <response code="400">The request was invalid, so the exercise wasn't added to training plan, verify your role or the params</response>
        [Authorize]
        [HttpPost("add-exercise-to-plan")]
        public async Task<ActionResult<TrainingPlan>> CreateTrainingPlan([FromBody] AddExerciseToPlanRequest addExerciseToPlanRequest)
        {
            TrainingPlan trainingPlanUpdated = _trainingPlanService.AddExerciseToPlanRequest(addExerciseToPlanRequest);

            if (trainingPlanUpdated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Training Plan created.",
                trainingPlanUpdated
            });
        }
        /// <summary>
        /// Gets the training plans of the user himself, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /training-plan-user
        ///
        /// </remarks>
        /// <param name="request_id">ID of who makes the request</param>
        /// <returns>Ok if the training plans of the user were returned, BadRequest if not authorized or training plans not valid</returns>
        /// <response code="200">The training plans were returned </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpGet("training-plan-user")]
        public ActionResult<TrainingPlan> GetByUserID(int request_id)
        {
            List<TrainingPlan> trainingPlans = _trainingPlanService.GetByUserID(request_id);

            if (trainingPlans == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            return Ok(trainingPlans);
        }
    }
}
