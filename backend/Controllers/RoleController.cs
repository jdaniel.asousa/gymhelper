﻿using backend.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Controllers
{
    /// <summary>
    /// Role Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private IRoleService _roleService;
        private readonly AppDbContext _appDbContext;
        /// <summary>
        /// Role Controller Constructor
        /// </summary>
        /// <param name="roleService">Role Service</param>
        /// <param name="appDbContext"> Database Context</param>
        public RoleController(IRoleService roleService, AppDbContext appDbContext)
        {
            _roleService = roleService;
            _appDbContext = appDbContext;
        }

        /// <summary>
        /// Verify roles on database and resets them if needed.
        /// </summary>
        /// <returns>If the roles were updated or not</returns>
        /// <response code="200">The roles were updated successfully</response>
        /// <response code="400">The roles were verified and didn't need update</response> 
        [HttpPost("generate-roles")]
        public IActionResult GenerateRoles()
        {
            Boolean areRolesValid = _roleService.VerifyRoles();

            if (areRolesValid)
                return BadRequest(new { message = "The roles on the database are already valid." });

            _roleService.GenerateRoles();

            return Ok(new
            { message ="Roles were updated successfully"
            });
        }
    }
}
