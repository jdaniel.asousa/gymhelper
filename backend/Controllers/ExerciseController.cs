﻿using backend.Entities;
using backend.Models;
using backend.Models.Exercises;
using backend.Models.Machines;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Controllers
{
    /// <summary>
    /// Exercise Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ExerciseController : ControllerBase
    {
        private IExerciseService _exerciseService;
        private readonly AppDbContext _appDbContext;
        /// <summary>
        /// Exercise Controller Contructor
        /// </summary>
        public ExerciseController(IExerciseService exerciseService, AppDbContext appDbContext)
        {
            _exerciseService = exerciseService;
            _appDbContext = appDbContext;
        }
        /// <summary>
        /// Cretes an exercise, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /create-exercise
        ///     {
        ///        "request_id": request_id,
        ///        "type": "Reps" or "Time",
        ///        "name": "Name",
        ///        "muscle": "muscle",
        ///        "video": "video", -> not required        
        ///        "description": "description", -> not required
        ///        "machine_id": machine_id -> not required
        ///     }
        ///
        /// </remarks> 
        /// <param name="createExerciseRequest">Json object with request ID, name, muscle, video, description and machine_id</param>
        /// <returns>If the exercise was created, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The exercise was created </response>
        /// <response code="400">The request was invalid, so the exercise wasn't created, verify your role or the params</response>
        [Authorize]
        [HttpPost("create-exercise")]
        public async Task<ActionResult<Exercise>> CreateExercise([FromBody] CreateExerciseRequest createExerciseRequest)
        {
            Exercise exerciseCreated = _exerciseService.CreateExercise(createExerciseRequest);

            if (exerciseCreated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Exercise created.",
                exerciseCreated
            });
        }
        /// <summary>
        /// Updates a exercise, if authorized and if it exists
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /update-exercise
        ///     {
        ///        "request_id": request_id,
        ///        "exercise_id": exercise_id,
        ///        "name": "name", -> not required
        ///        "muscle": muscle, -> not required
        ///        "video": "video", -> not required
        ///        "description": "description", -> not required
        ///        "machine_id": "machine_id" -> not required
        ///     }
        ///
        /// </remarks> 
        /// <param name="updateExerciseRequest">Json object with request ID, exercise_id, name, muscle, video, description and machine_id</param>
        /// <returns>If the exercise was updated, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The exercise was updated </response>
        /// <response code="400">The request was invalid, so the exercise wasn't updated, verify your role or the params</response>
        [Authorize]
        [HttpPut("update-exercise")]
        public async Task<ActionResult<Exercise>> UpdateExercise([FromBody] UpdateExerciseRequest updateExerciseRequest)
        {
            Exercise exerciseUpdated = _exerciseService.UpdateExercise(updateExerciseRequest);

            if (exerciseUpdated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Machine updated.",
                exerciseUpdated
            });
        }
        /// <summary>
        /// Gets all the exercises in the system, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /exercises?request_id=0
        ///
        /// </remarks>
        /// <returns>Ok if the exercises were returned, NotAutorized if not authorized</returns>       
        /// <param name="request_id">ID of who makes the request.</param>
        /// <response code="200">The exercises were returned </response>
        /// <response code="401">Not Authorized</response>
        [Authorize]
        [HttpGet("exercises")]
        public ActionResult<Exercise> GetAll(int request_id)
        {
            List<Exercise> exercises = _exerciseService.GetExercises(request_id);

            if (exercises == null)
                return Unauthorized(new { message = "Invalid request. Check your role." });

            return Ok(exercises);
        }
        /// <summary>
        /// Gets a exercise by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /exercise
        ///
        /// </remarks>
        /// <param name="request_id">ID of who makes the request</param>
        /// <param name="id">ID of the exercise to get</param>
        /// <returns>Ok if the exercise was returned, BadRequest if not authorized or id not valid</returns>
        /// <response code="200">The exercise was returned </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpGet("exercise")]
        public  ActionResult<Exercise> GetByID(int request_id, int id)
        {
            ExerciseIDRequest exerciseIDRequest = new ExerciseIDRequest { request_id = request_id, exercise_id = id };
            Exercise exercise = _exerciseService.GetByID(exerciseIDRequest);

            if (exercise == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            return Ok(exercise);
        }
        /// <summary>
        /// Deletes a exercise by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /exercise
        ///      {
        ///        "request_id": id,
        ///        "exercise_id": id,
        ///      }
        ///
        /// </remarks>
        /// <returns>Ok if the exercise was deleted, BadRequest if not authorized or exercise not valid</returns>       
        /// <param name="exerciseIDRequest">ID of who makes the request and ID of the exercise to delete.</param>
        /// <response code="200">The exercise was deleted </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpDelete("exercise")]
        public async Task<ActionResult<Exercise>> Delete([FromBody] ExerciseIDRequest exerciseIDRequest)
        {
            Exercise exercise = _exerciseService.Delete(exerciseIDRequest);

            if (exercise == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new { message = "Exercise deleted.", exercise });
        }
        /// <summary>
        /// Gets all the exercises of a specific machine the system, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /exercises
        ///
        /// </remarks>
        /// <returns>Ok if the exercises were returned, NotAutorized if not authorized</returns>       
        /// <param name="request_id">ID of who makes the request.</param>
        /// <param name="machine_id">Id of the machine</param>
        /// <response code="200">The exercises were returned </response>
        /// <response code="400">Bad Request</response>
        [Authorize]
        [HttpGet("exercises-by-machine")]
        public ActionResult<Exercise> GetAll(int request_id, int machine_id)
        {
            MachineIDRequest machineIDRequest = new MachineIDRequest { request_id = request_id, machine_id = machine_id };
            List<Exercise> exercises = _exerciseService.GetExercisesOfMachine(machineIDRequest);

            if (exercises == null)
                return BadRequest(new { message = "Invalid request. Check your role." });

            return Ok(exercises);
        }
    }
}

