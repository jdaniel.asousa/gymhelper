﻿using backend.Entities;
using backend.Models;
using backend.Models.Exercises;
using backend.Models.PhysicalEvaluations;
using backend.Models.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Controllers
{
    /// <summary>
    /// Physical Evaluation Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PhysicalEvaluationController : ControllerBase
    {
        private IPhysicalEvaluationService _physicalEvaluationService;
        private readonly AppDbContext _appDbContext;
        /// <summary>
        /// Physical Evaluation Controller Constructor
        /// </summary>
        public PhysicalEvaluationController(IPhysicalEvaluationService physicalEvaluationService, AppDbContext appDbContext)
        {
            _physicalEvaluationService = physicalEvaluationService;
            _appDbContext = appDbContext;
        }
        /// <summary>
        /// Creates a physical evaluation, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /create-physical-evaluation
        ///     {
        ///        "request_id": request_id,
        ///        "height": height,
        ///        "weight": weight,
        ///        "imc": imc,
        ///        "description": "description",
        ///        "body_water": body_water,
        ///        "body_fat": body_fat,
        ///        "muscle_mass": muscle_mass,
        ///        "lat_size": lat_size,
        ///        "left_bicep_size": left_bicep_size,
        ///        "right_bicep_size": right_bicep_size,
        ///        "chest_size": chest_size,
        ///        "waist_size": waist_size,
        ///        "glute_size": glute_size,
        ///        "hip_size": hip_size,
        ///        "thigh_size": thigh_size,
        ///        "calfs_size": calfs_size,
        ///        "athlete_id": athlete_id
        ///     }
        ///
        /// </remarks> 
        /// <param name="createPhysicalEvaluationRequest">Json object with request ID, height, weight, imc, description, body_water, body_fat, muscle_mass, lat_size, left_bicep_size, right_bicep_size, chest_size, waist_size, glute_size, hip_size, thigh_size, calfs_size and athlete_id</param>
        /// <returns>If the physical evaluation was created, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The physical evaluation was created </response>
        /// <response code="400">The request was invalid, so the physical evaluation wasn't created, verify your role or the params</response>
        [Authorize]
        [HttpPost("create-physical-evaluation")]
        public async Task<ActionResult<PhysicalEvaluation>> CreatePhysicalEvaluation([FromBody] CreatePhysicalEvaluationRequest createPhysicalEvaluationRequest)
        {
            PhysicalEvaluation physicalEvaluationCreated = _physicalEvaluationService.CreatePhysicalEvaluation(createPhysicalEvaluationRequest);

            if (physicalEvaluationCreated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Physical Evaluation created.",
                physicalEvaluationCreated
            });
        }
        /// <summary>
        /// Updates a physical evaluation, if authorized and if it exists
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /update-physical-evaluation
        ///     {
        ///        "request_id": request_id,
        ///        "physical_evaluation_id": exercise_id,
        ///        "height": height, -> not required
        ///        "weight": weight, -> not required
        ///        "imc": imc, -> not required
        ///        "description": "description", -> not required
        ///        "body_water": body_water, -> not required
        ///        "body_fat": body_fat, -> not required
        ///        "muscle_mass": muscle_mass, -> not required
        ///        "lat_size": lat_size, -> not required
        ///        "left_bicep_size": left_bicep_size, -> not required
        ///        "right_bicep_size": right_bicep_size, -> not required
        ///        "chest_size": chest_size, -> not required
        ///        "waist_size": waist_size, -> not required
        ///        "glute_size": glute_size, -> not required
        ///        "hip_size": hip_size, -> not required
        ///        "thigh_size": thigh_size, -> not required
        ///        "calfs_size": calfs_size, -> not required
        ///        "athlete_id": athlete_id -> not required
        ///     }
        ///
        /// </remarks> 
        /// <param name="updatePhysicalEvaluationRequest">Json object with request ID and physical_evaluation_id required and height, weight, imc, description, body_water, body_fat, muscle_mass, lat_size, left_bicep_size, right_bicep_size, chest_size, waist_size, glute_size, hip_size, thigh_size, calfs_size and athlete_id non required</param>
        /// <returns>If the physical evaluation was updated, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The physical evaluation was updated </response>
        /// <response code="400">The request was invalid, so the physical evaluation wasn't updated, verify your role or the params</response>
        [Authorize]
        [HttpPut("update-physical-evaluation")]
        public async Task<ActionResult<PhysicalEvaluation>> UpdatePhysicalEvaluation([FromBody] UpdatePhysicalEvaluationRequest updatePhysicalEvaluationRequest)
        {
            PhysicalEvaluation physicalEvaluation = _physicalEvaluationService.UpdatePhysicalEvaluation(updatePhysicalEvaluationRequest);

            if (physicalEvaluation == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Physcial Evaluation updated.",
                physicalEvaluation
            });
        }
        /// <summary>
        /// Gets all the physical evaluations in the system, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /physical-evaluations
        ///
        /// </remarks>
        /// <returns>Ok if the physical evaluations were returned, NotAutorized if not authorized</returns>       
        /// <param name="request_id">ID of who makes the request.</param>
        /// <response code="200">The physical evaluations were returned </response>
        /// <response code="401">Not Authorized</response>
        [Authorize]
        [HttpGet("physical-evaluations")]
        public ActionResult<PhysicalEvaluation> GetAll(int request_id)
        {
            List<PhysicalEvaluation> physicalEvaluations = _physicalEvaluationService.GetPhysicalEvaluations(request_id);

            if (physicalEvaluations == null)
                return Unauthorized(new { message = "Invalid request. Check your role." });

            return Ok(physicalEvaluations);
        }
        /// <summary>
        /// Gets a physical evaluation by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /physical-evaluation
        ///
        /// </remarks>
        /// <param name="request_id">ID of who makes the request</param>
        /// <param name="id">ID of the physical evaluation to get</param>
        /// <returns>Ok if the physical evaluation was returned, BadRequest if not authorized or id not valid</returns>
        /// <response code="200">The physical evaluation was returned </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpGet("physical-evaluation")]
        public ActionResult<PhysicalEvaluation> GetByID(int request_id, int id)
        {
            PhysicalEvaluationIDRequest physicalEvaluationIDRequest = new PhysicalEvaluationIDRequest { request_id = request_id, physical_evaluation_id = id };
            PhysicalEvaluation physicalEvaluation = _physicalEvaluationService.GetByID(physicalEvaluationIDRequest);

            if (physicalEvaluation == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            return Ok(physicalEvaluation);
        }
        /// <summary>
        /// Deletes a physical evaluation by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /physical-evaluation
        ///      {
        ///        "request_id": id,
        ///        "physical_evaluation_id": id,
        ///      }
        ///
        /// </remarks>
        /// <returns>Ok if the physical evaluation was deleted, BadRequest if not authorized or physical evaluation not valid</returns>       
        /// <param name="physicalEvaluationIDRequest">ID of who makes the request and ID of the physical evaluation to delete.</param>
        /// <response code="200">The physical evaluation was deleted </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpDelete("physical-evaluation")]
        public async Task<ActionResult<PhysicalEvaluation>> Delete([FromBody] PhysicalEvaluationIDRequest physicalEvaluationIDRequest)
        {
            PhysicalEvaluation physicalEvaluation = _physicalEvaluationService.Delete(physicalEvaluationIDRequest);

            if (physicalEvaluation == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new { message = "Physical Evaluation deleted.", physicalEvaluation });
        }
        /// <summary>
        /// Gets all the physical evaluations in the system of a specific athlete, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /physical-evaluations-by-athlete-id
        ///
        /// </remarks>
        /// <returns>Ok if the physical evaluations were returned, NotAutorized if not authorized</returns>       
        /// <param name="request_id">ID of who makes the request.</param>
        /// <response code="200">The physical evaluations were returned </response>
        /// <response code="401">Not Authorized</response>
        [Authorize]
        [HttpGet("physical-evaluations-by-athlete-id")]
        public ActionResult<PhysicalEvaluation> GetAllOfAthlete(int request_id)
        {
            List<PhysicalEvaluation> physicalEvaluations = _physicalEvaluationService.GetAllOfAthlete(request_id);

            if (physicalEvaluations == null)
                return Unauthorized(new { message = "Invalid request. Check your role." });

            return Ok(physicalEvaluations);
        }
    }
}

