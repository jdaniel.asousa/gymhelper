using backend.Entities;
using backend.Models;
using backend.Models.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace backend.Controllers
{
    /// <summary>
    /// User Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserService _userService;
        private readonly AppDbContext _appDbContext;
        /// <summary>
        /// User Controller Constructor
        /// </summary>
        /// <param name="userService">User Service</param>
        /// <param name="appDbContext">Database Context</param>
        public UserController(IUserService userService, AppDbContext appDbContext)
        {
            _userService = userService;
            _appDbContext = appDbContext;
        }
        /// <summary>
        /// Authenticates a given cretentials.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /login
        ///     {
        ///        "Email": "email@email.com",
        ///        "Password": "Password"
        ///     }
        ///
        /// </remarks>
        /// <param name="userInfo">Json model send on body with email and password</param>
        /// <returns>If the credentials are valid, retrieve ok message with ID, email,role and token</returns>
        /// <response code="200">The credentials are valid</response>
        /// <response code="400">The credentials weren't valid</response>
        [HttpPost("login")]
        [AllowAnonymous]
        public IActionResult Login([FromBody] LoginRequest userInfo)
        {
            var user = _userService.Authenticate(userInfo.Email, userInfo.Password);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect." });

            var tokenString = _userService.GenerateJSONWebToken(user);

            UserInfoResponse userReturned;

            if(user.role_id == 1)
            {
                userReturned = new UserInfoResponse { Id = user.id, Email = user.email, Name = user.name, role = user.role_id, avatar = user.avatar, Token = tokenString };
            }
            else
            {
                if (user.role_id == 2)
                {
                    List<AthletesInfo> athletesInfos = _appDbContext.athletes_info.ToList();
                    userReturned = new UserInfoResponse { Id = user.id, Email = user.email, Name = user.name, role = user.role_id, avatar = user.avatar, Token = tokenString , athlete_id= user.athlete_info.id};
                }
                else
                {
                    List<EmployeeInfo> employeeInfos = _appDbContext.employees_info.ToList();
                    userReturned = new UserInfoResponse { Id = user.id, Email = user.email, Name = user.name, role = user.role_id, avatar = user.avatar, Token = tokenString, employee_id = user.employee_info.id };
                }
            }

            return Ok(userReturned);
        }

        /// <summary>
        /// Creates an user, if authorized.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /create-user
        ///     {
        ///        "request_id": request_id,
        ///        "name": "Name",
        ///        "email": "email@email.com",
        ///        "origin": "Origin",
        ///        "role_id": role_id,
        ///        "personal_trainer_id": personal_trainer_id, -> only needed when it's an athlete
        ///        "birth_date": "1/1/0001 12:00:00 AM" -> only needed when it's an athlete
        ///     }
        ///
        /// </remarks>
        /// <param name="user">Json model with request_id, name, email, origin and role_id required, personal_trainer_id and birth_date in case of an atlhete.</param>
        /// <returns>If the user was created, retrieve ok message with ID, name, email and role</returns>
        /// <response code="200">The user was created </response>
        /// <response code="400">The request was invalid, so the user wasn't created</response>
        [Authorize]
        [HttpPost("create-user")]
        public async Task<ActionResult<User>> CreateUser([FromBody] CreateUserRequest user)
        {
            User userCreated = _userService.CreateUser(user);

            if (userCreated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });


            await _appDbContext.SaveChangesAsync();

            _userService.SetPassword(userCreated.email, user.origin);


            return Ok(new { message = "User created.",
                id = userCreated.id,
                name = userCreated.name,
                email = userCreated.email,
                role_id = userCreated.role_id
            });
        }

        /// <summary>
        /// Updates an user, if authorized and if it exists
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /update-user
        ///     {
        ///        "id": id,
        ///        "name": "Name",-> optional
        ///        "password": "Password",-> optional
        ///        "confirmPassword": confirmPassword,-> optional
        ///        "avatar": "avatar", -> optional and with file
        ///        "birth_date": "1/1/0001 12:00:00 AM" -> optional
        ///     }
        ///
        /// </remarks>
        /// <param name="user">Json model with id, name, password, confirmPassword, avatar and birth date, only id is required.</param>
        /// <returns>If the user was updated, retrieve ok message with ID, name, email and role</returns>
        /// <response code="200">The user was updated </response>
        /// <response code="400">The request was invalid, so the user wasn't updated</response>
        [Authorize]
        [HttpPut("update-user")]
        public async Task<ActionResult<User>> Update([FromForm] UpdateUserRequest user)
        {
            User userUpdated = _userService.UpdateUser(user).Result;

            if (userUpdated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });


            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "User updated.",
                id = userUpdated.id,
                name = userUpdated.name,
                email = userUpdated.email,
                role_id = userUpdated.role_id,
                avatar = userUpdated.avatar
            });
        }
        /// <summary>
        /// Gets all the users in the system, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /users
        ///
        /// </remarks>
        /// <returns>Ok if the users were returned, NotAutorized if not authorized</returns>       
        /// <param name="request_id">ID of who makes the request.</param>
        /// <response code="200">The users were returned </response>
        /// <response code="401">Not Authorized</response>
        [Authorize]
        [HttpGet("users")]
        public ActionResult<User> GetAll(int request_id)
        {
            List<User> allUsers = _userService.GetUsers(request_id);

            if (allUsers == null)
                return Unauthorized(new { message = "Invalid request. Check your role." });

            List<User> usersFormated = new List<User>(allUsers.Count);

            foreach (User user in  allUsers)
            {
                user.password = "";
                user.password_reset = null;
                user.reset_token_expires = null;
                user.reset_token = "";

                usersFormated.Add(user);
            }

            return Ok(usersFormated);
        }
        /// <summary>
        /// Gets a user by id, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /user
        ///
        /// </remarks>
        /// <returns>Ok if the user is returned, BadRequest if not authorized or don't exist</returns>       
        /// <param name="request_id">ID of who makes the request</param>
        /// <param name="id">ID of the user to get</param>
        /// <response code="200">The user is returned </response>
        /// <response code="400">Not Authorized or not found</response>
        [Authorize]
        [HttpGet("user")]
        public ActionResult<User> GetByID(int request_id, int id)
        {
            GetUserRequest getUserRequest = new GetUserRequest{ request_id = request_id, id = id};
            User user = _userService.GetUserByID(getUserRequest);

            if (user == null)
                return BadRequest(new { message = "Invalid request. Check your role or validate the id of the user" });


            user.password = "";
            user.password_reset = null;
            user.reset_token_expires = null;
            user.reset_token = "";
                

            return Ok(user);
        }

        /// <summary>
        /// Sends an email to a given user to reset the password
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /forgot-password
        ///     {
        ///        "email": "email@email.com",
        ///        "origin": "origin-route-on-frontend" -> on mobile, this field isn't required
        ///     }
        ///
        /// </remarks>
        /// <param name="forgotPasswordRequest">Json model with email required and origin field.</param>
        /// <returns>Ok if the email was or wasn't send because of security patterns</returns>
        /// <response code="200">The email was sent to reset password </response>
        [HttpPost("forgot-password")]
        public IActionResult ForgotPassword([FromBody] ForgotPasswordRequest forgotPasswordRequest)
        {
            _userService.ForgotPassword(forgotPasswordRequest.Email, forgotPasswordRequest.Origin);
            return Ok(new { message = "Follow the instructions sent to your E-mail." });
        }

        /// <summary>
        /// Given an valid token generated previously, password and confirmPassword, resets user password.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /reset-password
        ///     {
        ///        "Token": "Token",
        ///        "Password": "Password",
        ///        "ConfirmPassword": "Password"
        ///     }
        ///
        /// </remarks>
        /// <param name="resetPasswordRequest">Json model with token, password and confirmPassword fields.</param>
        /// <returns>Ok if the password was reseted with success</returns>
        /// <response code="200">The password was reseted with success </response>
        [HttpPost("reset-password")]
        public IActionResult ResetPassword([FromBody] ResetPasswordRequest resetPasswordRequest)
        {
            _userService.ResetPassword(resetPasswordRequest.Token, resetPasswordRequest.Password, resetPasswordRequest.ConfirmPassword);
            return Ok(new { message = "Password reset with success, you can now login with the new credentials." });
        }
        /// <summary>
        /// Deletes a user by id, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /user
        ///      {
        ///        "request_id": id,
        ///        "user_id": id,
        ///      }
        ///
        /// </remarks>
        /// <returns>Ok if the user was deleted, BadRequest if not authorized or user not valid</returns>       
        /// <param name="userIDRequest">ID of who makes the request and ID of the user to delete.</param>
        /// <response code="200">The user was deleted </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpDelete("user")]
        public async Task<ActionResult<User>> Delete([FromBody] GetUserRequest userIDRequest)
        {
            User user = _userService.Delete(userIDRequest);

            if (user == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new { message = "User deleted.", user });
        }
    }
}