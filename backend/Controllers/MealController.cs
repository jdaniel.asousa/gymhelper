﻿using backend.Entities;
using backend.Models;
using backend.Models.Meals;
using backend.Models.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Controllers
{
    /// <summary>W
    /// Meal Controller
    /// </summary>
   [Route("api/[controller]")]
   [ApiController]

    public class MealController : ControllerBase
    {

        private IMealService _mealService;
        private readonly AppDbContext _appDbContext;
        /// <summary>
        /// Meal Controller Constructor
        /// </summary>
        /// <param name="mealService">Meal Service</param>
        /// <param name="appDbContext">Database Context</param>
        public MealController(IMealService mealService, AppDbContext appDbContext)
        {
            _mealService = mealService;
            _appDbContext = appDbContext;
        }

        /// <summary>
        /// Creates a meal, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /create-meal
        ///     {
        ///        "request_id": request_id,
        ///        "calories": calories,
        ///        "protein": protein,
        ///        "fats": fats,
        ///        "carbohydrates": carbohydrates,
        ///        "description":"description", -> not required
        ///        "nutrition_plan_id": nutrition_plan_id
        ///     }
        ///
        /// </remarks>
        /// <param name="createMealRequest">Json object with request ID, name, calories,protein,fats,carbohydrates,description and plan_nutrition_id</param>
        /// <returns>If the meal was created, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The meal was created </response>
        /// <response code="400">The request was invalid, so the meal wasn't created, verify your role or the params</response>

        [Authorize]
        [HttpPost("create-meal")]
        public async Task<ActionResult<Meal>> CreateMeal([FromBody] CreateMealRequest createMealRequest)
        {
            Meal mealCreated = _mealService.CreateMeal(createMealRequest);

            if (mealCreated == null)
                return BadRequest(new { message = "Invalid request. Check given params." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Meal created.",
                mealCreated
            });
        }

        /// <summary>
        /// Gets all the meals in the system, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /meals
        ///      {
        ///        "request_id": id
        ///      }
        ///
        /// </remarks>
        /// <returns>Ok if the meals were returned, NotAutorized if not authorized</returns>
        /// <param name="request_id">ID of who makes the request.</param>
        /// <response code="200">The meals were returned </response>
        /// <response code="401">Not Authorized</response>

        [Authorize]
        [HttpGet("meals")]
        public ActionResult<Meal> GetAll(int request_id)
        {
            List<Meal> meals = _mealService.GetMeals(request_id);

            if (meals == null)
                return Unauthorized(new { message = "Invalid request. Check your role." });

            return Ok(meals);
         }

        /// <summary>
        /// Gets a meal by ID, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     GET /meal
        ///      {
        ///        "request_id": id,
        ///        "id": meal_id,
        ///      }
        /// </remarks>
        /// <returns>Ok if the meal was returned, BadRequest if not authorized or meal not valid</returns>
        /// <param name="request_id">ID of who makes the request and ID of the meal to get.</param>
        /// /// <param name="id">ID of the meal to get</param>
        /// <response code="200">The meal was returned </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpGet("meal")]
        public ActionResult<Meal> GetByID(int request_id, int id)
        {
            MealIDRequest getMealRequest = new MealIDRequest { request_id = request_id, meal_id = id };
            Meal meal = _mealService.GetByID(getMealRequest);

            if(meal==null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            return Ok(meal);

        }


        /// <summary>
        /// Deletes a meal by ID, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /meal
        ///      {
        ///        "request_id": id,
        ///        "meal_id": id,
        ///      }
        ///
        /// </remarks>
        /// <returns>Ok if the meal was deleted, BadRequest if not authorized or meal_id not valid</returns>
        /// <param name="mealIDRequest">ID of who makes the request and ID of the meal to delete.</param>
        /// <response code="200">The meal was deleted </response>
        /// <response code="400">Not Authorized or bad request</response>

        [Authorize]
        [HttpDelete("meal")]

        public async Task<ActionResult<Meal>> Delete([FromBody] MealIDRequest mealIDRequest)
        {
            Meal meal = _mealService.Delete(mealIDRequest);

            if (meal == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new { message = "Meal deleted.", meal});
        }
        /// <summary>
        /// Updates a meal, if authorized and if it exists
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /update-meal
        ///     {
        ///        "request_id": request_id,
        ///        "meal_id": meal_id,
        ///        "calories": calories, -> not required
        ///        "protein": protein, -> not required
        ///        "fats": fats -> not required
        ///        "carbohydrates":carbohydrates -> not required
        ///        "description":description -> not required
        ///     }
        /// </remarks>
        /// <param name="updateMealRequest">Json object with request ID, meal ID, calories,protein,fats,carbohydrates and description</param>
        /// <returns>If the meal was updated, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The meal was updated </response>
        /// <response code="400">The request was invalid, so the meal wasn't updated, verify your role or the params</response>
        [Authorize]
        [HttpPut("update-meal")]
        public async Task<ActionResult<Meal>> UpdateMeal([FromBody] UpdateMealRequest updateMealRequest)
        {
            Meal mealUpdated = _mealService.UpdateMeal(updateMealRequest);

            if (mealUpdated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Meal updated.",
                mealUpdated
            });
        }
    }

}
