﻿using backend.Entities;
using backend.Models;
using backend.Models.TrainingExerciseHistories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Controllers
{
    /// <summary>
    /// Training Exercise History Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TrainingExerciseHistoryController : ControllerBase
    {
        private ITrainingExerciseHistoryService _trainingExerciseHistoryService;
        private readonly AppDbContext _appDbContext;
        /// <summary>
        /// Training Exercise History Controller Contructor
        /// </summary>
        public TrainingExerciseHistoryController(ITrainingExerciseHistoryService trainingExerciseHistoryService, AppDbContext appDbContext)
        {
            _trainingExerciseHistoryService = trainingExerciseHistoryService;
            _appDbContext = appDbContext;
        }
        /// <summary>
        /// Cretes a Training Exercise History, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /create-training-exercise-history
        ///     {
        ///        "request_id": request_id,
        ///        "training_exercise_id": training_exercise_id,
        ///        "time": "time_in_sec", -> if exercise type is Time (e.g. - when set is 4 "10-10-10-10")
        ///        "repetitions": "repetitions", -> if exercise type is Reps (e.g. - when set is 4 "10-10-10-10")
        ///     }
        ///
        /// </remarks> 
        /// <param name="createTrainingExerciseHistoryRequest">Json object with request ID, training plan ID and time or repetitions</param>
        /// <returns>If the Training Exercise History was created, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The Training Exercise History was created </response>
        /// <response code="400">The request was invalid, so the Training Exercise History wasn't created, verify your role or the params</response>
        [Authorize]
        [HttpPost("create-training-exercise-history")]
        public async Task<ActionResult<TrainingExercisesHistory>> CreateTrainingExerciseHistory([FromBody] CreateTrainingExerciseHistoryRequest createTrainingExerciseHistoryRequest)
        {
            TrainingExercisesHistory trainingExercisesHistoryCreated = _trainingExerciseHistoryService.CreateTrainingExerciseHistory(createTrainingExerciseHistoryRequest);

            if (trainingExercisesHistoryCreated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Training Exercise History created.",
                trainingExercisesHistoryCreated
            });
        }
        /// <summary>
        /// Gets all the Training Exercise History in the system, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /training-exercise-historys
        ///
        /// </remarks>
        /// <returns>Ok if the Training Exercise History were returned, NotAutorized if not authorized</returns>       
        /// <param name="request_id">ID of who makes the request.</param>
        /// <response code="200">The Training Exercise History were returned </response>
        /// <response code="401">Not Authorized</response>
        [Authorize]
        [HttpGet("training-exercise-historys")]
        public ActionResult<TrainingExercisesHistory> GetAll(int request_id)
        {
            List<TrainingExercisesHistory> trainingExercisesHistory = _trainingExerciseHistoryService.GetTrainingExercisesHistories(request_id);

            if (trainingExercisesHistory == null)
                return Unauthorized(new { message = "Invalid request. Check your role." });

            return Ok(trainingExercisesHistory);
        }
        /// <summary>
        /// Gets a Training Exercise History by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /training-exercise-history
        ///
        /// </remarks>
        /// <param name="request_id">ID of who makes the request</param>
        /// <param name="id">ID of the Training Exercise History to get</param>
        /// <returns>Ok if the Training Exercise History was returned, BadRequest if not authorized or id not valid</returns>
        /// <response code="200">The Training Exercise History was returned </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpGet("training-exercise-history")]
        public ActionResult<TrainingExercisesHistory> GetByID(int request_id, int id)
        {
            TrainingExerciseHistoryIDRequest trainingExerciseHistoryIDRequest = new TrainingExerciseHistoryIDRequest { request_id = request_id, training_exercise_history_id = id };
            TrainingExercisesHistory trainingExercisesHistory = _trainingExerciseHistoryService.GetByID(trainingExerciseHistoryIDRequest);

            if (trainingExercisesHistory == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            return Ok(trainingExercisesHistory);
        }
        /// <summary>
        /// Gets all Training Exercises History by user ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /training-exercise-history-user
        ///
        /// </remarks>
        /// <param name="request_id">ID of who makes the request</param>
        /// <returns>Ok if the Training Exercises History were returned, BadRequest if not authorized or id not valid</returns>
        /// <response code="200">The Training Exercises History were returned </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpGet("training-exercise-history-user")]
        public ActionResult<TrainingExercisesHistory> GetByUserID(int request_id)
        {
            List<TrainingExercisesHistory> trainingExercisesHistory = _trainingExerciseHistoryService.GetByUserID(request_id);

            if (trainingExercisesHistory == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            return Ok(trainingExercisesHistory);
        }
    }
}
