﻿using backend.Entities;
using backend.Models;
using backend.Models.MachineImages;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Controllers
{
    /// <summary>
    /// Machine Image Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MachineImageController : ControllerBase
    {
        private IMachineImageService _machineImageService;
        private readonly AppDbContext _appDbContext;
        /// <summary>
        /// Machine Image Controller Constructor
        /// </summary>
        public MachineImageController(IMachineImageService machineImageService, AppDbContext appDbContext)
        {
            _machineImageService = machineImageService;
            _appDbContext = appDbContext;
        }
        /// <summary>
        /// Cretes a machine image, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /create-machine-image
        ///     {
        ///        "request_id": request_id,
        ///        "image": "image",
        ///        "machine_id": machine_id
        ///     }
        ///
        /// </remarks> 
        /// <param name="createMachineImageRequest">Json object with request ID, image and machine id</param>
        /// <returns>If the machine Image was created, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The machine image was created </response>
        /// <response code="400">The request was invalid, so the machine image wasn't created, verify your role or the params</response>
        [Authorize]
        [HttpPost("create-machine-image")]
        public async Task<ActionResult<MachineImage>> CreateMachine([FromForm] CreateMachineImageRequest createMachineImageRequest)
        {
            MachineImage machineImageCreated = _machineImageService.CreateMachineImage(createMachineImageRequest).Result;

            if (machineImageCreated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Machine Image created.",
                machineImageCreated
            });
        }
        /// <summary>
        /// Updates a machine image, if authorized and if it exists
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /update-machine-image
        ///     {
        ///        "request_id": request_id,
        ///        "machine_image_id": machine_image_id,
        ///        "image": image, -> not required
        ///        "machine_id": machine_id -> not required
        ///     }
        ///
        /// </remarks> 
        /// <param name="updateMachineImageRequest">Json object with request ID, machine_image_id, image and machine_id</param>
        /// <returns>If the machine image was updated, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The machine image was updated </response>
        /// <response code="400">The request was invalid, so the machine image wasn't updated, verify your role or the params</response>
        [Authorize]
        [HttpPut("update-machine-image")]
        public async Task<ActionResult<MachineImage>> UpdateMachineImage([FromForm] UpdateMachineImageRequest updateMachineImageRequest)
        {
            MachineImage machineImageUpdated = await _machineImageService.UpdateMachineImage(updateMachineImageRequest);

            if (machineImageUpdated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Machine Image updated.",
                machineImageUpdated
            });
        }
        /// <summary>
        /// Gets all the machines images in the system, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /machine-images
        ///
        /// </remarks>
        /// <returns>Ok if the machines images were returned, NotAutorized if not authorized</returns>       
        /// <param name="request_id">ID of who makes the request.</param>
        /// <response code="200">The machine images were returned </response>
        /// <response code="401">Not Authorized</response>
        [Authorize]
        [HttpGet("machine-images")]
        public ActionResult<MachineImage> GetAll(int request_id)
        {
            List<MachineImage> machineImages = _machineImageService.GetMachineImages(request_id);

            if (machineImages == null)
                return Unauthorized(new { message = "Invalid request. Check your role." });

            return Ok(machineImages);
        }
        /// <summary>
        /// Gets a machine image by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /machine-image
        ///
        /// </remarks>
        /// <returns>Ok if the machine image was returned, BadRequest if not authorized or machine_id not valid</returns>       
        /// <param name="request_id">ID of who makes the request</param>
        /// <param name="id">ID of the machine image to get</param>
        /// <response code="200">The machine image was returned </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpGet("machine-image")]
        public ActionResult<Machine> GetByID(int request_id, int id)
        {
            MachineImageIDRequest machineImageIDRequest = new MachineImageIDRequest { request_id = request_id, machine_image_id = id };
            MachineImage machineImage = _machineImageService.GetByID(machineImageIDRequest);

            if (machineImage == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            return Ok(machineImage);
        }
        /// <summary>
        /// Deletes a machine image by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /machine-image
        ///      {
        ///        "request_id": id,
        ///        "machine_image_id": id,
        ///      }
        ///
        /// </remarks>
        /// <returns>Ok if the machine image was deleted, BadRequest if not authorized or machine_id not valid</returns>       
        /// <param name="machineImageIDRequest">ID of who makes the request and ID of the machine image to delete.</param>
        /// <response code="200">The machine image was deleted </response>
        /// <response code="400">Not Authorized or bad request</response>
        [Authorize]
        [HttpDelete("machine-image")]
        public async Task<ActionResult<MachineImage>> Delete([FromBody] MachineImageIDRequest machineImageIDRequest)
        {
            MachineImage machineImage = _machineImageService.Delete(machineImageIDRequest);

            if (machineImage == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new { message = "Machine Image deleted.", machineImage });
        }
    }
}
