﻿using backend.Entities;
using backend.Models;
using backend.Models.NutritionalEvaluations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Controllers
{

    /// <summary>
    /// Nutritional Evaluation Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class NutritionalEvaluationController : ControllerBase
    {
        private INutritionalEvaluationService _nutritionalEvaluationService;
        private readonly AppDbContext _appDbContext;
        /// <summary>
        /// Nutritional Evaluation Controller Constructor
        /// </summary>
        public NutritionalEvaluationController(INutritionalEvaluationService nutritionalEvaluationService, AppDbContext appDbContext)
        {
            _nutritionalEvaluationService = nutritionalEvaluationService;
            _appDbContext = appDbContext;
        }
        /// <summary>
        /// Creates an nutritional evaluation, if authorized
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /create-nutritionalEvaluation
        ///     {
        ///        "request_id":"request_id",
        ///        "suggestions":"suggestions"-> not required,
        ///        "schedule":"schedule",
        ///         "athlete_id":"athlete_id"
        ///        
        ///     }
        ///
        /// </remarks> 
        /// <param name="createNutritionalEvaluationRequest">Json object with request ID, suggestions, schedule, athlete_id and nutricionist_id</param>
        /// <returns>If the nutritional evaluation was created, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The nutritional evaluation was created </response>
        /// <response code="400">The request was invalid, so the exercise wasn't created, verify your role or the params</response>
        [Authorize]
        [HttpPost("create-nutritionalEvaluation")]

        public async Task<ActionResult<NutritionalEvaluation>> CreateNutritionalEvaluation([FromBody] CreateNutritionalEvaluationRequest createNutritionalEvaluationRequest)
        {
            NutritionalEvaluation nutritionalEvaluationCreated = _nutritionalEvaluationService.CreateNutritionalEvaluation(createNutritionalEvaluationRequest);

            if (nutritionalEvaluationCreated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Nutritional Evaluation created.",
                nutritionalEvaluationCreated
            });
        }


        /// <summary>
        /// Gets all the nutritional evaluations in the system, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /nutritionalEvaluations
        ///
        /// </remarks>
        /// <returns>Ok if the nutritional evaluations were returned, NotAutorized if not authorized</returns>       
        /// <param name="request_id">ID of who makes the request.</param>
        /// <response code="200">The evaluations were returned </response>
        /// <response code="401">Not Authorized</response>
        /// 
        [Authorize]
        [HttpGet("nutritionalEvaluations")]
        public ActionResult<NutritionalEvaluation> GetAll(int request_id)
        {
            List<NutritionalEvaluation> nutritionalEvaluations = _nutritionalEvaluationService.GetNutritionalEvaluations(request_id);

            if (nutritionalEvaluations == null)
                return Unauthorized(new { message = "Invalid request. Check your role." });

            return Ok(nutritionalEvaluations);
        }

        /// <summary>
        /// Gets an nutritional evaluation by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /nutritionalEvaluation
        ///
        /// </remarks>
        /// <param name="request_id">ID of who makes the request</param>
        /// <param name="id">ID of the nutritional evaluation to get</param>
        /// <returns>Ok if the evaluation was returned, BadRequest if not authorized.</returns>
        /// <response code="200">The evaluation was returned </response>
        /// <response code="400">Not Authorized or bad request</response>

        [Authorize]
        [HttpGet("nutritionalEvaluation")]
        public ActionResult<NutritionalEvaluation> GetByID(int request_id, int id)
        {
            NutritionalEvaluationIDRequest nutritionalEvaluationIDRequest = new NutritionalEvaluationIDRequest { request_id = request_id, nutritional_evaluation_id = id };
            NutritionalEvaluation nutritionalEvaluation = _nutritionalEvaluationService.GetByID(nutritionalEvaluationIDRequest);

            if (nutritionalEvaluation == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            return Ok(nutritionalEvaluation);
        }

        /// <summary>
        /// Deletes a nutritional evaluation by ID, if authorized 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /nutritionalEvaluation
        ///      {
        ///        "request_id":"request_id",
        ///        "nutritional_evaluation_id":"nutritional_evaluation_id"
        ///      }
        ///
        /// </remarks>
        /// <returns>Ok if the nutritional evaluation was deleted, BadRequest if not authorized.</returns>       
        /// <param name="nutritionalEvaluationIDRequest">ID of who makes the request and ID of the exercise to delete.</param>
        /// <response code="200">The nutritional was deleted </response>
        /// <response code="400">Not Authorized or bad request</response>

        [Authorize]
        [HttpDelete("nutritionalEvaluation")]

        public async Task<ActionResult<NutritionalEvaluation>> Delete([FromBody] NutritionalEvaluationIDRequest nutritionalEvaluationIDRequest)
        {
            NutritionalEvaluation nutritionalEvaluation = _nutritionalEvaluationService.Delete(nutritionalEvaluationIDRequest);

            if (nutritionalEvaluation == null)
                return BadRequest(new { message = "Invalid request. Check your role or given params." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new { message = "Nutritional Evaluation deleted", nutritionalEvaluation });
        }

        /// <summary>
        /// Updates a nutritional evaluation, if authorized and if it exists
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /update-nutritionalEvaluation
        ///     {
        ///        "request_id": "request_id",
        ///        "nutritional_evaluation_id":"nutritional_evaluation_id"
        ///        "suggestions":"suggestions" ->not required 
        ///        "schedule":"schedule" ->not required
        ///        "athlete_id":"athlete_id" ->not required
        /// }
        ///
        /// </remarks> 
        /// <param name="updateNutritionalEvaluationRequest">Json object with request ID,nutritional_evalution_id,suggestions,schedule,and athlete_id</param>
        /// <returns>If the nutrition evaluation was updated, ok is retrived, badrequest otherwise</returns>
        /// <response code="200">The evaluation was updated </response>
        /// <response code="400">The request was invalid, so the evaluation wasn't updated, verify your role or the params</response>
        [Authorize]
        [HttpPut("update-nutritionalEvaluation")]
        public async Task<ActionResult<NutritionalEvaluation>> UpdateNutritioonalEvaluation([FromBody] UpdateNutritionalEvaluationRequest updateNutritionalEvaluationRequest)
        {
            NutritionalEvaluation nutritionalEvaluationUpdated= _nutritionalEvaluationService.UpdateNutritionalEvaluation(updateNutritionalEvaluationRequest);

            if (nutritionalEvaluationUpdated == null)
                return BadRequest(new { message = "Invalid request. Check given params or your role." });

            await _appDbContext.SaveChangesAsync();

            return Ok(new
            {
                message = "Nutritional Evaluation updated.",
                nutritionalEvaluationUpdated
            });
        }

    }
}
