﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.TrainingPlans
{
    /// <summary>
    /// Update Training Plan Request Model
    /// </summary>
    public class UpdateTrainingPlanRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Training Plan ID
        /// </summary>
        [Required]
        public int training_plan_id { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// Athlete ID 
        /// </summary>
        public int athlete_id { get; set; }
    }
}
