﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.TrainingPlans
{
    /// <summary>
    /// Add Exercise To Plan Request Model
    /// </summary>
    public class AddExerciseToPlanRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Training Plan ID
        /// </summary>
        [Required]
        public int plan_id { get; set; }
        /// <summary>
        /// Exercise ID
        /// </summary>
        [Required]
        public int exercise_id { get; set; }
        /// <summary>
        /// Circuit
        /// </summary>
        [Required]
        public int circuit { get; set; }
        /// <summary>
        /// Time
        /// </summary>
        public String time { get; set; }
        /// <summary>
        /// Repetitions
        /// </summary>
        public String repetitions { get; set; }
        /// <summary>
        /// Sets
        /// </summary>
        [Required]
        public int sets { get; set; }
    }
}
