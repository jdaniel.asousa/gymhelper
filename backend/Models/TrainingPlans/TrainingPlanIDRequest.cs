﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.TrainingPlans
{
    /// <summary>
    /// Training Plan ID Request Model
    /// </summary>
    public class TrainingPlanIDRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Training Plan ID
        /// </summary>
        [Required]
        public int training_plan_id { get; set; }
    }
}
