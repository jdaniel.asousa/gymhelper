﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Exercises
{
    /// <summary>
    /// Exercise ID Request Model
    /// </summary>
    public class ExerciseIDRequest
    {
        /// <summary>
        /// Request ID 
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Exercise ID 
        /// </summary>
        [Required]
        public int exercise_id { get; set; }
    }
}
