﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Exercises
{
    /// <summary>
    /// Update Exercise Request Model
    /// </summary>
    public class UpdateExerciseRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Exercise ID
        /// </summary>
        [Required]
        public int exercise_id { get; set; }
        /// <summary>
        /// Type
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// Muscle
        /// </summary>
        public string muscle { get; set; }
        /// <summary>
        /// Video
        /// </summary>
        public string video { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// Machine ID
        /// </summary>
        public int? machine_id { get; set; }
    }
}