﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Exercises
{
    /// <summary>
    /// Create Exercise Request Model
    /// </summary>
    public class CreateExerciseRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Type
        /// </summary>
        [Required]
        public string type { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        [Required]
        public string name { get; set; }
        /// <summary>
        /// Muscle 
        /// </summary>
        [Required]
        public string muscle { get; set; }
        /// <summary>
        /// Video
        /// </summary>
        public string video { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// Machine ID
        /// </summary>
        public int machine_id { get; set; }
    }
}
