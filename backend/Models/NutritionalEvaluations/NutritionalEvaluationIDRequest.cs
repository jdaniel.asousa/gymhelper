﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.NutritionalEvaluations
{
    /// <summary>
    /// Nutritional Evaluation ID Request Model
    /// </summary>
    public class NutritionalEvaluationIDRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Nutritional Evaluation ID
        /// </summary>
        [Required]
        public int nutritional_evaluation_id { get; set; }
    }
}
