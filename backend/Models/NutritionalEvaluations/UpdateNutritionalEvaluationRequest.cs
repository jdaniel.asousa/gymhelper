﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.NutritionalEvaluations
{
    /// <summary>
    /// Update Nutritional Evaluation Model
    /// </summary>
    public class UpdateNutritionalEvaluationRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Nutritional Evaluation ID
        /// </summary>
        [Required]
        public int nutritional_evaluation_id;
        /// <summary>
        /// Suggestions
        /// </summary>
        public string suggestions { get; set; }
        /// <summary>
        /// Schedule
        /// </summary>
        public String schedule { get; set; }
        /// <summary>
        /// Athlete ID
        /// </summary>
        public int athlete_id { get; set; } 
    
    }
}
