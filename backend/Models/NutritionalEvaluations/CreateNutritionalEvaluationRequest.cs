﻿using backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.NutritionalEvaluations
{
    /// <summary>
    /// Nutritional Evaluation Request Model
    /// </summary>
    public class CreateNutritionalEvaluationRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Suggestions
        /// </summary>
        public string suggestions { get; set; }
        /// <summary>
        /// Schedule
        /// </summary>
        [Required]
        public String schedule { get; set; }
        /// <summary>
        /// Athlete ID
        /// </summary>
        [Required]
        public int athlete_id { get; set; }
        /// <summary>
        /// Nutricionist ID
        /// </summary>
        [Required]
        public int nutricionist_id { get; set; }
    }
}
