﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.TrainingExerciseHistories
{
    /// <summary>
    /// Create Training Exercise History Request
    /// </summary>
    public class CreateTrainingExerciseHistoryRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Training Exercise ID
        /// </summary>
        [Required]
        public int training_exercise_id { get; set; }
        /// <summary>
        /// Time
        /// </summary>
        public String time { get; set; }
        /// <summary>
        /// Repetitions
        /// </summary>
        public String repetitions { get; set; }
    }
}
