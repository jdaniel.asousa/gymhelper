﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.TrainingExerciseHistories
{
    /// <summary>
    /// Training Exercise History ID Request
    /// </summary>
    public class TrainingExerciseHistoryIDRequest
    {
        /// <summary>
        /// Request ID 
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Trainig Exercise History ID 
        /// </summary>
        [Required]
        public int training_exercise_history_id { get; set; }
    }
}
