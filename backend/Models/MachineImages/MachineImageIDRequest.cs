﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.MachineImages
{
    /// <summary>
    /// Machine Image ID Request
    /// </summary>
    public class MachineImageIDRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Machine Image ID
        /// </summary>
        [Required]
        public int machine_image_id { get; set; }
    }
}
