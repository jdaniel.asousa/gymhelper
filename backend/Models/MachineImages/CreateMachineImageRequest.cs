﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.MachineImages
{
    /// <summary>
    /// Create Machine Image Request
    /// </summary>
    public class CreateMachineImageRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// image
        /// </summary>
        [Required]
        public IFormFile image { get; set; }
        /// <summary>
        /// Machine ID
        /// </summary>
        [Required]
        public int machine_id { get; set; }
    }
}
