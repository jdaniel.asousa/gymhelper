﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.MachineImages
{
    /// <summary>
    /// Update Machine Image Request
    /// </summary>
    public class UpdateMachineImageRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// machine image ID
        /// </summary>
        [Required]
        public int machine_image_id { get; set; }
        /// <summary>
        /// Image
        /// </summary>
        public IFormFile image { get; set; }
        /// <summary>
        /// Machine ID
        /// </summary>
        public int machine_id { get; set; }
    }
}
