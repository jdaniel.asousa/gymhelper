﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.MemberPlans
{
    /// <summary>
    /// Create Member Plan Request Model
    /// </summary>
    public class CreateMemberPlanRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Monthly Plan ID
        /// </summary>
        [Required]
        public int monthly_plan_id { get; set; }
        /// <summary>
        /// Athlete ID
        /// </summary>
        [Required]
        public int athlete_id { get; set; }
  
        

    }
}
