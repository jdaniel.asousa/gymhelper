﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.MemberPlans
{
    /// <summary>
    /// Update Member Plan Request
    /// </summary>
    public class UpdateMemberPlanRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Member Plan ID
        /// </summary>
        [Required]
        public int member_plan_id { get; set; }
        /// <summary>
        /// Athlete ID
        /// </summary>
        public int athlete_id { get; set; }
        /// <summary>
        /// Monthly Plan ID
        /// </summary>
        public int monthly_id { get; set; }
    }
}