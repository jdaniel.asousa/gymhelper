﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.MemberPlans
{
   /// <summary>
   /// Member Plan ID Request Model
   /// </summary>
    public class MemberPlanIDRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id;
        /// <summary>
        /// Member Plan ID
        /// </summary>
        [Required]
        public int member_plan_id;
    }
}
