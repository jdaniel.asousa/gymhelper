﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.MemberPlans
{
    /// <summary>
    /// Give Entry Receptionist Request
    /// </summary>
    public class GiveEntryReceptionistRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id;
        /// <summary>
        /// Member Plan ID
        /// </summary>
        [Required]
        public int athlete_id;
    }
}
