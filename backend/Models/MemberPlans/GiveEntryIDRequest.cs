﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.MemberPlans
{
    /// <summary>
    /// Give Entry ID Request
    /// </summary>
    public class GiveEntryIDRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id;
    }
}
