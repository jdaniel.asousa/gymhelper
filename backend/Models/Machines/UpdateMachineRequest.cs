﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Machines
{
    /// <summary>
    /// Update Machine Request Model
    /// </summary>
    public class UpdateMachineRequest
    {
        /// <summary>
        /// Request ID 
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Machine ID
        /// </summary>
        [Required]
        public int machine_id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// State
        /// </summary>
        [Required]
        public Boolean state { get; set; }
    }
}
