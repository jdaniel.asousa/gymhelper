﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Machines
{
    /// <summary>
    /// Machine ID Request Model
    /// </summary>
    public class MachineIDRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Machine ID
        /// </summary>
        [Required]
        public int machine_id { get; set; }
    }
}
