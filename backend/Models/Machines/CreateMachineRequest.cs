﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Machines
{
    /// <summary>
    /// Create Machine Request Model
    /// </summary>
    public class CreateMachineRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        [Required]
        public string name { get; set; }
        /// <summary>
        /// State
        /// </summary>
        [Required]
        public Boolean state { get; set; }
    }
}
