﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Entities;
using Microsoft.EntityFrameworkCore;

namespace backend.Models
{
    /// <summary>
    /// App Database Context
    /// </summary>
    public class AppDbContext : DbContext
    {
        /// <summary>
        ///  App Database Context Constructor
        /// </summary>
        /// <param name="options">Options</param>
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }
        /// <summary>
        /// Method On Configuring
        /// </summary>
        /// <param name="optionsBuilder">Options Builder</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
        /// <summary>
        /// Users
        /// </summary>
        public DbSet<User> users { get; set; }
        /// <summary>
        /// Athletes Info
        /// </summary>
        public DbSet<AthletesInfo> athletes_info { get; set; }
        /// <summary>
        /// Exercises
        /// </summary>
        public DbSet<Exercise> exercises { get; set; }
        /// <summary>
        /// Machines
        /// </summary>
        public DbSet<Machine> machines { get; set; }
        /// <summary>
        /// Meals
        /// </summary>
        public DbSet<Meal> meals { get; set; }
        /// <summary>
        /// Monthly plans
        /// </summary>
        public DbSet<MonthlyPlan> monthly_plans { get; set; }
       /// <summary>
       /// MemberPlans
       /// </summary>
        public DbSet<MemberPlan> member_plans { get; set; }
        /// <summary>
        /// Nutritional Evaluations
        /// </summary>
        public DbSet<NutritionalEvaluation> nutritional_evaluations { get; set; }
        /// <summary>
        /// Nutricion Plans
        /// </summary>
        public DbSet<NutritionPlan> nutrition_plans { get; set; }
        /// <summary>
        /// Physical Evaluations
        /// </summary>
        public DbSet<PhysicalEvaluation> physical_evaluations { get; set; }
        /// <summary>
        /// Roles
        /// </summary>
        public DbSet<Role> roles { get; set; }
        /// <summary>
        /// Training Plans
        /// </summary>
        public DbSet<TrainingPlan> training_plans { get; set; }
        /// <summary>
        /// Training Exercises
        /// </summary>
        public DbSet<TrainingExercise> training_exercises { get; set; }
        /// <summary>
        /// Employees Info
        /// </summary>
        public DbSet<EmployeeInfo> employees_info { get; set; }
        /// <summary>
        /// Exercise Images
        /// </summary>
        public DbSet<ExerciseImage> exercise_images { get; set; }
        /// <summary>
        /// Machine Images
        /// </summary>
        public DbSet<MachineImage> machine_images { get; set; }
        /// <summary>
        /// Training Exercises History
        /// </summary>
        public DbSet<TrainingExercisesHistory> training_exercises_history { get; set; }
        /// <summary>
        /// On Model Creating Method
        /// </summary>
        /// <param name="modelBuilder">Model Builder</param>
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasKey(p => p.id);
            modelBuilder.Entity<AthletesInfo>()
                .HasKey(p => p.id);
            modelBuilder.Entity<Exercise>()
                .HasKey(p => p.id);
            modelBuilder.Entity<Machine>()
                .HasKey(p => p.id);
            modelBuilder.Entity<Meal>()
                 .HasKey(p => p.id);
            modelBuilder.Entity<MonthlyPlan>()
                .HasKey(p => p.id);
            modelBuilder.Entity<MemberPlan>()
                .HasKey(p => p.id);
            modelBuilder.Entity<NutritionalEvaluation>()
                 .HasKey(p => p.id);
            modelBuilder.Entity<NutritionPlan>()
                 .HasKey(p => p.id);
            modelBuilder.Entity<PhysicalEvaluation>()
                 .HasKey(p => p.id);
            modelBuilder.Entity<Role>()
                 .HasKey(p => p.id);
            modelBuilder.Entity<TrainingPlan>()
                 .HasKey(p => p.id);
            modelBuilder.Entity<TrainingExercise>()
                 .HasKey(p => p.id);
            modelBuilder.Entity<EmployeeInfo>()
                 .HasKey(p => p.id);
            modelBuilder.Entity<ExerciseImage>()
                 .HasKey(p => p.id);
            modelBuilder.Entity<MachineImage>()
                 .HasKey(p => p.id);
            modelBuilder.Entity<TrainingExercisesHistory>()
                 .HasKey(p => p.id);

            modelBuilder.Entity<User>()
            .HasOne(p => p.role)
            .WithMany(b => b.users)
            .HasForeignKey(p => p.role_id);
            modelBuilder.Entity<Exercise>()
            .HasOne(p => p.machine)
            .WithMany(b => b.exercises)
            .HasForeignKey(p => p.machine_id);
            modelBuilder.Entity<Meal>()
            .HasOne(p => p.nutrition_plan)
            .WithMany(b => b.meals)
            .HasForeignKey(p => p.nutrition_plan_id);
            modelBuilder.Entity<MemberPlan>()
                .HasOne(p => p.monthlyPlan)
                .WithMany(b => b.member_plans)
                .HasForeignKey(p => p.monthly_plan_id);
            modelBuilder.Entity<MemberPlan>()
               .HasOne(p => p.athletesInfo)
               .WithMany(b => b.member_plans)
               .HasForeignKey(p => p.athlete_id);
            modelBuilder.Entity<NutritionalEvaluation>()
            .HasOne(p => p.athlete)
            .WithMany(b => b.nutritional_evaluations)
            .HasForeignKey(p => p.athlete_id);
            modelBuilder.Entity<PhysicalEvaluation>()
            .HasOne(p => p.athletesInfo)
            .WithMany(b => b.physical_evaluations)
            .HasForeignKey(p => p.athlete_id);
            modelBuilder.Entity<TrainingPlan>()
            .HasOne(p => p.athlete)
            .WithMany(b => b.training_plans)
            .HasForeignKey(p => p.athlete_id);
            modelBuilder.Entity<TrainingExercise>()
            .HasOne(p => p.plan)
            .WithMany(b => b.training_exercises)
            .HasForeignKey(p => p.plan_id);
            modelBuilder.Entity<TrainingExercise>()
            .HasOne(p => p.exercise)
            .WithMany(b => b.training_exercises)
            .HasForeignKey(p => p.exercise_id);
            modelBuilder.Entity<ExerciseImage>()
            .HasOne(p => p.exercise)
            .WithMany(b => b.exercise_images)
            .HasForeignKey(p => p.exercise_id);
            modelBuilder.Entity<MachineImage>()
            .HasOne(p => p.machine)
            .WithMany(b => b.machine_images)
            .HasForeignKey(p => p.machine_id);
            modelBuilder.Entity<NutritionalEvaluation>()
            .HasOne(p => p.nutrition_plan)
            .WithOne(b => b.nutritional_evaluation)
            .HasForeignKey<NutritionPlan>(p => p.nutritional_evaluation_id);
            modelBuilder.Entity<User>()
            .HasOne(p => p.employee_info)
            .WithOne(b => b.user)
            .HasForeignKey<EmployeeInfo>(p => p.user_id);
            modelBuilder.Entity<User>()
            .HasOne(p => p.athlete_info)
            .WithOne(b => b.user)
            .HasForeignKey<AthletesInfo>(p => p.user_id);
            modelBuilder.Entity<AthletesInfo>()
            .HasOne(p => p.personal_trainer)
            .WithMany(b => b.athletes)
            .HasForeignKey(p => p.personal_trainer_id);
            modelBuilder.Entity<NutritionalEvaluation>()
            .HasOne(p => p.nutricionist)
            .WithMany(b => b.nutritional_evaluations)
            .HasForeignKey(p => p.nutricionist_id);
            modelBuilder.Entity<TrainingExercisesHistory>()
            .HasOne(p => p.training_exercise)
            .WithMany(b => b.training_exercises_history)
            .HasForeignKey(p => p.training_exercise_id);
            modelBuilder.Entity<ExerciseImage>()
            .HasOne(p => p.exercise)
            .WithMany(b => b.exercise_images)
            .HasForeignKey(p => p.exercise_id);
            modelBuilder.Entity<MachineImage>()
            .HasOne(p => p.machine)
            .WithMany(b => b.machine_images)
            .HasForeignKey(p => p.machine_id);
        }
    }
}
