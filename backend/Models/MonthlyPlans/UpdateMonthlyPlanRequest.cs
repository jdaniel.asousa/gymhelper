﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.MonthlyPlans
{
    /// <summary>
    /// Update Monthly Plan Request Model
    /// </summary>
    public class UpdateMonthlyPlanRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Monthly Plan ID
        /// </summary>
        [Required]
        public int monthly_plan_id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// Price
        /// </summary>
        public int price { get; set; }
        /// <summary>
        /// Entrys 
        /// </summary>
        public int entrys { get; set; }

    }
}
