﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.MonthlyPlans
{
    /// <summary>
    /// Create Monthly Plan Request Model
    /// </summary>
    public class CreateMonthlyPlanRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        [Required]
        public string name { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// Price
        /// </summary>
        [Required]
        public int price { get; set; }
        /// <summary>
        /// Entrys
        /// </summary>
        [Required]
        public int entrys { get; set; }
    }
}
