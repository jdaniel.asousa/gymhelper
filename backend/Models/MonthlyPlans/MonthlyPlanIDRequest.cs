﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.MonthlyPlans
{
    /// <summary>
    /// Monthly Plan ID Request Model
    /// </summary>
    public class MonthlyPlanIDRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Monthly Plan ID
        /// </summary>
        [Required]
        public int monthly_plan_id { get; set; }
    }
}
