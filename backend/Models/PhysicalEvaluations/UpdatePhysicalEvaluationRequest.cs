﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace backend.Models.PhysicalEvaluations
{
    /// <summary>
    /// Model used in update Physical Evaluation Request
    /// </summary>
    public class UpdatePhysicalEvaluationRequest
    {
        /// <summary>
        /// Id of who makes the request
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Id of physical evaluation to update
        /// </summary>
        [Required]
        public int physical_evaluation_id { get; set; }
        /// <summary>
        /// Height 
        /// </summary>
        public float height { get; set; }
        /// <summary>
        /// Weight 
        /// </summary>
        public float weight { get; set; }
        /// <summary>
        /// IMC 
        /// </summary>
        public float imc { get; set; }
        /// <summary>
        /// Description 
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// Body water 
        /// </summary>
        public float body_water { get; set; }
        /// <summary>
        /// Body fat 
        /// </summary>
        public float body_fat { get; set; }
        /// <summary>
        /// Muscle mass 
        /// </summary>
        public float muscle_mass { get; set; }
        /// <summary>
        /// Lat size 
        /// </summary>
        public float lat_size { get; set; }
        /// <summary>
        /// Left Bicep Size 
        /// </summary>
        public float left_bicep_size { get; set; }
        /// <summary>
        /// Right Bicep Size 
        /// </summary>
        public float right_bicep_size { get; set; }
        /// <summary>
        /// Chest Size 
        /// </summary>
        public float chest_size { get; set; }
        /// <summary>
        /// Waist Size 
        /// </summary>
        public float waist_size { get; set; }
        /// <summary>
        /// Glute Size 
        /// </summary>
        public float glute_size { get; set; }
        /// <summary>
        /// Hip Size 
        /// </summary>
        public float hip_size { get; set; }
        /// <summary>
        /// Tigh Size 
        /// </summary>
        public float thigh_size { get; set; }
        /// <summary>
        /// Calfs Size 
        /// </summary>
        public float calfs_size { get; set; }
        /// <summary>
        /// Athlete ID 
        /// </summary>
        public int athlete_id { get; set; }
    }
}
