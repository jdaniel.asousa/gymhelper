﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.PhysicalEvaluations
{
    /// <summary>
    /// Create Physical Evaluation Request Model
    /// </summary>
    public class CreatePhysicalEvaluationRequest
    {
        /// <summary>
        /// Request ID 
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Height
        /// </summary>
        [Required]
        public float height { get; set; }
        /// <summary>
        /// Weight
        /// </summary>
        [Required]
        public float weight { get; set; }
        /// <summary>
        /// IMC
        /// </summary>
        [Required]
        public float imc { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        [Required] 
        public string description { get; set; }
        /// <summary>
        /// Body Water
        /// </summary>
        [Required] 
        public float body_water { get; set; }
        /// <summary>
        /// Body Fat
        /// </summary>
        [Required] 
        public float body_fat { get; set; }
        /// <summary>
        /// Muscle mass
        /// </summary>
        [Required] 
        public float muscle_mass { get; set; }
        /// <summary>
        /// Lat Size
        /// </summary>
        [Required] 
        public float lat_size { get; set; }
        /// <summary>
        /// Left Bicep Size
        /// </summary>
        [Required]
        public float left_bicep_size { get; set; }
        /// <summary>
        /// Right Bicep Size
        /// </summary>
        [Required]
        public float right_bicep_size { get; set; }
        /// <summary>
        /// Chest Size
        /// </summary>
        [Required]
        public float chest_size { get; set; }
        /// <summary>
        /// Waist Size
        /// </summary>
        [Required]
        public float waist_size { get; set; }
        /// <summary>
        /// Glute Size
        /// </summary>
        [Required]
        public float glute_size { get; set; }
        /// <summary>
        /// Hip Size
        /// </summary>
        [Required]
        public float hip_size { get; set; }
        /// <summary>
        /// Thigh Size
        /// </summary>
        [Required]
        public float thigh_size { get; set; }
        /// <summary>
        /// Calfs Size
        /// </summary>
        [Required]
        public float calfs_size { get; set; }
        /// <summary>
        /// Athlete ID
        /// </summary>
        [Required]
        public int athlete_id { get; set; }
    }
}
