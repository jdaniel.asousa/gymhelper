﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.PhysicalEvaluations
{
    /// <summary>
    /// Physical Evaluation ID Request Model
    /// </summary>
    public class PhysicalEvaluationIDRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Physical evaluation id
        /// </summary>
        [Required]
        public int physical_evaluation_id { get; set; }
    }
}
