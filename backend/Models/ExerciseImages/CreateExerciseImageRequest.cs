﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.ExerciseImages
{
    /// <summary>
    /// Create Exercise Image Request
    /// </summary>
    public class CreateExerciseImageRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// image
        /// </summary>
        [Required]
        public IFormFile image { get; set; }
        /// <summary>
        /// Exercise ID
        /// </summary>
        [Required]
        public int exercise_id { get; set; }
    }
}
