﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.ExerciseImages
{
    /// <summary>
    /// Update Exercise Image Request
    /// </summary>
    public class UpdateExerciseImageRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Exercise Image ID
        /// </summary>
        [Required]
        public int exercise_image_id { get; set; }
        /// <summary>
        /// Image
        /// </summary>
        public IFormFile image { get; set; }
        /// <summary>
        /// Exercise ID
        /// </summary>
        public int exercise_id { get; set; }
    }
}
