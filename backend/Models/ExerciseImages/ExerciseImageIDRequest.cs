﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.ExerciseImages
{
    /// <summary>
    /// Exercise Image ID Request
    /// </summary>
    public class ExerciseImageIDRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Exercise Image ID
        /// </summary>
        [Required]
        public int exercise_image_id { get; set; }
    }
}
