﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.NutritionPlans
{
    /// <summary>
    /// Create Nutrition Plan Request Model
    /// </summary>
    public class CreateNutritionPlanRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Nutritional Evaluation ID
        /// </summary>
        [Required]
        public int nutritional_evaluation_id { get; set; }
       
    }
}
