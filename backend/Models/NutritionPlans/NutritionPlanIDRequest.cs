﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.NutritionPlans
{
    /// <summary>
    /// Nutrition Plan ID Request Model
    /// </summary>

    public class NutritionPlanIDRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Nutrition Plan ID
        /// </summary>
        [Required]
        public int nutrition_plan_id { get; set; }

    }

}
