﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.NutritionPlans
{
    /// <summary>
    /// Update Nutrition Plan Request Model
    /// </summary>
    public class UpdateNutritionPlanRequest
    {
        /// <summary>
        /// Nutrition Plan ID
        /// </summary>
        [Required]
        public int nutrition_plan_id { get; set; }
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Nutritional Evaluation ID 
        /// </summary>
        public int nutritional_evaluation_id { get; set; }

    }
}
