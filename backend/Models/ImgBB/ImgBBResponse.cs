﻿using backend.Models.ImgBB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models
{
    /// <summary>
    /// ImgBBResponse
    /// </summary>
    public class ImgBBResponse
    {
        /// <summary>
        /// data
        /// </summary>
        public ImgBBdata data { get; set; }
        /// <summary>
        /// success
        /// </summary>
        public bool success { get; set; }
        /// <summary>
        /// status
        /// </summary>
        public int status { get; set; }
    }
}
