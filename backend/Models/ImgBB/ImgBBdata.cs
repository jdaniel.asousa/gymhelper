﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.ImgBB
{
    /// <summary>
    /// ImgBBdata
    /// </summary>
    public class ImgBBdata
    {
        /// <summary>
        /// id
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// title
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// url viewer
        /// </summary>
        public string url_viewer { get; set; }
        /// <summary>
        /// url
        /// </summary>
        public string url { get; set; }
        /// <summary>
        /// display url
        /// </summary>
        public string display_url { get; set; }
        /// <summary>
        /// size
        /// </summary>
        public int size { get; set; }
        /// <summary>
        /// time
        /// </summary>
        public string time { get; set; }
        /// <summary>
        /// expiration
        /// </summary>
        public string expiration { get; set; }
        /// <summary>
        /// image
        /// </summary>
        public Image image { get; set; }
        /// <summary>
        /// thumb
        /// </summary>
        public Thumb thumb { get; set; }
        /// <summary>
        /// medium
        /// </summary>
        public Medium medium { get; set; }
        /// <summary>
        /// delete url
        /// </summary>
        public string delete_url { get; set; }
    }
    /// <summary>
    /// Image
    /// </summary>
    public class Image
    {
        /// <summary>
        /// filename
        /// </summary>
        public string filename { get; set; }
        /// <summary>
        /// name
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// mime
        /// </summary>
        public string mime { get; set; }
        /// <summary>
        /// extension
        /// </summary>
        public string extension { get; set; }
        /// <summary>
        /// url
        /// </summary>
        public string url { get; set; }
    }
    /// <summary>
    /// Thumb
    /// </summary>
    public class Thumb
    {
        /// <summary>
        /// filename
        /// </summary>
        public string filename { get; set; }
        /// <summary>
        /// name
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// mime
        /// </summary>
        public string mime { get; set; }
        /// <summary>
        /// extension
        /// </summary>
        public string extension { get; set; }
        /// <summary>
        /// url
        /// </summary>
        public string url { get; set; }
    }
    /// <summary>
    /// Medium
    /// </summary>
    public class Medium
    {
        /// <summary>
        /// filename
        /// </summary>
        public string filename { get; set; }
        /// <summary>
        /// name
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// mime
        /// </summary>
        public string mime { get; set; }
        /// <summary>
        /// extension
        /// </summary>
        public string extension { get; set; }
        /// <summary>
        /// url
        /// </summary>
        public string url { get; set; }
    }
}
