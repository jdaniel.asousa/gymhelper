﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Meals
{
    /// <summary>
    /// Create Meal Model
    /// </summary>
    public class CreateMealRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Calories
        /// </summary>
        [Required]
        public float calories { get; set; }
        /// <summary>
        /// Protein
        /// </summary>
        [Required]
        public float protein { get; set; }
        /// <summary>
        /// Fats
        /// </summary>
        [Required]
        public float fats { get; set; }
        /// <summary>
        /// Carbohydrates
        /// </summary>
        [Required]
        public float carbohydrates { get; set; }
        /// <summary>
        /// Meal description
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// Nutrition Plan ID
        /// </summary>
        [Required]
        public int nutrition_plan_id { get; set; }

    }
}
