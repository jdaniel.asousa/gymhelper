﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Meals
{
    /// <summary>
    /// Meal ID Request Model
    /// </summary>
    public class MealIDRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Meal ID
        /// </summary>
        [Required]
        public int meal_id { get; set; }
    }
}
