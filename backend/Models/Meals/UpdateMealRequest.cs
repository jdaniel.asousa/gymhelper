﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Meals
{
    /// <summary>
    /// Update Meal Request Model
    /// </summary>
    public class UpdateMealRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Meal ID
        /// </summary>
        [Required]
        public int meal_id { get; set; }
        /// <summary>
        /// Calories
        /// </summary>
        public float calories { get; set; }
        /// <summary>
        /// Protein
        /// </summary>
        public float protein { get; set; }
        /// <summary>
        /// Fats
        /// </summary>
        public float fats { get; set; }
        /// <summary>
        /// Carbohydrates
        /// </summary>
        public float carbohydrates { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string description { get; set; }

    }
}
