﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace backend.Models.Users
{
    /// <summary>
    /// Update User Request Model
    /// </summary>
    public class UpdateUserRequest
    {
        /// <summary>
        /// ID
        /// </summary>
        [Required]
        public int id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// Password
        /// </summary>
        public string password { get; set; }
        /// <summary>
        /// Confirm Password
        /// </summary>
        public string confirmPassword { get; set; }
        /// <summary>
        /// Avatar
        /// </summary>
        public IFormFile avatar { get; set; }
        /// <summary>
        /// Birth date
        /// </summary>
        public String birth_date { get; set; }
    }
}
