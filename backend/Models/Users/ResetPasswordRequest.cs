﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Users
{
    /// <summary>
    /// Reset Password Request Model
    /// </summary>
    public class ResetPasswordRequest
    {
        /// <summary>
        /// Token
        /// </summary>
        [Required]
        public string Token { get; set; }
        /// <summary>
        /// Password
        /// </summary>
        [Required]
        public string Password { get; set; }
        /// <summary>
        /// Confirm Password
        /// </summary>
        [Required]
        public string ConfirmPassword { get; set; }
    }
}
