﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Users
{
    /// <summary>
    /// Forgot Password Request Model
    /// </summary>
    public class ForgotPasswordRequest
    {
        /// <summary>
        /// Email
        /// </summary>
        [Required]
        public string Email { get; set; }
        /// <summary>
        /// Origin
        /// </summary>
        [Required]
        public string Origin { get; set; }
    }
}
