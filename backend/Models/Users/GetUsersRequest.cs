﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Users
{
    /// <summary>
    /// Get Users Request Model
    /// </summary>
    public class GetUsersRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
    }
}
