﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Users
{
    /// <summary>
    /// User Info Response Model
    /// </summary>
    public class UserInfoResponse
    {
        /// <summary>
        /// ID
        /// </summary>
        [Required]
        public int Id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        [Required]
        public string Email { get; set; }
        /// <summary>
        /// Avatar
        /// </summary>
        public string avatar { get; set; }
        /// <summary>
        /// Role ID
        /// </summary>
        [Required]
        public int role { get; set; }
        /// <summary>
        /// Athlete ID
        /// </summary>
        public int athlete_id { get; set; }
        /// <summary>
        /// Employee ID
        /// </summary>
        public int employee_id { get; set; }
        /// <summary>
        /// Birth date
        /// </summary>
        public string Token { get; set; }
    }
}
