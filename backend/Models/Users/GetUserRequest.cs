﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Users
{
    /// <summary>
    /// Get User Request Model
    /// </summary>
    public class GetUserRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// ID
        /// </summary>
        [Required]
        public int id { get; set; }
    }
}
