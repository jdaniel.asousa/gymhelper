﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Users
{
    /// <summary>
    /// Create User Request Model
    /// </summary>
    public class CreateUserRequest
    {
        /// <summary>
        /// Request ID
        /// </summary>
        [Required]
        public int request_id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        [Required]
        public string name { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        [Required]
        public string email { get; set; }
        /// <summary>
        /// Origin
        /// </summary>
        [Required]
        public string origin { get; set; }
        /// <summary>
        /// Role ID
        /// </summary>
        [Required]
        public int role_id { get; set; }
        /// <summary>
        /// Personal Trainer ID
        /// </summary>
        public int personal_trainer_id { get; set; }
        /// <summary>
        /// Birth Date
        /// </summary>
        public String birth_date { get; set; }
    }
}
