import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AthletesComponent } from './athletes.component';

const routes: Routes = [
    { path: '', component: AthletesComponent },
    {
        path: ':id',
        loadChildren: () =>
            import('../athlete/athlete.module').then((m) => m.AthleteModule)
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AthletesRoutingModule {}
