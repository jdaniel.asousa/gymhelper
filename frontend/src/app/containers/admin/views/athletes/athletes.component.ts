import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ViewType } from '@shared/enums/view-type.enum';
import { dialogSize } from '@shared/functions/dialog-size.function';
import { Athlete } from '@shared/models/athlete.model';
import { CommunicationService } from '@services/communication.service';
import { UiService } from '@services/ui.service';
import { UserService } from '@services/user.service';
import { CreateAthleteComponent } from './components/dialogs/create-athlete/create-athlete.component';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-athletes',
    templateUrl: './athletes.component.html',
    styleUrls: ['./athletes.component.sass']
})
export class AthletesComponent implements OnInit, OnDestroy {
    athletes: Athlete[];
    /**
     * Change view between grid and list
     * Using created enum
     * Grid => 1
     * List => 2
     */
    viewType = ViewType.Grid;
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialog: MatDialog,
        public uiService: UiService,
        public userService: UserService,
        public communication: CommunicationService
    ) {
        this.uiService.setTitle('Atletas');
    }

    ngOnInit(): void {
        this.fetchData();

        this.subs.sink = this.communication.getStatus().subscribe((status) => {
            this.fetchData();
        });
    }

    fetchData(): void {
        this.subs.sink = this.userService
            .getAthletes()
            .subscribe((data) => (this.athletes = data));
    }

    openCreateDialog() {
        this.subs.sink = this.dialog
            .open(CreateAthleteComponent, {
                width: dialogSize()
            })
            .afterClosed()
            .subscribe((res) => {
                if (res) {
                    this.uiService.showSnackBar(res.message);

                    if (res.status === true) {
                        this.fetchData();
                    }
                }
            });
    }

    /**
     * Method to change view type
     * @param view integer using ViewType Enum
     */
    changeView(view: ViewType) {
        this.viewType = view;
    }

    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
