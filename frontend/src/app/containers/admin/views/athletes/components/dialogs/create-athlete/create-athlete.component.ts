import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { UserService } from '@services/user.service';
import { UiService } from '@services/ui.service';
import { SessionService } from '@services/session.service';
import { validateEmail } from '@shared/validators/custom-email.validator';
import { Messages } from '@enums/messages.enum';
import { Roles } from '@enums/roles.enum';
import { Observable } from 'rxjs';
import { User } from '@models/user.model';
import { formatDate } from '@functions/format-date.function';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-create-athlete',
    templateUrl: './create-athlete.component.html',
    styleUrls: ['./create-athlete.component.sass']
})
export class CreateAthleteComponent implements OnInit, OnDestroy {
    personalTrainers$: Observable<User[]>;
    /**
     * User Logged, using user service and method me() to get User Data
     */
    userLogged;
    athleteForm: FormGroup;
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialogRef: MatDialogRef<CreateAthleteComponent>,
        public userService: UserService,
        public uiService: UiService,
        public session: SessionService
    ) {
        this.userLogged = this.session.me();
        this.loadPersonalTrainers();
    }

    loadPersonalTrainers() {
        if (this.userLogged.role !== Roles.PersonalTrainer) {
            this.personalTrainers$ = this.userService.getPersonalTrainers();
        }
    }

    ngOnInit(): void {
        this.athleteForm = new FormGroup({
            email: new FormControl('', [
                Validators.required,
                Validators.email,
                validateEmail
            ]),
            name: new FormControl('', [Validators.required]),
            birthdayDate: new FormControl('', [Validators.required]),
            personalTrainer: new FormControl('', [Validators.required])
        });
    }

    /**
     * When Submit Create Athlete Form
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        // Invalid form submitted
        if (this.athleteForm.invalid) {
            this.uiService.showSnackBar(Messages.InvalidForm);
        }

        const body = {
            name: this.athleteForm.get('name').value,
            email: this.athleteForm.get('email').value,
            role_id: Roles.Athlete,
            personal_trainer_id:
                this.userLogged.role === Roles.PersonalTrainer
                    ? this.userLogged.id
                    : this.athleteForm.get('personalTrainer').value,
            birth_date: formatDate(
                this.athleteForm.get('birthdayDate').value._i,
                'database'
            )
        };

        /**
         * Note: is used user service because the method is the same
         * Only add birthday date on athlete
         */
        this.subs.sink = this.userService.new(body).subscribe(
            () => {
                this.dialogRef.close({
                    message: Messages.AthleteCreated,
                    status: true
                });
            },
            (error) => {
                this.dialogRef.close({
                    message:
                        error.status === 401
                            ? Messages.Unauthorized
                            : Messages.ErrorAthleteCreation,
                    status: false
                });
            }
        );
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }

    /**
     * Method to execute when destroy view
     * Using this method to unsubscribe all subscriptions make
     */
    ngOnDestroy(): void {
        // Prevent unsubscribe submit form without submit
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
