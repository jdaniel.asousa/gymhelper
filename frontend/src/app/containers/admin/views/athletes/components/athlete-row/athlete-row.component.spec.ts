import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AthleteRowComponent } from './athlete-row.component';

describe('AthleteRowComponent', () => {
    let component: AthleteRowComponent;
    let fixture: ComponentFixture<AthleteRowComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [AthleteRowComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(AthleteRowComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
