import { UiService } from '@services/ui.service';
import { dialogSize } from '@shared/functions/dialog-size.function';
import { DeleteAthleteComponent } from '../dialogs/delete-athlete/delete-athlete.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, Input, OnDestroy } from '@angular/core';
import { Athlete } from '@models/athlete.model';
import { CommunicationService } from '@services/communication.service';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-athlete-card',
    templateUrl: './athlete-card.component.html',
    styleUrls: ['./athlete-card.component.sass']
})
export class AthleteCardComponent implements OnDestroy {
    @Input()
    athlete: Athlete;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialog: MatDialog,
        public ui: UiService,
        public communication: CommunicationService
    ) {}

    /**
     * Executed method when view is destroyed
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    openDeleteDialog() {
        this.subs.sink = this.dialog
            .open(DeleteAthleteComponent, {
                width: dialogSize(),
                data: this.athlete
            })
            .afterClosed()
            .subscribe((res) => {
                if (res) {
                    this.ui.showSnackBar(res.message);

                    if (res.status === true) {
                        this.communication.sendStatus(true);
                    }
                }
            });
    }
}
