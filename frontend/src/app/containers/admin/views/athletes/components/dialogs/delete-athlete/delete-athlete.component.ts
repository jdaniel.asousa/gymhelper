import { User } from '@models/user.model';
import { SessionService } from '@services/session.service';
import { Messages } from '@enums/messages.enum';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserService } from '@services/user.service';
import { UiService } from '@services/ui.service';
import { Component, Inject, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-delete-athlete',
    templateUrl: './delete-athlete.component.html',
    styleUrls: ['./delete-athlete.component.sass']
})
export class DeleteAthleteComponent implements OnDestroy {
    athlete: User;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialogRef: MatDialogRef<DeleteAthleteComponent>,
        public usersService: UserService,
        public uiService: UiService,
        public session: SessionService,
        @Inject(MAT_DIALOG_DATA) private data: User
    ) {
        this.athlete = data;
    }

    /**
     * Executed method when view is destroyed
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    /**
     * When Submit Create Athlete Form
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        /**
         * Note: is used user service because the method is the same
         */
        this.subs.sink = this.usersService
            .deleteById(this.athlete.id)
            .subscribe(
                () => {
                    // Success
                    this.dialogRef.close({
                        message: Messages.AthleteDeleted,
                        status: true
                    });
                },
                () => {
                    // Error
                    this.dialogRef.close({
                        message: Messages.ErrorDeletingAthlete,
                        status: false
                    });
                }
            );
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }
}
