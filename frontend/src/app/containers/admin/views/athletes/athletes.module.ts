import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { AthletesRoutingModule } from './athletes-routing.module';
import { AthletesComponent } from './athletes.component';
import { AthleteCardComponent } from './components/athlete-card/athlete-card.component';
import { AthleteRowComponent } from './components/athlete-row/athlete-row.component';
import { CreateAthleteComponent } from './components/dialogs/create-athlete/create-athlete.component';
import { DeleteAthleteComponent } from './components/dialogs/delete-athlete/delete-athlete.component';
import { AthletesGridComponent } from './views/athletes-grid/athletes-grid.component';
import { AthletesListComponent } from './views/athletes-list/athletes-list.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';

@NgModule({
    declarations: [
        AthletesComponent,
        AthletesGridComponent,
        AthletesListComponent,
        AthleteCardComponent,
        AthleteRowComponent,
        CreateAthleteComponent,
        DeleteAthleteComponent
    ],
    imports: [
        CommonModule,
        AthletesRoutingModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatIconModule,
        FormsModule,
        ReactiveFormsModule,
        MatSidenavModule,
        MatListModule,
        MatMenuModule,
        MatDialogModule,
        MatSelectModule,
        FlexLayoutModule,
        MatDatepickerModule,
        MatMomentDateModule
    ]
})
export class AthletesModule {}
