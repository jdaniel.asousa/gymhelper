import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AthletesGridComponent } from './athletes-grid.component';

describe('AthletesGridComponent', () => {
    let component: AthletesGridComponent;
    let fixture: ComponentFixture<AthletesGridComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AthletesGridComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AthletesGridComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
