import { Component, Input, OnInit } from '@angular/core';
import { Athlete } from '@models/athlete.model';

@Component({
    selector: 'app-athletes-list',
    templateUrl: './athletes-list.component.html',
    styleUrls: ['./athletes-list.component.sass']
})
export class AthletesListComponent implements OnInit {
    @Input()
    athletes: Athlete[];

    constructor() {}

    ngOnInit(): void {}
}
