import { Component, Input, OnInit } from '@angular/core';
import { Athlete } from '@models/athlete.model';

@Component({
    selector: 'app-athletes-grid',
    templateUrl: './athletes-grid.component.html',
    styleUrls: ['./athletes-grid.component.sass']
})
export class AthletesGridComponent implements OnInit {
    @Input()
    athletes: Athlete[];

    constructor() {}

    ngOnInit(): void {}
}
