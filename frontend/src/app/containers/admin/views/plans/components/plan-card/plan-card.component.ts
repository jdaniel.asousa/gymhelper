import { Component, Input, OnInit } from '@angular/core';
import { MonthlyPlan } from '@models/monthly-plan.model';

@Component({
    selector: 'app-plan-card',
    templateUrl: './plan-card.component.html',
    styleUrls: ['./plan-card.component.sass']
})
export class PlanCardComponent implements OnInit {
    @Input()
    plan: MonthlyPlan;

    constructor() {}

    ngOnInit(): void {}
}
