import { Component, OnDestroy, OnInit } from '@angular/core';
import { MonthlyPlan } from '@models/monthly-plan.model';
import { MonthlyPlanService } from '@services/monthly-plan.service';
import { UiService } from '@services/ui.service';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-plans',
    templateUrl: './plans.component.html',
    styleUrls: ['./plans.component.sass']
})
export class PlansComponent implements OnInit, OnDestroy {
    plans: MonthlyPlan[];

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(private service: MonthlyPlanService, private ui: UiService) {
        this.ui.setTitle('Tarifas');
    }

    ngOnInit(): void {
        this.fetchData();
    }

    fetchData(): void {
        this.subs.sink = this.service.getAll().subscribe((data) => {
            this.plans = data;
        });
    }

    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
