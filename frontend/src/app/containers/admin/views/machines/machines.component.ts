import { CreateMachineComponent } from './components/dialogs/create-machine/create-machine.component';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ViewType } from '@shared/enums/view-type.enum';
import { Machine } from '@shared/models/machine.model';
import { MachineService } from '@services/machine.service';
import { UiService } from '@services/ui.service';
import { dialogSize } from '@shared/functions/dialog-size.function';
import { CommunicationService } from '@services/communication.service';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-machines',
    templateUrl: './machines.component.html',
    styleUrls: ['./machines.component.sass']
})
export class MachinesComponent implements OnInit, OnDestroy {
    machines: Machine[];
    /**
     * Change view between grid and list
     * Using created enum
     * Grid => 1
     * List => 2
     */
    viewType = ViewType.Grid;
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        private ui: UiService,
        private service: MachineService,
        public dialog: MatDialog,
        public communication: CommunicationService
    ) {
        this.ui.setTitle('Máquinas');
    }

    fetchData() {
        this.subs.sink = this.service.getAll().subscribe((data) => {
            this.machines = data;
        });
    }

    ngOnInit(): void {
        this.subs.sink = this.communication.getStatus().subscribe((status) => {
            this.fetchData();
        });

        this.fetchData();
    }

    /**
     * Method to change view type
     * @param view integer using ViewType Enum
     */
    changeView(view: ViewType) {
        this.viewType = view;
    }

    ngOnDestroy() {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    /**
     * Method to open dialog to create machine
     */
    openCreateDialog() {
        this.subs.sink = this.dialog
            .open(CreateMachineComponent, {
                width: dialogSize()
            })
            .afterClosed()
            .subscribe((res) => {
                if (res) {
                    this.ui.showSnackBar(res.message);

                    if (res.status === true) {
                        this.fetchData();
                    }
                }
            });
    }
}
