import { SharedModule } from '@shared/shared.module';
import { MachinesRoutingModule } from './machines-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MachinesComponent } from './machines.component';
import { MachineRowComponent } from './components/machine-row/machine-row.component';
import { MachineCardComponent } from './components/machine-card/machine-card.component';
import { MachinesGridComponent } from './views/machines-grid/machines-grid.component';
import { MachinesListComponent } from './views/machines-list/machines-list.component';
import { CreateMachineComponent } from './components/dialogs/create-machine/create-machine.component';
import { DeleteMachineComponent } from './components/dialogs/delete-machine/delete-machine.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { ViewMachineComponent } from './components/dialogs/view-machine/view-machine.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { ViewQrcodeComponent } from './components/dialogs/view-qrcode/view-qrcode.component';
import { EditMachineComponent } from './components/dialogs/edit-machine/edit-machine.component';

@NgModule({
    declarations: [
        MachinesComponent,
        MachinesGridComponent,
        MachinesListComponent,
        MachineCardComponent,
        MachineRowComponent,
        CreateMachineComponent,
        DeleteMachineComponent,
        ViewMachineComponent,
        ViewQrcodeComponent,
        EditMachineComponent
    ],
    exports: [MachineCardComponent],
    imports: [
        CommonModule,
        MachinesRoutingModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatIconModule,
        FormsModule,
        ReactiveFormsModule,
        MatListModule,
        MatMenuModule,
        MatDialogModule,
        MatSelectModule,
        FlexLayoutModule,
        MatExpansionModule
    ]
})
export class MachinesModule {}
