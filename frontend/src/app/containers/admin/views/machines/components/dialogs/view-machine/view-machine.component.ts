import { Component, Inject, Input, OnDestroy, OnInit } from '@angular/core';
import { Exercise } from '@models/exercise.model';
import { Machine } from '@models/machine.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ExerciseService } from '@services/exercise.service';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-view-machine',
    templateUrl: './view-machine.component.html',
    styleUrls: ['./view-machine.component.sass']
})
export class ViewMachineComponent implements OnInit, OnDestroy {
    exercises: Exercise[];

    @Input()
    machine: Machine;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialogRef: MatDialogRef<ViewMachineComponent>,
        @Inject(MAT_DIALOG_DATA) private data: Machine,
        private exerciseService: ExerciseService
    ) {
        this.machine = data;
    }

    ngOnInit(): void {
        this.subs.sink = this.exerciseService
            .getByMachineID(this.machine.id.toString())
            .subscribe((res) => {
                if (res !== null) {
                    this.exercises = res;
                }
            });
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }

    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
