import { Component, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UiService } from '@services/ui.service';
import { SessionService } from '@services/session.service';
import { Messages } from '@enums/messages.enum';
import { Machine } from '@models/machine.model';
import { MachineService } from '@services/machine.service';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-delete-machine',
    templateUrl: './delete-machine.component.html',
    styleUrls: ['./delete-machine.component.sass']
})
export class DeleteMachineComponent implements OnDestroy {
    machine: Machine;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialogRef: MatDialogRef<DeleteMachineComponent>,
        public ui: UiService,
        private service: MachineService,
        public session: SessionService,
        @Inject(MAT_DIALOG_DATA) private data: Machine
    ) {
        this.machine = data;
    }

    /**
     * Executed method when view is destroyed
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    /**
     * When Submit Create Athlete Form
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        this.subs.sink = this.service.deleteById(this.machine.id).subscribe(
            () => {
                // TODO: DELETE IMAGES
                this.dialogRef.close({
                    message: Messages.MachineDeleted,
                    status: true
                });
            },
            (error) => {
                // Error
                this.dialogRef.close({
                    message: Messages.ErrorDeletedingMachine,
                    status: false
                });
            }
        );
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }
}
