import { MachineService } from '@services/machine.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { UiService } from '@services/ui.service';
import { Messages } from '@shared/enums/messages.enum';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-create-machine',
    templateUrl: './create-machine.component.html',
    styleUrls: ['./create-machine.component.sass']
})
export class CreateMachineComponent implements OnInit, OnDestroy {
    machineForm: FormGroup;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialogRef: MatDialogRef<CreateMachineComponent>,
        public uiService: UiService,
        private machineService: MachineService
    ) {}

    ngOnInit(): void {
        /**
         * Set validators to machine form
         */
        this.machineForm = new FormGroup({
            name: new FormControl('', [Validators.required]),
            image: new FormControl('', [Validators.required])
        });
    }

    /**
     * When Submit Create Machine Form
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        // Invalid form submitted
        if (this.machineForm.invalid) {
            this.uiService.showSnackBar(Messages.InvalidForm);
        }

        const body = {
            name: this.machineForm.get('name').value,
            state: true
        };

        this.subs.sink = this.machineService.new(body).subscribe(
            (response) => {
                const formData: FormData = new FormData();
                formData.append('machine_id', response.id.toString());
                formData.append(
                    'image',
                    this.machineForm.get('image').value,
                    this.machineForm.get('image').value.name
                );

                this.subs.sink = this.machineService
                    .newMachineImage(formData)
                    .subscribe(
                        () => {
                            // Success
                            this.dialogRef.close({
                                message: Messages.MachineCreated,
                                status: true
                            });
                        },
                        (error) => {
                            this.dialogRef.close({
                                message:
                                    error.status === 401
                                        ? Messages.Unauthorized
                                        : Messages.ErrorCreatingMachine,
                                status: false
                            });
                        }
                    );
            },
            (error) => {
                this.dialogRef.close({
                    message:
                        error.status === 401
                            ? Messages.Unauthorized
                            : Messages.ErrorCreatingMachine,
                    status: false
                });
            }
        );
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }

    /**
     * Method to execute when destroy view
     * Using this method to unsubscribe all subscriptions
     */
    ngOnDestroy(): void {
        // Prevent unsubscribe submit form without submit
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    onChangeFile(event) {
        const file = (event.target as HTMLInputElement).files[0];
        this.machineForm.get('image').setValue(file);

        if (
            !['jpeg', 'png', 'jpg', 'JPG', 'PNG', 'JPEG'].includes(
                file.name.split('.').slice(-1).pop()
            )
        ) {
            this.machineForm.get('image').setErrors({ invalidFile: true });
        }
    }
}
