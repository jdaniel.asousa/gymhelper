import { Component, Inject, Input, OnInit } from '@angular/core';
import { Machine } from '@models/machine.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-view-qrcode',
    templateUrl: './view-qrcode.component.html',
    styleUrls: ['./view-qrcode.component.sass']
})
export class ViewQrcodeComponent implements OnInit {
    @Input()
    machine: Machine;

    constructor(
        public dialogRef: MatDialogRef<ViewQrcodeComponent>,
        @Inject(MAT_DIALOG_DATA) private data: Machine
    ) {
        this.machine = data;
    }

    ngOnInit(): void {
        if (!this.machine.qrcode) {
            this.onClose();
        }
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }
}
