import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Machine } from '@models/machine.model';
import { ViewMachineComponent } from '../dialogs/view-machine/view-machine.component';
import { dialogSize } from '@functions/dialog-size.function';
import { ViewQrcodeComponent } from '../dialogs/view-qrcode/view-qrcode.component';
import { MatDialog } from '@angular/material/dialog';
import { DeleteMachineComponent } from '../dialogs/delete-machine/delete-machine.component';
import { CommunicationService } from '@services/communication.service';
import { UiService } from '@services/ui.service';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-machine-row',
    templateUrl: './machine-row.component.html',
    styleUrls: ['./machine-row.component.sass']
})
export class MachineRowComponent implements OnInit, OnDestroy {
    @Input()
    machine: Machine;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        private dialog: MatDialog,
        private ui: UiService,
        private communication: CommunicationService
    ) {}

    ngOnInit(): void {}

    imageStyle() {
        const image =
            this.machine.machine_images !== null
                ? this.machine.machine_images[0].path
                : '/assets/media/placeholder.png';

        return {
            'background-image': 'url(' + image + ')'
        };
    }

    openViewDialog(): void {
        this.dialog.open(ViewMachineComponent, {
            data: this.machine,
            width: dialogSize('view-dialog'),
            height: '65vh'
        });
    }

    openViewQRCodeDialog(): void {
        this.dialog.open(ViewQrcodeComponent, {
            data: this.machine,
            width: dialogSize()
        });
    }

    openDeleteDialog(): void {
        this.subs.sink = this.dialog
            .open(DeleteMachineComponent, {
                data: this.machine,
                width: dialogSize()
            })
            .afterClosed()
            .subscribe((result) => {
                if (result) {
                    this.ui.showSnackBar(result.message);

                    if (result.status === true) {
                        this.communication.sendStatus(true);
                    }
                }
            });
    }

    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
