import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineRowComponent } from './machine-row.component';

describe('MachineRowComponent', () => {
    let component: MachineRowComponent;
    let fixture: ComponentFixture<MachineRowComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [MachineRowComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(MachineRowComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
