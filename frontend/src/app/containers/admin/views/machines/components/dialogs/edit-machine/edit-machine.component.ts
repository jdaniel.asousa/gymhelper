import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MachineService } from '@services/machine.service';
import { UiService } from '@services/ui.service';
import { Messages } from '@shared/enums/messages.enum';
import { Machine } from '@shared/models/machine.model';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-edit-machine',
    templateUrl: './edit-machine.component.html',
    styleUrls: ['./edit-machine.component.sass']
})
export class EditMachineComponent implements OnInit, OnDestroy {
    machineForm: FormGroup;
    machine: Machine;
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialogRef: MatDialogRef<EditMachineComponent>,
        private machineService: MachineService,
        public uiService: UiService,
        @Inject(MAT_DIALOG_DATA) private data: Machine
    ) {
        this.machine = data;
    }

    ngOnInit(): void {
        /**
         * Set validators to machine form
         */
        this.machineForm = new FormGroup({
            name: new FormControl(this.machine.name, [Validators.required]),
            status: new FormControl(this.machine.state, [Validators.required]),
            image: new FormControl('', [])
        });
    }

    /**
     * When Submit Update Machine Form
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        // Invalid form submitted
        if (this.machineForm.invalid) {
            this.uiService.showSnackBar(Messages.InvalidForm);
        }

        const body = {
            name: this.machineForm.get('name').value,
            state: this.machineForm.get('status').value,
            machine_id: this.machine.id
        };

        this.subs.sink = this.machineService.updateById(body).subscribe(
            (response) => {
                if (this.machineForm.get('image').value !== '') {
                    const formData: FormData = new FormData();
                    formData.append('machine_id', response.id.toString());
                    formData.append(
                        'image',
                        this.machineForm.get('image').value,
                        this.machineForm.get('image').value.name
                    );

                    this.subs.sink = this.machineService
                        .newMachineImage(formData)
                        .subscribe(
                            () => {
                                // Success
                                this.dialogRef.close({
                                    message: Messages.ExerciseUpdated,
                                    status: true
                                });
                            },
                            (error) => {
                                this.dialogRef.close({
                                    message:
                                        error.status === 401
                                            ? Messages.Unauthorized
                                            : 'Não foi possível associar imagem à máquina.',
                                    status: false,
                                    fetch: true
                                });
                            }
                        );
                } else {
                    this.dialogRef.close({
                        message: Messages.ExerciseUpdated,
                        status: true
                    });
                }
            },
            (error) => {
                this.dialogRef.close({
                    message:
                        error.status === 401
                            ? Messages.Unauthorized
                            : Messages.ErrorCreatingMachine,
                    status: false
                });
            }
        );
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }

    /**
     * This method is executed when view was destroyed
     * Use this method to unsubscribe all subscriptions
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    onChangeFile(event) {
        const file = (event.target as HTMLInputElement).files[0];
        this.machineForm.get('image').setValue(file);

        if (
            !['jpeg', 'png', 'jpg', 'JPG', 'PNG', 'JPEG'].includes(
                file.name.split('.').slice(-1).pop()
            )
        ) {
            this.machineForm.get('image').setErrors({ invalidFile: true });
        }
    }
}
