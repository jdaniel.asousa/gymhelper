import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MachinesGridComponent } from './machines-grid.component';

describe('MachinesGridComponent', () => {
    let component: MachinesGridComponent;
    let fixture: ComponentFixture<MachinesGridComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [MachinesGridComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(MachinesGridComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
