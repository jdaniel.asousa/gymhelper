import { Component, Input, OnInit } from '@angular/core';
import { Machine } from '@models/machine.model';

@Component({
    selector: 'app-machines-grid',
    templateUrl: './machines-grid.component.html',
    styleUrls: ['./machines-grid.component.sass']
})
export class MachinesGridComponent implements OnInit {
    @Input()
    machines: Machine[];

    constructor() {}

    ngOnInit(): void {}
}
