import { Component, Input, OnInit } from '@angular/core';
import { Machine } from '@models/machine.model';

@Component({
    selector: 'app-machines-list',
    templateUrl: './machines-list.component.html',
    styleUrls: ['./machines-list.component.sass']
})
export class MachinesListComponent implements OnInit {
    @Input()
    machines: Machine[];

    constructor() {}

    ngOnInit(): void {}
}
