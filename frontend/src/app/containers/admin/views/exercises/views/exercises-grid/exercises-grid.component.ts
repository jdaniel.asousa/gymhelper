import { Exercise } from './../../../../../../shared/models/exercise.model';
import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-exercises-grid',
    templateUrl: './exercises-grid.component.html',
    styleUrls: ['./exercises-grid.component.sass']
})
export class ExercisesGridComponent implements OnInit {
    @Input()
    exercises: Exercise[];

    constructor() {}

    ngOnInit(): void {}
}
