import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExercisesGridComponent } from './exercises-grid.component';

describe('ExercisesGridComponent', () => {
    let component: ExercisesGridComponent;
    let fixture: ComponentFixture<ExercisesGridComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ExercisesGridComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ExercisesGridComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
