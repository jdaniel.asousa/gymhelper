import { Component, Input, OnInit } from '@angular/core';
import { Exercise } from '@models/exercise.model';

@Component({
    selector: 'app-exercises-list',
    templateUrl: './exercises-list.component.html',
    styleUrls: ['./exercises-list.component.sass']
})
export class ExercisesListComponent implements OnInit {
    @Input()
    exercises: Exercise[];

    constructor() {}

    ngOnInit(): void {}
}
