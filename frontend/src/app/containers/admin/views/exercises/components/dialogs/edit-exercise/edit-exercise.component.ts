import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ExerciseService } from '@services/exercise.service';
import { MachineService } from '@services/machine.service';
import { UiService } from '@services/ui.service';
import { Messages } from '@shared/enums/messages.enum';
import { Muscles } from '@shared/enums/muscles.enum';
import { Exercise } from '@shared/models/exercise.model';
import { Machine } from '@shared/models/machine.model';
import { Observable } from 'rxjs';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-edit-exercise',
    templateUrl: './edit-exercise.component.html',
    styleUrls: ['./edit-exercise.component.sass']
})
export class EditExerciseComponent implements OnInit, OnDestroy {
    exerciseForm: FormGroup;
    exercise: Exercise;
    muscles = Muscles;
    machines$: Observable<Machine[]>;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialogRef: MatDialogRef<EditExerciseComponent>,
        private machineService: MachineService,
        public uiService: UiService,
        private exerciseService: ExerciseService,
        @Inject(MAT_DIALOG_DATA) private data: Exercise
    ) {
        this.loadMachines();
        this.exercise = data;
    }

    /**
     * Method to get machines from API
     */
    loadMachines(): void {
        this.machines$ = this.machineService.getAll();
    }

    ngOnInit(): void {
        this.exerciseForm = new FormGroup({
            name: new FormControl(this.exercise.name, [Validators.required]),
            video: new FormControl(this.exercise.video, []),
            machine: new FormControl(this.exercise.machine_id, []),
            description: new FormControl(this.exercise.description, []),
            type: new FormControl(this.exercise.type, []),
            muscle: new FormControl(this.exercise.muscle, []),
            image: new FormControl('', [])
        });
    }

    /**
     * When Submit Create Exercise Form
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        // Invalid form submitted
        if (this.exerciseForm.invalid) {
            this.uiService.showSnackBar(Messages.InvalidForm);
        }

        this.subs.sink = this.exerciseService
            .updateById({
                type: this.exerciseForm.get('type').value,
                name: this.exerciseForm.get('name').value,
                machine_id: this.exerciseForm.get('machine').value,
                description: this.exerciseForm.get('description').value,
                video: this.exerciseForm.get('video').value,
                muscle: this.exerciseForm.get('muscle').value,
                exercise_id: this.exercise.id
            })
            .subscribe(
                (exercise) => {
                    if (this.exerciseForm.get('image').value !== '') {
                        const formData: FormData = new FormData();
                        formData.append(
                            'exercise_id',
                            exercise.exerciseUpdated.id.toString()
                        );
                        formData.append(
                            'image',
                            this.exerciseForm.get('image').value,
                            this.exerciseForm.get('image').value.name
                        );

                        this.subs.sink = this.exerciseService
                            .newImage(formData)
                            .subscribe(
                                () => {
                                    // Success
                                    this.dialogRef.close({
                                        message: Messages.ExerciseUpdated,
                                        status: true
                                    });
                                },
                                (error) => {
                                    this.dialogRef.close({
                                        message:
                                            error.status === 401
                                                ? Messages.Unauthorized
                                                : 'Não foi possível associar imagem ao exercício.',
                                        status: false,
                                        fetch: true
                                    });
                                }
                            );
                    } else {
                        this.dialogRef.close({
                            message: Messages.ExerciseUpdated,
                            status: true
                        });
                    }
                },
                (error) => {
                    this.dialogRef.close({
                        message:
                            error.status === 401
                                ? Messages.Unauthorized
                                : Messages.ErrorUpdatingExercise,
                        status: false
                    });
                }
            );
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }

    /**
     * This method is executed when view was destroyed
     * Use this method to unsubscribe all subscriptions
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    onChangeFile(event) {
        const file = (event.target as HTMLInputElement).files[0];
        this.exerciseForm.get('image').setValue(file);

        if (
            !['jpeg', 'png', 'jpg', 'JPG', 'PNG', 'JPEG'].includes(
                file.name.split('.').slice(-1).pop()
            )
        ) {
            this.exerciseForm.get('image').setErrors({ invalidFile: true });
        }
    }
}
