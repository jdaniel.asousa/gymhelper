import { Component, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UiService } from '@services/ui.service';
import { SessionService } from '@services/session.service';
import { Exercise } from '@models/exercise.model';
import { ExerciseService } from '@services/exercise.service';
import { Messages } from '@enums/messages.enum';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-delete-exercise',
    templateUrl: './delete-exercise.component.html',
    styleUrls: ['./delete-exercise.component.sass']
})
export class DeleteExerciseComponent implements OnDestroy {
    exercise: Exercise;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialogRef: MatDialogRef<DeleteExerciseComponent>,
        public ui: UiService,
        private service: ExerciseService,
        public session: SessionService,
        @Inject(MAT_DIALOG_DATA) private data: Exercise
    ) {
        this.exercise = data;
    }

    /**
     * Executed method when view is destroyed
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    /**
     * When Submit Delete Exercise Form
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        this.subs.sink = this.service.deleteById(this.exercise.id).subscribe(
            () => {
                // Success
                // TODO: DELETE IMAGES
                this.dialogRef.close({
                    message: Messages.ExerciseDeleted,
                    status: true
                });
            },
            () => {
                // Error
                this.dialogRef.close({
                    message: Messages.ErrorDeletedingExercise,
                    status: false
                });
            }
        );
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }
}
