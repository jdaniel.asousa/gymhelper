import { Component, Inject, Input, OnInit } from '@angular/core';
import { Machine } from '@models/machine.model';
import { Exercise } from '@models/exercise.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-view-exercise',
    templateUrl: './view-exercise.component.html',
    styleUrls: ['./view-exercise.component.sass']
})
export class ViewExerciseComponent implements OnInit {
    exercise: Exercise;
    machine: Machine;

    constructor(
        public dialogRef: MatDialogRef<ViewExerciseComponent>,
        @Inject(MAT_DIALOG_DATA) private data: Exercise
    ) {
        this.exercise = data;
    }

    ngOnInit(): void {
        this.machine = this.exercise.machine;
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }

    /**
     * Method to get completed string of exercise type
     */
    getTypeOfExercise(): string {
        return this.exercise.type === 'Reps' ? 'Repetições' : 'Tempo';
    }
}
