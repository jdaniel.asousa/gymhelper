import { Observable } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Machine } from '@shared/models/machine.model';
import { MatDialogRef } from '@angular/material/dialog';
import { UiService } from '@services/ui.service';
import { Muscles } from '@shared/enums/muscles.enum';
import { Messages } from '@shared/enums/messages.enum';
import { ExerciseService } from '@services/exercise.service';
import { MachineService } from '@services/machine.service';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-create-exercise',
    templateUrl: './create-exercise.component.html',
    styleUrls: ['./create-exercise.component.sass']
})
export class CreateExerciseComponent implements OnInit, OnDestroy {
    exerciseForm: FormGroup;

    muscles = Muscles;
    machines$: Observable<Machine[]>;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialogRef: MatDialogRef<CreateExerciseComponent>,
        private machineService: MachineService,
        public uiService: UiService,
        private exerciseService: ExerciseService
    ) {
        this.loadMachines();
    }

    /**
     * Method to get machines from API
     */
    loadMachines(): void {
        this.machines$ = this.machineService.getAll();
    }

    ngOnInit(): void {
        this.exerciseForm = new FormGroup({
            name: new FormControl('', [Validators.required]),
            video: new FormControl(),
            machine: new FormControl(),
            description: new FormControl(),
            type: new FormControl(null, [Validators.required]),
            muscle: new FormControl(null, [Validators.required]),
            image: new FormControl('', [Validators.required])
        });
    }

    /**
     * When Submit Create Exercise Form
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        // Invalid form submitted
        if (this.exerciseForm.invalid) {
            this.uiService.showSnackBar(Messages.InvalidForm);
        }

        this.subs.sink = this.exerciseService
            .new({
                type: this.exerciseForm.get('type').value,
                name: this.exerciseForm.get('name').value,
                machine_id: this.exerciseForm.get('machine').value,
                description: this.exerciseForm.get('description').value,
                video: this.exerciseForm.get('video').value,
                muscle: this.exerciseForm.get('muscle').value
            })
            .subscribe(
                (exercise) => {
                    const formData: FormData = new FormData();
                    formData.append(
                        'exercise_id',
                        exercise.exerciseCreated.id.toString()
                    );
                    formData.append(
                        'image',
                        this.exerciseForm.get('image').value,
                        this.exerciseForm.get('image').value.name
                    );

                    this.subs.sink = this.exerciseService
                        .newImage(formData)
                        .subscribe(
                            () => {
                                // Success
                                this.dialogRef.close({
                                    message: Messages.ExerciseCreated,
                                    status: true
                                });
                            },
                            (error) => {
                                this.dialogRef.close({
                                    message:
                                        error.status === 401
                                            ? Messages.Unauthorized
                                            : 'Exercício criado sem imagem associada',
                                    status: false,
                                    fetch: true
                                });
                            }
                        );
                },
                (error) => {
                    this.dialogRef.close({
                        message:
                            error.status === 401
                                ? Messages.Unauthorized
                                : Messages.ErrorCreatingExercise,
                        status: false
                    });
                }
            );
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }

    /**
     * This method is executed when view was destroyed
     * Use this method to unsubscribe all subscriptions
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    onChangeFile(event) {
        const file = (event.target as HTMLInputElement).files[0];
        this.exerciseForm.get('image').setValue(file);

        if (
            !['jpeg', 'png', 'jpg', 'JPG', 'PNG', 'JPEG'].includes(
                file.name.split('.').slice(-1).pop()
            )
        ) {
            this.exerciseForm.get('image').setErrors({ invalidFile: true });
        }
    }
}
