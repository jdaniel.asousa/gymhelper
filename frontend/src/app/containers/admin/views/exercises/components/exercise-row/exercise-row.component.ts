import { Component, Input, OnDestroy } from '@angular/core';
import { Exercise } from '@models/exercise.model';
import { DeleteExerciseComponent } from '../dialogs/delete-exercise/delete-exercise.component';
import { dialogSize } from '@functions/dialog-size.function';
import { MatDialog } from '@angular/material/dialog';
import { UiService } from '@services/ui.service';
import { CommunicationService } from '@services/communication.service';
import { ViewExerciseComponent } from '../dialogs/view-exercise/view-exercise.component';
import { EditExerciseComponent } from '../dialogs/edit-exercise/edit-exercise.component';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-exercise-row',
    templateUrl: './exercise-row.component.html',
    styleUrls: ['./exercise-row.component.sass']
})
export class ExerciseRowComponent implements OnDestroy {
    @Input()
    exercise: Exercise;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        private dialog: MatDialog,
        private ui: UiService,
        private communication: CommunicationService
    ) {}

    /**
     * Method to get completed string of exercise type
     */
    getTypeOfExercise(): string {
        return this.exercise.type === 'Reps' ? 'Repetições' : 'Tempo';
    }

    /**
     * Method to return style of card image
     */
    imageStyle() {
        const image =
            this.exercise.exercise_images !== null
                ? this.exercise.exercise_images[0].path
                : '/assets/media/placeholder.png';

        return {
            'background-image': `url(${image})`
        };
    }

    openDeleteDialog(): void {
        this.subs.sink = this.dialog
            .open(DeleteExerciseComponent, {
                data: this.exercise,
                width: dialogSize()
            })
            .afterClosed()
            .subscribe((result) => {
                if (result) {
                    this.ui.showSnackBar(result.message);

                    if (result.status === true) {
                        this.communication.sendStatus(true);
                    }
                }
            });
    }

    openViewDialog(): void {
        this.dialog.open(ViewExerciseComponent, {
            data: this.exercise,
            width: dialogSize('view-dialog'),
            height: '65vh'
        });
    }

    openUpdateDialog(): void {
        this.subs.sink = this.dialog
            .open(EditExerciseComponent, {
                data: this.exercise,
                width: dialogSize()
            })
            .afterClosed()
            .subscribe((result) => {
                if (result) {
                    this.ui.showSnackBar(result.message);

                    if (result.status === true) {
                        this.communication.sendStatus(true);
                    }
                }
            });
    }

    /**
     * Executed method when view is destroyed
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
