import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseRowComponent } from './exercise-row.component';

describe('ExerciseRowComponent', () => {
    let component: ExerciseRowComponent;
    let fixture: ComponentFixture<ExerciseRowComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ExerciseRowComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ExerciseRowComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
