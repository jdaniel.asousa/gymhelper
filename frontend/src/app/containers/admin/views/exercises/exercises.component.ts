import { CreateExerciseComponent } from './components/dialogs/create-exercise/create-exercise.component';
import { ExerciseService } from '@services/exercise.service';
import { Exercise } from '@shared/models/exercise.model';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ViewType } from '@shared/enums/view-type.enum';
import { UiService } from '@services/ui.service';
import { MatDialog } from '@angular/material/dialog';
import { dialogSize } from '@shared/functions/dialog-size.function';
import { CommunicationService } from '@services/communication.service';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-exercises',
    templateUrl: './exercises.component.html',
    styleUrls: ['./exercises.component.sass']
})
export class ExercisesComponent implements OnInit, OnDestroy {
    /**
     * Change view between grid and list
     * Using created enum
     * Grid => 1
     * List => 2
     */
    viewType = ViewType.Grid;

    exercises: Exercise[];

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        private ui: UiService,
        private service: ExerciseService,
        public dialog: MatDialog,
        private communication: CommunicationService
    ) {
        this.ui.setTitle('Exercícios');
    }

    ngOnInit(): void {
        this.fetchData();

        this.subs.sink = this.communication.getStatus().subscribe(() => {
            this.fetchData();
        });
    }

    /**
     * Method to change view type
     * @param view integer using ViewType Enum
     */
    changeView(view: ViewType) {
        this.viewType = view;
    }

    /**
     * Method to get data from service
     */
    fetchData(): void {
        this.subs.sink = this.service.getAll().subscribe((data) => {
            this.exercises = data;
        });
    }

    openCreateDialog() {
        this.subs.sink = this.dialog
            .open(CreateExerciseComponent, {
                width: dialogSize()
            })
            .afterClosed()
            .subscribe((res) => {
                if (res) {
                    this.ui.showSnackBar(res.message);

                    if (res.status === true || res.fetch) {
                        this.fetchData();
                    }
                }
            });
    }

    /**
     * Method executed to unsubscribe all subscriptions
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
