import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/shared.module';
import { ExercisesRoutingModule } from './exercises-routing.module';
import { ExercisesComponent } from './exercises.component';
import { ExerciseCardComponent } from './components/exercise-card/exercise-card.component';
import { ExercisesListComponent } from './views/exercises-list/exercises-list.component';
import { ExercisesGridComponent } from './views/exercises-grid/exercises-grid.component';
import { ExerciseRowComponent } from './components/exercise-row/exercise-row.component';
import { DeleteExerciseComponent } from './components/dialogs/delete-exercise/delete-exercise.component';
import { CreateExerciseComponent } from './components/dialogs/create-exercise/create-exercise.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { ViewExerciseComponent } from './components/dialogs/view-exercise/view-exercise.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { EditExerciseComponent } from './components/dialogs/edit-exercise/edit-exercise.component';

@NgModule({
    declarations: [
        ExercisesComponent,
        ExerciseCardComponent,
        ExerciseRowComponent,
        ExercisesGridComponent,
        ExercisesListComponent,
        DeleteExerciseComponent,
        CreateExerciseComponent,
        ViewExerciseComponent,
        EditExerciseComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        ExercisesRoutingModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatIconModule,
        FormsModule,
        ReactiveFormsModule,
        MatListModule,
        MatMenuModule,
        MatDialogModule,
        MatSelectModule,
        FlexLayoutModule,
        MatExpansionModule
    ]
})
export class ExercisesModule {}
