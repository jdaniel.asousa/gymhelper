import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Messages } from '@shared/enums/messages.enum';
import { Roles } from '@shared/enums/roles.enum';
import { CustomErrorStateMatcher } from '@shared/matcher/error-state.matcher';
import { User } from '@shared/models/user.model';
import { SessionService } from '@services/session.service';
import { UiService } from '@services/ui.service';
import { UserService } from '@services/user.service';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit, OnDestroy {
    /**
     * Variable used to show or hide old password
     */
    oldHide = true;

    /**
     * Variable used to show or hide new and confirm password
     */
    hide = true;

    /**
     * Error State Matcher
     * This matcher is used to validate match field when change one
     */
    matcher = new CustomErrorStateMatcher();

    /**
     * User to update profile
     */
    user: User;
    /**
     * Promise to not loaded the HTML content before receive the data from service
     */
    isLoaded: Promise<boolean>;
    /**
     * From Group with inputs and validators
     */
    profileForm: FormGroup;
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        private session: SessionService,
        private ui: UiService,
        private userService: UserService
    ) {
        this.ui.setTitle('Perfil');
    }

    ngOnInit(): void {
        this.fetchData();

        this.profileForm = new FormGroup({
            email: new FormControl({ value: '', disabled: true }),
            name: new FormControl('', [Validators.required]),
            birthdayDate: new FormControl(''),
            avatar: new FormControl('', [])
        });
    }

    fetchData(): void {
        this.subs.sink = this.userService
            .getById(this.session.me().id)
            .subscribe(
                (data) => {
                    this.user = data;

                    /**
                     * Set values on form after have the user data
                     */
                    this.profileForm.patchValue({
                        name: this.user.name,
                        email: this.user.email,
                        birthdayDate: this.user.birth_date || undefined
                    });

                    this.isLoaded = Promise.resolve(true);
                },
                (error) => {
                    this.ui.showSnackBar(error.message);
                }
            );
    }

    /**
     * This method is executed when view as destroyed
     * Use this method to unsubscribe all subscriptions you have
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    /**
     * Method executed when submit profile form
     * @param event Event received by form
     */
    onUpdateProfile(event) {
        event.preventDefault();

        // Invalid form submitted
        if (this.profileForm.invalid) {
            this.ui.showSnackBar(Messages.InvalidForm);
        }

        // Create FormData
        const formData: FormData = new FormData();
        formData.append('id', this.user.id.toString());
        formData.append('name', this.profileForm.get('name').value);
        formData.append('avatar', this.profileForm.get('avatar').value);

        // Add Birthday Date if is Athlete
        if (this.user.role_id === Roles.Athlete) {
            formData.append(
                'birth_date',
                this.profileForm.get('birthdayDate').value
            );
        }

        this.subs.sink = this.userService.update(formData).subscribe(
            () => {
                this.ui.showSnackBar(Messages.ProfileUpdated);
                this.fetchData();
            },
            (error) => {
                const message =
                    error.status === 401
                        ? Messages.Unauthorized
                        : error.status === 500
                        ? Messages.DefaultError
                        : Messages.ErrorUpdatingProfile;

                this.ui.showSnackBar(message, 'Ok');
            }
        );
    }

    /**
     * Method executed when change password form was submitted
     * @param event Event received by change password form
     */
    onChangePassword(event) {
        event.preventDefault();
    }

    /**
     * Method to show name of selected file
     * This method as executed when input has change
     */
    onChangeFile(event): void {
        const file = (event.target as HTMLInputElement).files[0];
        this.profileForm.get('avatar').setValue(file);

        if (
            !['jpeg', 'png', 'jpg', 'JPG', 'PNG', 'JPEG'].includes(
                file.name.split('.').slice(-1).pop()
            )
        ) {
            this.profileForm.get('avatar').setErrors({ invalidFile: true });
        }
    }
}
