import { Component, OnDestroy, OnInit } from '@angular/core';
import { SessionService } from '@services/session.service';
import { User } from '@models/user.model';
import { UiService } from '@services/ui.service';
import { MachineService } from '@services/machine.service';
import { UserService } from '@services/user.service';
import { PhysicalEvaluation } from '@models/physical-evaluation.model';
import { ChartDataSets } from 'chart.js';
import { PhysicalEvaluationService } from '@services/physical-evaluation.service';
import { formatDate } from '@functions/format-date.function';
import { SubSink } from 'subsink';
import { Roles } from '@enums/roles.enum';
import { Athlete } from '@models/athlete.model';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit, OnDestroy {
    userLogged: User;
    athlete: Athlete;
    numberOfUsers: number;
    numberOfAthletes: number;
    numberOfMachines: number;
    physicalEvaluations: PhysicalEvaluation[];
    haveData: Promise<boolean>;
    haveEvaluations: Promise<boolean>;
    labels: string[];
    datasets: ChartDataSets[];
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        private session: SessionService,
        private ui: UiService,
        private machineService: MachineService,
        private userService: UserService,
        private physicalService: PhysicalEvaluationService
    ) {
        this.ui.setTitle('Dashboard');

        this.userLogged = this.session.me();
    }

    ngOnInit(): void {
        this.fetchData();
    }

    getNumbers() {
        this.subs.sink = this.machineService
            .numberOfMachines()
            .subscribe((res) => (this.numberOfMachines = res));

        this.subs.sink = this.userService
            .numberOfAtletes()
            .subscribe((res) => (this.numberOfAthletes = res));

        this.subs.sink = this.userService
            .numberOfUsers()
            .subscribe((res) => (this.numberOfUsers = res));
    }

    fetchData(): void {
        if (
            this.userLogged.role === Roles.Admin ||
            this.userLogged.role === Roles.Receptionist
        ) {
            this.getNumbers();
            this.haveData = Promise.resolve(true);
        }

        if (this.userLogged.role === Roles.Athlete) {
            this.getEvaluations();
        }
    }

    getEvaluations(): void {
        this.subs.sink = this.physicalService
            .getLastTwoByMe()
            .subscribe((evals) => {
                if (evals) {
                    this.physicalEvaluations = evals;

                    this.physicalEvaluationChartLabels();
                    this.physicalEvaluationChartData();
                }

                this.haveEvaluations = Promise.resolve(true);
            });
    }

    physicalEvaluationChartLabels() {
        this.labels = ['Peso (em KG)', 'IMC', 'Massa Muscular', 'Massa Gorda'];
    }

    physicalEvaluationChartData() {
        this.datasets = [
            {
                data: [
                    this.physicalEvaluations[0].weight,
                    this.physicalEvaluations[0].imc,
                    this.physicalEvaluations[0].muscle_mass,
                    this.physicalEvaluations[0].body_fat
                ],
                label: formatDate(
                    this.physicalEvaluations[1].created_at,
                    'd/m/y h:m'
                )
            },
            {
                data: [
                    this.physicalEvaluations[1].weight,
                    this.physicalEvaluations[1].imc,
                    this.physicalEvaluations[1].muscle_mass,
                    this.physicalEvaluations[1].body_fat
                ],
                label: formatDate(
                    this.physicalEvaluations[1].created_at,
                    'd/m/y h:m'
                )
            }
        ];
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }
}
