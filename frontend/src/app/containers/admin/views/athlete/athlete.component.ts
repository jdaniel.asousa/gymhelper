import { Athlete } from '@models/athlete.model';
import { UiService } from '@services/ui.service';
import { UserService } from '@services/user.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Messages } from '@enums/messages.enum';
import { RouterStateService } from '@services/router-state.service';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-athlete',
    templateUrl: './athlete.component.html',
    styleUrls: ['./athlete.component.sass']
})
export class AthleteComponent implements OnInit, OnDestroy {
    /**
     * Promise to not loaded the HTML content before receive the data from service
     */
    isLoaded: Promise<boolean>;
    private id: string;
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();
    private athlete: Athlete;

    constructor(
        private activatedRoute: ActivatedRoute,
        private userService: UserService,
        public ui: UiService,
        private router: Router,
        private routerState: RouterStateService
    ) {
        this.ui.setTitle(`Atleta`);
    }

    ngOnInit(): void {
        this.routerState.params$ = this.activatedRoute.params;
        this.id = this.activatedRoute.snapshot.paramMap.get('id');

        this.getAthlete();
    }

    getAthlete(): void {
        this.subs.sink = this.userService
            .getByAthleteId(+this.id)
            .subscribe((athlete) => {
                if (!athlete) {
                    this.router.navigate(['/admin/athletes']).then(() => {
                        this.ui.showSnackBar(Messages.InvalidAthlete);
                    });
                }

                this.athlete = athlete;

                this.isLoaded = Promise.resolve(true);
            });
    }

    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
