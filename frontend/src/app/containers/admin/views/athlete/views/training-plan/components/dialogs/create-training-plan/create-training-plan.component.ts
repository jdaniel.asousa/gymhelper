import { Component, Inject, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Messages } from '@enums/messages.enum';
import { UiService } from '@services/ui.service';
import { TrainingPlanService } from '@services/training-plan.service';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-create-training-plan',
    templateUrl: './create-training-plan.component.html',
    styleUrls: ['./create-training-plan.component.sass']
})
export class CreateTrainingPlanComponent implements OnInit, OnDestroy {
    @Input() athleteId: string;

    planForm: FormGroup;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        private ui: UiService,
        private service: TrainingPlanService,
        private router: Router,
        public dialogRef: MatDialogRef<CreateTrainingPlanComponent>,
        @Inject(MAT_DIALOG_DATA) private data: string
    ) {
        this.athleteId = data;
    }

    ngOnInit(): void {
        this.planForm = new FormGroup({
            description: new FormControl()
        });
    }

    /**
     * When Submit Create Form
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        // Invalid form submitted
        if (this.planForm.invalid) {
            this.ui.showSnackBar(Messages.InvalidForm);
        }

        this.subs.sink = this.service
            .new({
                description: this.planForm.get('description').value,
                athlete_id: this.athleteId
            })
            .subscribe(
                (result) => {
                    this.router
                        .navigate([
                            'admin/athletes',
                            this.athleteId,
                            'training-plans',
                            result.trainingPlanCreated.id
                        ])
                        .then(() => {
                            this.ui.showSnackBar(Messages.TrainingPlanCreated);
                        });

                    this.dialogRef.close();
                },
                (error) => {
                    this.dialogRef.close({
                        message:
                            error.status === 401
                                ? Messages.Unauthorized
                                : Messages.ErrorCreatingTrainingPlan,
                        status: false
                    });
                }
            );
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }

    /**
     * Executed method when view is destroyed
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
