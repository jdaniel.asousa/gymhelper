import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NutrionalEvaluationCardComponent } from './nutrional-evaluation-card.component';

describe('NutrionalEvaluationCardComponent', () => {
    let component: NutrionalEvaluationCardComponent;
    let fixture: ComponentFixture<NutrionalEvaluationCardComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [NutrionalEvaluationCardComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(NutrionalEvaluationCardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
