import { Messages } from '@enums/messages.enum';
import { TrainingPlan } from '@models/training-plan.model';
import { TrainingPlanService } from '@services/training-plan.service';
import { Component, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SessionService } from '@services/session.service';
import { UiService } from '@services/ui.service';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-delete-training-plan',
    templateUrl: './delete-training-plan.component.html',
    styleUrls: ['./delete-training-plan.component.sass']
})
export class DeleteTrainingPlanComponent implements OnDestroy {
    plan: TrainingPlan;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialogRef: MatDialogRef<DeleteTrainingPlanComponent>,
        public ui: UiService,
        private service: TrainingPlanService,
        public session: SessionService,
        @Inject(MAT_DIALOG_DATA) private data: TrainingPlan
    ) {
        this.plan = data;
    }

    /**
     * Delete Training Plan
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        this.subs.sink = this.service.deleteById(this.plan.id).subscribe(
            () => {
                this.dialogRef.close({
                    message: Messages.TrainingPlanDeleted,
                    status: true
                });
            },
            () => {
                this.dialogRef.close({
                    message: Messages.ErrorDeletingTrainingPlan,
                    status: false
                });
            }
        );
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }

    /**
     * Executed method when view is destroyed
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
