import { NutrionalEvaluationsComponent } from './nutrional-evaluations/nutrional-evaluations.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NutrionalEvaluationComponent } from './nutrional-evaluation/nutrional-evaluation.component';

const routes: Routes = [
    { path: '', component: NutrionalEvaluationsComponent },
    { path: ':eval', component: NutrionalEvaluationComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NutrionalEvaluationRoutingModule {}
