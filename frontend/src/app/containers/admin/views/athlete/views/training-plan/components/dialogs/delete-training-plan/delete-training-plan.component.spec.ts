import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteTrainingPlanComponent } from './delete-training-plan.component';

describe('DeleteTrainingPlanComponent', () => {
    let component: DeleteTrainingPlanComponent;
    let fixture: ComponentFixture<DeleteTrainingPlanComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [DeleteTrainingPlanComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(DeleteTrainingPlanComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
