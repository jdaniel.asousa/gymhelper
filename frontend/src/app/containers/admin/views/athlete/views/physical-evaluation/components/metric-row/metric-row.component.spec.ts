import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetricRowComponent } from './metric-row.component';

describe('MetricRowComponent', () => {
    let component: MetricRowComponent;
    let fixture: ComponentFixture<MetricRowComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [MetricRowComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(MetricRowComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
