import { Component, OnDestroy, OnInit } from '@angular/core';
import { UiService } from '@services/ui.service';
import { PhysicalEvaluation } from '@models/physical-evaluation.model';
import { PhysicalEvaluationService } from '@services/physical-evaluation.service';
import { RouterStateService } from '@services/router-state.service';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-physical-evaluation',
    templateUrl: './physical-evaluations.component.html',
    styleUrls: ['./physical-evaluations.component.sass']
})
export class PhysicalEvaluationsComponent implements OnInit, OnDestroy {
    haveEvaluations: Promise<boolean>;
    evaluations: PhysicalEvaluation[];
    private id: string;
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        private routerState: RouterStateService,
        private ui: UiService,
        private service: PhysicalEvaluationService
    ) {
        this.ui.setTitle(`Avaliações Física`);
    }

    ngOnInit(): void {
        this.subs.sink = this.routerState.params$.subscribe(
            (params) => (this.id = params.id)
        );

        this.fetchData();
    }

    fetchData(): void {
        this.subs.sink = this.service
            .getByAthlete(Number(this.id))
            .subscribe((data) => {
                this.evaluations = data;
                this.haveEvaluations = Promise.resolve(true);
            });
    }

    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
