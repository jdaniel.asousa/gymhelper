import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-metric-row',
    templateUrl: './metric-row.component.html',
    styleUrls: ['./metric-row.component.sass']
})
export class MetricRowComponent implements OnInit {
    @Input() unit = '--';

    @Input() name: string;

    @Input() value = '--';

    constructor() {}

    ngOnInit(): void {}
}
