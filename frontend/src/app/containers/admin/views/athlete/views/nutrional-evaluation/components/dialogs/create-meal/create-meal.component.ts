import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UiService } from '@services/ui.service';
import { SessionService } from '@services/session.service';
import { Messages } from '@enums/messages.enum';
import { NutrionalEvaluationService } from '@services/nutrional-evaluation.service';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-create-meal',
    templateUrl: './create-meal.component.html',
    styleUrls: ['./create-meal.component.sass']
})
export class CreateMealComponent implements OnInit, OnDestroy {
    mealForm: FormGroup;
    /**
     * User Logged, using user service and method me() to get User Data
     */
    userLogged;
    planId: string;
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialogRef: MatDialogRef<CreateMealComponent>,
        public Service: NutrionalEvaluationService,
        public uiService: UiService,
        public session: SessionService,
        @Inject(MAT_DIALOG_DATA) private data: string
    ) {
        this.userLogged = this.session.me();
        this.planId = data;
    }

    ngOnInit(): void {
        this.mealForm = new FormGroup({
            calories: new FormControl('', [Validators.required]),
            protein: new FormControl('', [Validators.required]),
            fats: new FormControl('', [Validators.required]),
            carbs: new FormControl('', [Validators.required]),
            description: new FormControl('', [])
        });
    }

    /**
     * When Submit Create Meal Form
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        // Invalid form submitted
        if (this.mealForm.invalid) {
            this.uiService.showSnackBar(Messages.InvalidForm);
        }

        const body = {
            calories: this.mealForm.get('calories').value,
            protein: this.mealForm.get('protein').value,
            fats: this.mealForm.get('fats').value,
            carbohydrates: this.mealForm.get('carbs').value,
            description: this.mealForm.get('description').value,
            nutrition_plan_id: this.planId
        };

        /**
         * Note: is used user service because the method is the same
         * Only add birthday date on athlete
         */
        this.subs.sink = this.Service.addMeal(body).subscribe(
            () => {
                this.dialogRef.close({
                    message: Messages.MealCreated,
                    status: true
                });
            },
            () => {
                this.dialogRef.close({
                    message: Messages.ErrorMealCreation,
                    status: false
                });
            }
        );
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }

    /**
     * Method to execute when destroy view
     * Using this method to unsubscribe all subscriptions make
     */
    ngOnDestroy(): void {
        // Prevent unsubscribe submit form without submit
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
