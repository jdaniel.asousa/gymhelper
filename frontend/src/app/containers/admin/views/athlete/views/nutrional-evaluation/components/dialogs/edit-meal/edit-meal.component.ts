import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NutrionalEvaluationService } from '@services/nutrional-evaluation.service';
import { SessionService } from '@services/session.service';
import { UiService } from '@services/ui.service';
import { Messages } from '@shared/enums/messages.enum';
import { Meal } from '@shared/models/meal.model';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-edit-meal',
    templateUrl: './edit-meal.component.html',
    styleUrls: ['./edit-meal.component.sass']
})
export class EditMealComponent implements OnInit, OnDestroy {
    mealForm: FormGroup;
    meal: Meal;
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialogRef: MatDialogRef<EditMealComponent>,
        public Service: NutrionalEvaluationService,
        public uiService: UiService,
        public session: SessionService,
        @Inject(MAT_DIALOG_DATA) private data: Meal
    ) {
        this.meal = data;
    }

    ngOnInit(): void {
        this.mealForm = new FormGroup({
            calories: new FormControl(this.meal.calories, []),
            protein: new FormControl(this.meal.protein, []),
            fats: new FormControl(this.meal.fats, []),
            carbs: new FormControl(this.meal.carbohydrates, []),
            description: new FormControl(this.meal.description, [])
        });
    }

    /**
     * When Submit Create Meal Form
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        // Invalid form submitted
        if (this.mealForm.invalid) {
            this.uiService.showSnackBar(Messages.InvalidForm);
        }

        const body = {
            meal_id: this.meal.id,
            calories: undefined,
            protein: undefined,
            fats: undefined,
            carbohydrates: undefined,
            description: undefined
        };

        if (this.mealForm.get('calories').value !== this.meal.calories) {
            body.calories = this.mealForm.get('calories').value;
        }
        if (this.mealForm.get('protein').value !== this.meal.protein) {
            body.protein = this.mealForm.get('protein').value;
        }
        if (this.mealForm.get('fats').value !== this.meal.fats) {
            body.fats = this.mealForm.get('fats').value;
        }
        if (this.mealForm.get('carbs').value !== this.meal.carbohydrates) {
            body.carbohydrates = this.mealForm.get('carbs').value;
        }
        if (this.mealForm.get('description').value !== this.meal.description) {
            body.description = this.mealForm.get('description').value;
        }

        /**
         * Note: is used user service because the method is the same
         * Only add birthday date on athlete
         */
        this.subs.sink = this.Service.editMeal(body).subscribe(
            () => {
                this.dialogRef.close({
                    message: Messages.MealUpdated,
                    status: true
                });
            },
            () => {
                this.dialogRef.close({
                    message: Messages.ErrorMealUpdating,
                    status: false
                });
            }
        );
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }

    /**
     * Method to execute when destroy view
     * Using this method to unsubscribe all subscriptions make
     */
    ngOnDestroy(): void {
        // Prevent unsubscribe submit form without submit
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
