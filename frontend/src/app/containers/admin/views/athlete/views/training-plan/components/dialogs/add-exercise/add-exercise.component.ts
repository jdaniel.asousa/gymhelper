import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Exercise } from '@models/exercise.model';
import { ExerciseService } from '@services/exercise.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UiService } from '@services/ui.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Messages } from '@enums/messages.enum';
import { TrainingPlanService } from '@services/training-plan.service';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-add-exercise',
    templateUrl: './add-exercise.component.html',
    styleUrls: ['./add-exercise.component.sass']
})
export class AddExerciseComponent implements OnInit, OnDestroy {
    form: FormGroup;
    exercises$: Observable<Exercise[]>;

    planId: string;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        private exerciseService: ExerciseService,
        private ui: UiService,
        private service: TrainingPlanService,
        public dialogRef: MatDialogRef<AddExerciseComponent>,
        @Inject(MAT_DIALOG_DATA) private data: string
    ) {
        this.planId = data;
    }

    ngOnInit(): void {
        this.loadExercises();

        this.form = new FormGroup({
            exercise: new FormControl('', [Validators.required]),
            circuit: new FormControl(),
            time: new FormControl(),
            repetitions: new FormControl(),
            sets: new FormControl()
        });
    }

    /**
     * Method to get machines from API
     */
    loadExercises(): void {
        this.exercises$ = this.exerciseService.getAll();
    }

    onClose() {
        this.dialogRef.close();
    }

    /**
     * Method executed when submit form
     * @param event event received by form
     */
    onSubmit(event) {
        event.preventDefault();

        // Show error message if form not valid
        if (this.form.invalid) {
            this.ui.showSnackBar(Messages.InvalidForm);
        }

        const body = {
            plan_id: Number(this.planId),
            exercise_id: this.form.get('exercise').value,
            circuit: this.form.get('circuit').value,
            sets: this.form.get('sets').value,
            time: undefined,
            repetitions: undefined
        };

        if (this.form.get('time').value !== null) {
            body.time = this.form.get('time').value;
        }

        if (this.form.get('repetitions').value !== '') {
            body.repetitions = this.form.get('repetitions').value;
        }

        this.subs.sink = this.service.addExercise(body).subscribe(
            () => {
                this.dialogRef.close({
                    message: Messages.ExerciseAdded,
                    status: true
                });
            },
            (error) => {
                this.dialogRef.close({
                    message:
                        error.status === 401
                            ? Messages.Unauthorized
                            : Messages.ErrorAddingExercise,
                    status: false
                });
            }
        );
    }

    /**
     * Executed method when view is destroyed
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
