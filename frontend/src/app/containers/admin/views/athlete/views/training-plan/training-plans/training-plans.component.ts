import { CommunicationService } from '@services/communication.service';
import { UiService } from '@services/ui.service';
import { TrainingPlanService } from '@services/training-plan.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { TrainingPlan } from '@models/training-plan.model';
import { RouterStateService } from '@services/router-state.service';
import { MatDialog } from '@angular/material/dialog';
import { dialogSize } from '@functions/dialog-size.function';
import { CreateTrainingPlanComponent } from '../components/dialogs/create-training-plan/create-training-plan.component';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-training-plans',
    templateUrl: './training-plans.component.html',
    styleUrls: ['./training-plans.component.sass']
})
export class TrainingPlansComponent implements OnInit, OnDestroy {
    trainingPlans: TrainingPlan[];
    havePlans: Promise<boolean>;
    private id: string;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        private routerState: RouterStateService,
        private trainingPlanService: TrainingPlanService,
        public ui: UiService,
        private dialog: MatDialog,
        private communication: CommunicationService
    ) {
        this.ui.setTitle(`Planos de Treino`);
    }

    ngOnInit(): void {
        this.subs.sink = this.routerState.params$.subscribe(
            (params) => (this.id = params.id)
        );

        this.fetchData();

        this.subs.sink = this.communication.getStatus().subscribe(() => {
            this.fetchData();
        });
    }

    fetchData() {
        this.subs.sink = this.trainingPlanService
            .getByUser(Number(this.id))
            .subscribe((plans) => {
                this.trainingPlans = plans;

                this.havePlans = Promise.resolve(true);
            });
    }

    /**
     * Executed method when view is destroyed
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    openCreateDialog() {
        this.subs.sink = this.dialog
            .open(CreateTrainingPlanComponent, {
                data: this.id,
                width: dialogSize()
            })
            .afterClosed()
            .subscribe((res) => {
                if (res) {
                    this.ui.showSnackBar(res.message);
                }
            });
    }
}
