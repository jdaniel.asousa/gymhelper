import { dialogSize } from '@functions/dialog-size.function';
import { UiService } from '@services/ui.service';
import { DeleteTrainingPlanComponent } from './../dialogs/delete-training-plan/delete-training-plan.component';
import { CommunicationService } from '@services/communication.service';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { TrainingPlan } from '@models/training-plan.model';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-training-plan-card',
    templateUrl: './training-plan-card.component.html',
    styleUrls: ['./training-plan-card.component.sass']
})
export class TrainingPlanCardComponent implements OnInit, OnDestroy {
    @Input()
    trainingPlan: TrainingPlan;

    @Input() number: number;

    id: string;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        private activatedRoute: ActivatedRoute,
        private dialog: MatDialog,
        private communication: CommunicationService,
        private ui: UiService
    ) {}

    ngOnInit(): void {
        this.subs.sink = this.activatedRoute.parent.params.subscribe(
            (params) => {
                this.id = params.id;
            }
        );
    }

    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    openDeleteDialog(): void {
        this.subs.sink = this.dialog
            .open(DeleteTrainingPlanComponent, {
                data: this.trainingPlan,
                width: dialogSize()
            })
            .afterClosed()
            .subscribe((result) => {
                if (result) {
                    this.ui.showSnackBar(result.message);

                    if (result.status === true) {
                        this.communication.sendStatus(true);
                    }
                }
            });
    }
}
