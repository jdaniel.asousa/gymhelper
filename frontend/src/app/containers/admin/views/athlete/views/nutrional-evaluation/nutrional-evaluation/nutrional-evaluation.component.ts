import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NutrionalEvaluation } from '@models/nutrional-evaluation.model';
import { UiService } from '@services/ui.service';
import { NutrionalEvaluationService } from '@services/nutrional-evaluation.service';
import { SubSink } from 'subsink';
import { CreateMealComponent } from '../components/dialogs/create-meal/create-meal.component';
import { MatDialog } from '@angular/material/dialog';
import { dialogSize } from '@shared/functions/dialog-size.function';
import { CommunicationService } from '@services/communication.service';

@Component({
    selector: 'app-nutrional-evaluation',
    templateUrl: './nutrional-evaluation.component.html',
    styleUrls: ['./nutrional-evaluation.component.sass']
})
export class NutrionalEvaluationComponent implements OnInit, OnDestroy {
    evalId: string;
    evaluation: NutrionalEvaluation;
    isLoaded: Promise<boolean>;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        private activeRoute: ActivatedRoute,
        private ui: UiService,
        private service: NutrionalEvaluationService,
        public dialog: MatDialog,
        private comunication: CommunicationService
    ) {
        this.ui.setTitle('Avaliação Nutricional');
    }

    ngOnInit(): void {
        this.evalId = this.activeRoute.snapshot.paramMap.get('eval');

        this.fetchData();

        this.subs.sink = this.comunication.getStatus().subscribe(() => {
            this.fetchData();
        });
    }

    fetchData(): void {
        this.subs.sink = this.service
            .getById(this.evalId)
            .subscribe((evaluation) => {
                this.evaluation = evaluation;

                this.isLoaded = Promise.resolve(true);
            });
    }

    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    openCreateDialog() {
        this.subs.sink = this.dialog
            .open(CreateMealComponent, {
                width: dialogSize(),
                data: this.evaluation.nutrition_plan.id
            })
            .afterClosed()
            .subscribe((res) => {
                if (res) {
                    this.ui.showSnackBar(res.message);

                    if (res.status === true) {
                        this.fetchData();
                    }
                }
            });
    }
}
