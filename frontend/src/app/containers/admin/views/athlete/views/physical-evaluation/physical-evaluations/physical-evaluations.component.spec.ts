import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicalEvaluationsComponent } from './physical-evaluations.component';

describe('PhysicalEvaluationComponent', () => {
    let component: PhysicalEvaluationsComponent;
    let fixture: ComponentFixture<PhysicalEvaluationsComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [PhysicalEvaluationsComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PhysicalEvaluationsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
