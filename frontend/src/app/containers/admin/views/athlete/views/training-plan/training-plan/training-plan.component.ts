import { TrainingPlanService } from '@services/training-plan.service';
import { UiService } from '@services/ui.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TrainingPlan } from '@models/training-plan.model';
import { dialogSize } from '@functions/dialog-size.function';
import { MatDialog } from '@angular/material/dialog';
import { AddExerciseComponent } from '../components/dialogs/add-exercise/add-exercise.component';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-training-plan',
    templateUrl: './training-plan.component.html',
    styleUrls: ['./training-plan.component.sass']
})
export class TrainingPlanComponent implements OnInit, OnDestroy {
    planId: string;
    plan: TrainingPlan;

    isLoaded: Promise<boolean>;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        private activatedRoute: ActivatedRoute,
        public ui: UiService,
        private dialog: MatDialog,
        private trainingPlanService: TrainingPlanService
    ) {}

    ngOnInit(): void {
        this.planId = this.activatedRoute.snapshot.paramMap.get('plan');
        this.ui.setTitle(`Plano de Treino ${this.planId}`);

        this.fetchData();
    }

    fetchData(): void {
        this.subs.sink = this.trainingPlanService
            .getById(this.planId)
            .subscribe((plan) => {
                this.plan = plan;

                this.isLoaded = Promise.resolve(true);
            });
    }

    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    openAddExercise() {
        this.subs.sink = this.dialog
            .open(AddExerciseComponent, {
                width: dialogSize(),
                data: this.planId
            })
            .afterClosed()
            .subscribe((res) => {
                if (res) {
                    this.ui.showSnackBar(res.message);

                    if (res.status === true || res.fetch) {
                        this.fetchData();
                    }
                }
            });
    }
}
