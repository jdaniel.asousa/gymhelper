import { Component, OnDestroy, OnInit } from '@angular/core';
import { PhysicalEvaluationService } from '@services/physical-evaluation.service';
import { UiService } from '@services/ui.service';
import { SubSink } from 'subsink';
import { PhysicalEvaluation } from '@models/physical-evaluation.model';
import { RouterStateService } from '@services/router-state.service';
import { formatDate } from '@functions/format-date.function';
import { ChartDataSets } from 'chart.js';
import { SessionService } from '@services/session.service';
import { Roles } from '@enums/roles.enum';

@Component({
    selector: 'app-overview',
    templateUrl: './overview.component.html',
    styleUrls: ['./overview.component.sass']
})
export class OverviewComponent implements OnInit, OnDestroy {
    physicalEvaluations: PhysicalEvaluation[];

    id: string;
    haveData: Promise<boolean>;

    labels: string[];
    datasets: ChartDataSets[];

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        private physicalService: PhysicalEvaluationService,
        private ui: UiService,
        private routerState: RouterStateService,
        private session: SessionService
    ) {}

    ngOnInit(): void {
        this.subs.sink = this.routerState.params$.subscribe(
            (params) => (this.id = params.id)
        );

        this.fetchData();
    }

    fetchData(): void {
        if (this.session.me().role === Roles.Athlete) {
            this.subs.sink = this.physicalService
                .getLastTwoByMe()
                .subscribe((response) => {
                    if (response) {
                        this.physicalEvaluations = response;

                        this.physicalEvaluationChartLabels();
                        this.physicalEvaluationChartData();
                    }

                    this.haveData = Promise.resolve(true);
                });
        } else {
            this.subs.sink = this.physicalService
                .getLastTwo(Number(this.id))
                .subscribe((res) => {
                    if (res) {
                        this.physicalEvaluations = res;

                        this.physicalEvaluationChartLabels();
                        this.physicalEvaluationChartData();
                    }
                    this.haveData = Promise.resolve(true);
                });
        }
    }

    physicalEvaluationChartLabels() {
        this.labels = ['Peso (em KG)', 'IMC', 'Massa Muscular', 'Massa Gorda'];
    }

    physicalEvaluationChartData() {
        this.datasets = [
            {
                data: [
                    this.physicalEvaluations[0].weight,
                    this.physicalEvaluations[0].imc,
                    this.physicalEvaluations[0].muscle_mass,
                    this.physicalEvaluations[0].body_fat
                ],
                label: formatDate(
                    this.physicalEvaluations[1].created_at,
                    'd/m/y h:m'
                )
            },
            {
                data: [
                    this.physicalEvaluations[1].weight,
                    this.physicalEvaluations[1].imc,
                    this.physicalEvaluations[1].muscle_mass,
                    this.physicalEvaluations[1].body_fat
                ],
                label: formatDate(
                    this.physicalEvaluations[1].created_at,
                    'd/m/y h:m'
                )
            }
        ];
    }

    /**
     * Executed method when view is destroyed
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
