import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PhysicalEvaluationsComponent } from './physical-evaluations/physical-evaluations.component';
import { PhysicalEvaluationComponent } from './physical-evaluation/physical-evaluation.component';

const routes: Routes = [
    { path: '', component: PhysicalEvaluationsComponent },
    { path: ':eval', component: PhysicalEvaluationComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PhysicalEvaluationRoutingModule {}
