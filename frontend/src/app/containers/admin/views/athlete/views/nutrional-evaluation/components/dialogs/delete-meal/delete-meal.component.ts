import { Component, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NutrionalEvaluationService } from '@services/nutrional-evaluation.service';
import { SessionService } from '@services/session.service';
import { UiService } from '@services/ui.service';
import { Messages } from '@shared/enums/messages.enum';
import { Meal } from '@shared/models/meal.model';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-delete-meal',
    templateUrl: './delete-meal.component.html',
    styleUrls: ['./delete-meal.component.sass']
})
export class DeleteMealComponent implements OnDestroy {
    meal: Meal;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialogRef: MatDialogRef<DeleteMealComponent>,
        public service: NutrionalEvaluationService,
        public uiService: UiService,
        public session: SessionService,
        @Inject(MAT_DIALOG_DATA) private data: Meal
    ) {
        this.meal = data;
    }

    /**
     * Executed method when view is destroyed
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    /**
     * When Submit Form
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        /**
         * Note: is used user service because the method is the same
         */
        this.subs.sink = this.service.deleteMeal(this.meal.id).subscribe(
            () => {
                // Success
                this.dialogRef.close({
                    message: Messages.MealDeleted,
                    status: true
                });
            },
            () => {
                // Error
                this.dialogRef.close({
                    message: Messages.ErrorDeletingMeal,
                    status: false
                });
            }
        );
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }
}
