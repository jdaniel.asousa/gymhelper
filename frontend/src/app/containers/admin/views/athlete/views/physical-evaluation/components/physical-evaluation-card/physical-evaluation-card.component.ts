import { Component, Input, OnInit } from '@angular/core';
import { PhysicalEvaluation } from '@models/physical-evaluation.model';

@Component({
    selector: 'app-physical-evaluation-card',
    templateUrl: './physical-evaluation-card.component.html',
    styleUrls: ['./physical-evaluation-card.component.sass']
})
export class PhysicalEvaluationCardComponent implements OnInit {
    @Input()
    evaluation: PhysicalEvaluation;

    @Input()
    number: number;

    constructor() {}

    ngOnInit(): void {}
}
