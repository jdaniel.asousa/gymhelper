import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingExerciseCardComponent } from './training-exercise-card.component';

describe('TrainingExerciseCardComponent', () => {
    let component: TrainingExerciseCardComponent;
    let fixture: ComponentFixture<TrainingExerciseCardComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [TrainingExerciseCardComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(TrainingExerciseCardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
