import { Component, OnDestroy, OnInit } from '@angular/core';
import { NutrionalEvaluationService } from '@services/nutrional-evaluation.service';
import { UiService } from '@services/ui.service';
import { NutrionalEvaluation } from '@shared/models/nutrional-evaluation.model';
import { RouterStateService } from '@services/router-state.service';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-nutrional-evaluations',
    templateUrl: './nutrional-evaluations.component.html',
    styleUrls: ['./nutrional-evaluations.component.sass']
})
export class NutrionalEvaluationsComponent implements OnInit, OnDestroy {
    evaluations: NutrionalEvaluation[];
    haveEvaluations: Promise<boolean>;
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();
    private id: string;

    constructor(
        private nutrionalEvaluationService: NutrionalEvaluationService,
        public ui: UiService,
        private routerState: RouterStateService
    ) {
        this.ui.setTitle(`Avaliações Nutricionais`);
    }

    ngOnInit(): void {
        this.subs.sink = this.routerState.params$.subscribe(
            (params) => (this.id = params.id)
        );
        this.fetchData();
    }

    fetchData() {
        this.subs.sink = this.nutrionalEvaluationService
            .getByAthlete(this.id)
            .subscribe((data) => {
                this.evaluations = data;

                this.haveEvaluations = Promise.resolve(true);
            });
    }

    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
