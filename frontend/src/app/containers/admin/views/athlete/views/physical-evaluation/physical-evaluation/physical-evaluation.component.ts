import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UiService } from '@services/ui.service';
import { PhysicalEvaluationService } from '@services/physical-evaluation.service';
import { PhysicalEvaluation } from '@models/physical-evaluation.model';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-physical-evaluation',
    templateUrl: './physical-evaluation.component.html',
    styleUrls: ['./physical-evaluation.component.sass']
})
export class PhysicalEvaluationComponent implements OnInit, OnDestroy {
    evaluation: PhysicalEvaluation;
    haveEvaluation: Promise<boolean>;
    private id: string;
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        private activatedRoute: ActivatedRoute,
        private ui: UiService,
        private service: PhysicalEvaluationService
    ) {
        this.ui.setTitle('Avaliação Física');
    }

    ngOnInit(): void {
        this.id = this.activatedRoute.snapshot.params.eval;

        this.fetchData();
    }

    fetchData(): void {
        this.subs.sink = this.service.getById(this.id).subscribe((res) => {
            this.evaluation = res;
            this.haveEvaluation = Promise.resolve(true);
        });
    }

    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
