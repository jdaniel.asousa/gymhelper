import { NutrionalEvaluation } from '@shared/models/nutrional-evaluation.model';
import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-nutrional-evaluation-card',
    templateUrl: './nutrional-evaluation-card.component.html',
    styleUrls: ['./nutrional-evaluation-card.component.sass']
})
export class NutrionalEvaluationCardComponent implements OnInit {
    @Input()
    evaluation: NutrionalEvaluation;

    @Input()
    number: number;

    constructor() {}

    ngOnInit(): void {}
}
