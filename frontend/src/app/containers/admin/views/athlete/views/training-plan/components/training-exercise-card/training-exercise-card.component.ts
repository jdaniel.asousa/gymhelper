import { dialogSize } from '@shared/functions/dialog-size.function';
import { ViewExerciseComponent } from './../../../../../exercises/components/dialogs/view-exercise/view-exercise.component';
import { MatDialog } from '@angular/material/dialog';
import { TrainingExercise } from '@models/training-exercise.model';
import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'training-exercise-card',
    templateUrl: './training-exercise-card.component.html',
    styleUrls: ['./training-exercise-card.component.sass']
})
export class TrainingExerciseCardComponent implements OnInit {
    @Input()
    exercise: TrainingExercise;

    constructor(private dialog: MatDialog) {}

    openViewDialog(): void {
        this.dialog.open(ViewExerciseComponent, {
            data: this.exercise.exercise,
            width: dialogSize('view-dialog'),
            height: '65vh'
        });
    }

    ngOnInit(): void {}
}
