import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhysicalEvaluationRoutingModule } from './physical-evaluation-routing.module';
import { PhysicalEvaluationsComponent } from './physical-evaluations/physical-evaluations.component';
import { SharedModule } from '@shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PhysicalEvaluationCardComponent } from './components/physical-evaluation-card/physical-evaluation-card.component';
import { PhysicalEvaluationComponent } from './physical-evaluation/physical-evaluation.component';
import { MetricRowComponent } from './components/metric-row/metric-row.component';

@NgModule({
    declarations: [
        PhysicalEvaluationsComponent,
        PhysicalEvaluationCardComponent,
        PhysicalEvaluationComponent,
        MetricRowComponent
    ],
    imports: [
        CommonModule,
        PhysicalEvaluationRoutingModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatIconModule,
        FormsModule,
        ReactiveFormsModule,
        MatListModule,
        MatMenuModule,
        MatDialogModule,
        MatSelectModule,
        FlexLayoutModule
    ]
})
export class PhysicalEvaluationModule {}
