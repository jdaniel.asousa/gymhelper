import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicalEvaluationCardComponent } from './physical-evaluation-card.component';

describe('PhysicalEvaluationCardComponent', () => {
    let component: PhysicalEvaluationCardComponent;
    let fixture: ComponentFixture<PhysicalEvaluationCardComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [PhysicalEvaluationCardComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PhysicalEvaluationCardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
