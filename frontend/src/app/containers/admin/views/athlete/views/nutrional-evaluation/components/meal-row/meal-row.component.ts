import { Component, Input, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Meal } from '@models/meal.model';
import { CommunicationService } from '@services/communication.service';
import { UiService } from '@services/ui.service';
import { dialogSize } from '@shared/functions/dialog-size.function';
import { DeleteMealComponent } from '../dialogs/delete-meal/delete-meal.component';
import { EditMealComponent } from '../dialogs/edit-meal/edit-meal.component';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-meal-row',
    templateUrl: './meal-row.component.html',
    styleUrls: ['./meal-row.component.sass']
})
export class MealRowComponent implements OnDestroy {
    @Input()
    meal: Meal;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialog: MatDialog,
        public ui: UiService,
        public communication: CommunicationService
    ) {}

    /**
     * Executed method when view is destroyed
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    openDeleteDialog() {
        this.subs.sink = this.dialog
            .open(DeleteMealComponent, {
                width: dialogSize(),
                data: this.meal
            })
            .afterClosed()
            .subscribe((res) => {
                if (res) {
                    this.ui.showSnackBar(res.message);

                    if (res.status === true) {
                        this.communication.sendStatus(true);
                    }
                }
            });
    }

    openEditDialog() {
        this.subs.sink = this.dialog
            .open(EditMealComponent, {
                width: dialogSize(),
                data: this.meal
            })
            .afterClosed()
            .subscribe((res) => {
                if (res) {
                    this.ui.showSnackBar(res.message);

                    if (res.status === true) {
                        this.communication.sendStatus(true);
                    }
                }
            });
    }
}
