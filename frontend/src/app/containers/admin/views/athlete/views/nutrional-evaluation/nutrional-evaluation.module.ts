import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NutrionalEvaluationRoutingModule } from './nutrional-evaluation-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@shared/shared.module';
import { NutrionalEvaluationCardComponent } from './components/nutrional-evaluation-card/nutrional-evaluation-card.component';
import { NutrionalEvaluationsComponent } from './nutrional-evaluations/nutrional-evaluations.component';
import { NutrionalEvaluationComponent } from './nutrional-evaluation/nutrional-evaluation.component';
import { MealRowComponent } from './components/meal-row/meal-row.component';
import { CreateMealComponent } from './components/dialogs/create-meal/create-meal.component';
import { DeleteMealComponent } from './components/dialogs/delete-meal/delete-meal.component';
import { EditMealComponent } from './components/dialogs/edit-meal/edit-meal.component';

@NgModule({
    declarations: [
        NutrionalEvaluationCardComponent,
        NutrionalEvaluationsComponent,
        NutrionalEvaluationComponent,
        MealRowComponent,
        CreateMealComponent,
        DeleteMealComponent,
        EditMealComponent
    ],
    imports: [
        CommonModule,
        NutrionalEvaluationRoutingModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatIconModule,
        FormsModule,
        ReactiveFormsModule,
        MatListModule,
        MatMenuModule,
        MatDialogModule,
        MatSelectModule,
        FlexLayoutModule
    ]
})
export class NutrionalEvaluationModule {}
