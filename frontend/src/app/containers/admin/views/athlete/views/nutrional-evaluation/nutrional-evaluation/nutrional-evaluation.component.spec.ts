import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NutrionalEvaluationComponent } from './nutrional-evaluation.component';

describe('NutrionalEvaluationComponent', () => {
    let component: NutrionalEvaluationComponent;
    let fixture: ComponentFixture<NutrionalEvaluationComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [NutrionalEvaluationComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(NutrionalEvaluationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
