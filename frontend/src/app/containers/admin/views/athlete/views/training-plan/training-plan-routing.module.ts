import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TrainingPlansComponent } from './training-plans/training-plans.component';
import { TrainingPlanComponent } from './training-plan/training-plan.component';

const routes: Routes = [
    { path: '', component: TrainingPlansComponent },
    {
        path: ':plan',
        component: TrainingPlanComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TrainingPlanRoutingModule {}
