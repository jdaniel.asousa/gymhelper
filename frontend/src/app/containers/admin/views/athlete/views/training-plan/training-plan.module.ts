import { ViewExerciseComponent } from './../../../exercises/components/dialogs/view-exercise/view-exercise.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrainingPlanRoutingModule } from './training-plan-routing.module';
import { TrainingPlanCardComponent } from './components/training-plan-card/training-plan-card.component';
import { TrainingExerciseCardComponent } from './components/training-exercise-card/training-exercise-card.component';
import { TrainingPlansComponent } from './training-plans/training-plans.component';
import { TrainingPlanComponent } from './training-plan/training-plan.component';
import { SharedModule } from '@shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CreateTrainingPlanComponent } from './components/dialogs/create-training-plan/create-training-plan.component';
import { DeleteTrainingPlanComponent } from './components/dialogs/delete-training-plan/delete-training-plan.component';
import { AddExerciseComponent } from './components/dialogs/add-exercise/add-exercise.component';
import { ExercisesModule } from '../../../exercises/exercises.module';

@NgModule({
    declarations: [
        TrainingPlanComponent,
        TrainingPlansComponent,
        TrainingPlanCardComponent,
        TrainingExerciseCardComponent,
        CreateTrainingPlanComponent,
        DeleteTrainingPlanComponent,
        AddExerciseComponent
    ],
    imports: [
        CommonModule,
        TrainingPlanRoutingModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatIconModule,
        FormsModule,
        ReactiveFormsModule,
        MatListModule,
        MatMenuModule,
        MatDialogModule,
        MatSelectModule,
        FlexLayoutModule,
        ExercisesModule
    ]
})
export class TrainingPlanModule {}
