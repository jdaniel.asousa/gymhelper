import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NutrionalEvaluationsComponent } from './nutrional-evaluations.component';

describe('NutrionalEvaluationsComponent', () => {
    let component: NutrionalEvaluationsComponent;
    let fixture: ComponentFixture<NutrionalEvaluationsComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [NutrionalEvaluationsComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(NutrionalEvaluationsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
