import { AthleteComponent } from './athlete.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OverviewComponent } from './views/overview/overview.component';

const routes: Routes = [
    {
        path: '',
        component: AthleteComponent,
        children: [
            { path: '', component: OverviewComponent },
            {
                path: 'nutrional-evaluations',
                loadChildren: () =>
                    import(
                        './views/nutrional-evaluation/nutrional-evaluation.module'
                    ).then((m) => m.NutrionalEvaluationModule)
            },
            {
                path: 'physical-evaluations',
                loadChildren: () =>
                    import(
                        './views/physical-evaluation/physical-evaluation.module'
                    ).then((m) => m.PhysicalEvaluationModule)
            },
            {
                path: 'training-plans',
                loadChildren: () =>
                    import('./views/training-plan/training-plan.module').then(
                        (m) => m.TrainingPlanModule
                    )
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AthleteRoutingModule {}
