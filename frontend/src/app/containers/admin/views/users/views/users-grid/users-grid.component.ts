import { Component, Input, OnInit } from '@angular/core';
import { User } from '@models/user.model';

@Component({
    selector: 'app-users-grid',
    templateUrl: './users-grid.component.html',
    styleUrls: ['./users-grid.component.sass']
})
export class UsersGridComponent implements OnInit {
    @Input()
    users: User[];

    constructor() {}

    ngOnInit(): void {}
}
