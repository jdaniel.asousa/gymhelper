import { DeleteUserComponent } from '../dialogs/delete-user/delete-user.component';
import { Roles } from '@enums/roles.enum';
import { Component, Input, OnDestroy } from '@angular/core';
import { User } from '@models/user.model';
import { getRoleLabel } from '@shared/functions/role-label.function';
import { MatDialog } from '@angular/material/dialog';
import { CommunicationService } from '@services/communication.service';
import { UiService } from '@services/ui.service';
import { dialogSize } from '@shared/functions/dialog-size.function';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-user-card',
    templateUrl: './user-card.component.html',
    styleUrls: ['./user-card.component.sass']
})
export class UserCardComponent implements OnDestroy {
    @Input()
    user: User;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialog: MatDialog,
        public ui: UiService,
        public communication: CommunicationService
    ) {}

    /**
     * Executed method when view is destroyed
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    roleLabel(role: Roles): string {
        return getRoleLabel(role);
    }

    openDeleteDialog() {
        this.subs.sink = this.dialog
            .open(DeleteUserComponent, {
                width: dialogSize(),
                data: this.user
            })
            .afterClosed()
            .subscribe((res) => {
                if (res) {
                    this.ui.showSnackBar(res.message);

                    if (res.status === true) {
                        this.communication.sendStatus(true);
                    }
                }
            });
    }
}
