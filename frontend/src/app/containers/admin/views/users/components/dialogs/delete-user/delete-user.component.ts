import { Component, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Messages } from '@shared/enums/messages.enum';
import { User } from '@shared/models/user.model';
import { SessionService } from '@services/session.service';
import { UiService } from '@services/ui.service';
import { UserService } from '@services/user.service';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-delete-user',
    templateUrl: './delete-user.component.html',
    styleUrls: ['./delete-user.component.sass']
})
export class DeleteUserComponent implements OnDestroy {
    user: User;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialogRef: MatDialogRef<DeleteUserComponent>,
        public userService: UserService,
        public uiService: UiService,
        public session: SessionService,
        @Inject(MAT_DIALOG_DATA) private data: User
    ) {
        this.user = data;
    }

    /**
     * Executed method when view is destroyed
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    /**
     * When Submit Create Athlete Form
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        /**
         * Note: is used user service because the method is the same
         */
        this.subs.sink = this.userService.deleteById(this.user.id).subscribe(
            () => {
                // Success
                this.dialogRef.close({
                    message: Messages.UserDeleted,
                    status: true
                });
            },
            () => {
                // Error
                this.dialogRef.close({
                    message: Messages.ErrorDeletingUser,
                    status: false
                });
            }
        );
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }
}
