import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { UserService } from '@services/user.service';
import { UiService } from '@services/ui.service';
import { validateEmail } from '@shared/validators/custom-email.validator';
import { Messages } from '@enums/messages.enum';
import { Roles } from '@enums/roles.enum';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-create-user',
    templateUrl: './create-user.component.html',
    styleUrls: ['./create-user.component.sass']
})
export class CreateUserDialogComponent implements OnInit, OnDestroy {
    userForm: FormGroup;
    /**
     * Available Roles
     * Create object with available roles using enum
     */
    roles = [
        {
            value: Roles.Nutritionist,
            label: 'Nutricionista'
        },
        {
            value: Roles.PersonalTrainer,
            label: 'Personal Trainer'
        },
        {
            value: Roles.Receptionist,
            label: 'Rececionista'
        }
    ];
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialogRef: MatDialogRef<CreateUserDialogComponent>,
        public userService: UserService,
        public uiService: UiService
    ) {}

    ngOnInit(): void {
        this.userForm = new FormGroup({
            email: new FormControl('', [
                Validators.required,
                Validators.email,
                validateEmail
            ]),
            name: new FormControl('', [Validators.required]),
            role: new FormControl('', [Validators.required])
        });
    }

    /**
     * When Submit Create User Form
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        // Invalid form submitted
        if (this.userForm.invalid) {
            this.uiService.showSnackBar(Messages.InvalidForm);
        }

        const body = {
            name: this.userForm.get('name').value,
            email: this.userForm.get('email').value,
            role_id: this.userForm.get('role').value
        };

        this.subs.sink = this.userService.new(body).subscribe(
            () => {
                this.dialogRef.close({
                    message: Messages.UserCreated,
                    status: true
                });
            },
            () => {
                this.dialogRef.close({
                    message: Messages.ErrorUserCreation,
                    status: false
                });
            }
        );
    }

    ngOnDestroy(): void {
        // Prevent unsubscribe without submit form
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    /**
     * When Click on Close Button
     */
    onClose(): void {
        this.dialogRef.close();
    }
}
