import { Component, Input, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from '@models/user.model';
import { Roles } from '@shared/enums/roles.enum';
import { dialogSize } from '@shared/functions/dialog-size.function';
import { getRoleLabel } from '@shared/functions/role-label.function';
import { CommunicationService } from '@services/communication.service';
import { UiService } from '@services/ui.service';
import { DeleteUserComponent } from '../dialogs/delete-user/delete-user.component';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-user-row',
    templateUrl: './user-row.component.html',
    styleUrls: ['./user-row.component.sass']
})
export class UserRowComponent implements OnDestroy {
    @Input()
    user: User;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialog: MatDialog,
        public ui: UiService,
        public communication: CommunicationService
    ) {}

    /**
     * Executed method when view is destroyed
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    roleLabel(role: Roles): string {
        return getRoleLabel(role);
    }

    openDeleteDialog() {
        this.subs.sink = this.dialog
            .open(DeleteUserComponent, {
                width: dialogSize(),
                data: this.user
            })
            .afterClosed()
            .subscribe((res) => {
                if (res) {
                    this.ui.showSnackBar(res.message);

                    if (res.status === true) {
                        this.communication.sendStatus(true);
                    }
                }
            });
    }
}
