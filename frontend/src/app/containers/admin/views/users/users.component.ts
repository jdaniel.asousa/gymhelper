import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ViewType } from '@shared/enums/view-type.enum';
import { dialogSize } from '@shared/functions/dialog-size.function';
import { User } from '@shared/models/user.model';
import { CommunicationService } from '@services/communication.service';
import { UiService } from '@services/ui.service';
import { UserService } from '@services/user.service';
import { CreateUserDialogComponent } from './components/dialogs/create-user/create-user.component';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.sass']
})
export class UsersComponent implements OnInit, OnDestroy {
    users: User[];
    /**
     * Change user view between grid and list
     * Using created enum
     * Grid => 1
     * List => 2
     */
    viewType = ViewType.Grid;
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public dialog: MatDialog,
        public uiService: UiService,
        private userService: UserService,
        private communication: CommunicationService
    ) {
        this.uiService.setTitle('Utilizadores');
    }

    /**
     * Method to get data from service
     */
    fetchData(): void {
        this.subs.sink = this.userService
            .getWithoutMe()
            .subscribe((data) => (this.users = data));
    }

    /**
     * Method to open dialog to create user
     */
    openCreateDialog() {
        this.subs.sink = this.dialog
            .open(CreateUserDialogComponent, {
                width: dialogSize()
            })
            .afterClosed()
            .subscribe((res) => {
                if (res) {
                    this.uiService.showSnackBar(res.message);

                    if (res.status === true) {
                        this.fetchData();
                    }
                }
            });
    }

    /**
     * Method executed when view as loaded
     * On this method is get data using fetchData method and use the communication service
     * to get status of dialogs, and fetchData again
     */
    ngOnInit(): void {
        this.fetchData();

        this.subs.sink = this.communication.getStatus().subscribe(() => {
            this.fetchData();
        });
    }

    /**
     * Method to change view type
     * @param view integer using ViewType Enum
     */
    changeView(view: ViewType) {
        this.viewType = view;
    }

    /**
     * Method executed when view as destroyed
     * On this method is unsubscribe all subscriptions
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
