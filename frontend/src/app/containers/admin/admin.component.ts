import { Component, OnInit } from '@angular/core';
import { User } from '@models/user.model';
import { Observable } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { SessionService } from '@services/session.service';
import { Router } from '@angular/router';
import { UiService } from '@services/ui.service';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.sass']
})
export class AdminComponent implements OnInit {
    user: User = this.session.me();

    isHandset$: Observable<boolean> = this.breakpointObserver
        .observe(Breakpoints.Handset)
        .pipe(
            map((result) => result.matches),
            shareReplay()
        );

    constructor(
        private breakpointObserver: BreakpointObserver,
        public session: SessionService,
        public router: Router,
        public ui: UiService
    ) {
        ui.load(); // Load Icons added on service
    }

    ngOnInit() {}

    removeSession() {
        this.session.clearSession();
    }
}
