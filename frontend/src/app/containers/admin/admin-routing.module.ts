import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminComponent } from './admin.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { AuthGuard } from '@guards/auth/auth.guard';

const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                component: DashboardComponent
            },
            {
                path: 'users',
                loadChildren: () =>
                    import('./views/users/users.module').then(
                        (m) => m.UsersModule
                    )
            },
            {
                path: 'machines',
                loadChildren: () =>
                    import('./views/machines/machines.module').then(
                        (m) => m.MachinesModule
                    )
            },
            {
                path: 'exercises',
                loadChildren: () =>
                    import('./views/exercises/exercises.module').then(
                        (m) => m.ExercisesModule
                    )
            },
            {
                path: 'athletes',
                loadChildren: () =>
                    import('./views/athletes/athletes.module').then(
                        (m) => m.AthletesModule
                    )
            },
            {
                path: 'profile',
                loadChildren: () =>
                    import('./views/profile/profile.module').then(
                        (m) => m.ProfileModule
                    )
            },
            {
                path: 'plans',
                loadChildren: () =>
                    import('./views/plans/plans.module').then(
                        (m) => m.PlansModule
                    )
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule {}
