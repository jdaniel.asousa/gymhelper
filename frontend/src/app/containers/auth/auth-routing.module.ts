import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthComponent } from './auth.component';
import { LoginComponent } from './views/login/login.component';
import { ResetPasswordComponent } from './views/reset-password/reset-password.component';
import { ChangePasswordComponent } from './views/change-password/change-password.component';
import { LoggedGuard } from '@guards/logged/logged.guard';

const routes: Routes = [
    {
        path: '',
        component: AuthComponent,
        canActivate: [LoggedGuard],
        children: [
            {
                path: '',
                component: LoginComponent
            },
            {
                path: 'reset-password',
                component: ResetPasswordComponent
            },
            {
                path: 'change-password/:token',
                component: ChangePasswordComponent
            },
            {
                path: 'change-password',
                redirectTo: 'reset-password'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule {}
