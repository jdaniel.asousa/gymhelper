import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from '@services/session.service';
import { UiService } from '@services/ui.service';
import { validateEmail } from '@shared/validators/custom-email.validator';
import { Messages } from '@enums/messages.enum';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit, OnDestroy {
    /**
     * Boolean to show or hide password
     */
    hide = true;
    loginForm: FormGroup;
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public session: SessionService,
        public router: Router,
        private uiService: UiService
    ) {
        this.uiService.setTitle('Início de Sessão');
    }

    ngOnInit() {
        /**
         * Create validators to input fields
         */
        this.loginForm = new FormGroup({
            email: new FormControl('', [
                Validators.required,
                Validators.email,
                validateEmail
            ]),
            password: new FormControl('', [Validators.required])
        });
    }

    /**
     * Execute when submit form
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        // Invalid form submitted
        if (this.loginForm.invalid) {
            this.uiService.showSnackBar(Messages.InvalidForm);
        }

        const user = {
            email: this.loginForm.get('email').value,
            password: this.loginForm.get('password').value
        };

        // Send POST Request to API
        this.subs.sink = this.session
            .login(user.email, user.password)
            .subscribe(
                () => {
                    this.router
                        .navigateByUrl('/admin')
                        .then((r) =>
                            this.uiService.showSnackBar(Messages.SuccessLogin)
                        );
                },
                (error) => {
                    const message =
                        error.status === 401
                            ? Messages.Unauthorized
                            : error.status === 500
                            ? Messages.DefaultError
                            : Messages.ErrorLogin;

                    this.uiService.showSnackBar(message, 'Ok');
                }
            );
    }

    /**
     * Method executed when view has "destroyed"
     * Use this method to unsubscribe all subscriptions you have on this view
     */
    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
