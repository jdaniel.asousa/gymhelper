import { CustomErrorStateMatcher } from './../../../../shared/matcher/error-state.matcher';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from '@services/session.service';
import { UiService } from '@services/ui.service';
import { Messages } from '@enums/messages.enum';
import { checkPasswords } from '@shared/validators/confirm-password.validator';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.sass']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
    /**
     * Boolean to show or hide password
     */
    hide = true;
    changeForm: FormGroup;
    /**
     * Error State Matcher
     * This matcher is used to validate match field when change one
     */
    matcher = new CustomErrorStateMatcher();
    /**
     * Token generated on backend
     * (URL parameter)
     */
    private token: string;
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        private activatedRoute: ActivatedRoute,
        public session: SessionService,
        public router: Router,
        public uiService: UiService
    ) {
        // Change Page Title using service
        this.uiService.setTitle('Alteração de Password');
    }

    ngOnInit() {
        this.token = this.activatedRoute.snapshot.paramMap.get('token');

        // Create validators to input fields
        this.changeForm = new FormGroup({
            password: new FormControl('', [Validators.required]),
            confirmPassword: new FormControl('', [Validators.required])
        });

        this.changeForm.setValidators(
            checkPasswords('password', 'confirmPassword')
        );
    }

    /**
     * Execute when Change Password form has submitted
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        // Invalid form submitted
        if (this.changeForm.invalid) {
            this.uiService.showSnackBar(Messages.InvalidForm);
        }

        this.subs.sink = this.session
            .change(
                this.changeForm.get('password').value,
                this.changeForm.get('confirmPassword').value,
                this.token
            )
            .subscribe(
                () => {
                    this.router
                        .navigateByUrl('/')
                        .then(() =>
                            this.uiService.showSnackBar(
                                'Password alterada com sucesso.'
                            )
                        );
                },
                (error) => {
                    const message =
                        error.status === 401
                            ? Messages.Unauthorized
                            : error.status === 500
                            ? Messages.DefaultError
                            : Messages.ErrorChangePwd;

                    this.uiService.showSnackBar(message);
                }
            );
    }

    /**
     * This method is executed when view was destroyed
     * This method is used to unsubscribe all subscriptions realized after
     */
    ngOnDestroy(): void {
        // Prevent to unsubscribe without exist a subscription
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
