import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SessionService } from '@services/session.service';
import { Router } from '@angular/router';
import { UiService } from '@services/ui.service';
import { validateEmail } from '@shared/validators/custom-email.validator';
import { Messages } from '@enums/messages.enum';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.sass']
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
    resetForm: FormGroup;

    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(
        public session: SessionService,
        public router: Router,
        private uiService: UiService
    ) {
        // Change Page Title using service
        this.uiService.setTitle('Recuperação de Password');
    }

    ngOnInit() {
        // Create validators to input fields
        this.resetForm = new FormGroup({
            email: new FormControl('', [
                Validators.required,
                Validators.email,
                validateEmail
            ])
        });
    }

    /**
     * Execute when Reset Form has submitted
     * @param evt Event received by form
     */
    onSubmit(evt) {
        // Prevent Default
        evt.preventDefault();

        // Invalid form submitted
        if (this.resetForm.invalid) {
            this.uiService.showSnackBar(Messages.InvalidForm);
        }

        const email = this.resetForm.get('email').value;

        this.subs.sink = this.session.reset(email).subscribe(
            () => {
                this.uiService.showSnackBar(
                    `O seu pedido de recuperação de password foi enviado para ${email}`
                );
            },
            (error) => {
                const message =
                    error.status === 401
                        ? Messages.Unauthorized
                        : Messages.DefaultError;

                this.uiService.showSnackBar(message);
            }
        );
    }

    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
