import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { SessionService } from '@services/session.service';
import { UiService } from '@services/ui.service';

@Injectable({
    providedIn: 'root'
})
export class LoggedGuard implements CanActivate {
    constructor(
        private session: SessionService,
        private ui: UiService,
        private router: Router
    ) {}

    /**
     * Validate if already have session
     * and if is true redirect to dashboard
     */
    canActivate() {
        // Already have session
        if (!this.session.isExpired() && this.session.me()) {
            this.router
                .navigateByUrl('/admin')
                .then(() => this.ui.showSnackBar('Já possui sessão iniciada'));

            return false;
        }

        return true;
    }
}
