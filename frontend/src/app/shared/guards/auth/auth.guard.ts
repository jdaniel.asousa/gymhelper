import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { SessionService } from '@services/session.service';
import { UiService } from '@services/ui.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(
        private session: SessionService,
        private ui: UiService,
        private router: Router
    ) {}

    /**
     * Validate if have session to continue
     * It's not redirect to login
     */
    canActivate() {
        if (!this.session.me() || this.session.isExpired()) {
            this.router
                .navigateByUrl('/')
                .then(() =>
                    this.ui.showSnackBar(
                        'Necessita ter uma sessão ativa para continuar.'
                    )
                );
            return false;
        }

        return true;
    }
}
