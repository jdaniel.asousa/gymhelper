import { FormControl } from '@angular/forms';

export function validateEmail(form: FormControl) {
    const EMAIL_REGEXP = new RegExp(
        '^[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}$'
    );

    return EMAIL_REGEXP.test(form.value)
        ? null
        : {
              invalidEmail: {
                  valid: false
              }
          };
}
