import { FormGroup } from '@angular/forms';

export function checkPasswords(
    controlName: string,
    matchingControlName: string
) {
    return (formGroup: FormGroup) => {
        const control = formGroup.get(controlName).value;
        const matchingControl = formGroup.get(matchingControlName).value;

        return control === matchingControl ? null : { notSame: false };
    };
}
