import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

@Component({
    selector: 'app-custom-file-input',
    templateUrl: './custom-file-input.component.html',
    styleUrls: ['./custom-file-input.component.sass']
})
export class CustomFileInputComponent implements OnInit {
    /**
     * Element where show file name after choose
     */
    @ViewChild('legend') FileLegend: ElementRef;

    @Input()
    text = 'Clique no botão para selecionar um ficheiro';

    @Input()
    button = 'Escolher';

    @Input()
    title: string;

    @Input()
    types = 'image/png, image/jpeg';

    constructor() {}

    ngOnInit(): void {}

    showSelectedFile(event) {
        const file = (event.target as HTMLInputElement).files[0].name;

        let fileName = file.split('\\').slice(-1).pop();

        //  Add ... if name have more than 15 chars
        if (fileName.length > 15) {
            const extension = fileName.split('.').slice(-1).pop();
            const fileNameNoExtension = fileName.split('.')[0];
            fileName = `${fileNameNoExtension.substr(0, 15)}(...).${extension}`;
        }

        this.FileLegend.nativeElement.innerHTML = `<strong>Ficheiro selecionado:</strong> "${fileName}"`;
    }
}
