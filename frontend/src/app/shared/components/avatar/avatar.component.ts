import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-avatar',
    templateUrl: './avatar.component.html',
    styleUrls: ['./avatar.component.sass']
})
export class AvatarComponent implements OnInit {
    @Input()
    public photo: string;

    @Input()
    public name: string;

    public showInitials = false;
    public initials: string;
    public circleColor: string;

    private colors = [
        '#F77F00', // Secondary Color
        '#003049' // Primary Color
    ];

    ngOnInit() {
        if (!this.photo) {
            this.showInitials = true;
            this.createInititals();

            const randomIndex = Math.floor(
                Math.random() * Math.floor(this.colors.length)
            );
            this.circleColor = this.colors[randomIndex];
        }
    }

    private createInititals(): void {
        let initials = '';

        for (let i = 0; i < this.name.length; i++) {
            if (this.name.charAt(i) === ' ') {
                continue;
            }

            if (this.name.charAt(i) === this.name.charAt(i).toUpperCase()) {
                initials += this.name.charAt(i);

                if (initials.length === 2) {
                    break;
                }
            }
        }

        this.initials = initials;
    }
}
