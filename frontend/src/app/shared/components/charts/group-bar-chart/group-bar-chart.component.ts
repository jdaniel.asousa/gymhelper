import { Component, Input, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';

@Component({
    selector: 'app-group-bar-chart',
    templateUrl: './group-bar-chart.component.html',
    styleUrls: ['./group-bar-chart.component.sass']
})
export class GroupBarChartComponent implements OnInit {
    public barChartOptions: ChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            xAxes: [
                {
                    gridLines: {
                        display: false
                    }
                }
            ],
            yAxes: [
                {
                    gridLines: {
                        display: false
                    }
                }
            ]
        },
        plugins: {
            datalabels: {
                anchor: 'end',
                align: 'end'
            }
        }
        // scales: {
        //     yAxes: [
        //         {
        //             ticks: {
        //                 beginAtZero: true,
        //                 stepSize: 20
        //             },
        //             gridLines: {}
        //         }
        //     ],
        //     xAxes: [
        //         {
        //             gridLines: {
        //                 display: false
        //             }
        //         }
        //     ]
        // }
    };

    @Input()
    labels: Label[];

    public barChartPlugins = [pluginDataLabels];

    public barChartType: ChartType = 'bar';
    public barChartLegend = true;

    // Set colors
    public colors = [
        {
            backgroundColor: '#003049'
        },
        {
            backgroundColor: '#F77F00'
        }
    ];

    @Input()
    data: ChartDataSets[];

    constructor() {}

    ngOnInit() {}
}
