export enum Muscles {
    Chest = 'Peito',
    Tricep = 'Trícep',
    Bicep = 'Bícep',
    Shoulders = 'Ombros',
    Abs = 'Abdominal',
    Back = 'Costas',
    Legs = 'Pernas'
}
