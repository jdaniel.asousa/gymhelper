export enum Roles {
    Admin = 1,
    Athlete = 2,
    Receptionist = 3,
    PersonalTrainer = 4,
    Nutritionist = 5
}
