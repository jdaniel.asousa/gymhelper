import { NgModule } from '@angular/core';
import { AvatarComponent } from '@components/avatar/avatar.component';
import { CommonModule } from '@angular/common';
import { LoadingComponent } from '@components/loading/loading.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CanViewDirective } from './directives/can-view/can-view.directive';
import { IsAdminDirective } from './directives/is-admin/is-admin.directive';
import { CustomFileInputComponent } from '@components/custom-file-input/custom-file-input.component';
import { FormatDatePipe } from './pipes/format-date.pipe';
import { SafePipe } from './pipes/safe.pipe';
import { CustomCarouselComponent } from '@components/custom-carousel/custom-carousel.component';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { GroupBarChartComponent } from '@components/charts/group-bar-chart/group-bar-chart.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
    declarations: [
        AvatarComponent,
        LoadingComponent,
        CanViewDirective,
        IsAdminDirective,
        CustomFileInputComponent,
        FormatDatePipe,
        SafePipe,
        CustomCarouselComponent,
        GroupBarChartComponent
    ],
    imports: [
        CommonModule,
        MatProgressSpinnerModule,
        IvyCarouselModule,
        ChartsModule
    ],
    exports: [
        AvatarComponent,
        LoadingComponent,
        CanViewDirective,
        IsAdminDirective,
        CustomFileInputComponent,
        MatProgressSpinnerModule,
        FormatDatePipe,
        SafePipe,
        CustomCarouselComponent,
        GroupBarChartComponent
    ]
})
export class SharedModule {}
