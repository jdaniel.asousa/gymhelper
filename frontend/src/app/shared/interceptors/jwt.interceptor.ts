import { SessionService } from '@services/session.service';
import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@env';
import { Router } from '@angular/router';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private session: SessionService, private router: Router) {}

    intercept(
        request: HttpRequest<unknown>,
        next: HttpHandler
    ): Observable<HttpEvent<unknown>> {
        const isApiUrl = request.url.startsWith(environment.apiUrl);
        const isAdminRoute = this.router.url.includes('admin');

        if (isApiUrl && isAdminRoute) {
            const accessToken = this.session.me().token;

            if (accessToken) {
                request = request.clone({
                    setHeaders: { Authorization: `Bearer ${accessToken}` }
                });
            }
        }

        return next.handle(request);
    }
}
