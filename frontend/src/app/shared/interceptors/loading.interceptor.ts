import { Injectable } from '@angular/core';
import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoadingService } from '@services/loading.service';
import { finalize } from 'rxjs/operators';
import { environment } from '@env';
import { SubSink } from 'subsink';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {
    /**
     * Variable contains all subscriptions
     * @private
     */
    private subs = new SubSink();

    constructor(private readonly loadingService: LoadingService) {}

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        const isApiUrl = request.url.startsWith(environment.apiUrl);

        if (isApiUrl) {
            this.subs.sink = this.loadingService.spinner$.subscribe();
            return next
                .handle(request)
                .pipe(finalize(() => this.subs.unsubscribe()));
        } else {
            return next.handle(request);
        }
    }
}
