import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SessionService } from '@services/session.service';
import { environment } from '@env';

@Injectable()
export class SessionInterceptor implements HttpInterceptor {
    constructor(public session: SessionService, public router: Router) {}

    intercept(
        req: HttpRequest<unknown>,
        next: HttpHandler
    ): Observable<HttpEvent<unknown>> {
        const isApiUrl = req.url.startsWith(environment.apiUrl);
        const isAdminRoute = this.router.url.includes('admin');

        if (isApiUrl && isAdminRoute) {
            if (this.session.isExpired()) {
                this.session.clearSession();
            } else {
                return next.handle(req).pipe(
                    catchError((err) => {
                        if (err.status === 401) {
                            this.session.clearSession();
                        }

                        return throwError(err.statusText);
                    })
                );
            }
        } else {
            return next.handle(req);
        }
    }
}
