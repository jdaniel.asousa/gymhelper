import { SessionService } from '@services/session.service';
import {
    Directive,
    Input,
    OnInit,
    TemplateRef,
    ViewContainerRef
} from '@angular/core';
import { Roles } from '@shared/enums/roles.enum';
import { validateRoles } from '@shared/functions/validate-roles.function';

@Directive({
    selector: '[canView]'
})
export class CanViewDirective implements OnInit {
    roles: Roles[];

    userLogged;

    constructor(
        private session: SessionService,
        private viewContainerRef: ViewContainerRef,
        private templateRef: TemplateRef<any>
    ) {
        this.userLogged = this.session.me();
    }

    @Input()
    set canView(roles) {
        this.roles = roles;
    }

    ngOnInit() {
        if (!validateRoles(this.userLogged.role, this.roles)) {
            this.viewContainerRef.clear();
        } else {
            this.viewContainerRef.createEmbeddedView(this.templateRef);
        }
    }
}
