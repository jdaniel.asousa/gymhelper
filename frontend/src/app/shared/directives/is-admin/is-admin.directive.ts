import { Roles } from '@enums/roles.enum';
import { User } from '@models/user.model';
import {
    Directive,
    Input,
    OnInit,
    TemplateRef,
    ViewContainerRef
} from '@angular/core';
import { SessionService } from '@services/session.service';

@Directive({
    selector: '[isAdmin]'
})
export class IsAdminDirective implements OnInit {
    userLogged;

    user;

    constructor(
        private session: SessionService,
        private viewContainerRef: ViewContainerRef,
        private templateRef: TemplateRef<any>
    ) {
        this.userLogged = this.session.me();
    }

    @Input()
    set isAdmin(user: User) {
        this.user = user;
    }

    ngOnInit() {
        if (
            this.userLogged.role !== Roles.Admin &&
            this.user.role_id === Roles.Admin
        ) {
            this.viewContainerRef.clear();
        } else {
            this.viewContainerRef.createEmbeddedView(this.templateRef);
        }
    }
}
