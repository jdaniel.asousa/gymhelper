import { Injectable } from '@angular/core';
import { environment } from '@env';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';
import { SessionService } from '@services/session.service';
import { Machine } from '@models/machine.model';

const API_URL = environment.apiUrl;

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    }),
    withCredentials: true
};

@Injectable({
    providedIn: 'root'
})
export class MachineService {
    constructor(public http: HttpClient, public session: SessionService) {}

    new(body: object): Observable<Machine> {
        return this.http
            .post<Machine>(
                `${API_URL}/machine/create-machine`,
                {
                    request_id: this.session.me().id,
                    ...body
                },
                httpOptions
            )
            .pipe(share());
    }

    newMachineImage(data: FormData): Observable<any> {
        data.append('request_id', this.session.me().id);

        return this.http
            .post(`${API_URL}/machineimage/create-machine-image`, data)
            .pipe(share());
    }

    getAll(): Observable<Machine[]> {
        return this.http
            .get<Machine[]>(`${API_URL}/machine/machines`, {
                ...httpOptions,
                params: {
                    request_id: this.session.me().id
                }
            })
            .pipe(share());
    }

    numberOfMachines(): Observable<number> {
        return this.http
            .get(`${API_URL}/machine/machines`, {
                ...httpOptions,
                params: {
                    request_id: this.session.me().id
                }
            })
            .pipe(
                map((res) => {
                    // @ts-ignore
                    return res.length;
                })
            );
    }

    /**
     * Method to delete machine by ID from api
     * @param id ID of machine to delete
     */
    deleteById(id: number): Observable<Machine> {
        const options = {
            ...httpOptions,
            body: {
                request_id: this.session.me().id,
                machine_id: id.toString()
            }
        };

        return this.http
            .delete<Machine>(`${API_URL}/machine/machine`, options)
            .pipe(share());
    }

    updateById(body: object): Observable<Machine> {
        return this.http
            .put<Machine>(
                `${API_URL}/machine/update-machine`,
                {
                    request_id: this.session.me().id,
                    ...body
                },
                httpOptions
            )
            .pipe(share());
    }

    updateImage(data: FormData): Observable<any> {
        data.append('request_id', this.session.me().id);

        return this.http
            .put(`${API_URL}/machineimage/update-machine-image`, data)
            .pipe(share());
    }
}
