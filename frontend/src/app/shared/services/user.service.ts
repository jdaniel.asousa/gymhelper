import { Athlete } from '@models/athlete.model';
import { Roles } from '@enums/roles.enum';
import { SessionService } from '@services/session.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env';
import { Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';
import { User } from '@models/user.model';

const API_URL = environment.apiUrl;

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    }),
    withCredentials: true
};

@Injectable({
    providedIn: 'root'
})
export class UserService {
    constructor(public http: HttpClient, public session: SessionService) {}

    /**
     * Method to communicate with api to add new user
     */
    new(body: object): Observable<any> {
        return this.http
            .post(
                `${API_URL}/user/create-user`,
                {
                    request_id: this.session.me().id,
                    ...body,
                    origin: `${environment.frontUrl}/change-password`
                },
                httpOptions
            )
            .pipe(share());
    }

    /**
     * Method to get all users from API and remove logged user
     */
    getWithoutMe(): Observable<User[]> {
        return this.http
            .get<User[]>(`${API_URL}/user/users`, {
                ...httpOptions,
                params: {
                    request_id: this.session.me().id
                }
            })
            .pipe(
                map((users) => {
                    const usersFiltered = [];

                    users.filter((obj) => {
                        if (
                            obj.id !== this.session.me().id &&
                            obj.role_id !== Roles.Athlete
                        ) {
                            usersFiltered.push(obj);
                        }
                    });

                    return usersFiltered;
                })
            );
    }

    /**
     * Method to get by ID from api
     * @param id ID of user
     */
    getById(id: number): Observable<any> {
        const options = {
            ...httpOptions,
            params: {
                request_id: this.session.me().id,
                id: id.toString()
            }
        };

        return this.http
            .get<any>(`${API_URL}/User/user`, options)
            .pipe(share());
    }

    update(body: FormData): Observable<User> {
        return this.http
            .put<User>(`${API_URL}/user/update-user`, body)
            .pipe(share());
    }

    /**
     * Method to delete user by ID from api
     * @param id ID of user to delete
     */
    deleteById(id: number): Observable<User> {
        const options = {
            ...httpOptions,
            body: {
                request_id: this.session.me().id,
                id
            }
        };

        return this.http
            .delete<User>(`${API_URL}/user/user`, options)
            .pipe(share());
    }

    /**
     * Method to get all users from API and only return personal trainers
     */
    getPersonalTrainers(): Observable<User[]> {
        return this.http
            .get<User[]>(`${API_URL}/user/users`, {
                ...httpOptions,
                params: {
                    request_id: this.session.me().id
                }
            })
            .pipe(
                map((personalTrainers) => {
                    const usersFiltered = [];

                    personalTrainers.filter((obj) => {
                        if (obj.role_id === Roles.PersonalTrainer) {
                            usersFiltered.push(obj);
                        }
                    });

                    return usersFiltered;
                })
            );
    }

    numberOfUsers(): Observable<number> {
        return this.http
            .get(`${API_URL}/user/users`, {
                ...httpOptions,
                params: {
                    request_id: this.session.me().id
                }
            })
            .pipe(
                map((res) => {
                    // @ts-ignore
                    return res.length;
                })
            );
    }

    numberOfAtletes(): Observable<number> {
        return this.http
            .get(`${API_URL}/user/users`, {
                ...httpOptions,
                params: {
                    request_id: this.session.me().id
                }
            })
            .pipe(
                map((res) => {
                    const athletes = [];

                    // @ts-ignore
                    res.map((athlete) => {
                        if (athlete.role_id === Roles.Athlete) {
                            athletes.push(athlete);
                        }
                    });

                    return athletes.length;
                })
            );
    }

    /**
     * Method to get all users from API and only return athletes
     */
    getAthletes(): Observable<Athlete[]> {
        return this.http
            .get<Athlete[]>(`${API_URL}/user/users`, {
                ...httpOptions,
                params: {
                    request_id: this.session.me().id
                }
            })
            .pipe(
                map((athletes) => {
                    const usersFiltered = [];

                    athletes.filter((obj) => {
                        if (obj.role_id === Roles.Athlete) {
                            usersFiltered.push(obj);
                        }
                    });

                    return usersFiltered;
                })
            );
    }

    getByAthleteId(id: number): Observable<Athlete> {
        return this.http
            .get<Athlete[]>(`${API_URL}/user/users`, {
                ...httpOptions,
                params: {
                    request_id: this.session.me().id
                }
            })
            .pipe(
                map((users) => {
                    let finalResult;

                    users.map((user) => {
                        if (
                            user.role_id === Roles.Athlete &&
                            user.athlete_info.id === id
                        ) {
                            finalResult = user;
                        }
                    });

                    return finalResult;
                })
            );
    }
}
