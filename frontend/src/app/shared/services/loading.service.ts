import { Injectable } from '@angular/core';
import { ComponentPortal } from '@angular/cdk/portal';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { LoadingComponent } from '@components/loading/loading.component';
import { defer, NEVER } from 'rxjs';
import { finalize, share } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class LoadingService {
    constructor(private overlay: Overlay) {}

    private overlayRef: OverlayRef = undefined;

    public readonly spinner$ = defer(() => {
        this.show();
        return NEVER.pipe(
            finalize(() => {
                this.hide();
            })
        );
    }).pipe(share());

    public show(): void {
        // Hack avoiding `ExpressionChangedAfterItHasBeenCheckedError` error
        Promise.resolve(null).then(() => {
            this.overlayRef = this.overlay.create({
                positionStrategy: this.overlay
                    .position()
                    .global()
                    .centerHorizontally()
                    .centerVertically(),
                hasBackdrop: true
            });
            this.overlayRef.attach(new ComponentPortal(LoadingComponent));
        });
    }

    public hide(): void {
        this.overlayRef.detach();
        this.overlayRef = undefined;
    }
}
