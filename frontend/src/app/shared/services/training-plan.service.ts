import { TrainingPlan } from '@shared/models/training-plan.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@envs/environment';
import { Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';
import { SessionService } from './session.service';

const API_URL = environment.apiUrl;

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    }),
    withCredentials: true
};

@Injectable({
    providedIn: 'root'
})
export class TrainingPlanService {
    constructor(public http: HttpClient, public session: SessionService) {}

    getAll(): Observable<TrainingPlan[]> {
        return this.http
            .get<TrainingPlan[]>(`${API_URL}/TrainingPlan/training-plans`, {
                ...httpOptions,
                params: {
                    request_id: this.session.me().id
                }
            })
            .pipe(share());
    }

    getById(id: string): Observable<TrainingPlan> {
        return this.http.get<TrainingPlan>(
            `${API_URL}/TrainingPlan/training-plan`,
            {
                ...httpOptions,
                params: {
                    request_id: this.session.me().id,
                    id
                }
            }
        );
    }

    getByUser(id: number): Observable<TrainingPlan[]> {
        return this.http
            .get<TrainingPlan[]>(`${API_URL}/TrainingPlan/training-plans`, {
                ...httpOptions,
                params: {
                    request_id: this.session.me().id
                }
            })
            .pipe(
                map((plans) => {
                    const filtered = [];

                    plans.filter((obj) => {
                        if (obj.athlete_id === id) {
                            filtered.push(obj);
                        }
                    });

                    return filtered;
                })
            );
    }

    new(body: object): Observable<TrainingPlan> {
        return this.http
            .post<TrainingPlan>(
                `${API_URL}/TrainingPlan/create-training-plan`,
                {
                    request_id: this.session.me().id,
                    ...body
                },
                httpOptions
            )
            .pipe(share());
    }

    addExercise(body: object): Observable<TrainingPlan> {
        return this.http
            .post<TrainingPlan>(
                `${API_URL}/TrainingPlan/add-exercise-to-plan`,
                {
                    request_id: this.session.me().id,
                    ...body
                },
                httpOptions
            )
            .pipe(share());
    }

    deleteById(plan: number): Observable<TrainingPlan> {
        const options = {
            ...httpOptions,
            body: {
                request_id: this.session.me().id,
                training_plan_id: plan.toString()
            }
        };

        return this.http
            .delete<TrainingPlan>(
                `${API_URL}/TrainingPlan/training-plan`,
                options
            )
            .pipe(share());
    }
}
