import { inject, TestBed } from '@angular/core/testing';

import { NutrionalEvaluationService } from './nutrional-evaluation.service';

describe('NutrionalEvaluationService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [NutrionalEvaluationService]
        });
    });

    it('should be created', inject(
        [NutrionalEvaluationService],
        (service: NutrionalEvaluationService) => {
            expect(service).toBeTruthy();
        }
    ));
});
