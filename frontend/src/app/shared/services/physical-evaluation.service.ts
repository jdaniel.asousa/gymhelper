import { Injectable } from '@angular/core';
import { environment } from '@env';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionService } from '@services/session.service';
import { Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';
import { PhysicalEvaluation } from '@models/physical-evaluation.model';

const API_URL = environment.apiUrl;

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    }),
    withCredentials: true
};

@Injectable({
    providedIn: 'root'
})
export class PhysicalEvaluationService {
    constructor(private http: HttpClient, private session: SessionService) {}

    /**
     * Method to get all physical evaluations from api
     */
    getAll(): Observable<PhysicalEvaluation[]> {
        return this.http
            .get<PhysicalEvaluation[]>(
                `${API_URL}/PhysicalEvaluation/physical-evaluations`,
                {
                    ...httpOptions,
                    params: {
                        request_id: this.session.me().id
                    }
                }
            )
            .pipe(share());
    }

    /**
     * Method to get all physical evaluations from api and filter by athlete
     */
    getByAthlete(athlete: number): Observable<PhysicalEvaluation[]> {
        return this.http
            .get<PhysicalEvaluation[]>(
                `${API_URL}/PhysicalEvaluation/physical-evaluations`,
                {
                    ...httpOptions,
                    params: {
                        request_id: this.session.me().id
                    }
                }
            )
            .pipe(
                map((res) => {
                    const result = [];

                    res.map((evaluation) => {
                        if (evaluation.athlete_id === athlete) {
                            result.push(evaluation);
                        }
                    });

                    return result;
                })
            );
    }

    /**
     * Method to get all physical evaluations from api and filter by athlete
     */
    getLastTwo(athlete: number): Observable<PhysicalEvaluation[]> {
        return this.http
            .get<PhysicalEvaluation[]>(
                `${API_URL}/PhysicalEvaluation/physical-evaluations`,
                {
                    ...httpOptions,
                    params: {
                        request_id: this.session.me().id
                    }
                }
            )
            .pipe(
                map((res) => {
                    const result = [];

                    res.map((evaluation) => {
                        console.log(evaluation);
                        if (evaluation.athlete_id === athlete) {
                            result.push(evaluation);
                        }
                    });

                    return result.slice(-2);
                })
            );
    }

    getLastTwoByMe(): Observable<PhysicalEvaluation[]> {
        return this.http
            .get<PhysicalEvaluation[]>(
                `${API_URL}/PhysicalEvaluation/physical-evaluations-by-athlete-id`,
                {
                    ...httpOptions,
                    params: {
                        request_id: this.session.me().id
                    }
                }
            )
            .pipe(
                map((res) => {
                    const result = [];

                    res.map((evaluation) => {
                        result.push(evaluation);
                    });

                    return result.slice(-2);
                })
            );
    }

    /**
     * Method to get one physical evaluation from api
     */
    getById(id: string): Observable<PhysicalEvaluation> {
        return this.http
            .get<PhysicalEvaluation>(
                `${API_URL}/PhysicalEvaluation/physical-evaluation`,
                {
                    ...httpOptions,
                    params: {
                        request_id: this.session.me().id,
                        id
                    }
                }
            )
            .pipe(share());
    }
}
