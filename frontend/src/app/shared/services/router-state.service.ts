import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Params } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class RouterStateService {
    params$: Observable<Params>;

    constructor() {}
}
