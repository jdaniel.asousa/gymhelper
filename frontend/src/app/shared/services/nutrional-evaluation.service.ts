import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionService } from '@services/session.service';
import { NutrionalEvaluation } from '@models/nutrional-evaluation.model';
import { Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';
import { environment } from '@env';
import { Meal } from '@shared/models/meal.model';

const API_URL = environment.apiUrl;

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    }),
    withCredentials: true
};

@Injectable({
    providedIn: 'root'
})
export class NutrionalEvaluationService {
    constructor(public http: HttpClient, public session: SessionService) {}

    /**
     * Method to get all exercises from api
     */
    getAll(): Observable<NutrionalEvaluation[]> {
        return this.http
            .get<NutrionalEvaluation[]>(
                `${API_URL}/nutritionalevaluation/nutritionalevaluations`,
                {
                    ...httpOptions,
                    params: {
                        request_id: this.session.me().id
                    }
                }
            )
            .pipe(share());
    }

    getByAthlete(athlete: string): Observable<NutrionalEvaluation[]> {
        return this.http
            .get<NutrionalEvaluation[]>(
                `${API_URL}/nutritionalevaluation/nutritionalevaluations`,
                {
                    ...httpOptions,
                    params: {
                        request_id: this.session.me().id
                    }
                }
            )
            .pipe(
                map((res) => {
                    const result = [];

                    res.map((evaluation) => {
                        if (evaluation.athlete_id === +athlete) {
                            result.push(evaluation);
                        }
                    });

                    return result;
                })
            );
    }

    getById(id: string): Observable<NutrionalEvaluation> {
        return this.http
            .get<NutrionalEvaluation>(
                `${API_URL}/NutritionalEvaluation/nutritionalEvaluation`,
                {
                    ...httpOptions,
                    params: {
                        request_id: this.session.me().id,
                        id
                    }
                }
            )
            .pipe(share());
    }

    /**
     * Method to delete exercise by ID from api
     * @param id ID of exercise to delete
     */
    deleteById(id: number): Observable<NutrionalEvaluation> {
        const options = {
            ...httpOptions,
            body: {
                request_id: this.session.me().id,
                nutritional_evaluation_id: id
            }
        };

        return this.http
            .delete<NutrionalEvaluation>(
                `${API_URL}/nutrionalevaluation/nutrionalevaluation`,
                options
            )
            .pipe(share());
    }
    addMeal(body: object): Observable<Meal> {
        return this.http
            .post<Meal>(
                `${API_URL}/Meal/create-meal`,
                {
                    request_id: this.session.me().id,
                    ...body
                },
                httpOptions
            )
            .pipe(share());
    }
    /**
     * Method to delete meal by ID from api
     * @param id ID of meal to delete
     */
    deleteMeal(id: number): Observable<Meal> {
        const options = {
            ...httpOptions,
            body: {
                request_id: this.session.me().id,
                meal_id: id
            }
        };

        return this.http
            .delete<Meal>(`${API_URL}/Meal/meal`, options)
            .pipe(share());
    }
    /**
     * Method to update meal by ID from api
     * @param body meal details to update
     */
    editMeal(body: object): Observable<Meal> {
        return this.http
            .put<Meal>(
                `${API_URL}/Meal/update-meal`,
                {
                    request_id: this.session.me().id,
                    ...body
                },
                httpOptions
            )
            .pipe(share());
    }
}
