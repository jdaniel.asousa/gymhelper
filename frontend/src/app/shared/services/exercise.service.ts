import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@envs/environment';
import { SessionService } from './session.service';
import { Exercise } from '@shared/models/exercise.model';
import { share } from 'rxjs/operators';

const API_URL = environment.apiUrl;

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    }),
    withCredentials: true
};

@Injectable({
    providedIn: 'root'
})
export class ExerciseService {
    constructor(public http: HttpClient, public session: SessionService) {}

    new(body: object): Observable<Exercise> {
        return this.http
            .post<Exercise>(
                `${API_URL}/exercise/create-exercise`,
                {
                    request_id: this.session.me().id,
                    ...body
                },
                httpOptions
            )
            .pipe(share());
    }

    // TODO: fix that any
    newImage(data: FormData): Observable<any> {
        data.append('request_id', this.session.me().id);

        return this.http
            .post(`${API_URL}/exerciseimage/create-exercise-image`, data)
            .pipe(share());
    }

    /**
     * Method to get all exercises from api
     */
    getAll(): Observable<Exercise[]> {
        return this.http
            .get<Exercise[]>(`${API_URL}/exercise/exercises`, {
                ...httpOptions,
                params: {
                    request_id: this.session.me().id
                }
            })
            .pipe(share());
    }

    getById(id: string): Observable<Exercise> {
        return this.http
            .get<Exercise>(`${API_URL}/exercise/exercise`, {
                ...httpOptions,
                params: {
                    request_id: this.session.me().id,
                    id
                }
            })
            .pipe(share());
    }

    getByMachineID(machine_id: string): Observable<Exercise[]> {
        return this.http
            .get<Exercise[]>(`${API_URL}/exercise/exercises-by-machine`, {
                ...httpOptions,
                params: {
                    request_id: this.session.me().id,
                    machine_id
                }
            })
            .pipe(share());
    }

    /**
     * Method to delete exercise by ID from api
     * @param id ID of exercise to delete
     */
    deleteById(id: number): Observable<Exercise> {
        const options = {
            ...httpOptions,
            body: {
                request_id: this.session.me().id,
                exercise_id: id
            }
        };

        return this.http
            .delete<Exercise>(`${API_URL}/exercise/exercise`, options)
            .pipe(share());
    }

    updateById(body: object): Observable<Exercise> {
        return this.http
            .put<Exercise>(
                `${API_URL}/exercise/update-exercise`,
                {
                    request_id: this.session.me().id,
                    ...body
                },
                httpOptions
            )
            .pipe(share());
    }

    updateImage(data: FormData): Observable<any> {
        data.append('request_id', this.session.me().id);

        return this.http
            .put(`${API_URL}/exerciseimage/update-exercise-image`, data)
            .pipe(share());
    }
}
