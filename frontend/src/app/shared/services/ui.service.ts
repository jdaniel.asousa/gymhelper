import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { environment } from '@env';

@Injectable({
    providedIn: 'any'
})
export class UiService {
    /**
     * Get Base Title (App. Title) from Environment File
     * @private
     */
    private baseTitle = environment.baseTitle;

    constructor(
        private iconRegistry: MatIconRegistry,
        private sanitizer: DomSanitizer,
        public snackBar: MatSnackBar,
        private angularTitle: Title
    ) {}

    /**
     * Method to show Material Snackbar with some options
     * @param message Message to show on Snackbar
     * @param button Text of button
     * @param duration Duration to auto close Snackbar
     */
    showSnackBar(message, button = 'OK', duration = 3000) {
        return this.snackBar.open(message, button, {
            duration
        });
    }

    /**
     * Method to set title of page
     * @param title Custom Page Title
     * @param divider Divider of Page Title and Application Name
     */
    setTitle(title?: string, divider: string = '|') {
        this.angularTitle.setTitle(
            `${title ? `${title} ${divider}` : ''} ${this.baseTitle}`
        );
    }

    /**
     * Method to add new icons using SVG Sprite icons
     */
    load() {
        // Load Sprite SVG
        this.iconRegistry.addSvgIconSetInNamespace(
            'sprite',
            this.sanitizer.bypassSecurityTrustResourceUrl(
                'assets/media/icons/sprite.svg'
            )
        );
    }
}
