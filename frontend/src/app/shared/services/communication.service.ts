import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CommunicationService {
    private subject = new Subject<boolean>();

    sendStatus(status: boolean) {
        this.subject.next(status);
    }

    getStatus(): Observable<any> {
        return this.subject.asObservable();
    }

    constructor() {}
}
