import { Injectable } from '@angular/core';
import { environment } from '@env';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionService } from '@services/session.service';
import { Observable } from 'rxjs';
import { MonthlyPlan } from '@models/monthly-plan.model';
import { share } from 'rxjs/operators';

const API_URL = environment.apiUrl;

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    }),
    withCredentials: true
};

@Injectable({
    providedIn: 'root'
})
export class MonthlyPlanService {
    constructor(public http: HttpClient, public session: SessionService) {}

    getAll(): Observable<MonthlyPlan[]> {
        return this.http
            .get<MonthlyPlan[]>(`${API_URL}/MonthlyPlan/monthly-plans`, {
                ...httpOptions,
                params: {
                    request_id: this.session.me().id
                }
            })
            .pipe(share());
    }
}
