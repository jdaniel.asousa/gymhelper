import { Pipe, PipeTransform } from '@angular/core';
import { formatDate } from '@functions/format-date.function';

@Pipe({
    name: 'formatDate'
})
export class FormatDatePipe implements PipeTransform {
    transform(value: string, format: string = 'y/m/d'): string {
        return formatDate(value, format);
    }
}
