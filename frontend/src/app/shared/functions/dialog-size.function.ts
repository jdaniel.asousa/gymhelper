/**
 * Function to return width for dialog
 * For desktop using 25 viewport width
 * For tablet using 50 viewport width
 * And for mobile using 85 viewport width
 */
export function dialogSize(variation = 'default') {
    let sizes;

    switch (variation) {
        case 'default':
            sizes =
                window.innerWidth >= 1200
                    ? '25vw'
                    : window.innerWidth >= 800
                    ? '50vw'
                    : '85vw';
            break;

        case 'view-dialog':
            sizes =
                window.innerWidth >= 1200
                    ? '35vw'
                    : window.innerWidth >= 800
                    ? '50vw'
                    : '85vw';
            break;

        default:
            sizes =
                window.innerWidth >= 1200
                    ? '25vw'
                    : window.innerWidth >= 800
                    ? '50vw'
                    : '85vw';
            break;
    }

    return sizes;
}
