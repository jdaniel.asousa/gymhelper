export function formatPhone(phone: string) {
    phone = phone.replace('/(d)(?=(d{3})+$)/g', '$1 ');

    return phone;
}
