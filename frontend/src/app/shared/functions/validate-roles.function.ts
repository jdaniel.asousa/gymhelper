import { Roles } from '@enums/roles.enum';

/**
 * Function to validate if user role have access to that resource
 * @param userRole User Role
 * @param roles All roles have access to that
 */
export function validateRoles(userRole: Roles, roles: Roles[]) {
    const have = roles.includes(userRole);

    return have;
}
