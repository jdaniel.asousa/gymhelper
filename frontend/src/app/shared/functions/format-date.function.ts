import * as moment from 'moment';

export function formatDate(stringDate: string, format: string = 'y/m/d') {
    const date = new Date(stringDate);
    let formattedDate;

    switch (format) {
        case 'y/m/d':
            formattedDate = moment(date).format('YYYY/MM/DD');
            break;
        case 'd/m/y':
            formattedDate = moment(date).format('DD/MM/YYYY');
            break;
        case 'd/m/y h:m':
            formattedDate = moment(date).format('DD/MM/YYYY HH:mm');
            break;
        case 'y/m/d h:m':
            formattedDate = moment(date).format('YYYY/MM/DD HH:mm');
            break;
        case 'database':
            formattedDate = `${moment(date).format('MM/DD/YYYY')} 00:00:00 AM`;
            break;
        default:
            formattedDate = moment(date).format('YYYY/MM/DD');
            break;
    }

    return formattedDate;
}
