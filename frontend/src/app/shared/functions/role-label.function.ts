import { Roles } from '@enums/roles.enum';

export function getRoleLabel(role: Roles) {
    let roleLabel: string;

    switch (role) {
        case 1:
            roleLabel = 'Administrador';
            break;
        case 2:
            roleLabel = 'Atleta';
            break;
        case 3:
            roleLabel = 'Rececionista';
            break;
        case 4:
            roleLabel = 'Personal Trainer';
            break;
        case 5:
            roleLabel = 'Nutricionista';
            break;
        default:
            break;
    }

    return roleLabel;
}
