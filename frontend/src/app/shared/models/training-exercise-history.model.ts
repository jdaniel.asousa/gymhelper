import { TrainingExercise } from '@models/training-exercise.model';

export class TrainingExerciseHistory {
    id?: number;

    training_exercise_id: number;
    training_exercise?: TrainingExercise;

    time?: string;
    repetitions?: string;

    created_at?: string;
    updated_at?: string;
}
