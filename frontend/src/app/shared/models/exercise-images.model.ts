import { Exercise } from '@models/exercise.model';

export class ExerciseImages {
    id?: number;
    path: string;

    exercise_id: number;
    exercise?: Exercise;

    created_at?: string;
    updated_at?: string;
}
