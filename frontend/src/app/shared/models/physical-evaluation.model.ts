import { Athlete } from '@models/athlete.model';

export class PhysicalEvaluation {
    id?: number;

    height: number;
    weight: number;
    imc: number;

    description?: string;

    body_water: number;
    body_fat: number;
    muscle_mass: number;
    lat_size: number;
    left_bicep_size: number;
    right_bice_size: number;
    chest_size: number;
    waist_size: number;
    glute_size: number;
    hip_size: number;
    thigh_size: number;
    calfs_size: number;

    athlete_id: number;
    athlete?: Athlete;

    created_at?: string;
    updated_at?: string;
}
