import { Exercise } from '@models/exercise.model';
import { MachineImages } from '@models/machine-images.model';

export class Machine {
    id?: number;
    name: string;
    state: boolean;
    qrcode?: string;

    machine_images?: MachineImages[];

    exercises?: Exercise[];

    created_at?: string;
    updated_at?: string;
}
