export class Meal {
    id?: number;
    calories: number;
    protein: number;
    fats: number;
    carbohydrates: number;
    description?: string;
    nutrition_plan_id?: number;
}
