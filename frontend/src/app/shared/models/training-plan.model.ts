import { Exercise } from './exercise.model';

export class TrainingPlan {
    id?: number;
    description?: string;

    training_exercises: Exercise[];

    // TODO: fix this (return on created)
    trainingPlanCreated?: any;

    athlete_id: number;
}
