export class User {
    id?: number;
    name: string;
    email: string;
    role_id?: number;
    role?: number;

    password?: string;
    avatar?: string;

    athlete_id?: number;
    employee_id?: number;
    employee_info?: any;

    // Because use on profile (in case of user is athlete)
    birth_date?: string;

    created_at?: string;
    updated_at?: string;
}
