import { User } from './user.model';

export class Athlete extends User {
    // id?: number;
    // name: string;
    // email: string;
    // avatar?: string;
    // role_id: number;
    birth_date?: string;

    athlete_id: number;
    athlete_info?: any;

    personal_trainer_id: number;
    personal_trainer?: User;
    //
    // created_at?: string;
    // updated_at?: string;
}
