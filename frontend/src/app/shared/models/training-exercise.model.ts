import { TrainingPlan } from '@models/training-plan.model';
import { Exercise } from '@models/exercise.model';

export class TrainingExercise {
    id?: number;

    plan_id: number;
    plan?: TrainingPlan;

    exercise_id: number;
    exercise?: Exercise;

    circuit: number;
    time?: string;
    repetitions?: string;
    sets: number;
}
