export class MonthlyPlan {
    id?: number;
    name: string;
    description?: string;
    price: number;
    entrys: number;

    created_at?: string;
    updated_at?: string;
}
