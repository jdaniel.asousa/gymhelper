import { Machine } from '@models/machine.model';

export class MachineImages {
    id?: number;
    path: string;

    machine_id: number;
    machine?: Machine;

    created_at?: string;
    updated_at?: string;
}
