import { Meal } from './meal.model';

export class NutritionPlan {
    id?: number;
    total_calories: number;
    total_protein: number;
    total_fats: number;
    total_carbohydrates: number;
    nutritional_evaluation_id: number;
    meals: Meal[];
}
