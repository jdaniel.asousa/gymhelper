import { MonthlyPlan } from '@models/monthly-plan.model';
import { Athlete } from '@models/athlete.model';

export class MemberPlan {
    id?: number;

    monthly_plan_id: number;
    monthly_plan?: MonthlyPlan;

    athlete_id: number;
    athlete?: Athlete;

    entry_date: string;
    expired_date: string;
    valid_entrys: number;

    created_at?: string;
    updated_at?: string;
}
