import { Athlete } from './athlete.model';
import { User } from './user.model';
import { NutritionPlan } from '@models/nutrition-plan.model';

export class NutrionalEvaluation {
    id?: number;
    suggestions: string;
    schedule: string;
    nutrition_plan: NutritionPlan;

    athlete_id: number;
    athlete?: Athlete;

    nutricionist_id: number;
    nutricionist?: User;

    created_at?: string;
    updated_at?: string;
}
