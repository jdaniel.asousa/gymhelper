import { Machine } from './machine.model';
import { ExerciseImages } from '@models/exercise-images.model';

export class Exercise {
    id?: number;
    name: string;
    muscle: string;
    video: string;
    description: string;
    type: string;

    machine_id?: number;
    machine?: Machine;

    exercise_images?: ExerciseImages[];

    // TODO: fix later => object returned on created method
    exerciseCreated?: any;
    exerciseUpdated?: any;

    created_at?: string;
    updated_at?: string;
}
