# GymHelper

### `.editorconfig`

<pre>
# Editor configuration, see https://editorconfig.org
root = true

[*]
charset = utf-8
indent_size = 4
trim_trailing_whitespace = true

[*.js]
indent_style = tab

[*.ts]
indent_style = space

[*.html]
indent_style = tab

[*.md]
max_line_length = off
trim_trailing_whitespace = false

[*.{sass, scss, yaml}]
indent_style = space

[*.yaml]
indent_size = 2
</pre>
