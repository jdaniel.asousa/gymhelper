<table>
<tbody>
	<tr>
		<td style="padding: 10px 20px">
			<a href="#">
				<img src="./light_version.png" alt="Gym Helper" width="50px" />
			</a>
		</td>
		<td style="padding: 10px 20px">
			<a href="https://estg.ipp.pt" target="_blank">
				<img src="https://www.estg.ipp.pt/logo-ipp.png" alt="ESTG | P.Porto" width="150px">
			</a>
		</td>
	</tr>
</tbody>
</table>

<h2>Logo</h2>

<pre>
logo
├── README.md
├── gym_helper_vetor.afdesing [1]
├── gym_helper_vetor.afdesing [2]
├── dark_version_horizontal.png
├── dark_version.png
├── favicon.png
└── light_version.png
</pre>

<hr style="height: 1px; margin-bottom: 0;" />
<h6>Notas:</h6>

<ul>
	<li><strong>[1]:</strong> Formato <strong>Affinity Desing</strong>;
	<li><strong>[2]:</strong> Logos em vetor no formato <code>.pdf</code> para permitir abrir e editar os mesmos noutras ferramentas (como <strong>Adobe Illustrator</strong> ou <strong>Corel Draw</strong>).
</ul>