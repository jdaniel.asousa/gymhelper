<table>
<tbody>
	<tr>
		<td style="padding: 10px 20px">
			<a href="#">
				<img src="../logo/light_version.png" alt="Gym Helper" width="50px" />
			</a>
		</td>
		<td style="padding: 10px 20px">
			<a href="https://estg.ipp.pt" target="_blank">
				<img src="https://www.estg.ipp.pt/logo-ipp.png" alt="ESTG | P.Porto" width="150px">
			</a>
		</td>
	</tr>
</tbody>
</table>

<h2>Mockups</h2>

<pre>
mockups
├── README.md
├── mockups.xd [1]
├── mockups_mobile.xd [2]
├── webapp [3]
└── extra_files
   ├── material_ui_dark.xd [4]
   ├── material_ui_light.xd [5]
   └── svg_icons [6]
</pre>

<hr style="height: 1px; margin-bottom: 0;" />
<h6>Notas:</h6>

<ul>
	<li><strong>[1]:</strong> Mockups para web app;
	<li><strong>[2]:</strong> Mockups para mobile app;
	<li><strong>[2]:</strong> Mockups em formato de imagem da web app;
	<li><strong>[4]:</strong> Componentes de <strong>Material UI</strong> na versão <em>Light Theme</em>;
	<li><strong>[5]:</strong> Componentes de <strong>Material UI</strong> na versão <em>Dark Theme</em>;
	<li><strong>[6]:</strong> Icons no formato <code>.svg</code> para utilização nos mockups.
</ul>