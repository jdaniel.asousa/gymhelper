const isTabletOrMobileDevice = useMediaQuery({    
  maxDeviceWidth: 1224,
  // alternatively...
  query: "(max-device-width: 1224px)"  
});