# Login on MariaDB using Sudo
sudo mysql -u root -p
#--> Insert user password on terminal
#--> On root password leave blank and press enter

# Create a new user
USE mysql;
CREATE USER user@'%' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO tutods@'%';
FLUSH PRIVILEGES;
exit;