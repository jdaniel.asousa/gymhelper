@NgModule({
	declarations: [ProfileComponent],
	imports: [
		CommonModule,
		ProfileRoutingModule,
		MatButtonModule,
		MatInputModule,
		MatFormFieldModule,
		MatIconModule,
		FlexLayoutModule,
		MatExpansionModule,
		MatDatepickerModule,
		MatMomentDateModule,
		SharedModule,
		FormsModule,
		ReactiveFormsModule,
	],
})
export class ProfileModule {}
