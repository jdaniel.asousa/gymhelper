# Linux
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -

echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt update && sudo apt install yarn

# macOS
brew install yarn #--> using Homebrew
curl -o- -L https://yarnpkg.com/install.sh | bash #--> using curl command
