public void Send(string to, string subject, string html)
{
	var email = new MimeMessage();

	email.From.Add(MailboxAddress.Parse(_appSettings.EmailFrom));
	email.To.Add(MailboxAddress.Parse(to));
	email.Subject = subject;
	email.Body = new TextPart(TextFormat.Html) { Text = html };

	using var smtp = new SmtpClient();
	smtp.Connect(_appSettings.SmtpHost, _appSettings.SmtpPort, SecureSocketOptions.SslOnConnect);
	smtp.Authenticate(_appSettings.SmtpUser, _appSettings.SmtpPass);
	smtp.Send(email);
	smtp.Disconnect(true);
}