/// <summary>
/// Create
/// </summary>
/// <param name="requestUrl">Request Url</param>
public async Task<string> Create(string requestUrl)
{
    HttpClient client = new HttpClient();
    string createURL = _appSettings.QRCodeURL.Replace("dataURL",requestUrl);
    HttpResponseMessage response = await client.GetAsyn(createURL);
	response.EnsureSuccessStatusCode();
    var result = await response.Content.ReadAsStringAsync();

    return createURL;
}