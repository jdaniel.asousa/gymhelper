/// <summary>
/// Post Method
/// </summary>
/// <param name="image">image</param>
/// <returns>ImgBBResponse</returns>
    public async Task<ImgBBResponse> PostPhoto(IFormFile image)
    {
        HttpClient client = new HttpClient();
        MultipartFormDataContent form = new MultipartFormDataContent();
        client.DefaultRequestHeaders.Clear();
        byte[] bytess;

        using (var memoryStream = new MemoryStream())
        {
            await image.CopyToAsync(memoryStream);
            bytess = memoryStream.ToArray();
        }

        HttpContent bytesContent = new ByteArrayContent(bytess);

        form.Add(new StringContent(_appSettings.ImgBBKey), "key");
        form.Add(bytesContent, "image", "image.jpg");

        HttpResponseMessage response = await client.PostAsyn(_appSettings.ImgBBPostUrl, form);
        var k = response.Content.ReadAsStringAsync().Result;
       	ImgBBResponse imgBBResponse = JsonConvertDeserializeObject<ImgBBResponse>(k);

        if (imgBBResponse.status != 200)
            return null;

        return imgBBResponse;
	}