# Change to Front-end folder
cd frontend

# Run Front-end using Angular Cli
ng s

# Run Front-end using Yarn
yarn start

# Run Front-end using NPM
npm start
