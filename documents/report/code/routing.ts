import { NgModule } from ""@angular/core"";
import { RouterModule, Routes } from ""@angular/router"";

const routes: Routes = [
	{
		path: "",
		loadChildren: () =>
			import('./containers/auth/auth.module').then((m) => m.AuthModule),
	},
	{
		path: "admin",
		loadChildren: () =>
			import("./containers/admin/admin.module").then(
				(m) => m.AdminModule
			),
	},
	{ path: "", redirectTo: "", pathMatch: "full" },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
