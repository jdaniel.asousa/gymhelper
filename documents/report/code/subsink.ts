import { SubSink } from 'subsink';

// ...

/**
 * Variable contains all subscriptions
 * @private
 */
private subs = new SubSink();

// ...

fetchData(): void {

	this.subs.sink = this.userService
		.getById(this.session.me().id)
		.subscribe(
			(data) => {
				this.user = data;
			});
}

// ...

/**
 * This method is executed when view as destroyed
 * Use this method to unsubscribe all subscriptions you have
 */
ngOnDestroy(): void {
	if (this.subs) {
		this.subs.unsubscribe();
	}
}