/**
 * Communicate with API method to reset password
 * @param email Email Password
 * @param password User Password
 */
login(email: string, password: string) {
	const request = this.http
		.post(
			`${API_URL}/user/login`,
			{
				email,
				password
			},
			httpOptions
		)
		.pipe(share());

	// Make subscription to get data
	this.subs.sink = request.subscribe((user) => {
		this.session = user;
		localStorage.setItem('user', JSON.stringify(user));
	});

	return request;
}