<table>
<tbody>
	<tr>
		<td style="padding: 10px 20px">
			<a href="#">
				<img src="logo/light_version.png" alt="Gym Helper" width="50px" />
			</a>
		</td>
		<td style="padding: 10px 20px">
			<a href="https://estg.ipp.pt" target="_blank">
				<img src="https://www.estg.ipp.pt/logo-ipp.png" alt="ESTG | P.Porto" width="150px">
			</a>
		</td>
	</tr>
</tbody>
</table>

## Documents

<pre>
documents
├── README.md
├── presentation
│   └── <a href="https://gitlab.estg.ipp.pt/DSousa/gymhelper/blob/master/documents/presentation/lds_presentation.pptx" target="_blank">lds_presentation.pptx</a>
├── atas_reunioes
│   └── <a href="https://gitlab.estg.ipp.pt/DSousa/gymhelper/blob/master/documents/atas_reunioes/ata_reuniao_1/main.pdf" target="_blank">ata_reuniao_1</a>
|   └── <a href="https://gitlab.estg.ipp.pt/DSousa/gymhelper/blob/master/documents/atas_reunioes/ata_reuniao_2/main.pdf" target="_blank">ata_reuniao_2</a>
|   └── <a href="https://gitlab.estg.ipp.pt/DSousa/gymhelper/blob/master/documents/atas_reunioes/ata_reuniao_3/main.pdf" target="_blank">ata_reuniao_3</a>
|   └── <a href="https://gitlab.estg.ipp.pt/DSousa/gymhelper/blob/master/documents/atas_reunioes/ata_reuniao_4/main.pdf" target="_blank">ata_reuniao_4</a>
|   └── <a href="https://gitlab.estg.ipp.pt/DSousa/gymhelper/blob/master/documents/atas_reunioes/ata_reuniao_5/main.pdf" target="_blank">ata_reuniao_5</a>
|   └── <a href="https://gitlab.estg.ipp.pt/DSousa/gymhelper/blob/master/documents/atas_reunioes/ata_reuniao_6/main.pdf" target="_blank">ata_reuniao_6</a>
|   └── <a href="https://gitlab.estg.ipp.pt/DSousa/gymhelper/blob/master/documents/atas_reunioes/ata_reuniao_7/main.pdf" target="_blank">ata_reuniao_7</a>
|   └── <a href="https://gitlab.estg.ipp.pt/DSousa/gymhelper/blob/master/documents/atas_reunioes/ata_reuniao_8/main.pdf" target="_blank">ata_reuniao_8</a>
└── templates
   ├── ata_de_reuniao.doc
   ├── ata_reuniao_latex
   ├── relatorio_individual.docx
   └── srs_template-ieee.doc
</pre>