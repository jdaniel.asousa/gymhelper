create table machines
(
	id int auto_increment
		primary key,
	name varchar(255) not null,
	state tinyint(1) not null,
	qrcode varchar(255) not null,
	created_at timestamp default current_timestamp() null,
	updated_at datetime default current_timestamp() null on update current_timestamp()
)
charset=utf8;

create table exercises
(
	id int auto_increment
		primary key,
	type enum('Reps', 'Time') not null,
	name varchar(255) not null,
	muscle varchar(255) not null,
	video varchar(255) null,
	description varchar(255) null,
	machine_id int null,
	created_at timestamp default current_timestamp() null,
	updated_at datetime default current_timestamp() null on update current_timestamp(),
	constraint machine_exercise_fk
		foreign key (machine_id) references machines (id)
			on update cascade on delete set null
)
charset=utf8;

create table exercise_images
(
	id int auto_increment
		primary key,
	path varchar(255) not null,
	exercise_id int not null,
	created_at timestamp default current_timestamp() null,
	updated_at datetime default current_timestamp() null on update current_timestamp(),
	constraint `exercise_id _image_fk`
		foreign key (exercise_id) references exercises (id)
			on update cascade on delete cascade
)
charset=utf8;

create table machine_images
(
	id int auto_increment
		primary key,
	path varchar(255) not null,
	machine_id int not null,
	created_at timestamp default current_timestamp() null,
	updated_at datetime default current_timestamp() null on update current_timestamp(),
	constraint machine_id_fk
		foreign key (machine_id) references machines (id)
			on update cascade on delete cascade
)
charset=utf8;

create table monthly_plans
(
	id int auto_increment
		primary key,
	name varchar(256) not null,
	description varchar(256) null,
	price float default 0 not null,
	entrys int not null,
	created_at timestamp default current_timestamp() null,
	updated_at datetime default current_timestamp() null
);

create table roles
(
	id int auto_increment
		primary key,
	name varchar(255) not null,
	created_at timestamp default current_timestamp() null,
	updated_at datetime default current_timestamp() null on update current_timestamp()
)
charset=utf8;

create table users
(
	id int auto_increment
		primary key,
	name varchar(255) null,
	email varchar(255) not null,
	password varchar(255) not null,
	reset_token varchar(255) null,
	reset_token_expires datetime null,
	password_reset datetime null,
	avatar varchar(255) null,
	role_id int not null,
	created_at timestamp default current_timestamp() null,
	updated_at datetime default current_timestamp() null on update current_timestamp(),
	constraint users_email_password_uindex
		unique (email, password),
	constraint role_id_fk
		foreign key (role_id) references roles (id)
			on update cascade
)
charset=utf8;

create table athletes_info
(
	id int auto_increment
		primary key,
	birth_date datetime not null,
	user_id int not null,
	personal_trainer_id int not null,
	created_at timestamp default current_timestamp() null,
	updated_at datetime default current_timestamp() null on update current_timestamp(),
	constraint FK_athletes_info_users
		foreign key (user_id) references users (id)
			on update cascade on delete cascade
)
charset=utf8;

create index FK_athletes_info_employees_info
	on athletes_info (personal_trainer_id);

create table employees_info
(
	id int auto_increment
		primary key,
	user_id int not null,
	created_at timestamp default current_timestamp() null,
	updated_at datetime default current_timestamp() null on update current_timestamp(),
	constraint user_id_fk
		foreign key (user_id) references users (id)
			on update cascade on delete cascade
)
charset=utf8;

create table member_plans
(
	id int auto_increment
		primary key,
	monthly_plan_id int not null,
	athlete_id int not null,
	entry_date datetime default current_timestamp() not null,
	expired_date datetime default current_timestamp() not null,
	valid_entrys int not null,
	created_at timestamp default current_timestamp() null,
	updated_at datetime default current_timestamp() null,
	constraint athlete_fk
		foreign key (athlete_id) references athletes_info (id)
			on update cascade on delete cascade
);

create index monthly_plan_fk
	on member_plans (monthly_plan_id);

create table nutritional_evaluations
(
	id int auto_increment
		primary key,
	suggestions text null,
	schedule date not null,
	athlete_id int not null,
	nutricionist_id int not null,
	created_at timestamp default current_timestamp() null,
	updated_at datetime default current_timestamp() null on update current_timestamp(),
	constraint athlete_id_fk
		foreign key (athlete_id) references athletes_info (id)
			on update cascade on delete cascade
)
charset=utf8;

create table nutrition_plans
(
	id int auto_increment
		primary key,
	total_calories float not null,
	total_protein float not null,
	total_fats float not null,
	total_carbohydrates float not null,
	nutritional_evaluation_id int not null,
	created_at timestamp default current_timestamp() null,
	updated_at datetime default current_timestamp() null on update current_timestamp(),
	constraint nutrional_evaluation_id_fk
		foreign key (nutritional_evaluation_id) references nutritional_evaluations (id)
			on update cascade on delete cascade
)
charset=utf8;

create table meals
(
	id int auto_increment
		primary key,
	calories float default 0 not null,
	protein float not null,
	fats float not null,
	carbohydrates float not null,
	description text null,
	nutrition_plan_id int not null,
	created_at timestamp default current_timestamp() null,
	updated_at datetime default current_timestamp() null on update current_timestamp(),
	constraint nutritional_plan_id_fk
		foreign key (nutrition_plan_id) references nutrition_plans (id)
			on update cascade on delete cascade
)
charset=utf8;

create index `Index 3`
	on nutritional_evaluations (nutricionist_id);

create table physical_evaluations
(
	id int auto_increment
		primary key,
	height float not null,
	weight float not null,
	imc float not null,
	description text null,
	body_water float(4,2) not null,
	body_fat float not null,
	muscle_mass float not null,
	lat_size float not null,
	left_bicep_size float not null,
	right_bicep_size float not null,
	chest_size float not null,
	waist_size float not null,
	glute_size float not null,
	hip_size float not null,
	thigh_size float not null,
	calfs_size float not null,
	athlete_id int not null,
	created_at timestamp default current_timestamp() null,
	updated_at datetime default current_timestamp() null on update current_timestamp(),
	constraint athlete_id
		foreign key (athlete_id) references athletes_info (id)
			on update cascade on delete cascade
)
charset=utf8;

create table training_plans
(
	id int auto_increment
		primary key,
	description text null,
	athlete_id int not null,
	created_at timestamp default current_timestamp() null,
	updated_at datetime default current_timestamp() null on update current_timestamp(),
	constraint athlete_id_training_plan_fk
		foreign key (athlete_id) references athletes_info (id)
			on update cascade on delete cascade
)
charset=utf8;

create table training_exercises
(
	id int auto_increment
		primary key,
	plan_id int(10) not null,
	exercise_id int not null,
	circuit int(2) not null,
	time varchar(50) null,
	repetitions varchar(50) null,
	sets int(2) not null,
	created_at timestamp default current_timestamp() null,
	updated_at datetime default current_timestamp() null on update current_timestamp(),
	constraint FK_Exercise_id
		foreign key (exercise_id) references exercises (id)
			on update cascade on delete cascade,
	constraint FK_Plan_id
		foreign key (plan_id) references training_plans (id)
			on update cascade on delete cascade
)
charset=utf8;

create table training_exercises_history
(
	id int auto_increment
		primary key,
	training_exercise_id int not null,
	time varchar(50) null,
	repetitions varchar(50) null,
	created_at datetime default current_timestamp() null,
	updated_at datetime default current_timestamp() null,
	constraint fk_training_exercise_id
		foreign key (training_exercise_id) references training_exercises (id)
			on update cascade on delete cascade
);

create index `Index 2`
	on training_exercises_history (training_exercise_id);

create index role_fk
	on users (role_id);

