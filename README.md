<table>
<tbody>
	<tr>
		<td style="padding: 10px 20px">
			<a href="#">
				<img src="./documents/logo/light_version.png" alt="Gym Helper" width="50px" />
			</a>
		</td>
		<td style="padding: 10px 20px">
			<a href="https://estg.ipp.pt" target="_blank">
				<img src="https://www.estg.ipp.pt/logo-ipp.png" alt="ESTG | P.Porto" width="150px">
			</a>
		</td>
	</tr>
</tbody>
</table>


## Estrutura de Pastas
<pre>
.
└── <strong>gymhelper</strong>
	├── backend
	├── frontend
	├── mobile
	└── documents
</pre>

- **`documents`:** contém toda a documentação do projeto desde:
  - Atas de Reunião;
  - *Software Requirements Specification*;
  - Relatório;
  - Apresentações;
  - *Mockups*;
  - Logo;
  - Templates de documentos (atas, relatório individual, ...).
- **`backend`:** contém a API desenvolvida em **.NET Core**;
- **`frontend`:** contém o *front-end* da aplicação desenvolvido em **Angular**;
- **`mobile`:** contém a aplicação *mobile* (*multi-device* - iOS & Android) desenvolvida em **React Native**.